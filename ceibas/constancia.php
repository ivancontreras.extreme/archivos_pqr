<?php

function constanciaCeibas($conexion, $pqr, $tipoPQR = null) // tipo PQR = Padre - hija
{
    $cod_munip = 1;
    $cod_empr = 1;
    $sql = "SELECT pqr.cod_pqr as rad, pqr.cod_pqr_padre, pqr.cod_pred as matricula,(select (select c.descripcion from ciclo c
            where c.cod_cclo=p.cod_cclo and c.cod_munip=pqr.cod_munip) from predio p WHERE
            p.cod_pred=pqr.cod_pred and p.cod_munip=pqr.cod_munip ) as ciclo,
            (select p.direccion from predio p WHERE
                p.cod_pred=pqr.cod_pred and p.cod_munip=pqr.cod_munip ) as direccion,
            to_char(pqr.fec_soli, 'dd/mm/yyyy') as fecsoli,
            to_char (pqr.fecha_max_sol,'dd/mm/yyyy') as vencimiento ,pqr.dir_clte as direccion_soli,
            b.descripcion as barrio,pqr.ciudad_noti as ciudad, pqr.mail,
            pqr.tel_clte as telefono, pqr.nro_celular as celular, pqr.fax_clte as fax, predio.nombre as propietario, 
            predio.direccion,predio.telefono as protel,
            pqr.nro_docu as cedula_soli, pqr.descripcion as descripcion,
            pqr_tipo.descripcion as tipo, pqr.cod_mpqr, --pqr_motivo.descripcion as motivo,
            pqr.cod_gc_pqr,
            pqr.nom_clte as solicitante,cliente.nro_documento as cedula_pro,
            pqr.cod_tpqr, orden_trabajo.cod_otrb as ot, pqr.cod_usua,predio.rutareparto as ruta,
            (select pm.descripcion from pqr_medio_recepcion pm where pm.cod_mrec=pqr.cod_mrec and pm.cod_empr=pqr.cod_empr)
            as recepcion, pqr.med_noti,
            case when pqr.fec_regi>to_date('30/06/2016','dd/mm/yyyy') 
                then 1 else 0 end as val
            FROM
            pqr
                JOIN pqr_tipo   USING(cod_tpqr, cod_empr)
                --JOIN pqr_motivo USING(cod_mpqr, cod_tpqr, cod_empr)
                LEFT OUTER JOIN orden_trabajo 
                USING (cod_pqr, cod_pred, cod_empr, cod_munip)
                LEFT OUTER JOIN predio  USING (cod_pred, cod_empr, cod_munip)
                JOIN cliente USING (cod_clte, cod_empr)
                LEFT JOIN barrio b ON (b.cod_barrio = pqr.barrio_noti)
            where
                pqr.cod_pqr   = " . $pqr . "   and
                pqr.cod_empr  = '" . $cod_empr . "' and
                pqr.cod_munip = " . $cod_munip;

    $rs = $conexion->prepare($sql);
    $rs->execute();
    $result = $rs->fetch();
    $radicado  = $result['rad'];
    $matricula = $result['matricula'];
    $fecsoli = $result['fecsoli'];
    $fecmaxsoli = $result['vencimiento'];
    $fecmaxsoli2 = $result['vencimiento'];
    $tipo = $result['tipo'];
    $solicitante = $result['solicitante'];
    $cedula_soli = $result['cedula_soli'];
    $propietario = $result['propietario'];
    $cedula_pro = $result['cedula_pro'];
    $direccion = $result['direccion_soli'];
    $direccion_pred = $result['direccion'];
    $telefono_pred = $result['protel'];
    $telefono = $result['telefono'];
    $descripcion = $result['descripcion'];
    $cod_mpqr = $result['cod_mpqr'];
    $cod_tipo = $result['cod_tpqr'];
    $cod_gc_pqr = $result['cod_gc_pqr'];
    $cod_pqr_padre = $result["cod_pqr_padre"];
    $val_     = $result['val'];

    if ($val_ == 1) {
        if ($cod_tipo == '2') {
            $sqlmot = "select descripcion from pqr_motivo where cod_tpqr='" . $cod_tipo . "' and cod_mpqr='" . $cod_mpqr . "'";
        } else {
            $sqlmot = "select descripcion from pqr_causales_det pcd where pcd.cod_gc_pqr='" . $cod_gc_pqr . "' and pcd.cod_cdet_pqr='" . $cod_mpqr . "'";
        }
    } else {
        $sqlmot = "select descripcion from pqr_motivo where cod_tpqr='" . $cod_tipo . "' and cod_mpqr='" . $cod_mpqr . "'";
    }
    $exec_mot = $conexion->prepare($sqlmot);
    $exec_mot->execute();
    $result_mot = $exec_mot->fetch(PDO::FETCH_ASSOC);

    //$exec_mot = odbc_exec($conexion, $sqlmot);
    $motivo = $result_mot['descripcion'];
    //$motivo = odbc_result($rs, 'motivo');
    $ot = $result['ot'];
    $cod_usua = $result['cod_usua'];
    $ciclo = $result['ciclo'];
    $mail = $result['mail'];
    $ciudad = $result['ciudad'];
    $barrio = $result['barrio'];
    $celular = $result['celular'];
    $fax = $result['fax'];
    $mrecep = $result['recepcion'];
    $rutar = $result['ruta'];
    $mednoti = $result['med_noti'];

    $desot = !empty($ot) ? 'CON ORDEN DE TRABAJO N° :' . $ot : '';

    $sql = "SELECT DISTINCT nro_factura FROM pqr_detalle
    WHERE cod_empr='$cod_empr' AND 
    cod_munip=$cod_munip AND cod_pqr=$pqr";

    $rs = $conexion->prepare($sql);
    $rs->execute();
    $resultRS = $rs->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
    //$rs = odbc_exec($conexion, $sql);
    if ($resultRS) {
        $factura = $resultRS[0];
    } else {
        $factura = "N/A";
    }


    /*if (!empty($cod_munip)) {
    $ruta = logo($cod_munip, $cod_empr);
    }*/
    $ciclo_ = substr($ciclo, 5);

    $sql_usu = "SELECT nombreper FROM usuarios WHERE idusuario=$cod_usua ";
    $rs_usu = $conexion->prepare($sql_usu);
    $rs_usu->execute();
    $result_usu = $rs_usu->fetch(PDO::FETCH_ASSOC);
    //$rs_usu = odbc_exec($conexion, $sql_usu);
    $nom_usu = $result_usu['nombreper'];
    $pdf = new FPDF();
    //$pdf = new PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('Arial', 'B', 11);
    //$style3 = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => '5, 5', 'color' => array(0, 0, 0));

    $pdf->SetY(20);
    $pdf->SetX(10); //LOGO
    $pdf->Cell(40, 26, ' ', 1, 0, 'C', 1);
    //$pdf->Image($ruta,14,11,33,19);
    //$pdf->Image('ROT/reportes/PDF/LAS_CEIBAS.jpg', 14, 24, 33, 19);
    $pdf->Image("assets/library/PDF/logo-v2_LASCEIBAS.jpg", 14, 24, 33, 19);

    $pdf->SetY(20);
    $pdf->SetX(51);
    $pdf->Cell(95, 26, 'CONSTANCIA DE PQRs ', 1, 0, 'C', 1);

    $pdf->SetY(20);
    $pdf->SetX(147); //RADICACION
    $pdf->Cell(58, 26, ' ', 1, 0, 'C', 1);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetY(21);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Radicación N° ') . $radicado, 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetY(25);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Matrícula: ') . $matricula, 0, 0, 'L', 1);
    $pdf->SetY(29);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, 'Ciclo ' . $ciclo_, 0, 0, 'L', 1);
    $pdf->SetY(33);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Ubicación:') . $rutar, 0, 0, 'L', 1);
    $pdf->SetY(37);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Fecha Radicación:') . $fecsoli, 0, 0, 'L', 1);
    $pdf->SetY(41);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, 'Fecha De Vencimiento:' . $fecmaxsoli, 0, 0, 'L', 1);


    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 48, 195, 15); //(10,35,195,13)
    $pdf->SetY(47);
    $pdf->SetX(12);
    $pdf->Cell(38, 3, 'DATOS DEL SOLICITANTE ', 0, 0, 'L', 1);
    $pdf->SetY(50.75);
    $pdf->SetX(12);
    $pdf->Cell(38, 2, 'Nombre y Apellidos: ' . $solicitante, 0, 0, 'L', 1);
    $pdf->SetY(50.75);
    $pdf->SetX(140);
    $pdf->Cell(38, 2, 'C.C Solicitante: ' . $cedula_soli, 0, 0, 'L', 1);
    /*$pdf->SetY(39);$pdf->SetX(150);
    $pdf->Cell(30,2,'C.C: ',0,0,'L',1);*/
    $pdf->SetY(53.75);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('Dirección de Notificación: ') . $direccion, 0, 0, 'L', 1);
    $pdf->SetY(53.75);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, utf8_decode('Teléfono Fijo: ') . $telefono, 0, 0, 'L', 1);
    //$pdf->SetY(43.75);$pdf->SetX(12);
    //$pdf->Cell(30,2,'Barrio: '.$barrio,0,0,'L',1);
    $pdf->SetY(56.75);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, 'Barrio: ' . $barrio, 0, 0, 'L', 1);
    $pdf->SetY(59.75);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('Correo electrónico:  ') . $mail, 0, 0, 'L', 1);
    $pdf->SetY(56.75);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, utf8_decode('Teléfono Celular: ') . $celular, 0, 0, 'L', 1);
    $pdf->SetY(59.75);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, 'Fax: ' . $fax, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 8);

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 64, 195, 15); //(10,51,195,13);
    $pdf->SetY(63.5);
    $pdf->SetX(12);
    $pdf->Cell(38, 3, 'DATOS DEL PROPIETARIO ', 0, 0, 'L', 1);
    $pdf->SetY(69);
    $pdf->SetX(12);
    $pdf->Cell(80, 2, 'Propietario: ' . $propietario, 0, 0, 'L', 1);
    $pdf->SetY(69);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, 'C.C: ' . $cedula_pro, 0, 0, 'L', 1);
    $pdf->SetY(73);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('Dirección: ') . $direccion_pred, 0, 0, 'L', 1); ////FALTA
    $pdf->SetY(73);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, utf8_decode('Teléfono: ') . $telefono_pred, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 80, 195, 15); //(10,67,195,13)
    $pdf->SetY(79.5);
    $pdf->SetX(12);
    $pdf->Cell(20, 3, 'DATOS PQRs', 0, 0, 'L', 1);

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(84);
    $pdf->SetX(6);
    $pdf->Cell(20, 0, 'TIPO: ', 0, 0, 'C', 1);

    $pdf->SetY(87);
    $pdf->SetX(11.5); //cierre
    $pdf->Cell(30, 0, 'CONCEPTO: ', 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(83);
    $pdf->SetX(25);
    $pdf->Cell(20, 2, $tipo, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(87);
    $pdf->SetX(30); //cierre
    $pdf->Cell(30, 0, $motivo, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(83);
    $pdf->SetX(144);
    $pdf->Cell(30, 0, utf8_decode('CON ORDEN DE TRABAJO N° ') . $ot, 0, 0, 'C', 1);

    $pqr_padre = $cod_pqr_padre === null ? 'N/A' : $cod_pqr_padre;
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(87);
    $pdf->SetX(134);
    $pdf->Cell(30, 0, utf8_decode('PQR Padre : '), 0, 0, 'C', 1);

    $pdf->SetFont('Arial', 'b', 7);
    $pdf->SetY(87);
    $pdf->SetX(150);
    $pdf->Cell(30, 0, $pqr_padre, 0, 0, 'C', 1);

    $pdf->SetY(90);
    $pdf->SetX(12);
    $pdf->Cell(30, 0, utf8_decode('MEDIO DE RECEPCIÓN: '), 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(90);
    $pdf->SetX(35);
    $pdf->Cell(45, 0, $mrecep, 0, 0, 'C', 1);

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(92.5);
    $pdf->SetX(13.5);
    $pdf->Cell(30, 0, utf8_decode('MEDIO DE NOTIFICACIÓN: '), 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(92.5);
    $pdf->SetX(40);
    $pdf->Cell(30, 0, $mednoti, 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(83);
    $pdf->SetX(25);

    $pdf->SetY(93);
    $pdf->SetX(30);

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 98, 195, 16); //(10,83,195,18)
    $pdf->SetY(97);
    $pdf->SetX(12);
    $pdf->Cell(23, 3, utf8_decode('DESCRIPCIÓN'), 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 5);
    $pdf->SetY(100);
    $pdf->SetX(12);
    $pdf->MultiCell(190, 2, utf8_decode(substr($descripcion, 0, 1000)), 0, 'J', 0);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(116);
    $pdf->SetX(12);
    // $trat_info = "Nota: Autorizo expresamente a Empresa de Servicios Publicos del Distrito de Santa Marta, sociedad operadora de los servicios de acueducto y alcantarillado en la ciudad de Santa Marta, el tratamiento de mi informacin o datos personales, para los fines establecidos en la Poltica de Tratamiento de Informacin y Poltica de Privacidad de la Empresa, elaboradas conforme a lo dispuesto en la Ley 1581 de 2012, su decreto reglamentario y dems normas concordantes.";
    $trat_info = utf8_decode("Nota: Autorizo expresamente a Las Ceibas - Empresas Públicas de Neiva, sociedad operadora de los servicios de acueducto y alcantarillado en la ciudad de Neiva, el tratamiento de mi información o datos personales, para los fines establecidos en la Política de Tratamiento de Información y Política de Privacidad de la Empresa, elaboradas conforme a lo dispuesto en la Ley 1581 de 2012, su decreto reglamentario y demás normas concordantes.");
    $pdf->MultiCell(190, 2.5, $trat_info, 0, 'J', 0); //falta

    $pdf->SetFont('Arial', '', 9);
    $pdf->SetY(132);
    $pdf->SetX(115); //firma
    $pdf->Cell(90, 18, ' ', 1, 0, 'C', 1);
    $pdf->SetY(136);
    $pdf->SetX(118); //datos factura
    $pdf->Cell(10, 2, 'FIRMA:___________________ ', 0, 0, 'L', 1);
    $pdf->SetY(140);
    $pdf->SetX(118);
    $pdf->Cell(10, 2, 'C.C: ', 0, 0, 'L', 1);
    $pdf->SetY(140);
    $pdf->SetX(125);
    $pdf->Cell(10, 2, $cedula_soli, 0, 0, 'L', 1);
    $pdf->SetY(144);
    $pdf->SetX(118);
    $pdf->Cell(10, 2, 'Nombre Solicitante: ', 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetY(144);
    $pdf->SetX(148);
    $pdf->Cell(10, 2, $solicitante, 0, 0, 'L', 1);


    $pdf->SetFont('Arial', 'B', 11);
    $pdf->SetY(127);
    $pdf->SetX(10);
    $pdf->Cell(195, 2, 'DATOS DE LA FACTURA', 0, 0, 'L', 1);


    //-----PUEDEN SER VARIOS--------------//

    $sql_peri = "SELECT pqr_detalle.cod_pqr, pqr_detalle.nro_factura, pqr_detalle.cod_peri, concepto.descripcion FROM pqr_detalle, concepto WHERE pqr_detalle.cod_empr='" . $cod_empr . "' and pqr_detalle.cod_munip='" . $cod_munip . "' and pqr_detalle.cod_conc=concepto.cod_conc and pqr_detalle.cod_pqr='" . $pqr . "'";
    $result_usu = $conexion->prepare($sql_peri);
    $result_usu->execute();
    //$rs_peri = odbc_exec($conexion, $sql_peri);
    //$n_fact  = odbc_result($rs_peri, 'nro_factura');
    $nr = $result_usu->rowCount();


    $pdf->SetFont('Arial', '', 10);
    $pdf->SetFillColor(220, 220, 220);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetY(132);
    $pdf->SetX(15);
    $pdf->Cell(30, 5, 'SERVICIO ', 1, 0, 'C', 1);
    $pdf->SetY(132);
    $pdf->SetX(45);
    $pdf->Cell(30, 5, utf8_decode('FACTURA N°'), 1, 0, 'C', 1);
    $pdf->SetY(132);
    $pdf->SetX(75);
    $pdf->Cell(30, 5, 'PERIODO(S)', 1, 0, 'C', 1);


    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetDrawColor(0, 0, 0);
    $j = 132;
    $z = 1;
    //while ($z<=$nr){
    while ($rw = $result_usu->fetch(PDO::FETCH_ASSOC)) {
        $n_fact  = $rw['nro_factura'];
        $descrip  = $rw['descripcion'];
        $peri = $rw['cod_peri'];
        $j = $j + 5;
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetY($j);
        $pdf->SetX(15);
        $pdf->Cell(30, 5, $descrip, 1, 0, 'L', 1);
        $pdf->SetY($j);
        $pdf->SetX(45);
        $pdf->Cell(30, 5, $n_fact, 1, 0, 'C', 1);
        $pdf->SetY($j);
        $pdf->SetX(75);
        $pdf->Cell(30, 5, $peri, 1, 0, 'C', 1);
        $pdf->SetY($j);
        $pdf->SetX(15);
        $z = $z + 1;
    }
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetY(180);
    $pdf->SetX(110); //cierre
    $pdf->Cell(30, 2, utf8_decode('Elaborá: ') . $nom_usu, 0, 0, 'L', 1);

    $pdf->SetY(190);
    $pdf->SetX(110); //cierre
    $pdf->Cell(30, 2, '__ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ ', 0, 0, 'L', 1);
    $sql_peri_ = "select (pqr_detalle.cod_peri) as peri  from pqr_detalle, concepto  
    where pqr_detalle.cod_empr='" . $cod_empr . "' and pqr_detalle.cod_munip='" . $cod_munip . "' 
    and pqr_detalle.cod_conc=concepto.cod_conc 
    and pqr_detalle.cod_pqr='" . $pqr . "' 
    group by pqr_detalle.cod_peri order 
    by pqr_detalle.cod_peri";
    // $rs_peri_ = odbc_exec($conexion, $sql_peri_);
    $rs_peri_ = $conexion->prepare($sql_peri_);
    $rs_peri_->execute();

    $x = 160.5;
    //while ($z<=$nr){
    while ($rw_ = $rs_peri_->fetch(PDO::FETCH_ASSOC)) {
        $peri_ = $rw_['peri'];
        $pdf->SetFont('Arial', '', 6);
        $pdf->SetY(205);
        $pdf->SetX($x); //cierre
        $pdf->Cell(30, 2, $peri_, 0, 0, 'L', 1);
        //$x=$x+10.5;		
        $x = $x + 8;
    }

    $pdf->SetFont('Arial', 'B', 8);

    //$pdf->Image("ROT/reportes/PDF/LAS_CEIBAS.jpg", 12, 200, 18, 12);
    $pdf->Image("assets/library/PDF/logo-v2_LASCEIBAS.jpg", 12, 200, 18, 12);


    $pdf->SetY(200.5);
    $pdf->SetX(30); //cierre
    $pdf->Cell(30, 2, utf8_decode('COMPROBANTE DE PQR NÚMERO: ') . $radicado, 0, 0, 'L', 1);

    $pdf->SetY(205);
    $pdf->SetX(30); //cierre
    $pdf->Cell(30, 2, 'Fecha Radicado: ' . $fecsoli, 0, 0, 'L', 1);
    $pdf->SetY(205);
    $pdf->SetX(130); //cierre traslado a otra posicion 26/07/2012
    $pdf->Cell(30, 2, utf8_decode('Período Facturado: '), 0, 0, 'L', 1);

    $pdf->SetY(215);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, utf8_decode('Matrícula: ') . $matricula, 0, 0, 'L', 1);

    $pdf->SetY(215);
    $pdf->SetX(130); //cierre
    $pdf->Cell(30, 2, utf8_decode('Elaborá: ') . $nom_usu, 0, 0, 'L', 1);

    $pdf->SetY(220);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, 'Nombre Del Solicitante: ' . $solicitante, 0, 0, 'L', 1);
    $pdf->SetY(225);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, 'Documento del Solicitante: ' . $cedula_soli, 0, 0, 'L', 1);

    $pdf->SetY(225);
    $pdf->SetX(130); //cierre
    $pdf->Cell(30, 2, 'TIPO PQR: ' . $tipo, 0, 0, 'L', 1);

    // $pdf->SetY(260.5);
    $pdf->SetY(235);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, 'CONCEPTO DE LA PQR: ' . $motivo, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 8);

    $msj = utf8_decode('Estimado Usuario: Su PQR tiene un plazo para emitir respuesta de 15 días habiles (Art. 158 Ley 142/94). De igual manera, si para atender su reclamación se requiere hacer revisiones, el tiempo de respuesta se ampliará 30 días hábiles más ( Art.14 Paragrafo C.C.A.)');

    $pdf->SetY(248); //240
    $pdf->SetX(10); //cierre
    $pdf->MultiCell(190, 3, $msj, 0, 'J', 0);

    //$pdf->Output();
    $pqr_ruta = 'ceibas/tmp/' . $pqr;
    if ($tipoPQR == 'P') { // En caso la PQR sea padre, debe ser la misma carpeta de archivos de la PQR hija
        $pqr_ruta = 'ceibas/tmp/' .  $_GET['pqr'];
    }

    if (!file_exists($pqr_ruta)) {
        mkdir($pqr_ruta, 0777, true);
    }
    $filename = $pqr_ruta . "/Constancia" . $pqr . ".pdf";
    $pdf->Output($filename, 'F', true);
}
