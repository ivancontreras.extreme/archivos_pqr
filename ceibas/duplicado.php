<?php

function duplicadoCeibas($conexion, $pqr, $predio, $periodo, $nro_factura, $tipoPQR = null)
{
    // Datos
    $cod_pred = $predio;
    $cod_peri = $periodo;
    $lotes = 500;
    $paginas = "";
    $orden = "";
    $sql_ciclo = "SELECT p.cod_cclo, p.cod_munip, p.cod_empr FROM predio p WHERE p.cod_pred = $cod_pred";
    $query_ciclo = $conexion->prepare($sql_ciclo);
    $query_ciclo->execute();
    $data_ini = $query_ciclo->fetch();
    $cod_cclo = $data_ini['cod_cclo'];
    $empresa = $data_ini['cod_empr'];
    $municipio = $data_ini['cod_munip'];
    $cod_lote = 0;
    die("ff");
    //--Propiedades PDF
    $orientacion_papel = "P";
    $medida = "pt";
    $formato = "A4";
    $unicode = true;
    $codificacion = "UTF-8";
    $clase = "TCPDF";
    $autor = "Kagua";
    $titulo = "Factura";
    $margen_izq = "0";
    $margen_der = "0";
    $margen_sup = "0";
    $margen_inf = "0";
    $margen_encabezado = "0";
    $margen_pie = "0";
    $data_font_size = "8";
    $data_font_type = "helvetica";
    $encbz_font_size = "6";
    $peq_font_size = "3";
    // $encbz_font_type="helvetica";
    $print_encbz_pg = true;
    $print_pie_pg = true;
    $y_ = 0;
    $x_ = 0;
    $img_jpg = 0;
    // create new PDF document
    $pdf = new TCPDF($orientacion_papel, $medida, $formato, $unicode, $codificacion, false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Kagua');
    $pdf->SetTitle('Factura LAS CEIBAS - Kagua');
    $pdf->SetSubject('Factura LAS CEIBAS');
    $pdf->SetKeywords('Factura LAS CEIBAS');
    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);
    // remove default footer
    $pdf->setPrintFooter(false);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    // set some language-dependent strings (optional)
    //$pdf->setLanguageArray($l);


    //--Consultas Globales
    $sql = "SELECT cod_conc_ppal,cod_conc_alc,cod_conc_aseo,cod_org,cod_ajuste_dec, codsaldfavor
            FROM empr_parametros 
            WHERE cod_empr='$empresa';";

    $query_empr = $conexion->prepare($sql);
    $query_empr->execute();
    $datos = $query_empr->fetch();



    $acueducto = $datos['cod_conc_ppal'];
    $alcantarillado = $datos['cod_conc_alc'];
    // $aseo = $datos['cod_conc_aseo']
    $cod_org = $datos['cod_org'];
    $ajuste_dec = $datos['cod_ajuste_dec'];
    $cod_sal_fav = $datos['codsaldfavor'];

    //--Rangos de Consumo
    $sql = "SELECT frecuencia_fact, tope_rango_1,tope_rango_2,codigo_recaudo, codigo_recaudo_, codigo_recaudo__
            FROM municipio 
            WHERE cod_munip=" . $municipio . " and cod_empr='" . $empresa . "'";


    $query_r = $conexion->prepare($sql);
    $query_r->execute();
    $datos = $query_r->fetch();


    $recuaudo_corriente = $datos['codigo_recaudo'];
    $recuaudo_deuda_total = $datos['codigo_recaudo_'];
    $recuaudo_deuda_anterior = $datos['codigo_recaudo__'];
    $frecuencia = $datos['frecuencia_fact'];

    $pag = ($paginas == "") ? 0 : $paginas;
    switch ($orden) {
        case "ruta":
            $orden = " fpc.rutareparto ";
            break;
        case "barrio":
            $orden = " b.descripcion, fpc.direccion";
            break;
        default:
            $orden = "fpc.rutareparto";
    }

    //--Consultar Facturacion BD
    if ($cod_pred != "") {
        $q  = " fpc.cod_pred = " . $cod_pred . " and ";
    }
    if ($cod_cclo != "") {
        $q  .= " fpc.cod_cclo = '" . $cod_cclo . "' and ";
        // echo"bbbb";
    }
    if ($cod_lote != "") {
        $q .= " fpc.cod_lote=$cod_lote and ";
        // echo"cccc";
    }

    $sql = "select fpc.cod_pred,fpc.cod_peri,fpc.nro_factura,fpc.fecexp,fpc.fecvto,fpc.feclectura,fpc.fec_lectura_ant,
            fpc.fec_susp,fpc.fecabre,fpc.feccierra,fpc.total,fpc.total_sin_descto,fpc.lectura,
            fpc.lectura_anterior,fpc.consumo,fpc.cons_ajustado,fpc.consttalizador,fpc.promedio,
            fpc.cons_manual,fpc.cons_fact,fpc.metodo,fpc.nro_seq,fpc.cod_cclo,fpc.cod_lote,
            fpc.nombre,fpc.direccion,fpc.estrato,fpc.rutareparto,fpc.nropermora,fpc.cod_pred_totalizador,
            fpc.dias_facturados, Coalesce(cod_obsl_i,cod_obsl_l) as obs_lectura,
            fpc.mensaje,fpc.total_mas_cartera,fpc.deuda,fpc.tipo_sector,fpc.total,
            --fpc.cod_peri_ult_pago,
            fpc.fch_ult_pago,fpc.valor_ult_pago,fpc.nro_cuenta_cobro,
            p.fecinstalmedi,p.serialmedi, p.nro_documento,m.descripcion as marca,cb.descripcion as calibre,
            b.descripcion as barrio,c.descripcion as ciclo,u.descripcion as uso,fpc.deuda_e1,fpc.deuda_e2, fpc.deuda_e3, m.tipo,
            (select limite_in(fpc.cod_cclo::varchar,fpc.cod_peri::integer,fpc.cod_munip::integer,
            fpc.cod_empr::varchar, 2)) as peri_ant
            from factura_per_cab fpc 
            join ciclo c using (cod_cclo,cod_munip,cod_empr)
            join medidor m using (cod_medi,cod_clbr,cod_empr)
            join calibre cb using (cod_clbr,cod_empr)
            join uso u using(cod_uso,cod_empr)
            join predio p using(cod_pred,cod_munip,cod_empr)
            join barrio b using(cod_barrio,cod_munip,cod_empr) 
            where " . $q . "
            fpc.cod_peri=" . $cod_peri . " and
            fpc.cod_munip=" . $municipio . " and
            fpc.cod_empr='" . $empresa . "' order by  " . $orden . " limit " . $lotes . " offset " . $pag;

    $exe = $conexion->prepare($sql);
    $exe->execute();
    //print_r($sql);die();
    if (!$exe) {
        $pdf->AddPage();
        $pdf->SetY(100);
        $pdf->SetX(200);
        $pdf->Cell(0, 0, "Error: Consultando los datos de facturacion", 0);
    } else {

        while ($result = $exe->fetch(PDO::FETCH_ASSOC)) {
            $deuda_ess = $result['deuda_e3'];

            if ($cod_peri <= 202011) {
                //INICIAMOS HOJA 1

                if ($result['nombre'] == "") {
                    $pdf->AddPage();
                    $pdf->SetY(100);
                    $pdf->SetX(200);
                    $pdf->Cell(0, 0, "Error: Factura sin nombre", 0);
                } else {
                    // add a page
                    $pdf->setPrintHeader(false);
                    $pdf->AddPage();
                    // -- set new background ---
                    // get the current page break margin
                    $bMargin = $pdf->getBreakMargin();
                    // get current auto-page-break mode
                    $auto_page_break = $pdf->getAutoPageBreak();
                    // disable auto-page-break
                    $pdf->SetAutoPageBreak(false, 0);
                    // set bacground image
                    $img_file = '../assets/images/FACTURA_LAS_CEIBAS.jpg';
                    $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                    // restore auto-page-break status
                    $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                    // set the starting point for the page content
                    $pdf->setPageMark();

                    if (isset($cod_pred)) {

                        $consulta_edad_coactivo = "SELECT edad_coactivo FROM municipio WHERE cod_empr = '$empresa' AND cod_munip = $municipio";
                        $query_coactivo_edad = $conexion->prepare($consulta_edad_coactivo);
                        $query_coactivo_edad->execute();

                        $row = $query_coactivo_edad->fetch();
                        $edad_coactivo = $row['edad_coactivo'];

                        $consulta = "SELECT * FROM predio 
                        WHERE cod_pred = $cod_pred 
                        AND cod_empr = '$empresa' 
                        AND edad_cartera > $edad_coactivo";
                        $query = $conexion->prepare($consulta);
                        $query->execute();

                        if ($data = $query->fetch()) {
                            $pdf->SetFont($data_font_type, '', 10);
                            $pdf->SetY($y_ + 72);
                            $pdf->SetX($x_ + 129);
                            $pdf->Cell(0, 0, "EN ETAPA DE COBRO COACTIVO", 0); //indica en una factura si el susuario tiene un cobro coactivo
                        }
                    }

                    // INFORMACION DEL CLIENTE 
                    $pdf->SetFont($data_font_type, '', 10);
                    $long_factura = strlen($result['nro_factura']);
                    if ($long_factura < 10) {
                        $factura_no = str_pad($result['nro_factura'], 9, '0', STR_PAD_LEFT);
                        $factura_no = "1" . $factura_no;
                    } else {
                        $factura_no = $result['nro_factura'];
                    }
                    $pdf->SetFont($data_font_type, 'B');
                    $pdf->SetY($y_ + 197);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, $factura_no, 0);

                    $pdf->SetY($y_ + 174);
                    $pdf->SetX($x_ + 225);
                    $pdf->Cell(0, 0, $result['cod_pred'], 0);
                    $pdf->SetFont($data_font_type, '');

                    $pdf->SetFont($data_font_type, '', 8);
                    $pdf->SetY($y_ + 60);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, $result['nombre'], 0);

                    $pdf->SetY($y_ + 75);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, mb_convert_encoding($result['direccion'] . " " . $result['barrio'], "UTF-8", "UTF-8"), 0);

                    $pdf->SetY($y_ + 90);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, "C.C o NIT: " . $result['nro_documento'], 0);

                    $pdf->SetY($y_ + 128);
                    $pdf->SetX($x_ + 345);
                    $pdf->Cell(0, 0, $result['cod_cclo'], 0);

                    $pdf->SetY($y_ + 102);
                    $pdf->SetX($x_ + 475);
                    $pdf->Cell(0, 0, $result['uso'], 0);

                    $estrato = ($result['estrato'] == 0) ? "1" : $result['estrato'];
                    $pdf->SetY($y_ + 101);
                    $pdf->SetX($x_ + 370);
                    $pdf->Cell(0, 0, $estrato, 0);

                    $pdf->SetY($y_ + 128);
                    $pdf->SetX($x_ + 510);
                    $rutareparto = ($result['rutareparto'] == "") ? "SIN RUTA" : $result['rutareparto'];
                    $pdf->Cell(0, 0, $rutareparto, 0);

                    $sql_conv = "SELECT cc.cod_conv,ct.valor_capital,ct.valor_interes,ct.id_detalle,
                    cc.cant_cuotas, ct.ccobro
                    from convenio_cab cc, convenio_cuota ct
                    WHERE cc.cod_conv=ct.cod_conv and cc.cod_empr=ct.cod_empr and cc.cod_munip=ct.cod_munip
                    AND cc.cod_pred=" . $result['cod_pred'] . " and cc.cod_munip=" . $municipio . " 
                    and ct.cod_peri_apli=" . $result['cod_peri'] . " ";
                    $exSql_conv = $conexion->prepare($sql_conv);
                    $exSql_conv->execute();
                    $data = $exSql_conv->fetch();
                    $codconv = $data['cod_conv'];

                    if ($codconv > 0) {
                        $txt_web = "DUPLICADO - EN CONVENIO";
                        $tm = 40;
                        $px = 10;
                        $py = 50;
                    } else {
                        $txt_web = "DUPLICADO";
                        $tm = 50;
                        $px = 150;
                        $py = 5;
                    }

                    /*Fondo Duplicado*/
                    $pdf->SetFont('helvetica', '', $tm);
                    $pdf->SetDrawColor(200);
                    $pdf->SetTextColor(200);
                    //MARCA DE AGUA

                    $pdf->StartTransform();

                    $pdf->Rotate(40);

                    $pdf->Text($px, $py, $txt_web);
                    // Stop Transformation
                    $pdf->StopTransform();

                    $pdf->SetTextColor(0, 0, 0);
                    // DATOS DE MEDIDOR 
                    $pdf->SetFont($data_font_type, '', 6);
                    $marca = ($result['marca'] == "") ? "S/M" : $result['marca'];
                    //$pdf->SetY($y_+155);$pdf->SetX($x_+335);
                    //$pdf->Cell(0,0,$marca,0);

                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 405);
                    $serial_medidor = ($result['serialmedi'] == "") ? "S/S" : $result['serialmedi'];
                    $pdf->Cell(0, 0, $serial_medidor, 0);

                    $explode_calibre  = $result['calibre'];
                    $array_calibre = explode(" ", $explode_calibre);

                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 535);
                    $pdf->Cell(0, 0, $array_calibre[0], 0); // string1

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 298);
                    $pdf->SetX($x_ + 370);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fec_lectura_ant']) . " al " . cambiofechaCeibas($result['feclectura']), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 183);
                    $pdf->SetX($x_ + 411);
                    $pdf->Cell(0, 0, $result['lectura'], 0);

                    $pdf->SetY($y_ + 192);
                    $pdf->SetX($x_ + 411);
                    $pdf->Cell(0, 0, $result['lectura_anterior'], 0);

                    $pdf->SetY($y_ + 200);
                    $pdf->SetX($x_ + 415);
                    $pdf->Cell(0, 0, abs($result['lectura'] - $result['lectura_anterior']), 0);

                    $pdf->SetY($y_ + 205);
                    $pdf->SetX($x_ + 536);
                    $pdf->Cell(0, 0, number_format($result['consumo'], 0, ',', '.'), 0, 0, 'L', 0);

                    $pdf->SetY($y_ + 250);
                    $pdf->SetX($x_ + 518);
                    $pdf->Cell(0, 0, number_format($result['promedio'], 0, ',', '.'), 0, 0, 'L', 0);

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 488);
                    $pdf->Cell(0, 0, $result['dias_facturados'], 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 216);
                    $pdf->SetX($x_ + 415);
                    $pdf->Cell(0, 0, $result['obs_lectura'], 0);

                    // DATOS DEUDA
                    $pdf->SetFont($data_font_type, 9);
                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 338);
                    $pdf->Cell(0, 0, $result['cod_peri'], 0);

                    $pdf->SetFont($data_font_type, 'B', 9);
                    $pdf->SetY($y_ + 250);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0);

                    $fecvto = $result['fecvto'];
                    $fecsus = $result['fec_susp'];

                    $pdf->SetY($y_ + 263);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($fecvto), 0); // FECHA PAGO OPORTUNO

                    $pdf->SetY($y_ + 299);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fch_ult_pago']), 0);

                    $pdf->SetY($y_ + 287);
                    $pdf->SetX($x_ + 229);
                    $pdf->Cell(0, 0, '$ ' . number_format($result['valor_ult_pago'], 0, ',', '.'), 0); //Valor ultimo pago

                    $pdf->SetY($y_ + 274);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($fecsus), 0); //fecha de suspension

                    $pdf->SetY($y_ + 43);
                    $pdf->SetX($x_ + 125);
                    $pdf->Cell(0, 0, $result['nropermora'] + 1, 0); //Numero de Facturas

                    $sql = "SELECT (fpc.deuda_e1+fpc.deuda_e2+fpc.deuda_e3) As facturado
                            FROM public.factura_per_cab As fpc
                            WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                    fpc.cod_peri = " . $cod_peri . " And 
                                    fpc.cod_munip = " . $municipio . " And
                                    fpc.cod_empr = '" . $empresa . "';";
                    $rfact = $conexion->prepare($sql);
                    $rfact->execute();
                    $datafact = $rfact->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                    $facturado = $datafact[0];

                    $sql = "SELECT fpc.deuda_e1, coalesce(fpc.nro_cuenta_cobro,0) as nro_cuenta_cobro
                            FROM public.factura_per_cab As fpc
                            WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                    fpc.cod_peri = " . $cod_peri . " And 
                                    fpc.cod_munip = " . $municipio . " And
                                    fpc.cod_empr = '" . $empresa . "';";
                    $rpago = $conexion->prepare($sql);
                    $rpago->execute();
                    $datapago = $rpago->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                    $deuda = $datapago[0];
                    $datapago2 = $rpago->fetch();
                    $nro_cuenta_cobro = $datapago2['nro_cuenta_cobro'];

                    $total_pagar = $deuda_ess + $deuda;

                    if ($nro_cuenta_cobro == 0) {
                        $factura_no_cuenta_cobro = $factura_no;
                        $prefijo = "";
                    } else {
                        $factura_no_cuenta_cobro = "5" . str_pad($nro_cuenta_cobro, 9, '0', STR_PAD_LEFT);
                    }

                    //Cambiar Manera de traer el total

                    $detalleFacturado1 = "SELECT * FROM factura_det_aseo 
                    WHERE cod_pred = '" . $cod_pred . "' 
                    AND cod_peri = '" . $cod_peri . "'
                    AND cod_munip = '" . $municipio . "' 
                    AND cod_empr = '" . $empresa . "'";
                    $query_facturado1 = $conexion->prepare($detalleFacturado1);
                    $query_facturado1->execute();
                    $total_concp_aseo_spoll1 = 0;
                    while ($rowFacturado1 = $query_facturado1->fetch(PDO::FETCH_ASSOC)) {
                        $total_concp_aseo_spoll1 += $rowFacturado1['total_concp'];
                    }

                    $totalFacturaCompleta1 = $facturado; // + $total_concp_aseo_spoll1;
                    //FIN

                    $pdf->SetFont($data_font_type, 'B', 12);
                    $pdf->SetY($y_ + 225);
                    $pdf->SetX($x_ + 218);
                    $pdf->Cell(0, 0, '$ ' . number_format($totalFacturaCompleta1, 0, ',', '.'), 0, 0, 'L', 0); //deuda total mes seleccionado


                    // TOTAL POR CONCEPTO
                    $sql = "SELECT * FROM factura_per_det
                            WHERE cod_pred=" . $result['cod_pred'] . " and
                                cod_peri=" . $cod_peri . " and 
                                cod_empr='" . $empresa . "' and 
                                cod_munip=" . $municipio . " and
                                cod_conc not in (select cod_conc from concepto where cod_serv=3)
                                order by cod_conc ";

                    $exe_detalle = $conexion->prepare($sql);
                    $exe_detalle->execute();

                    $total_concepto_acueducto       = 0;
                    $total_concepto_alcantarillado  = 0;
                    $total_otros_conceptos          = 0;
                    // $total_veolia                   = 0;
                    $total_pag                      = 0;

                    while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {
                        //--sumatoria para el resumen
                        if ($result_det['cod_conc'] == $acueducto) { // ACUEDUCTO
                            $total_concepto_acueducto += $result_det['total_concp'];
                        }
                        if ($result_det['cod_conc'] == $alcantarillado) { // ALCANTARILLADO
                            $total_concepto_alcantarillado += $result_det['total_concp'];
                        }

                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) { // and ($result_det['cod_conc']!=100)){ // OTROS
                            $total_otros_conceptos += $result_det['total_concp'];
                        }
                    }

                    // GRAFICA - HISTORICO DE CONSUMO
                    GraficaHistoricaCeibas($conexion, $pdf, $result['cod_pred'], $result['peri_ant'], $result['cod_cclo'], $municipio, $empresa);

                    $style = array(
                        'position' => '',
                        'align' => 'L',
                        'stretch' => false,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'hpadding' => 'auto',
                        'vpadding' => 'auto',
                        'fgcolor' => array(0, 0, 0),
                        'bgcolor' => false, //array(255,255,255),
                        'text' => false,
                        'font' => 'helvetica',
                        'fontsize' => 8,
                        'stretchtext' => 4
                    );

                    //DETALLE

                    $total_concepto_acueducto       = 0;
                    $total_concepto_alcantarillado  = 0;
                    $total_otros_conceptos          = 0;
                    $total_otros                    = 0;
                    $tc1 = 0;
                    $tc2 = 0;
                    $tc3 = 0;
                    $tca1 = 0;
                    $tca2 = 0;
                    $tca3 = 0;
                    $secy = 0;
                    $unidades = "";

                    // DETALLES CONCEPTOS DE LA FACTURA
                    $sql = "SELECT * FROM factura_per_det
                            WHERE cod_pred=" . $result['cod_pred'] . " and
                                cod_peri=" . $cod_peri . " and 
                                cod_empr='" . $empresa . "' and 
                                cod_munip=" . $municipio . " and
                                cod_conc not in (select cod_conc from concepto where cod_serv=3)
                                order by cod_conc ";

                    $exe_detalle = $conexion->prepare($sql);
                    $exe_detalle->execute();

                    while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {

                        $suapoR1    = $result_det['subapo_r1'];
                        $suapoR2    = $result_det['subapo_r2'];
                        $suapoR3    = $result_det['subapo_r3'];
                        $suapoCF    = $result_det['subapo_cfijo'];
                        $tasaUso    = $result_det['tasa_ur'];

                        //--sumatoria para el resumen
                        if ($result_det['cod_conc'] == $acueducto) { //acueducto
                            $total_concepto_acueducto += $result_det['total_concp'];
                        }
                        if ($result_det['cod_conc'] == $alcantarillado) { //alcantarillado
                            $total_concepto_alcantarillado += $result_det['total_concp'];
                        }
                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                            $total_otros_conceptos += $result_det['total_concp'];
                        }

                        //--liquidacion Acueducto
                        if ($result_det['cod_conc'] == $acueducto) {
                            $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                            $tc1 = $result_det['canti_e1'] * $result_det['precioe1'];
                            $tc2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                            $tc3 = $result_det['canti_e3'] * $result_det['precioe3'];

                            //Tasas
                            $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                            $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                            $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                            $sqlacu = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                    fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                    from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                    where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                        fpc.cod_peri=" . $cod_peri . " and 
                                        fpc.cod_munip=" . $municipio . " and 
                                        fpc.cod_empr='" . $empresa . "' and
                                        fpd.cod_conc=" . $acueducto;

                            $sql_acu = $conexion->prepare($sqlacu) or die("Error: Detalle Acueducto \n" . $sqlacu);
                            $sql_acu->execute();
                            $data_acu = $sql_acu->fetch();

                            $cod_concep_acu = $data_acu['cod_conc']; //codigo cocepto acueducto
                            $uso_acu        = $data_acu['cod_uso']; //codigo cocepto acueducto
                            $cod_estado_acu = $data_acu['cod_estd']; //codigo estado acueducto
                            $fec_lec_acu    = $data_acu['feclectura']; //codigo estado acueducto
                            $fec_lec_ant_acu = $data_acu['fec_lectura_ant']; //codigo estado acueducto
                            $cod_tari_acu   = $data_acu['cod_tarf_ref']; //codigo tarifa acueducto
                            $cod_uso_acu    = $data_acu['cod_uso_ref']; //codigo uso acueducto
                            $codtarf        = $data_acu['cod_tarf'];
                            //--calcular tarifas
                            $sq_cal_tari_acu1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_acu', $cod_tari_acu, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_acu1 = $conexion->prepare($sq_cal_tari_acu1);
                            $sql_cal_tari_acu1->execute();
                            $data_tari = $sql_cal_tari_acu1->fetch();

                            $cod_cal_tari_acu = $data_tari['fact_calcular_tarifa'];

                            $sq_cal_tari_acu2 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$uso_acu', $codtarf, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_acu2 = $conexion->prepare($sq_cal_tari_acu2);
                            $sql_cal_tari_acu2->execute();
                            $data_tari2 = $sql_cal_tari_acu2->fetch();

                            $cod_cal_tari_acu2 = $data_tari2['fact_calcular_tarifa'];
                            $resto2 = str_replace("{", " ", $cod_cal_tari_acu2);
                            $cal_tari_acu2 = explode(",", $resto2);

                            $resto = str_replace("{", " ", $cod_cal_tari_acu);
                            $cal_tari_acu = explode(",", $resto); //si fecha esta en formato dia-mes-año 
                            //print_r($cal_tari_acu);die();
                            $consu_basic1   = $cal_tari_acu[1];
                            $consu_comple1  = $cal_tari_acu[2];
                            $consu_sunt1    = $cal_tari_acu[3];
                            $cargo_fijo1    = $cal_tari_acu[0];
                            $porcen         = $cal_tari_acu2[5];

                            //Aqui debo tomar el dato de tasa 
                            $ta_r1 = $cal_tari_acu[14];
                            $ta_r2 = $cal_tari_acu[15];
                            $ta_r3 = $cal_tari_acu[16];

                            if (!isset($subapo_t1)) {
                                $subapo_t1 = 0;
                            }
                            if (!isset($subapo_t2)) {
                                $subapo_t2 = 0;
                            }
                            if (!isset($subapo_t3)) {
                                $subapo_t3 = 0;
                            }
                            $sum_sub = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF;
                            $sum_sub_tasa = $subapo_t1 + $subapo_t2 + $subapo_t3;

                            $valortotal_acueducto = $result_det['total_concp'];

                            //--Descripcion
                            $pdf->SetFont($data_font_type, '', 8);
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Cargo Fijo", 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 37);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, "Consumo Básico", 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 37);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, "Consumo Complementario", 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 37);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, "Consumo Suntuario", 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Tasa Uso", 0);

                            //--acueducto consumo
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 160);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 160);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 160);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0);

                            //--acueducto tarifa referencia
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 182);
                            $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo1, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 182);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_basic1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 182);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_comple1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 182);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 182);
                            $pdf->Cell(0, 0, number_format($ta_r1, 1, ',', '.'), 0);


                            //valor total
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_fija'] * $cargo_fijo1), 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 215);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] * $consu_basic1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 215);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e2'] * $consu_comple1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 215);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e3'] * $consu_sunt1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, number_format((($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) * $ta_r1), 1, ',', '.'), 0);

                            //--acueducto valor
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 250);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 250);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 250);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, number_format($sum_sub_tasa, 1, ',', '.'), 0);


                            //--acueducto valor a pagar
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 283);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 283);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 283);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, number_format(($ta_u1 + $ta_u2 + $ta_u3), 1, ',', '.'), 0);

                            //--Total concepto acueducto
                            $pdf->SetFont($data_font_type, 'B', 6);
                            $pdf->SetY($y_ + 418);
                            $pdf->SetX($x_ + 281);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        }

                        //--Liquidacion Alcantarillado
                        if ($result_det['cod_conc'] == $alcantarillado) {
                            $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                            $tca1 = $result_det['canti_e1'] * $result_det['precioe1'];
                            $tca2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                            $tca3 = $result_det['canti_e3'] * $result_det['precioe3'];

                            //Tasas
                            $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                            $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                            $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                            $sqlalc = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                        fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                        from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                        where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                            fpc.cod_peri=" . $cod_peri . " and 
                                            fpc.cod_munip=" . $municipio . " and 
                                            fpc.cod_empr='" . $empresa . "' and
                                            fpd.cod_conc=" . $alcantarillado;
                            $sql_alc = $conexion->prepare($sqlalc) or die("Error: Detalle Alcantarillado \n" . $sqlalc);
                            $sql_alc->execute();
                            $data_alc = $sql_alc->fetch();

                            $cod_concep_alc = $data_alc['cod_conc'];
                            $uso_alc        = $data_alc['cod_uso'];
                            $codtarf        = $data_alc['cod_tarf'];
                            $cod_estado_alc = $data_alc['cod_estd'];
                            $fec_lec_alc    = $data_alc['feclectura'];
                            $fec_lec_ant_alc = $data_alc['fec_lectura_ant'];
                            $cod_tari_alc   = $data_alc['cod_tarf_ref'];
                            $cod_uso_alc    = $data_alc['cod_uso_ref'];

                            $sq_cal_tari_alc1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_alc', $cod_tari_alc, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_alc1 = $conexion->prepare($sq_cal_tari_alc1);
                            $sql_cal_tari_alc1->execute();
                            $data_tari = $sql_cal_tari_alc1->fetch();

                            $cod_cal_tari_alc = $data_tari['fact_calcular_tarifa'];
                            $resto1 = str_replace("{", " ", $cod_cal_tari_alc);
                            $cal_tari = explode(",", $resto1); //si fecha esta en formato dia-mes-año 

                            $sq_cal_tari_alca2 = "SELECT fact_calcular_tarifa('" . $empresa . "'," . $municipio . ", '$uso_alc', $codtarf, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_alca2 = $conexion->prepare($sq_cal_tari_alca2);
                            $sql_cal_tari_alca2->execute();
                            $data_tari2 = $sql_cal_tari_alca2->fetch();

                            $cod_cal_tari_alca2 = $data_tari2['fact_calcular_tarifa'];
                            $resto2 = str_replace("{", " ", $cod_cal_tari_alca2);
                            $cal_tari_alca2 = explode(",", $resto2);

                            $consu_basic    = $cal_tari[1];
                            $consu_comple   = $cal_tari[2];
                            $consu_sunt     = $cal_tari[3];
                            $cargo_fijo     = $cal_tari[0];
                            $porcen         = $cal_tari_alca2[5];

                            //Aqui debo tomar el dato de tasa 
                            $ta_r1_alc = $cal_tari[14];
                            $ta_r2_alc = $cal_tari[15];
                            $ta_r3_alc = $cal_tari[16];

                            $valortotal_alcantarillado = $result_det['total_concp'];
                            $valortotal_acueducto = $result_det['total_concp'];

                            //--Descripcion
                            $pdf->SetFont($data_font_type, '', 8);
                            $pdf->SetY($y_ + 443);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Cargo Fijo", 0);
                            $pdf->SetY($y_ + 453);
                            $pdf->SetX($x_ + 37);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, "Consumo Básico", 0); //}
                            $pdf->SetY($y_ + 463);
                            $pdf->SetX($x_ + 37);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, "Consumo Complementario", 0); //}
                            $pdf->SetY($y_ + 473);
                            $pdf->SetX($x_ + 37);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, "Consumo Suntuario", 0); //}
                            $pdf->SetY($y_ + 483);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Tasa Retributiva", 0);

                            //--alcantarillado consumo
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 160);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 160);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 160);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0);

                            //--alcantarillado tarifa referencia
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 183);
                            $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 183);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_basic, 2, ',', '.'), 0); //} 
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 183);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_comple, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 183);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 183);
                            $pdf->Cell(0, 0, number_format($ta_r1_alc, 1, ',', '.'), 0);



                            //Valor total
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_fija'] * $cargo_fijo), 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 215);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] * $consu_basic), 2, ',', '.'), 0); //} 
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 215);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e2'] * $consu_comple), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 215);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e3'] * $consu_sunt), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, number_format((($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) * $ta_r1_alc), 1, ',', '.'), 0);

                            $sum_sub_1 = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF;
                            //$sum_sub_tasa_alc = $subapo_t1 + $subapo_t2 + $subapo_t3;
                            $sum_sub_tasa_alc = 0;
                            //--alcantarillado valor
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 251);
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 251);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 251);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 251);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 251);
                            $pdf->Cell(0, 0, number_format($sum_sub_tasa_alc, 1, ',', '.'), 0);

                            $valor_acueducto_alcantarillado = $valortotal_acueducto + $valortotal_alcantarillado;
                            $total_dia = $valor_acueducto_alcantarillado / $result['dias_facturados'];

                            //--alcantarillado valor Total
                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + 510);
                            $pdf->SetX($x_ + 419);
                            $pdf->Cell(0, 0, '$ ' . number_format($valor_acueducto_alcantarillado, 2, ',', '.'), 0); //valor total acue + alcantarillado 


                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + 509);
                            $pdf->SetX($x_ + 519);
                            $pdf->Cell(0, 0, '$ ' . number_format($total_dia, 2, ',', '.'), 0); //valor dia

                            //--alcantarillado valor a pagar
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 284);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 284);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tca1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 284);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tca2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 284);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tca3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 284);
                            $pdf->Cell(0, 0, number_format(($ta_u1 + $ta_u2 + $ta_u3), 1, ',', '.'), 0);
                            //--Alcantarillado Total a pagar
                            $pdf->SetFont($data_font_type, 'B', 6);
                            $pdf->SetY($y_ + 495);
                            $pdf->SetX($x_ + 281);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        }

                        //--Liquidacion Otros conceptos
                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + $secy + 337);
                            $pdf->SetX($x_ + 330);
                            $pdf->Cell(0, 0, $result_det['descripcion'], 0);

                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + $secy + 337);
                            $pdf->SetX($x_ + 420);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                            $total_otros += $result_det['total_concp'];
                            $secy += 10;
                        }
                    }
                    if (((($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total'])) > 0) {
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + $secy + 337);
                        $pdf->SetX($x_ + 330);
                        $pdf->Cell(0, 0, "DEUDAS ANTERIORES", 0);

                        $pdf->SetFont($data_font_type, '', 7);
                        $pdf->SetY($y_ + $secy + 337);
                        $pdf->SetX($x_ + 420);
                        $pdf->Cell(0, 0, '$ ' . number_format((($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total']), 2, ',', '.'), 0);
                        $total_otros += (($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total']);
                        $secy += 10;
                    }
                    $pdf->SetFont($data_font_type, 'B', 7);
                    $pdf->SetY($y_ + 417);
                    $pdf->SetX($x_ + 521);
                    $pdf->Cell(0, 0, '$ ' . number_format($total_otros, 2, ',', '.'), 0);
                    $pdf->SetFont($data_font_type, '', 7);

                    //TOTAL 1 + 2 + 3
                    $pdf->SetFont($data_font_type, 'B', 8);
                    //$pdf->SetY($y_+589);$pdf->SetX($x_+308);
                    $pdf->SetY($y_ + 509);
                    $pdf->SetX($x_ + 300);
                    //$pdf->Cell(0,0,'$ '.number_format(($total_concepto_acueducto+$total_concepto_alcantarillado+$total_otros_conceptos),0,',','.'),0,0,'L',0);//deuda total mes seleccionado
                    $pdf->Cell(0, 0, '$ ' . number_format(($total_concepto_acueducto + $total_concepto_alcantarillado + $total_otros), 0, ',', '.'), 0, 0, 'L', 0);
                }

                //INICIAMOS HOJA 2
                $pdf->setPrintHeader(false);
                $pdf->AddPage();
                // -- set new background ---
                // get the current page break margin
                $bMargin = $pdf->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $pdf->getAutoPageBreak();
                // disable auto-page-break
                $pdf->SetAutoPageBreak(false, 0);
                // set bacground image
                //IMAGEN DEL DORSO------------------
                //$img_file = 'ESSMAR_FACTURA-02.jpg';
                $img_file = '../assets/images/FACTURA_LAS_CEIBAS_2.jpg';
                $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                // restore auto-page-break status
                $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $pdf->setPageMark();

                //DATOS BASICOS FACTURA ASEO

                $datosBasicos = "SELECT * FROM factura_cab_aseo 
                WHERE cod_pred = '" . $cod_pred . "' 
                AND cod_peri = '" . $cod_peri . "'
                AND cod_munip = '" . $municipio . "' 
                AND cod_empr = '" . $empresa . "'";

                $query_datosBasicos = $conexion->prepare($datosBasicos);
                $query_datosBasicos->execute();
                foreach ($query_datosBasicos as $rowBasicos) {
                    $n_fac_aseo = $rowBasicos['nro_factura'];
                    $pdf->SetFont($data_font_type, '', 10);
                    $pdf->SetY($y_ + 40);
                    $pdf->SetX($x_ + 485);
                    $pdf->Cell(0, 0, $rowBasicos['nro_factura'], 0);

                    $pdf->SetY($y_ + 65);
                    $pdf->SetX($x_ + 485);
                    $pdf->Cell(0, 0, $rowBasicos['cod_pred'], 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 130);
                    $pdf->SetX($x_ + 60);
                    $pdf->Cell(0, 0, $rowBasicos['nombre'], 0);

                    $pdf->SetY($y_ + 138);
                    $pdf->SetX($x_ + 90);
                    $pdf->Cell(0, 0, mb_convert_encoding($rowBasicos['direccion'], "UTF-8", "UTF-8"), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 125);
                    $pdf->Cell(0, 0, mb_convert_encoding($rowBasicos['direccion'], "UTF-8", "UTF-8"), 0);

                    $pdf->SetFont($data_font_type, '', 5.5);
                    $pdf->SetY($y_ + 147);
                    $pdf->SetX($x_ + 50);
                    $pdf->Cell(0, 0, $rowBasicos['barrio'], 0);

                    $pdf->SetFont($data_font_type, '', 5.5);
                    $pdf->SetY($y_ + 138);
                    $pdf->SetX($x_ + 245);
                    $pdf->Cell(0, 0, $rowBasicos['comuna'], 0);

                    $estrato = ($rowBasicos['estrato'] == 0) ? "1" : $rowBasicos['estrato'];
                    $pdf->SetY($y_ + 164);
                    $pdf->SetX($x_ + 205);
                    $pdf->Cell(0, 0, $rowBasicos['estrato'], 0);

                    $pdf->SetFont($data_font_type, '', 8);
                    $pdf->SetY($y_ + 101);
                    $pdf->SetX($x_ + 325);
                    $pdf->Cell(0, 0, $rowBasicos['cod_cclo'], 0);

                    $pdf->SetY($y_ + 101);
                    $pdf->SetX($x_ + 456);
                    $pdf->Cell(0, 0, cambiofechaCeibas($rowBasicos['fec_ini_periodo']) . " al " . cambiofechaCeibas($rowBasicos['fec_fin_periodo']), 0); //Periodo Facturado

                    $pdf->SetFont($data_font_type, '', 8);
                    $pdf->SetY($y_ + 101);
                    $pdf->SetX($x_ + 355);
                    $pdf->Cell(0, 0, $rowBasicos['cod_peri_aseo'], 0);

                    $pdf->SetFont($data_font_type, '', 12);
                    $pdf->SetFont($data_font_type, 'B');
                    $pdf->SetY($y_ + 308);
                    $pdf->SetX($x_ + 490);
                    //$pdf->Cell(0,0,number_format($total_concp_aseo),0); // Total Conceptos
                    $pdf->Cell(0, 0, number_format($rowBasicos['total']), 0); // Total cabecera cargado aseo
                    $pdf->SetFont($data_font_type, '');
                }

                //DATOS HISTORICOS DE ASEO

                $historicoAseo = "SELECT mes, tdi, valor, mes2, tdi2, valor2, mes3, tdi3, valor3, mes4, tdi4, valor4, mes5, tdi5, valor5, mes6, tdi6, valor6 FROM factura_historico_aseo 
                WHERE cod_pred = '" . $cod_pred . "' 
                AND cod_peri = '" . $cod_peri . "'
                AND cod_munip = '" . $municipio . "' 
                AND cod_empr = '" . $empresa . "'";
                $query_historicoAseo = $conexion->prepare($historicoAseo);
                $query_historicoAseo->execute();

                while ($rowHistorico = $query_historicoAseo->fetch(PDO::FETCH_ASSOC)) {

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 212);
                    $pdf->SetX($x_ + 58);
                    $pdf->Cell(0, 0, $rowHistorico['mes'], 0);

                    $pdf->SetY($y_ + 222);
                    $pdf->SetX($x_ + 58);
                    $val_tdi1 = round($rowHistorico['tdi'], 1);
                    if ($val_tdi1 == 0) {
                        $tdi1 = "0.00";
                    } else {
                        $tdi1 = round($rowHistorico['tdi'], 1);
                    }
                    $pdf->Cell(0, 0, $tdi1, 0);

                    $pdf->SetY($y_ + 235);
                    $pdf->SetX($x_ + 58);
                    $pdf->Cell(0, 0, round($rowHistorico['valor'], 1), 0);

                    $pdf->SetY($y_ + 212);
                    $pdf->SetX($x_ + 96);
                    $pdf->Cell(0, 0, $rowHistorico['mes2'], 0);

                    $pdf->SetY($y_ + 222);
                    $pdf->SetX($x_ + 96);
                    $val_tdi2 = round($rowHistorico['tdi2'], 1);
                    if ($val_tdi2 == 0) {
                        $tdi2 = "0.00";
                    } else {
                        $tdi2 = round($rowHistorico['tdi2'], 1);
                    }
                    $pdf->Cell(0, 0, $tdi2, 0);

                    $pdf->SetY($y_ + 235);
                    $pdf->SetX($x_ + 96);
                    $pdf->Cell(0, 0, round($rowHistorico['valor2'], 1), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 212);
                    $pdf->SetX($x_ + 134);
                    $pdf->Cell(0, 0, $rowHistorico['mes3'], 0);

                    $pdf->SetY($y_ + 222);
                    $pdf->SetX($x_ + 134);
                    $val_tdi3 = round($rowHistorico['tdi3'], 1);
                    if ($val_tdi3 == 0) {
                        $tdi3 = "0.00";
                    } else {
                        $tdi3 = round($rowHistorico['tdi3'], 1);
                    }
                    $pdf->Cell(0, 0, $tdi3, 0);

                    $pdf->SetY($y_ + 235);
                    $pdf->SetX($x_ + 134);
                    $pdf->Cell(0, 0, round($rowHistorico['valor3'], 1), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 212);
                    $pdf->SetX($x_ + 172);
                    $pdf->Cell(0, 0, $rowHistorico['mes4'], 0);

                    $pdf->SetY($y_ + 222);
                    $pdf->SetX($x_ + 172);
                    $val_tdi4 = round($rowHistorico['tdi4'], 1);
                    if ($val_tdi4 == 0) {
                        $tdi4 = "0.00";
                    } else {
                        $tdi4 = round($rowHistorico['tdi4'], 1);
                    }
                    $pdf->Cell(0, 0, $tdi4, 0);

                    $pdf->SetY($y_ + 235);
                    $pdf->SetX($x_ + 172);
                    $pdf->Cell(0, 0, round($rowHistorico['valor4'], 1), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 212);
                    $pdf->SetX($x_ + 209);
                    $pdf->Cell(0, 0, $rowHistorico['mes5'], 0);

                    $pdf->SetY($y_ + 222);
                    $pdf->SetX($x_ + 209);
                    $val_tdi5 = round($rowHistorico['tdi5'], 1);
                    if ($val_tdi5 == 0) {
                        $tdi5 = "0.00";
                    } else {
                        $tdi5 = round($rowHistorico['tdi5'], 1);
                    }
                    $pdf->Cell(0, 0, $tdi5, 0);

                    $pdf->SetY($y_ + 235);
                    $pdf->SetX($x_ + 209);
                    $pdf->Cell(0, 0, round($rowHistorico['valor5'], 1), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 212);
                    $pdf->SetX($x_ + 248);
                    $pdf->Cell(0, 0, $rowHistorico['mes6'], 0);

                    $pdf->SetY($y_ + 222);
                    $pdf->SetX($x_ + 248);
                    $val_tdi6 = round($rowHistorico['tdi6'], 1);
                    if ($val_tdi6 == 0) {
                        $tdi6 = "0.00";
                    } else {
                        $tdi6 = round($rowHistorico['tdi6'], 1);
                    }
                    $pdf->Cell(0, 0, $tdi6, 0);

                    $pdf->SetY($y_ + 235);
                    $pdf->SetX($x_ + 248);
                    $pdf->Cell(0, 0, round($rowHistorico['valor6'], 1), 0);
                }

                //DATOS COMPONENTES ASEO

                $componentesAseo = "SELECT * FROM factura_componentes_aseo 
                WHERE cod_pred = '" . $cod_pred . "' 
                AND cod_peri = '" . $cod_peri . "'
                AND cod_munip = '" . $municipio . "' 
                AND cod_empr = '" . $empresa . "'";
                $query_componentesAseo = $conexion->prepare($componentesAseo);
                $query_componentesAseo->execute();

                while ($rowComponentesAseo = $query_componentesAseo->fetch(PDO::FETCH_ASSOC)) {

                    //RESIDENCIALES
                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 273);
                    $pdf->SetX($x_ + 90);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tbl_res'], 1), 0);

                    $pdf->SetY($y_ + 273);
                    $pdf->SetX($x_ + 124);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['trt_res'], 1), 0);

                    $pdf->SetY($y_ + 273);
                    $pdf->SetX($x_ + 154);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tdt_res'], 1), 0);

                    $pdf->SetY($y_ + 273);
                    $pdf->SetX($x_ + 183);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tfr_res'], 1), 0);

                    $pdf->SetY($y_ + 273);
                    $pdf->SetX($x_ + 242);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tarifa_res'], 1), 0);

                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 90);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tbl_res'], 1), 0);

                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 124);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_trt_res'], 1), 0);

                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 154);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tdt_res'], 1), 0);

                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 183);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tfr_res'], 1), 0);

                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 242);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tarifa_res'], 1), 0);

                    // $pdf->SetY($y_+293);$pdf->SetX($x_+154);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['densidad_res'],1),0);

                    // $pdf->SetY($y_+293);$pdf->SetX($x_+76);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['metros_res'],1),0);

                    // $pdf->SetY($y_+293);$pdf->SetX($x_+242);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['peso_res'],1),0);

                    // $pdf->SetY($y_+302);$pdf->SetX($x_+95);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['porcpart_res'],1),0);


                    //NO RESIDENCIALES
                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 323);
                    $pdf->SetX($x_ + 90);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tbl_nres'], 1), 0);

                    $pdf->SetY($y_ + 323);
                    $pdf->SetX($x_ + 124);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['trt_nres'], 1), 0);

                    $pdf->SetY($y_ + 323);
                    $pdf->SetX($x_ + 154);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tdt_nres'], 1), 0);

                    $pdf->SetY($y_ + 323);
                    $pdf->SetX($x_ + 183);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tfr_nres'], 1), 0);

                    $pdf->SetY($y_ + 323);
                    $pdf->SetX($x_ + 242);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['tarifa_nres'], 1), 0);

                    $pdf->SetY($y_ + 334);
                    $pdf->SetX($x_ + 90);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tbl_nres'], 1), 0);

                    $pdf->SetY($y_ + 334);
                    $pdf->SetX($x_ + 124);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_trt_nres'], 1), 0);

                    $pdf->SetY($y_ + 334);
                    $pdf->SetX($x_ + 154);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tdt_nres'], 1), 0);

                    $pdf->SetY($y_ + 334);
                    $pdf->SetX($x_ + 183);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tfr_nres'], 1), 0);

                    $pdf->SetY($y_ + 334);
                    $pdf->SetX($x_ + 242);
                    $pdf->Cell(0, 0, round($rowComponentesAseo['deso_tarifa_nres'], 1), 0);

                    // $pdf->SetY($y_+344);$pdf->SetX($x_+154);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['densidad_nr'],1),0);

                    // $pdf->SetY($y_+344);$pdf->SetX($x_+76);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['metros_nr'],1),0);

                    // $pdf->SetY($y_+344);$pdf->SetX($x_+242);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['peso_res'],1),0);

                    // $pdf->SetY($y_+353);$pdf->SetX($x_+95);
                    // $pdf->Cell(0,0,round($rowComponentesAseo['porcpart_nr'],1),0);
                }

                //DATOS FACTURADOS

                $detalleFacturado = "SELECT * FROM factura_det_aseo 
                WHERE cod_pred = '" . $cod_pred . "' 
                AND cod_peri = '" . $cod_peri . "'
                AND cod_munip = '" . $municipio . "' 
                AND cod_empr = '" . $empresa . "'";
                $query_facturado = $conexion->prepare($detalleFacturado);
                $query_facturado->execute();
                $total_concp_aseo = 0;
                while ($rowFacturado = $query_facturado->fetch(PDO::FETCH_ASSOC)) {

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + $secy + 137);
                    $pdf->SetX($x_ + 283);
                    $pdf->Cell(0, 0, $rowFacturado['descripcion'], 0);

                    $pdf->SetY($y_ + $secy + 137);
                    $pdf->SetX($x_ + 495);
                    $pdf->Cell(0, 0, round($rowFacturado['total_concp'], 1), 0);

                    $total_concp_aseo += $rowFacturado['total_concp'];
                    $secy += 10;
                }

                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetY($y_ + 101);
                $pdf->SetX($x_ + 415);
                $pdf->Cell(0, 0, $result['dias_facturados'], 0);

                $pdf->SetFont($data_font_type, '', 6);
                $pdf->SetY($y_ + 164);
                $pdf->SetX($x_ + 65);
                $pdf->Cell(0, 0, $result['uso'], 0);

                $pdf->SetFont($data_font_type, '', 10);
                $pdf->SetY($y_ + 299);
                $pdf->SetX($x_ + 384);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0); //Fecha de Expedicion

                $pdf->SetY($y_ + 315);
                $pdf->SetX($x_ + 384);
                $pdf->Cell(0, 0, cambiofechaCeibas($fecvto), 0); // FECHA PAGO OPORTUNO

                //Aqui estaba total ASEO//

                $pdf->SetFont($data_font_type, '', 11);
                $pdf->SetY($y_ + 588);
                $pdf->SetX($x_ + 270);
                $pdf->Cell(0, 0, $n_fac_aseo, 0);

                $pdf->SetY($y_ + 588);
                $pdf->SetX($x_ + 480);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetY($y_ + 622);
                $pdf->SetX($x_ + 203);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fec_lectura_ant']) . " al " . cambiofechaCeibas($result['feclectura']), 0); //Periodo Facturado

                $totalFacturaCompleta = $facturado; // + $total_concp_aseo;

                $pdf->SetFont($data_font_type, 'B');
                $pdf->SetY($y_ + 630);
                $pdf->SetX($x_ + 497);
                $pdf->Cell(0, 0, number_format($totalFacturaCompleta), 0); // Total Factura Completa 
                $pdf->SetFont($data_font_type, '');

                $pdf->SetY($y_ + 723);
                $pdf->SetX($x_ + 483);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0); //Fecha de emision

                // TOTAL A PAGAR DEL MES Codigo de barras

                //$barcode= chr(241)."415".$recuaudo_corriente."8020".str_pad("47580881",10,"0",STR_PAD_LEFT).chr(241)."3900".str_pad(number_format($totalFacturaCompleta,0,'',''),10,"0",STR_PAD_LEFT).chr(241)."96"."20200620";
                $barcode = chr(241) . "415" . $recuaudo_corriente . "8020" . str_pad($factura_no_cuenta_cobro, 14, "0", STR_PAD_LEFT) . chr(241) . "3900" . str_pad(number_format($totalFacturaCompleta, 0, '', ''), 10, "0", STR_PAD_LEFT) . chr(241) . "96" . str_replace("-", "", $result['fecvto']);

                //$textBarcode = "(415)".$recuaudo_corriente."(8020)".str_pad("47580881",10,"0",STR_PAD_LEFT)."(3900)".str_pad(number_format($totalFacturaCompleta,0,'',''),10,"0",STR_PAD_LEFT)."(96)"."20200620";
                $textBarcode = "(415)" . $recuaudo_corriente . "(8020)" . str_pad($factura_no_cuenta_cobro, 14, "0", STR_PAD_LEFT) . "(3900)" . str_pad(number_format($totalFacturaCompleta, 0, '', ''), 10, "0", STR_PAD_LEFT) . "(96)" . str_replace("-", "", $result['fecvto']);

                $pdf->write1DBarcode($barcode, 'C128', $x_ + 30, $y_ + 665, 320, 30, 1, $style, 'N');
                $pdf->SetFontSize('6');

                $pdf->Text($x_ + 92, $y_ + 695, $textBarcode);

                // FIN DATOS PAGINA CIUDAD LIMPIA            

            } elseif ($cod_peri == 202012) {

                //INICIAMOS HOJA 1

                if ($result['nombre'] == "") {
                    $pdf->AddPage();
                    $pdf->SetY(100);
                    $pdf->SetX(200);
                    $pdf->Cell(0, 0, "Error: Factura sin nombre", 0);
                } else {
                    // add a page
                    $pdf->setPrintHeader(false);
                    $pdf->AddPage();
                    // -- set new background ---
                    // get the current page break margin
                    $bMargin = $pdf->getBreakMargin();
                    // get current auto-page-break mode
                    $auto_page_break = $pdf->getAutoPageBreak();
                    // disable auto-page-break
                    $pdf->SetAutoPageBreak(false, 0);
                    // set bacground image
                    $img_file = '../assets/images/FACTURA_LAS_CEIBAS.jpg';
                    $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                    // restore auto-page-break status
                    $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                    // set the starting point for the page content
                    $pdf->setPageMark();

                    if (isset($_GET['poliza'])) {

                        $consulta_edad_coactivo = "SELECT edad_coactivo FROM municipio WHERE cod_empr = '$empresa' AND cod_munip = $municipio";
                        $query_coactivo_edad = $conexion->prepare($consulta_edad_coactivo);
                        $query_coactivo_edad->execute();

                        $row = $query_coactivo_edad->fetch();
                        $edad_coactivo = $row['edad_coactivo'];

                        $consulta = "SELECT * FROM predio 
                        WHERE cod_pred = $cod_pred 
                        AND cod_empr = '$empresa' 
                        AND edad_cartera > $edad_coactivo";
                        $query = $conexion->prepare($consulta);
                        $query->execute();

                        if ($data = $query->fetch()) {
                            $pdf->SetFont($data_font_type, '', 10);
                            $pdf->SetY($y_ + 72);
                            $pdf->SetX($x_ + 129);
                            $pdf->Cell(0, 0, "EN ETAPA DE COBRO COACTIVO", 0); //indica en una factura si el susuario tiene un cobro coactivo
                        }
                    }

                    // INFORMACION DEL CLIENTE 
                    $pdf->SetFont($data_font_type, '', 10);
                    $long_factura = strlen($result['nro_factura']);
                    if ($long_factura < 10) {
                        $factura_no = str_pad($result['nro_factura'], 9, '0', STR_PAD_LEFT);
                        $factura_no = "1" . $factura_no;
                    } else {
                        $factura_no = $result['nro_factura'];
                    }
                    $pdf->SetFont($data_font_type, 'B');
                    $pdf->SetY($y_ + 197);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, $factura_no, 0);

                    $pdf->SetY($y_ + 174);
                    $pdf->SetX($x_ + 225);
                    $pdf->Cell(0, 0, $result['cod_pred'], 0);
                    $pdf->SetFont($data_font_type, '');

                    $pdf->SetFont($data_font_type, '', 8);
                    $pdf->SetY($y_ + 60);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, $result['nombre'], 0);

                    $pdf->SetY($y_ + 75);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, mb_convert_encoding($result['direccion'] . " " . $result['barrio'], "UTF-8", "UTF-8"), 0);

                    $pdf->SetY($y_ + 90);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, "C.C o NIT: " . $result['nro_documento'], 0);

                    $pdf->SetY($y_ + 128);
                    $pdf->SetX($x_ + 345);
                    $pdf->Cell(0, 0, $result['cod_cclo'], 0);

                    $pdf->SetY($y_ + 102);
                    $pdf->SetX($x_ + 475);
                    $pdf->Cell(0, 0, $result['uso'], 0);

                    $estrato = ($result['estrato'] == 0) ? "1" : $result['estrato'];
                    $pdf->SetY($y_ + 101);
                    $pdf->SetX($x_ + 370);
                    $pdf->Cell(0, 0, $estrato, 0);

                    $pdf->SetY($y_ + 128);
                    $pdf->SetX($x_ + 510);
                    $rutareparto = ($result['rutareparto'] == "") ? "SIN RUTA" : $result['rutareparto'];
                    $pdf->Cell(0, 0, $rutareparto, 0);

                    $sql_conv = "SELECT cc.cod_conv,ct.valor_capital,ct.valor_interes,ct.id_detalle,
                    cc.cant_cuotas, ct.ccobro
                    from convenio_cab cc, convenio_cuota ct
                    WHERE cc.cod_conv=ct.cod_conv and cc.cod_empr=ct.cod_empr and cc.cod_munip=ct.cod_munip
                    AND cc.cod_pred=" . $result['cod_pred'] . " and cc.cod_munip=" . $municipio . " 
                    and ct.cod_peri_apli=" . $result['cod_peri'] . " ";
                    $exSql_conv = $conexion->prepare($sql_conv);
                    $exSql_conv->execute();
                    $data_conv = $exSql_conv->fetch();
                    $codconv = $data_conv['cod_conv'];

                    if ($codconv > 0) {
                        $txt_web = "DUPLICADO - EN CONVENIO";
                        $tm = 40;
                        $px = 10;
                        $py = 50;
                    } else {
                        $txt_web = "DUPLICADO";
                        $tm = 50;
                        $px = 150;
                        $py = 5;
                    }

                    /*Fondo Duplicado*/
                    $pdf->SetFont('helvetica', '', $tm);
                    $pdf->SetDrawColor(200);
                    $pdf->SetTextColor(200);
                    //MARCA DE AGUA

                    $pdf->StartTransform();

                    $pdf->Rotate(40);

                    $pdf->Text($px, $py, $txt_web);
                    // Stop Transformation
                    $pdf->StopTransform();

                    $pdf->SetTextColor(0, 0, 0);
                    // DATOS DE MEDIDOR 
                    $pdf->SetFont($data_font_type, '', 6);
                    $marca = ($result['marca'] == "") ? "S/M" : $result['marca'];
                    //$pdf->SetY($y_+155);$pdf->SetX($x_+335);
                    //$pdf->Cell(0,0,$marca,0);

                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 405);
                    $serial_medidor = ($result['serialmedi'] == "") ? "S/S" : $result['serialmedi'];
                    $pdf->Cell(0, 0, $serial_medidor, 0);

                    $explode_calibre  = $result['calibre'];
                    $array_calibre = explode(" ", $explode_calibre);

                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 535);
                    $pdf->Cell(0, 0, $array_calibre[0], 0); // string1

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 298);
                    $pdf->SetX($x_ + 370);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fec_lectura_ant']) . " al " . cambiofechaCeibas($result['feclectura']), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 183);
                    $pdf->SetX($x_ + 411);
                    $pdf->Cell(0, 0, $result['lectura'], 0);

                    $pdf->SetY($y_ + 192);
                    $pdf->SetX($x_ + 411);
                    $pdf->Cell(0, 0, $result['lectura_anterior'], 0);

                    $pdf->SetY($y_ + 200);
                    $pdf->SetX($x_ + 415);
                    $pdf->Cell(0, 0, abs($result['lectura'] - $result['lectura_anterior']), 0);

                    $pdf->SetY($y_ + 205);
                    $pdf->SetX($x_ + 536);
                    $pdf->Cell(0, 0, number_format($result['consumo'], 0, ',', '.'), 0, 0, 'L', 0);

                    $pdf->SetY($y_ + 250);
                    $pdf->SetX($x_ + 518);
                    $pdf->Cell(0, 0, number_format($result['promedio'], 0, ',', '.'), 0, 0, 'L', 0);

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 488);
                    $pdf->Cell(0, 0, $result['dias_facturados'], 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 216);
                    $pdf->SetX($x_ + 415);
                    $pdf->Cell(0, 0, $result['obs_lectura'], 0);

                    // DATOS DEUDA
                    $pdf->SetFont($data_font_type, 9);
                    $pdf->SetY($y_ + 284);
                    $pdf->SetX($x_ + 338);
                    $pdf->Cell(0, 0, $result['cod_peri'], 0);

                    $pdf->SetFont($data_font_type, 'B', 9);
                    $pdf->SetY($y_ + 250);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0);

                    $fecvto = $result['fecvto'];
                    $fecsus = $result['fec_susp'];

                    $pdf->SetY($y_ + 263);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($fecvto), 0); // FECHA PAGO OPORTUNO

                    $pdf->SetY($y_ + 299);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fch_ult_pago']), 0);

                    $pdf->SetY($y_ + 287);
                    $pdf->SetX($x_ + 229);
                    $pdf->Cell(0, 0, '$ ' . number_format($result['valor_ult_pago'], 0, ',', '.'), 0); //Valor ultimo pago

                    $pdf->SetY($y_ + 274);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($fecsus), 0); //fecha de suspension

                    $pdf->SetY($y_ + 43);
                    $pdf->SetX($x_ + 125);
                    $pdf->Cell(0, 0, $result['nropermora'] + 1, 0); //Numero de Facturas

                    $sql = "SELECT (fpc.deuda_e1+fpc.deuda_e2+fpc.deuda_e3) As facturado
                            FROM public.factura_per_cab As fpc
                            WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                    fpc.cod_peri = " . $cod_peri . " And 
                                    fpc.cod_munip = " . $municipio . " And
                                    fpc.cod_empr = '" . $empresa . "';";
                    $rfact = $conexion->prepare($sql);
                    $rfact->execute();
                    $data_fact = $rfact->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                    $facturado = $data_fact[0];

                    $sql = "SELECT fpc.deuda_e1, coalesce(fpc.nro_cuenta_cobro,0) as nro_cuenta_cobro
                            FROM public.factura_per_cab As fpc
                            WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                    fpc.cod_peri = " . $cod_peri . " And 
                                    fpc.cod_munip = " . $municipio . " And
                                    fpc.cod_empr = '" . $empresa . "';";
                    $rpago = $conexion->prepare($sql);
                    $rpago->execute();
                    $data_pago = $rpago->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                    $deuda = $data_pago[0];
                    $data_pago2 = $rpago->fetch();
                    $nro_cuenta_cobro = $data_pago2['nro_cuenta_cobro'];

                    $total_pagar = $deuda_ess + $deuda;

                    if ($nro_cuenta_cobro == 0) {
                        $factura_no_cuenta_cobro = $factura_no;
                        $prefijo = "";
                    } else {
                        $factura_no_cuenta_cobro = "5" . str_pad($nro_cuenta_cobro, 9, '0', STR_PAD_LEFT);
                    }

                    //Cambiar Manera de traer el total

                    $detalleFacturado1 = "SELECT * FROM factura_det_aseo 
                    WHERE cod_pred = '" . $cod_pred . "' 
                    AND cod_peri = '" . $cod_peri . "'
                    AND cod_munip = '" . $municipio . "' 
                    AND cod_empr = '" . $empresa . "'";
                    $query_facturado1 = $conexion->prepare($detalleFacturado1);
                    $query_facturado1->execute();
                    $total_concp_aseo_spoll1 = 0;
                    while ($rowFacturado1 = $query_facturado1->fetch(PDO::FETCH_ASSOC)) {
                        $total_concp_aseo_spoll1 += $rowFacturado1['total_concp'];
                    }

                    $totalFacturaCompleta1 = $facturado; // + $total_concp_aseo_spoll1;
                    //FIN

                    $pdf->SetFont($data_font_type, 'B', 12);
                    $pdf->SetY($y_ + 225);
                    $pdf->SetX($x_ + 218);
                    $pdf->Cell(0, 0, '$ ' . number_format($totalFacturaCompleta1, 0, ',', '.'), 0, 0, 'L', 0); //deuda total mes seleccionado


                    // TOTAL POR CONCEPTO
                    $sql = "SELECT * FROM factura_per_det
                            WHERE cod_pred=" . $result['cod_pred'] . " and
                                cod_peri=" . $cod_peri . " and 
                                cod_empr='" . $empresa . "' and 
                                cod_munip=" . $municipio . " and
                                cod_conc not in (select cod_conc from concepto where cod_serv=3)
                                order by cod_conc ";

                    $exe_detalle = $conexion->prepare($sql);

                    $total_concepto_acueducto       = 0;
                    $total_concepto_alcantarillado  = 0;
                    $total_otros_conceptos          = 0;
                    // $total_veolia                   = 0;
                    $total_pag                      = 0;
                    $exe_detalle->execute();
                    while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {
                        //--sumatoria para el resumen
                        if ($result_det['cod_conc'] == $acueducto) { // ACUEDUCTO
                            $total_concepto_acueducto += $result_det['total_concp'];
                        }
                        if ($result_det['cod_conc'] == $alcantarillado) { // ALCANTARILLADO
                            $total_concepto_alcantarillado += $result_det['total_concp'];
                        }

                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) { // and ($result_det['cod_conc']!=100)){ // OTROS
                            $total_otros_conceptos += $result_det['total_concp'];
                        }
                    }

                    // GRAFICA - HISTORICO DE CONSUMO
                    GraficaHistoricaCeibas($conexion, $pdf, $result['cod_pred'], $result['peri_ant'], $result['cod_cclo'], $municipio, $empresa);

                    $style = array(
                        'position' => '',
                        'align' => 'L',
                        'stretch' => false,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'hpadding' => 'auto',
                        'vpadding' => 'auto',
                        'fgcolor' => array(0, 0, 0),
                        'bgcolor' => false, //array(255,255,255),
                        'text' => false,
                        'font' => 'helvetica',
                        'fontsize' => 8,
                        'stretchtext' => 4
                    );

                    //DETALLE

                    $total_concepto_acueducto       = 0;
                    $total_concepto_alcantarillado  = 0;
                    $total_otros_conceptos          = 0;
                    $total_otros                    = 0;
                    $tc1 = 0;
                    $tc2 = 0;
                    $tc3 = 0;
                    $tca1 = 0;
                    $tca2 = 0;
                    $tca3 = 0;
                    $secy = 0;
                    $unidades = "";

                    // DETALLES CONCEPTOS DE LA FACTURA
                    $sql = "SELECT * FROM factura_per_det
                            WHERE cod_pred=" . $result['cod_pred'] . " and
                                cod_peri=" . $cod_peri . " and 
                                cod_empr='" . $empresa . "' and 
                                cod_munip=" . $municipio . " and
                                cod_conc not in (select cod_conc from concepto where cod_serv=3)
                                order by cod_conc ";

                    $exe_detalle = $conexion->prepare($sql);
                    $exe_detalle->execute();

                    while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {

                        $suapoR1    = $result_det['subapo_r1'];
                        $suapoR2    = $result_det['subapo_r2'];
                        $suapoR3    = $result_det['subapo_r3'];
                        $suapoCF    = $result_det['subapo_cfijo'];
                        $tasaUso    = $result_det['tasa_ur'];

                        //--sumatoria para el resumen
                        if ($result_det['cod_conc'] == $acueducto) { //acueducto
                            $total_concepto_acueducto += $result_det['total_concp'];
                        }
                        if ($result_det['cod_conc'] == $alcantarillado) { //alcantarillado
                            $total_concepto_alcantarillado += $result_det['total_concp'];
                        }
                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                            $total_otros_conceptos += $result_det['total_concp'];
                        }

                        //--liquidacion Acueducto
                        if ($result_det['cod_conc'] == $acueducto) {
                            $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                            $tc1 = $result_det['canti_e1'] * $result_det['precioe1'];
                            $tc2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                            $tc3 = $result_det['canti_e3'] * $result_det['precioe3'];

                            //Tasas
                            $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                            $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                            $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                            $sqlacu = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                    fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                    from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                    where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                        fpc.cod_peri=" . $cod_peri . " and 
                                        fpc.cod_munip=" . $municipio . " and 
                                        fpc.cod_empr='" . $empresa . "' and
                                        fpd.cod_conc=" . $acueducto;

                            $sql_acu = $conexion->prepare($sqlacu) or die("Error: Detalle Acueducto \n" . $sqlacu);
                            $sql_acu->execute();
                            $data_acu = $sql_acu->fetch();

                            $cod_concep_acu = $data_acu['cod_conc']; //codigo cocepto acueducto
                            $uso_acu        = $data_acu['cod_uso']; //codigo cocepto acueducto
                            $cod_estado_acu = $data_acu['cod_estd']; //codigo estado acueducto
                            $fec_lec_acu    = $data_acu['feclectura']; //codigo estado acueducto
                            $fec_lec_ant_acu = $data_acu['fec_lectura_ant']; //codigo estado acueducto
                            $cod_tari_acu   = $data_acu['cod_tarf_ref']; //codigo tarifa acueducto
                            $cod_uso_acu    = $data_acu['cod_uso_ref']; //codigo uso acueducto
                            $codtarf        = $data_acu['cod_tarf'];
                            //--calcular tarifas
                            $sq_cal_tari_acu1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_acu', $cod_tari_acu, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_acu1 = $conexion->prepare($sq_cal_tari_acu1);
                            $sql_cal_tari_acu1->execute();
                            $data_tari = $sql_cal_tari_acu1->fetch();
                            $cod_cal_tari_acu = $data_tari['fact_calcular_tarifa'];

                            $sq_cal_tari_acu2 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$uso_acu', $codtarf, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_acu2 = $conexion->prepare($sq_cal_tari_acu2);
                            $sql_cal_tari_acu2->execute();
                            $data_tari2 = $sql_cal_tari_acu2->fetch();
                            $cod_cal_tari_acu2 = $data_tari2['fact_calcular_tarifa'];
                            $resto2 = str_replace("{", " ", $cod_cal_tari_acu2);
                            $cal_tari_acu2 = explode(",", $resto2);

                            $resto = str_replace("{", " ", $cod_cal_tari_acu);
                            $cal_tari_acu = explode(",", $resto); //si fecha esta en formato dia-mes-año 

                            $consu_basic1   = $cal_tari_acu[1];
                            $consu_comple1  = $cal_tari_acu[2];
                            $consu_sunt1    = $cal_tari_acu[3];
                            $cargo_fijo1    = $cal_tari_acu[0];
                            $porcen         = $cal_tari_acu2[5];

                            if (!isset($subapo_t1)) {
                                $subapo_t1 = 0;
                            }
                            if (!isset($subapo_t2)) {
                                $subapo_t2 = 0;
                            }
                            if (!isset($subapo_t3)) {
                                $subapo_t3 = 0;
                            }

                            //Aqui debo tomar el dato de tasa 
                            $ta_r1 = $cal_tari_acu[14];
                            $ta_r2 = $cal_tari_acu[15];
                            $ta_r3 = $cal_tari_acu[16];

                            $sum_sub = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF;
                            $sum_sub_tasa = $subapo_t1 + $subapo_t2 + $subapo_t3;

                            $valortotal_acueducto = $result_det['total_concp'];

                            //--Descripcion
                            $pdf->SetFont($data_font_type, '', 8);
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Cargo Fijo", 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 37);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, "Consumo Básico", 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 37);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, "Consumo Complementario", 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 37);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, "Consumo Suntuario", 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Tasa Uso", 0);

                            //--acueducto consumo
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 160);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 160);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 160);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0);

                            //--acueducto tarifa referencia
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 182);
                            $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo1, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 182);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_basic1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 182);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_comple1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 182);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 182);
                            $pdf->Cell(0, 0, number_format($ta_r1, 1, ',', '.'), 0);


                            //valor total
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_fija'] * $cargo_fijo1), 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 215);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] * $consu_basic1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 215);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e2'] * $consu_comple1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 215);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e3'] * $consu_sunt1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, number_format((($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) * $ta_r1), 1, ',', '.'), 0);

                            //--acueducto valor
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 250);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 250);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 250);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, number_format($sum_sub_tasa, 1, ',', '.'), 0);


                            //--acueducto valor a pagar
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 365);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 283);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 283);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 283);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, number_format(($ta_u1 + $ta_u2 + $ta_u3), 1, ',', '.'), 0);

                            //--Total concepto acueducto
                            $pdf->SetFont($data_font_type, 'B', 6);
                            $pdf->SetY($y_ + 418);
                            $pdf->SetX($x_ + 281);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        }

                        //--Liquidacion Alcantarillado
                        if ($result_det['cod_conc'] == $alcantarillado) {
                            $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                            $tca1 = $result_det['canti_e1'] * $result_det['precioe1'];
                            $tca2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                            $tca3 = $result_det['canti_e3'] * $result_det['precioe3'];

                            //Tasas
                            $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                            $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                            $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                            $sqlalc = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                        fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                        from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                        where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                            fpc.cod_peri=" . $cod_peri . " and 
                                            fpc.cod_munip=" . $municipio . " and 
                                            fpc.cod_empr='" . $empresa . "' and
                                            fpd.cod_conc=" . $alcantarillado;
                            $sql_alc = $conexion->prepare($sqlalc) or die("Error: Detalle Alcantarillado \n" . $sqlalc);
                            $sql_alc->execute();
                            $data_alc = $sql_alc->fetch();
                            $cod_concep_alc = $data_alc['cod_conc'];
                            $uso_alc        = $data_alc['cod_uso'];
                            $codtarf        = $data_alc['cod_tarf'];
                            $cod_estado_alc = $data_alc['cod_estd'];
                            $fec_lec_alc    = $data_alc['feclectura'];
                            $fec_lec_ant_alc = $data_alc['fec_lectura_ant'];
                            $cod_tari_alc   = $data_alc['cod_tarf_ref'];
                            $cod_uso_alc    = $data_alc['cod_uso_ref'];

                            $sq_cal_tari_alc1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_alc', $cod_tari_alc, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_alc1 = $conexion->prepare($sq_cal_tari_alc1);
                            $sql_cal_tari_alc1->execute();
                            $data_tari = $sql_cal_tari_alc1->fetch();
                            $cod_cal_tari_alc = $data_tari['fact_calcular_tarifa'];
                            $resto1 = str_replace("{", " ", $cod_cal_tari_alc);
                            $cal_tari = explode(",", $resto1); //si fecha esta en formato dia-mes-año 

                            $sq_cal_tari_alca2 = "SELECT fact_calcular_tarifa('" . $empresa . "'," . $municipio . ", '$uso_alc', $codtarf, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_alca2 = $conexion->prepare($sq_cal_tari_alca2);
                            $sql_cal_tari_alca2->execute();
                            $data_tari2 = $sql_cal_tari_alca2->fetch();
                            $cod_cal_tari_alca2 = $data_tari2['fact_calcular_tarifa'];
                            $resto2 = str_replace("{", " ", $cod_cal_tari_alca2);
                            $cal_tari_alca2 = explode(",", $resto2);

                            $consu_basic    = $cal_tari[1];
                            $consu_comple   = $cal_tari[2];
                            $consu_sunt     = $cal_tari[3];
                            $cargo_fijo     = $cal_tari[0];
                            $porcen         = $cal_tari_alca2[5];

                            //Aqui debo tomar el dato de tasa 
                            $ta_r1_alc = $cal_tari[14];
                            $ta_r2_alc = $cal_tari[15];
                            $ta_r3_alc = $cal_tari[16];

                            $valortotal_alcantarillado = $result_det['total_concp'];
                            $valortotal_acueducto = $result_det['total_concp'];

                            //--Descripcion
                            $pdf->SetFont($data_font_type, '', 8);
                            $pdf->SetY($y_ + 443);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Cargo Fijo", 0);
                            $pdf->SetY($y_ + 453);
                            $pdf->SetX($x_ + 37);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, "Consumo Básico", 0); //}
                            $pdf->SetY($y_ + 463);
                            $pdf->SetX($x_ + 37);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, "Consumo Complementario", 0); //}
                            $pdf->SetY($y_ + 473);
                            $pdf->SetX($x_ + 37);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, "Consumo Suntuario", 0); //}
                            $pdf->SetY($y_ + 483);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Tasa Retributiva", 0);

                            //--alcantarillado consumo
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 160);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 160);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 160);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0);

                            //--alcantarillado tarifa referencia
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 183);
                            $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 183);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_basic, 2, ',', '.'), 0); //} 
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 183);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_comple, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 183);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 183);
                            $pdf->Cell(0, 0, number_format($ta_r1_alc, 1, ',', '.'), 0);



                            //Valor total
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_fija'] * $cargo_fijo), 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 215);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] * $consu_basic), 2, ',', '.'), 0); //} 
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 215);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e2'] * $consu_comple), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 215);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e3'] * $consu_sunt), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, number_format((($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) * $ta_r1_alc), 1, ',', '.'), 0);

                            $sum_sub_1 = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF;
                            //$sum_sub_tasa_alc = $subapo_t1 + $subapo_t2 + $subapo_t3;
                            $sum_sub_tasa_alc = 0;
                            //--alcantarillado valor
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 251);
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 251);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 251);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 251);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 251);
                            $pdf->Cell(0, 0, number_format($sum_sub_tasa_alc, 1, ',', '.'), 0);

                            $valor_acueducto_alcantarillado = $valortotal_acueducto + $valortotal_alcantarillado;
                            $total_dia = $valor_acueducto_alcantarillado / $result['dias_facturados'];

                            //--alcantarillado valor Total
                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + 510);
                            $pdf->SetX($x_ + 419);
                            $pdf->Cell(0, 0, '$ ' . number_format($valor_acueducto_alcantarillado, 2, ',', '.'), 0); //valor total acue + alcantarillado 


                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + 509);
                            $pdf->SetX($x_ + 519);
                            $pdf->Cell(0, 0, '$ ' . number_format($total_dia, 2, ',', '.'), 0); //valor dia

                            //--alcantarillado valor a pagar
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 445);
                            $pdf->SetX($x_ + 284);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 284);
                            //if ($tca1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tca1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 465);
                            $pdf->SetX($x_ + 284);
                            //if ($tca2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tca2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 475);
                            $pdf->SetX($x_ + 284);
                            //if ($tca3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tca3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 485);
                            $pdf->SetX($x_ + 284);
                            $pdf->Cell(0, 0, number_format(($ta_u1 + $ta_u2 + $ta_u3), 1, ',', '.'), 0);
                            //--Alcantarillado Total a pagar
                            $pdf->SetFont($data_font_type, 'B', 6);
                            $pdf->SetY($y_ + 495);
                            $pdf->SetX($x_ + 281);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        }

                        //--Liquidacion Otros conceptos
                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + $secy + 337);
                            $pdf->SetX($x_ + 330);
                            $pdf->Cell(0, 0, $result_det['descripcion'], 0);

                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + $secy + 337);
                            $pdf->SetX($x_ + 420);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                            $total_otros += $result_det['total_concp'];
                            $secy += 10;
                        }
                    }
                    if (((($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total'])) > 0) {
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + $secy + 337);
                        $pdf->SetX($x_ + 330);
                        $pdf->Cell(0, 0, "DEUDAS ANTERIORES", 0);

                        $pdf->SetFont($data_font_type, '', 7);
                        $pdf->SetY($y_ + $secy + 337);
                        $pdf->SetX($x_ + 420);
                        $pdf->Cell(0, 0, '$ ' . number_format((($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total']), 2, ',', '.'), 0);
                        $total_otros += (($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total']);
                        $secy += 10;
                    }
                    $pdf->SetFont($data_font_type, 'B', 7);
                    $pdf->SetY($y_ + 417);
                    $pdf->SetX($x_ + 521);
                    $pdf->Cell(0, 0, '$ ' . number_format($total_otros, 2, ',', '.'), 0);
                    $pdf->SetFont($data_font_type, '', 7);

                    //TOTAL 1 + 2 + 3
                    $pdf->SetFont($data_font_type, 'B', 8);
                    //$pdf->SetY($y_+589);$pdf->SetX($x_+308);
                    $pdf->SetY($y_ + 509);
                    $pdf->SetX($x_ + 300);
                    //$pdf->Cell(0,0,'$ '.number_format(($total_concepto_acueducto+$total_concepto_alcantarillado+$total_otros_conceptos),0,',','.'),0,0,'L',0);//deuda total mes seleccionado
                    $pdf->Cell(0, 0, '$ ' . number_format(($total_concepto_acueducto + $total_concepto_alcantarillado + $total_otros), 0, ',', '.'), 0, 0, 'L', 0);
                }

                //INICIAMOS HOJA 2
                $pdf->setPrintHeader(false);
                $pdf->AddPage();
                // -- set new background ---
                // get the current page break margin
                $bMargin = $pdf->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $pdf->getAutoPageBreak();
                // disable auto-page-break
                $pdf->SetAutoPageBreak(false, 0);
                // set bacground image
                $img_file = '../assets/images/FACTURA_LAS_CEIBAS_DICIEMBRE_2020.jpg';
                $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                // restore auto-page-break status
                $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $pdf->setPageMark();
                $n_fac_aseo = "";
                $pdf->SetFont($data_font_type, '', 11);
                $pdf->SetY($y_ + 600);
                $pdf->SetX($x_ + 270);
                $pdf->Cell(0, 0, $n_fac_aseo, 0);

                $pdf->SetY($y_ + 600);
                $pdf->SetX($x_ + 480);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetY($y_ + 632);
                $pdf->SetX($x_ + 203);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fec_lectura_ant']) . " al " . cambiofechaCeibas($result['feclectura']), 0); //Periodo Facturado

                $totalFacturaCompleta = $facturado; // + $total_concp_aseo;

                $pdf->SetFont($data_font_type, 'B');
                $pdf->SetY($y_ + 640);
                $pdf->SetX($x_ + 497);
                $pdf->Cell(0, 0, "$ " . number_format($totalFacturaCompleta), 0); // Total Factura Completa 
                $pdf->SetFont($data_font_type, '');

                $pdf->SetY($y_ + 735);
                $pdf->SetX($x_ + 485);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0); //Fecha de emision

                // TOTAL A PAGAR DEL MES Codigo de barras

                $barcode = chr(241) . "415" . $recuaudo_corriente . "8020" . str_pad($factura_no_cuenta_cobro, 14, "0", STR_PAD_LEFT) . chr(241) . "3900" . str_pad(number_format($totalFacturaCompleta, 0, '', ''), 10, "0", STR_PAD_LEFT) . chr(241) . "96" . str_replace("-", "", $result['fecvto']);

                $textBarcode = "(415)" . $recuaudo_corriente . "(8020)" . str_pad($factura_no_cuenta_cobro, 14, "0", STR_PAD_LEFT) . "(3900)" . str_pad(number_format($totalFacturaCompleta, 0, '', ''), 10, "0", STR_PAD_LEFT) . "(96)" . str_replace("-", "", $result['fecvto']);

                $pdf->write1DBarcode($barcode, 'C128', $x_ + 30, $y_ + 675, 320, 30, 1, $style, 'N');
                $pdf->SetFontSize('6');

                $pdf->Text($x_ + 92, $y_ + 705, $textBarcode);
            } else {
                //INICIAMOS HOJA 1
                if ($result['nombre'] == "") {
                    $pdf->AddPage();
                    $pdf->SetY(100);
                    $pdf->SetX(200);
                    $pdf->Cell(0, 0, "Error: Factura sin nombre", 0);
                } else {
                    // add a page
                    $pdf->setPrintHeader(false);
                    $pdf->AddPage();
                    // -- set new background ---
                    // get the current page break margin
                    $bMargin = $pdf->getBreakMargin();
                    // get current auto-page-break mode
                    $auto_page_break = $pdf->getAutoPageBreak();
                    // disable auto-page-break
                    $pdf->SetAutoPageBreak(false, 0);
                    // set bacground image
                    $img_file = '../assets/images/FACTURA_LAS_CEIBAS_2021.jpg';
                    $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                    // restore auto-page-break status
                    $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                    // set the starting point for the page content
                    $pdf->setPageMark();

                    if (isset($_GET['poliza'])) {

                        $consulta_edad_coactivo = "SELECT edad_coactivo FROM municipio WHERE cod_empr = '$empresa' AND cod_munip = $municipio";
                        $query_coactivo_edad = $conexion->prepare($consulta_edad_coactivo);
                        $query_coactivo_edad->execute();
                        $row = $query_coactivo_edad->fetch();
                        $edad_coactivo = $row['edad_coactivo'];

                        $consulta = "SELECT * FROM predio 
                        WHERE cod_pred = $cod_pred 
                        AND cod_empr = '$empresa' 
                        AND edad_cartera > $edad_coactivo";
                        $query = $conexion->prepare($consulta);
                        $query->execute();
                        if ($data = $query->fetch()) {
                            $pdf->SetFont($data_font_type, '', 10);
                            $pdf->SetY($y_ + 63);
                            $pdf->SetX($x_ + 122);
                            $pdf->Cell(0, 0, "EN ETAPA DE COBRO COACTIVO", 0); //indica en una factura si el susuario tiene un cobro coactivo
                        }
                    }

                    // INFORMACION DEL CLIENTE 
                    $pdf->SetFont($data_font_type, '', 10);
                    $long_factura = strlen($result['nro_factura']);
                    if ($long_factura < 10) {
                        $factura_no = str_pad($result['nro_factura'], 9, '0', STR_PAD_LEFT);
                        $factura_no = "1" . $factura_no;
                    } else {
                        $factura_no = $result['nro_factura'];
                    }
                    $pdf->SetFont($data_font_type, 'B');
                    $pdf->SetY($y_ + 197);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, $factura_no, 0);

                    $pdf->SetY($y_ + 174);
                    $pdf->SetX($x_ + 225);
                    $pdf->Cell(0, 0, $result['cod_pred'], 0);
                    $pdf->SetFont($data_font_type, '');

                    $pdf->SetFont($data_font_type, '', 8);
                    $pdf->SetY($y_ + 55);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, $result['nombre'], 0);

                    $pdf->SetY($y_ + 70);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, mb_convert_encoding($result['direccion'] . " " . $result['barrio'], "UTF-8", "UTF-8"), 0);

                    $pdf->SetY($y_ + 85);
                    $pdf->SetX($x_ + 315);
                    $pdf->Cell(0, 0, "C.C o NIT: " . $result['nro_documento'], 0);

                    $pdf->SetY($y_ + 128);
                    $pdf->SetX($x_ + 345);
                    $pdf->Cell(0, 0, $result['cod_cclo'], 0);

                    $pdf->SetY($y_ + 100);
                    $pdf->SetX($x_ + 500);
                    $pdf->Cell(0, 0, $result['uso'], 0);

                    $estrato = ($result['estrato'] == 0) ? "1" : $result['estrato'];
                    $pdf->SetY($y_ + 100);
                    $pdf->SetX($x_ + 370);
                    $pdf->Cell(0, 0, $estrato, 0);

                    $pdf->SetY($y_ + 128);
                    $pdf->SetX($x_ + 490);
                    $rutareparto = ($result['rutareparto'] == "") ? "SIN RUTA" : $result['rutareparto'];
                    $pdf->Cell(0, 0, $rutareparto, 0);

                    $sql_conv = "SELECT cc.cod_conv,ct.valor_capital,ct.valor_interes,ct.id_detalle,
                    cc.cant_cuotas, ct.ccobro
                    from convenio_cab cc, convenio_cuota ct
                    WHERE cc.cod_conv=ct.cod_conv and cc.cod_empr=ct.cod_empr and cc.cod_munip=ct.cod_munip
                    AND cc.cod_pred=" . $result['cod_pred'] . " and cc.cod_munip=" . $municipio . " 
                    and ct.cod_peri_apli=" . $result['cod_peri'] . " ";
                    $exSql_conv = $conexion->prepare($sql_conv);
                    $exSql_conv->execute();
                    $data_conv = $exSql_conv->fetch();
                    $codconv = $data_conv['cod_conv'];

                    if ($codconv > 0) {
                        $txt_web = "DUPLICADO - EN CONVENIO";
                        $tm = 40;
                        $px = 10;
                        $py = 50;
                    } else {
                        $txt_web = "DUPLICADO";
                        $tm = 50;
                        $px = 150;
                        $py = 5;
                    }

                    /*Fondo Duplicado*/
                    $pdf->SetFont('helvetica', '', $tm);
                    $pdf->SetDrawColor(200);
                    $pdf->SetTextColor(200);
                    //MARCA DE AGUA

                    $pdf->StartTransform();

                    $pdf->Rotate(40);

                    $pdf->Text($px, $py, $txt_web);
                    // Stop Transformation
                    $pdf->StopTransform();

                    $pdf->SetTextColor(0, 0, 0);
                    // DATOS DE MEDIDOR 
                    $pdf->SetFont($data_font_type, '', 6);
                    $marca = ($result['marca'] == "") ? "S/M" : $result['marca'];
                    //$pdf->SetY($y_+155);$pdf->SetX($x_+335);
                    //$pdf->Cell(0,0,$marca,0);

                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 405);
                    $serial_medidor = ($result['serialmedi'] == "") ? "S/S" : $result['serialmedi'];
                    $pdf->Cell(0, 0, $serial_medidor, 0);

                    $explode_calibre  = $result['calibre'];
                    $array_calibre = explode(" ", $explode_calibre);

                    $pdf->SetY($y_ + 155);
                    $pdf->SetX($x_ + 535);
                    $pdf->Cell(0, 0, $array_calibre[0], 0); // string1

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 300);
                    $pdf->SetX($x_ + 390);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fec_lectura_ant']) . " al " . cambiofechaCeibas($result['feclectura']), 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 183);
                    $pdf->SetX($x_ + 411);
                    $pdf->Cell(0, 0, $result['lectura'], 0);

                    $pdf->SetY($y_ + 192);
                    $pdf->SetX($x_ + 411);
                    $pdf->Cell(0, 0, $result['lectura_anterior'], 0);

                    $pdf->SetY($y_ + 200);
                    $pdf->SetX($x_ + 415);
                    $pdf->Cell(0, 0, abs($result['lectura'] - $result['lectura_anterior']), 0);

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 205);
                    $pdf->SetX($x_ + 546);
                    $pdf->Cell(0, 0, number_format($result['consumo'], 0, ',', '.'), 0, 0, 'L', 0);

                    $pdf->SetY($y_ + 250);
                    $pdf->SetX($x_ + 525);
                    $pdf->Cell(0, 0, number_format($result['promedio'], 0, ',', '.'), 0, 0, 'L', 0);

                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 287);
                    $pdf->SetX($x_ + 496);
                    $pdf->Cell(0, 0, $result['dias_facturados'], 0);

                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + 216);
                    $pdf->SetX($x_ + 415);
                    $pdf->Cell(0, 0, $result['obs_lectura'], 0);

                    // DATOS DEUDA
                    $pdf->SetFont($data_font_type, '', 9);
                    $pdf->SetY($y_ + 288);
                    $pdf->SetX($x_ + 340);
                    $pdf->Cell(0, 0, $result['cod_peri'], 0);

                    $pdf->SetFont($data_font_type, 'B', 9);
                    $pdf->SetY($y_ + 251);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0);

                    $fecvto = $result['fecvto'];
                    $fecsus = $result['fec_susp'];

                    $pdf->SetY($y_ + 263);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($fecvto), 0); // FECHA PAGO OPORTUNO

                    $pdf->SetY($y_ + 299);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($result['fch_ult_pago']), 0);

                    $pdf->SetY($y_ + 287);
                    $pdf->SetX($x_ + 229);
                    $pdf->Cell(0, 0, '$ ' . number_format($result['valor_ult_pago'], 0, ',', '.'), 0); //Valor ultimo pago

                    $pdf->SetY($y_ + 274);
                    $pdf->SetX($x_ + 220);
                    $pdf->Cell(0, 0, cambiofechaCeibas($fecsus), 0); //fecha de suspension

                    $pdf->SetY($y_ + 40);
                    $pdf->SetX($x_ + 114);
                    $pdf->Cell(0, 0, $result['nropermora'] + 1, 0); //Numero de Facturas

                    $sql = "SELECT (fpc.deuda_e1+fpc.deuda_e2+fpc.deuda_e3) As facturado
                            FROM public.factura_per_cab As fpc
                            WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                    fpc.cod_peri = " . $cod_peri . " And 
                                    fpc.cod_munip = " . $municipio . " And
                                    fpc.cod_empr = '" . $empresa . "';";
                    $rfact = $conexion->prepare($sql);
                    $rfact->execute();
                    $data_fact = $rfact->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                    $facturado = $data_fact[0];

                    $sql = "SELECT fpc.deuda_e1, coalesce(fpc.nro_cuenta_cobro,0) as nro_cuenta_cobro
                            FROM public.factura_per_cab As fpc
                            WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                    fpc.cod_peri = " . $cod_peri . " And 
                                    fpc.cod_munip = " . $municipio . " And
                                    fpc.cod_empr = '" . $empresa . "';";
                    $rpago = $conexion->prepare($sql);
                    $rpago->execute();
                    $data_pago = $rpago->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                    $deuda = $data_pago[0];
                    $data_pago2 = $rpago->fetch();
                    $nro_cuenta_cobro = $data_pago2['nro_cuenta_cobro'];

                    $total_pagar = $deuda_ess + $deuda;

                    if ($nro_cuenta_cobro == 0) {
                        $factura_no_cuenta_cobro = $factura_no;
                        $prefijo = "";
                    } else {
                        $factura_no_cuenta_cobro = "5" . str_pad($nro_cuenta_cobro, 9, '0', STR_PAD_LEFT);
                    }

                    //Cambiar Manera de traer el total

                    $detalleFacturado1 = "SELECT * FROM factura_det_aseo 
                    WHERE cod_pred = '" . $cod_pred . "' 
                    AND cod_peri = '" . $cod_peri . "'
                    AND cod_munip = '" . $municipio . "' 
                    AND cod_empr = '" . $empresa . "'";
                    $query_facturado1 = $conexion->prepare($detalleFacturado1);
                    $query_facturado1->execute();
                    $total_concp_aseo_spoll1 = 0;
                    while ($rowFacturado1 = $query_facturado1->fetch(PDO::FETCH_ASSOC)) {
                        $total_concp_aseo_spoll1 += $rowFacturado1['total_concp'];
                    }

                    $totalFacturaCompleta1 = $facturado; // + $total_concp_aseo_spoll1;
                    //FIN

                    $pdf->SetFont($data_font_type, 'B', 12);
                    $pdf->SetY($y_ + 225);
                    $pdf->SetX($x_ + 221);
                    $pdf->Cell(0, 0, '$ ' . number_format($totalFacturaCompleta1, 0, ',', '.'), 0, 0, 'L', 0); //deuda total mes seleccionado


                    // TOTAL POR CONCEPTO
                    $sql = "SELECT * FROM factura_per_det
                            WHERE cod_pred=" . $result['cod_pred'] . " and
                                cod_peri=" . $cod_peri . " and 
                                cod_empr='" . $empresa . "' and 
                                cod_munip=" . $municipio . " and
                                cod_conc not in (select cod_conc from concepto where cod_serv=3)
                                order by cod_conc ";

                    $exe_detalle = $conexion->prepare($sql);

                    $total_concepto_acueducto       = 0;
                    $total_concepto_alcantarillado  = 0;
                    $total_otros_conceptos          = 0;
                    // $total_veolia                   = 0;
                    $total_pag                      = 0;
                    $exe_detalle->execute();
                    while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {
                        //--sumatoria para el resumen
                        if ($result_det['cod_conc'] == $acueducto) { // ACUEDUCTO
                            $total_concepto_acueducto += $result_det['total_concp'];
                        }
                        if ($result_det['cod_conc'] == $alcantarillado) { // ALCANTARILLADO
                            $total_concepto_alcantarillado += $result_det['total_concp'];
                        }

                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) { // and ($result_det['cod_conc']!=100)){ // OTROS
                            $total_otros_conceptos += $result_det['total_concp'];
                        }
                    }

                    // GRAFICA - HISTORICO DE CONSUMO
                    GraficaHistoricaCeibas($conexion, $pdf, $result['cod_pred'], $result['peri_ant'], $result['cod_cclo'], $municipio, $empresa);

                    $style = array(
                        'position' => '',
                        'align' => 'L',
                        'stretch' => false,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'hpadding' => 'auto',
                        'vpadding' => 'auto',
                        'fgcolor' => array(0, 0, 0),
                        'bgcolor' => false, //array(255,255,255),
                        'text' => false,
                        'font' => 'helvetica',
                        'fontsize' => 8,
                        'stretchtext' => 4
                    );

                    //DETALLE

                    $total_concepto_acueducto       = 0;
                    $total_concepto_alcantarillado  = 0;
                    $total_otros_conceptos          = 0;
                    $total_otros                    = 0;
                    $tc1 = 0;
                    $tc2 = 0;
                    $tc3 = 0;
                    $tca1 = 0;
                    $tca2 = 0;
                    $tca3 = 0;
                    $secy = 0;
                    $unidades = "";

                    // DETALLES CONCEPTOS DE LA FACTURA
                    $sql = "SELECT * FROM factura_per_det
                            WHERE cod_pred=" . $result['cod_pred'] . " and
                                cod_peri=" . $cod_peri . " and 
                                cod_empr='" . $empresa . "' and 
                                cod_munip=" . $municipio . " and
                                cod_conc not in (select cod_conc from concepto where cod_serv=3)
                                order by cod_conc ";

                    $exe_detalle = $conexion->prepare($sql);
                    $exe_detalle->execute();

                    while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {

                        $suapoR1    = $result_det['subapo_r1'];
                        $suapoR2    = $result_det['subapo_r2'];
                        $suapoR3    = $result_det['subapo_r3'];
                        $suapoCF    = $result_det['subapo_cfijo'];
                        $tasaUso    = $result_det['tasa_ur'];

                        //--sumatoria para el resumen
                        if ($result_det['cod_conc'] == $acueducto) { //acueducto
                            $total_concepto_acueducto += $result_det['total_concp'];
                        }
                        if ($result_det['cod_conc'] == $alcantarillado) { //alcantarillado
                            $total_concepto_alcantarillado += $result_det['total_concp'];
                        }
                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                            $total_otros_conceptos += $result_det['total_concp'];
                        }

                        //--liquidacion Acueducto
                        if ($result_det['cod_conc'] == $acueducto) {
                            $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                            $tc1 = $result_det['canti_e1'] * $result_det['precioe1'];
                            $tc2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                            $tc3 = $result_det['canti_e3'] * $result_det['precioe3'];

                            //Tasas
                            $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                            $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                            $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                            $sqlacu = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                    fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                    from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                    where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                        fpc.cod_peri=" . $cod_peri . " and 
                                        fpc.cod_munip=" . $municipio . " and 
                                        fpc.cod_empr='" . $empresa . "' and
                                        fpd.cod_conc=" . $acueducto;

                            $sql_acu = $conexion->prepare($sqlacu) or die("Error: Detalle Acueducto \n" . $sqlacu);
                            $sql_acu->execute();
                            $data_acu = $sql_acu->fetch();

                            $cod_concep_acu = $data_acu['cod_conc']; //codigo cocepto acueducto
                            $uso_acu        = $data_acu['cod_uso']; //codigo cocepto acueducto
                            $cod_estado_acu = $data_acu['cod_estd']; //codigo estado acueducto
                            $fec_lec_acu    = $data_acu['feclectura']; //codigo estado acueducto
                            $fec_lec_ant_acu = $data_acu['fec_lectura_ant']; //codigo estado acueducto
                            $cod_tari_acu   = $data_acu['cod_tarf_ref']; //codigo tarifa acueducto
                            $cod_uso_acu    = $data_acu['cod_uso_ref']; //codigo uso acueducto
                            $codtarf        = $data_acu['cod_tarf'];
                            //--calcular tarifas
                            $sq_cal_tari_acu1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_acu', $cod_tari_acu, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_acu1 = $conexion->prepare($sq_cal_tari_acu1);
                            $sql_cal_tari_acu1->execute();
                            $data_tari = $sql_cal_tari_acu1->fetch();
                            $cod_cal_tari_acu = $data_tari['fact_calcular_tarifa'];

                            $sq_cal_tari_acu2 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$uso_acu', $codtarf, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_acu2 = $conexion->prepare($sq_cal_tari_acu2);
                            $sql_cal_tari_acu2->execute();
                            $data_tari2 = $sql_cal_tari_acu2->fetch();
                            $cod_cal_tari_acu2 = $data_tari2['fact_calcular_tarifa'];
                            $resto2 = str_replace("{", " ", $cod_cal_tari_acu2);
                            $cal_tari_acu2 = explode(",", $resto2);

                            $resto = str_replace("{", " ", $cod_cal_tari_acu);
                            $cal_tari_acu = explode(",", $resto); //si fecha esta en formato dia-mes-año 

                            $consu_basic1   = $cal_tari_acu[1];
                            $consu_comple1  = $cal_tari_acu[2];
                            $consu_sunt1    = $cal_tari_acu[3];
                            $cargo_fijo1    = $cal_tari_acu[0];
                            $porcen         = $cal_tari_acu2[5];

                            if (!isset($subapo_t1)) {
                                $subapo_t1 = 0;
                            }
                            if (!isset($subapo_t2)) {
                                $subapo_t2 = 0;
                            }
                            if (!isset($subapo_t3)) {
                                $subapo_t3 = 0;
                            }

                            //Aqui debo tomar el dato de tasa 
                            $ta_r1 = $cal_tari_acu[14];
                            $ta_r2 = $cal_tari_acu[15];
                            $ta_r3 = $cal_tari_acu[16];

                            $sum_sub = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF;
                            $sum_sub_tasa = $subapo_t1 + $subapo_t2 + $subapo_t3;

                            $valortotal_acueducto = $result_det['total_concp'];

                            //--Descripcion
                            $pdf->SetFont($data_font_type, '', 8);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 28);
                            $pdf->Cell(0, 0, "Cargo Fijo", 0);
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 28);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, "Consumo Básico", 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 28);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, "Consumo Complementario", 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 28);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, "Consumo Suntuario", 0); //}
                            $pdf->SetY($y_ + 415);
                            $pdf->SetX($x_ + 28);
                            $pdf->Cell(0, 0, "Tasa Uso", 0);

                            //--acueducto consumo
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 160);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 160);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 160);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 415);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0);

                            //--acueducto tarifa referencia
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 179);
                            $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo1, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 179);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_basic1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 179);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_comple1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 179);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 415);
                            $pdf->SetX($x_ + 179);
                            $pdf->Cell(0, 0, number_format($ta_r1, 1, ',', '.'), 0);


                            //valor total
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_fija'] * $cargo_fijo1), 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 215);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] * $consu_basic1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 215);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e2'] * $consu_comple1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 215);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e3'] * $consu_sunt1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 415);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, number_format((($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) * $ta_r1), 1, ',', '.'), 0);

                            //--acueducto valor
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 250);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 250);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 250);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 415);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, number_format($sum_sub_tasa, 1, ',', '.'), 0);


                            //--acueducto valor a pagar
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 375);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 385);
                            $pdf->SetX($x_ + 283);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 395);
                            $pdf->SetX($x_ + 283);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 405);
                            $pdf->SetX($x_ + 283);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 415);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, number_format(($ta_u1 + $ta_u2 + $ta_u3), 1, ',', '.'), 0);

                            //--Total concepto acueducto
                            $pdf->SetFont($data_font_type, 'B', 6);
                            $pdf->SetY($y_ + 440);
                            $pdf->SetX($x_ + 284);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        }

                        //--Liquidacion Alcantarillado
                        if ($result_det['cod_conc'] == $alcantarillado) {
                            $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                            $tca1 = $result_det['canti_e1'] * $result_det['precioe1'];
                            $tca2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                            $tca3 = $result_det['canti_e3'] * $result_det['precioe3'];

                            //Tasas
                            $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                            $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                            $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                            $sqlalc = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                        fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                        from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                        where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                            fpc.cod_peri=" . $cod_peri . " and 
                                            fpc.cod_munip=" . $municipio . " and 
                                            fpc.cod_empr='" . $empresa . "' and
                                            fpd.cod_conc=" . $alcantarillado;
                            $sql_alc = $conexion->prepare($sqlalc) or die("Error: Detalle Alcantarillado \n" . $sqlalc);
                            $sql_alc->execute();
                            $data_alc = $sql_alc->fetch();

                            $cod_concep_alc = $data_alc['cod_conc'];
                            $uso_alc        = $data_alc['cod_uso'];
                            $codtarf        = $data_alc['cod_tarf'];
                            $cod_estado_alc = $data_alc['cod_estd'];
                            $fec_lec_alc    = $data_alc['feclectura'];
                            $fec_lec_ant_alc = $data_alc['fec_lectura_ant'];
                            $cod_tari_alc   = $data_alc['cod_tarf_ref'];
                            $cod_uso_alc    = $data_alc['cod_uso_ref'];

                            $sq_cal_tari_alc1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_alc', $cod_tari_alc, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_alc1 = $conexion->prepare($sq_cal_tari_alc1);
                            $sql_cal_tari_alc1->execute();
                            $data_tari = $sql_cal_tari_alc1->fetch();

                            $cod_cal_tari_alc = $data_tari['fact_calcular_tarifa'];
                            $resto1 = str_replace("{", " ", $cod_cal_tari_alc);
                            $cal_tari = explode(",", $resto1); //si fecha esta en formato dia-mes-año 

                            $sq_cal_tari_alca2 = "SELECT fact_calcular_tarifa('" . $empresa . "'," . $municipio . ", '$uso_alc', $codtarf, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                            $sql_cal_tari_alca2 = $conexion->prepare($sq_cal_tari_alca2);
                            $sql_cal_tari_alca2->execute();
                            $data_tari2 = $sql_cal_tari_alca2->fetch();

                            $cod_cal_tari_alca2 = $data_tari2['fact_calcular_tarifa'];
                            $resto2 = str_replace("{", " ", $cod_cal_tari_alca2);
                            $cal_tari_alca2 = explode(",", $resto2);

                            $consu_basic1    = $cal_tari[1];
                            $consu_comple1   = $cal_tari[2];
                            $consu_sunt1     = $cal_tari[3];
                            $cargo_fijo1     = $cal_tari[0];
                            $porcen         = $cal_tari_alca2[5];

                            //Aqui debo tomar el dato de tasa 
                            $ta_r1_alc = $cal_tari[14];
                            $ta_r2_alc = $cal_tari[15];
                            $ta_r3_alc = $cal_tari[16];

                            $ta_r1 = $cal_tari[14];
                            $ta_r2 = $cal_tari[15];
                            $ta_r3 = $cal_tari[16];

                            // $valortotal_alcantarillado = $result_det['total_concp'];
                            $sum_sub = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF;
                            //$sum_sub_tasa = $subapo_t1 + $subapo_t2 + $subapo_t3;
                            $sum_sub_tasa = 0;
                            $valortotal_acueducto = $result_det['total_concp'];

                            //--Descripcion
                            $pdf->SetFont($data_font_type, '', 8);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Cargo Fijo", 0);
                            $pdf->SetY($y_ + 466);
                            $pdf->SetX($x_ + 37);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, "Consumo Básico", 0); //}
                            $pdf->SetY($y_ + 477);
                            $pdf->SetX($x_ + 37);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, "Consumo Complementario", 0); //}
                            $pdf->SetY($y_ + 488);
                            $pdf->SetX($x_ + 37);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, "Consumo Suntuario", 0); //}
                            $pdf->SetY($y_ + 599);
                            $pdf->SetX($x_ + 37);
                            $pdf->Cell(0, 0, "Tasa Uso", 0);

                            //--acueducto consumo
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 466);
                            $pdf->SetX($x_ + 160);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 477);
                            $pdf->SetX($x_ + 160);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 488);
                            $pdf->SetX($x_ + 160);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 499);
                            $pdf->SetX($x_ + 160);
                            $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0);

                            //--acueducto tarifa referencia
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 182);
                            $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo1, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 466);
                            $pdf->SetX($x_ + 182);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_basic1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 477);
                            $pdf->SetX($x_ + 182);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_comple1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 488);
                            $pdf->SetX($x_ + 182);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 499);
                            $pdf->SetX($x_ + 182);
                            $pdf->Cell(0, 0, number_format($ta_r1, 1, ',', '.'), 0);


                            //valor total
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_fija'] * $cargo_fijo1), 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 466);
                            $pdf->SetX($x_ + 215);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] * $consu_basic1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 477);
                            $pdf->SetX($x_ + 215);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e2'] * $consu_comple1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 488);
                            $pdf->SetX($x_ + 215);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e3'] * $consu_sunt1), 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 499);
                            $pdf->SetX($x_ + 215);
                            $pdf->Cell(0, 0, number_format((($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) * $ta_r1), 1, ',', '.'), 0);

                            //--acueducto valor
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 466);
                            $pdf->SetX($x_ + 250);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 477);
                            $pdf->SetX($x_ + 250);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 488);
                            $pdf->SetX($x_ + 250);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 499);
                            $pdf->SetX($x_ + 250);
                            $pdf->Cell(0, 0, number_format($sum_sub_tasa, 1, ',', '.'), 0);


                            //--acueducto valor a pagar
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + 455);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                            $pdf->SetY($y_ + 466);
                            $pdf->SetX($x_ + 283);
                            //if ($tc1 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc1, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 477);
                            $pdf->SetX($x_ + 283);
                            //if ($tc2 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc2, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 488);
                            $pdf->SetX($x_ + 283);
                            //if ($tc3 != 0){
                            $pdf->Cell(0, 0, '$ ' . number_format($tc3, 2, ',', '.'), 0); //}
                            $pdf->SetY($y_ + 499);
                            $pdf->SetX($x_ + 283);
                            $pdf->Cell(0, 0, number_format(($ta_u1 + $ta_u2 + $ta_u3), 1, ',', '.'), 0);

                            //--Total concepto acueducto
                            $pdf->SetFont($data_font_type, 'B', 6);
                            $pdf->SetY($y_ + 520);
                            $pdf->SetX($x_ + 281);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        }

                        //--Liquidacion Otros conceptos
                        if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                            $pdf->SetFont($data_font_type, '', 6);
                            $pdf->SetY($y_ + $secy + 357);
                            $pdf->SetX($x_ + 330);
                            $pdf->Cell(0, 0, $result_det['descripcion'], 0);

                            $pdf->SetFont($data_font_type, '', 7);
                            $pdf->SetY($y_ + $secy + 357);
                            $pdf->SetX($x_ + 420);
                            $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                            $total_otros += $result_det['total_concp'];
                            $secy += 10;
                        }
                    }
                    if (((($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total'])) > 0) {
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + $secy + 357);
                        $pdf->SetX($x_ + 330);
                        $pdf->Cell(0, 0, "DEUDAS ANTERIORES", 0);

                        $pdf->SetFont($data_font_type, '', 7);
                        $pdf->SetY($y_ + $secy + 357);
                        $pdf->SetX($x_ + 420);
                        $pdf->Cell(0, 0, '$ ' . number_format((($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total']), 2, ',', '.'), 0);
                        $total_otros += (($result['deuda_e1'] + $result['deuda_e2'] + $result['deuda_e3']) - $result['total']);
                        $secy += 10;
                    }
                    $pdf->SetFont($data_font_type, 'B', 7);
                    $pdf->SetY($y_ + 440);
                    $pdf->SetX($x_ + 529);
                    $pdf->Cell(0, 0, '$ ' . number_format($total_otros, 2, ',', '.'), 0);
                    $pdf->SetFont($data_font_type, '', 7);

                    //TOTAL 1 + 2 + 3
                    $pdf->SetFont($data_font_type, 'B', 8);
                    //$pdf->SetY($y_+589);$pdf->SetX($x_+308);
                    $pdf->SetY($y_ + 534);
                    $pdf->SetX($x_ + 300);
                    //$pdf->Cell(0,0,'$ '.number_format(($total_concepto_acueducto+$total_concepto_alcantarillado+$total_otros_conceptos),0,',','.'),0,0,'L',0);//deuda total mes seleccionado
                    $pdf->Cell(0, 0, '$ ' . number_format(($total_concepto_acueducto + $total_concepto_alcantarillado + $total_otros), 0, ',', '.'), 0, 0, 'L', 0);
                }

                $n_fac_aseo = "";

                //INICIAMOS HOJA 2
                $pdf->setPrintHeader(false);
                $pdf->AddPage();
                // -- set new background ---
                // get the current page break margin
                $bMargin = $pdf->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $pdf->getAutoPageBreak();
                // disable auto-page-break
                $pdf->SetAutoPageBreak(false, 0);
                // set bacground image
                $img_file = '../assets/images/FACTURA_LAS_CEIBAS_ENERO_2021.jpg';
                $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                // restore auto-page-break status
                $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $pdf->setPageMark();

                $pdf->SetFont($data_font_type, '', 11);
                $pdf->SetY($y_ + 600);
                $pdf->SetX($x_ + 270);
                $pdf->Cell(0, 0, $n_fac_aseo, 0);

                $pdf->SetY($y_ + 600);
                $pdf->SetX($x_ + 480);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetY($y_ + 635);
                $pdf->SetX($x_ + 202);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fec_lectura_ant']) . " al " . cambiofechaCeibas($result['feclectura']), 0); //Periodo Facturado

                $totalFacturaCompleta = $facturado; // + $total_concp_aseo;

                $pdf->SetFont($data_font_type, 'B', 12);
                $pdf->SetY($y_ + 640);
                $pdf->SetX($x_ + 497);
                $pdf->Cell(0, 0, "$" . number_format($totalFacturaCompleta), 0); // Total Factura Completa 
                $pdf->SetFont($data_font_type, '');

                $pdf->SetY($y_ + 737);
                $pdf->SetX($x_ + 483);
                $pdf->Cell(0, 0, cambiofechaCeibas($result['fecexp']), 0); //Fecha de emision

                // TOTAL A PAGAR DEL MES Codigo de barras

                //$barcode= chr(241)."415".$recuaudo_corriente."8020".str_pad("47580881",10,"0",STR_PAD_LEFT).chr(241)."3900".str_pad(number_format($totalFacturaCompleta,0,'',''),10,"0",STR_PAD_LEFT).chr(241)."96"."20200620";
                $barcode = chr(241) . "415" . $recuaudo_corriente . "8020" . str_pad($factura_no_cuenta_cobro, 14, "0", STR_PAD_LEFT) . chr(241) . "3900" . str_pad(number_format($totalFacturaCompleta, 0, '', ''), 10, "0", STR_PAD_LEFT) . chr(241) . "96" . str_replace("-", "", $result['fecvto']);

                //$textBarcode = "(415)".$recuaudo_corriente."(8020)".str_pad("47580881",10,"0",STR_PAD_LEFT)."(3900)".str_pad(number_format($totalFacturaCompleta,0,'',''),10,"0",STR_PAD_LEFT)."(96)"."20200620";
                $textBarcode = "(415)" . $recuaudo_corriente . "(8020)" . str_pad($factura_no_cuenta_cobro, 14, "0", STR_PAD_LEFT) . "(3900)" . str_pad(number_format($totalFacturaCompleta, 0, '', ''), 10, "0", STR_PAD_LEFT) . "(96)" . str_replace("-", "", $result['fecvto']);

                $pdf->write1DBarcode($barcode, 'C128', $x_ + 30, $y_ + 680, 320, 30, 1, $style, 'N');
                $pdf->SetFontSize('6');

                $pdf->Text($x_ + 92, $y_ + 710, $textBarcode);
            }
        }
    }
    //Close and output PDF document
    $pqr_ruta = 'ceibas/tmp/' . $pqr;
    if ($tipoPQR == 'P') { // En caso la PQR sea padre, debe ser la misma carpeta de archivos de la PQR hija
        $pqr_ruta = 'ceibas/tmp/' . $_GET['pqr'];
    }

    if (!file_exists($pqr_ruta)) {
        mkdir($pqr_ruta, 0777, true);
    }
    $filename = $pqr_ruta . "/" . $nro_factura . "-" . $pqr . ".pdf";
    $pdf->Output($filename, 'F', true);
}

function GraficaHistoricaCeibas($conexion, $pdf, $predio, $_periodo, $ciclo, $municipio, $empresa)
{
    $l          = 1;
    $largomin   = 3;
    $dra        = 0.5;
    $conta      = 0;
    $largo_1    = 0;
    $historico  = 55;
    $cont = -8;
    $ancho      = 15;
    $py         = 270;
    $py1        = 399;
    $mesnum     = date('m');
    $calmes     = $mesnum - 5;
    $query_historico = $conexion->prepare("select * from historico(" . $predio . ",'" . $empresa . "'," . $municipio . "," . $_periodo . ",'" . $ciclo . "')");

    $query_max_hist = $conexion->prepare("select max(consumo) AS maxcon from historico(" . $predio . ",'" . $empresa . "'," . $municipio . "," . $_periodo . ",'" . $ciclo . "')");
    $query_max_hist->execute();
    $data_max = $query_max_hist->fetch();
    $max_con = $data_max['maxcon'];

    $rs_max_peri_con = $conexion->prepare("select max(periodo) AS maxperi from historico(" . $predio . ",'" . $empresa . "'," . $municipio . "," . $_periodo . ",'" . $ciclo . "')");
    $rs_max_peri_con->execute();
    $data_max_peri = $rs_max_peri_con->fetch();
    $max_peri = $data_max_peri['maxperi'];

    $query_historico->execute();
    //$num= $query_historico->fetch(PDO::FETCH_NUM);
    while ($result_h = $query_historico->fetch(PDO::FETCH_ASSOC)) {
        $conta = $conta + 1;
        $periodos = $result_h['periodo'];
        if ($max_con == null or $max_con == 0) {
            $largo_1 = 0;
        } else {
            if ($result_h['consumo'] == 0) {
                $consumo = 1;
            } else {
                $consumo = $result_h['consumo'];
            }
            $max_pro = $max_con;
            $por = ($consumo / $max_pro) * 45;
            $por1 = ((int)$por);
            $largo_1 = $largomin + ($por1 * $dra);
        }
        if ($periodos == $max_peri) {
            $pdf->SetFillColor(112, 128, 144);
            $pdf->SetDrawColor(0, 0, 0);
        } else {
            $pdf->SetFillColor(220, 220, 220);
            $pdf->SetDrawColor(0, 0, 0);
        }
        $pdf->SetY($py - $largo_1);
        $pdf->SetX($historico + 270); //barra
        $pdf->Cell($ancho, $largo_1, ' ', 0, 0, 'L', 1); //consumo
        $pdf->SetFontSize('6');
        $pdf->SetY(236);
        $pdf->SetX($historico + 271);
        $pdf->Cell($ancho, 7, number_format($result_h['consumo'], 0, ',', '.'), 0, 1, 'L', 0); //consumo 
        // $pdf->SetY(505);$pdf->SetX($cont+$historico+6);
        //--consumo cuadros
        // $pdf->Cell($ancho,7,number_format($periodos,0,',','.'),0,1,'L',0); //consumo 

        $sql = "select cod_peri,descripcion
                from periodo_fac
                where
                cod_empr='" . $empresa . "' and 
                cod_munip='" . $municipio . "' and
                cod_peri='$periodos'";

        $query_peri = $conexion->prepare($sql);
        $query_peri->execute();
        while ($result_p = $query_peri->fetch(PDO::FETCH_ASSOC)) {
            $pdf->SetFontSize('6');
            $nommes = MesCeibas(substr($result_p['cod_peri'], -2)); //echo $result_p['cod_peri']." - ";
            $pdf->SetY(270);
            $pdf->SetX($historico + 269);
            $pdf->Cell(20, 7, $nommes, 0, 1, 'L', 0); //nombre mes
            $calmes = $calmes + 1;
        }
        //$pdf->SetFontSize('8');
        $historico = $historico +  25;

        /*if ($periodos!=$max_peri){
            $pro = $pro + $result_h['consumo'];							
        }*/
    } //fin historico       
}

function MesCeibas($mes_numero)
{
    switch ($mes_numero) {
        case '01':
            $mes = "ENE";
            break;
        case '02':
            $mes = "FEB";
            break;
        case '03':
            $mes = "MAR";
            break;
        case '04':
            $mes = "ABR";
            break;
        case '05':
            $mes = "MAY";
            break;
        case '06':
            $mes = "JUN";
            break;
        case '07':
            $mes = "JUL";
            break;
        case '08':
            $mes = "AGO";
            break;
        case '09':
            $mes = "SEP";
            break;
        case '10':
            $mes = "OCT";
            break;
        case '11':
            $mes = "NOV";
            break;
        case '12':
            $mes = "DIC";
            break;
    }
    return $mes;
}

function cambiofechaCeibas($fecha)
{

    $mess = substr($fecha, 5, 2);
    $diaa   = substr($fecha, 8, 9);
    $anoo = substr($fecha, 0, 4);

    switch ($mess) {

        case 1:
            $mess = "Ene";
            break;

        case 2:
            $mess = "Feb";
            break;

        case 3:
            $mess = "Mar";
            break;

        case 4:
            $mess = "Abr";
            break;

        case 5:
            $mess = "May";
            break;

        case 6:
            $mess = "Jun";
            break;

        case 7:
            $mess = "Jul";
            break;

        case 8:
            $mess = "Ago";
            break;

        case 9:
            $mess = "Sep";
            break;

        case 10:
            $mess = "Oct";
            break;

        case 11:
            $mess = "Nov";
            break;

        case 12:
            $mess = "Dic";
            break;
    }
    $fecha = $diaa . '-' . $mess . '-' . $anoo;
    return $fecha;
}
