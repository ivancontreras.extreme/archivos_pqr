<?php
ini_set("display_errors", 1);
header("Content-type: application/json;charset=utf-8");
require 'vendor/autoload.php';

use Aws\S3\S3Client;
// Instantiate an Amazon S3 client.
$s3Client = new S3Client([
    'version' => 'latest',
    'region'  => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAIPHQTEVTQFQKPNNA',
        'secret' => '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'
    ]
]);
require 'assets/database/conexion.php';
require 'assets/files/S3Files.php';
require 'assets/library/PDF/fpdf.php';
require 'assets/library/PDF/fpdf_merge.php';
require 'assets/library/PDF/fpdi.php';
require_once('assets/library/config/tcpdf_config_alt.php');
require_once('assets/library/config/lang/eng.php');
require_once('assets/library/tcpdf_nueva/tcpdf.php');
require_once('assets/library/tcpdf_nueva/tcpdf_barcodes_1d.php');
require 'essmar/constancia.php';
require 'essmar/duplicado.php';
require 'ceibas/constancia.php';
require 'ceibas/duplicado.php';
require 'functions/archivos.php';


$output = (object) array();
if ($_SERVER['SERVER_PORT']) {
    $ruta = "http://" . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . "/archivos_pqr";
} else {
    $ruta = "http://" . $_SERVER['SERVER_NAME'] . "/archivos_pqr";
}
if (isset($_GET['empresa']) && !empty($_GET['empresa'])) {
    if ($_GET['empresa'] == 'ceibas' || $_GET['empresa'] == 'essmar') {

        // válidacion para PQR
        $conexion = conectar($_GET['empresa']);
        $sql = "SELECT cod_pqr , cod_pqr_padre FROM pqr WHERE cod_pqr=" . $_GET['pqr'];
        $query = $conexion->prepare($sql);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            $output->status = "406"; // Not Acceptable
            $output->message = "No se encontró la PQR (" . $result . ") ";
            $output->sql = $sql;
        } else {
            if ($_GET['empresa'] == 'essmar') {
                // Creación de constancia PQR ( Hija - Padre )
                constanciaEssmar($conexion, $result['cod_pqr']);
                constanciaEssmar($conexion, $result['cod_pqr_padre'], 'P');

                $array_files = array('Constancia' . $result['cod_pqr_padre'] . '.pdf');
                // Archivos para la PQR padre
                $array_files = generar_archivos($conexion, $result['cod_pqr_padre'], $result['cod_pqr'], $array_files, 'E');

                // Archivos para la PQR hija

                // inicio union de archivos
                $pdf = new FPDI();
                for ($i = 0; $i < count($array_files); $i++) {
                    $pageCount = $pdf->setSourceFile('essmar/tmp/' . $_GET['pqr'] . '/' . $array_files[$i]);
                    for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                        $tpl = $pdf->importPage($pageNo, '/MediaBox');
                        $pdf->addPage();
                        $pdf->useTemplate($tpl);
                    }
                }
                $file_name = 'Expediente_' . $_GET["pqr"] . '.pdf';
                $pdf->Output('essmar/tmp/' . $_GET["pqr"] . "/" . $file_name, 'F', true);
                //file_put_contents("essmar/tmp/" . $_GET['pqr'] . "/" . $file_name, fopen($pdf->Output('', 'F'), 'r'));

                $urlFile = dirname(__FILE__) . '/essmar/tmp/' . $_GET["pqr"] . '/Expediente_' . $_GET["pqr"] . '.pdf';
                $folder = "kagua/essmar/predio/cartas/" . $_GET['pqr'];
                $key = "kagua/essmar/predio/cartas/" .  $_GET['pqr'] . "/Expediente_" . $_GET['pqr'] . ".pdf";
                $key = uploadReport(str_replace('\\', "/", $urlFile), $file_name, $folder, $s3Client);
                $insertFile = "INSERT INTO pqr_archivos (cod_pqr, cod_munip, cod_empr, key, nombre, tipo, fecha) 
                                VALUES (" . $_GET['pqr'] . ", 4, '3', 'kagua/essmar/predio/cartas/" . $_GET['pqr'] . "/Expediente_" . $_GET['pqr'] . ".pdf', '$file_name', 'application/pdf', now())";
                $queryInsert = $conexion->prepare($insertFile);
                $queryInsert->execute();

                $update = "UPDATE pqr SET gen_exp = 'S' WHERE cod_pqr = " . $_GET['pqr'];
                $queryUpdate = $conexion->prepare($update);
                $queryUpdate->execute();
                rmDir_rf('essmar/tmp/' . $_GET["pqr"]);
                download_exp($urlFile);
            }

            if ($_GET['empresa'] == 'ceibas') {
                constanciaCeibas($conexion, $result['cod_pqr']);
                constanciaCeibas($conexion, $result['cod_pqr_padre'], 'P');

                $array_files = array('Constancia' . $result['cod_pqr_padre'] . '.pdf');
                // Archivos para la PQR padre
                $array_files = generar_archivos($conexion, $result['cod_pqr_padre'], $result['cod_pqr'], $array_files, 'C');

                // Archivos para la PQR hija

                // // inicio union de archivos
                $pdf = new FPDI();
                for ($i = 0; $i < count($array_files); $i++) {
                    $pageCount = $pdf->setSourceFile('ceibas/tmp/' . $_GET['pqr'] . '/' . $array_files[$i]);
                    for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                        $tpl = $pdf->importPage($pageNo, '/MediaBox');
                        $pdf->addPage();
                        $pdf->useTemplate($tpl);
                    }
                }
                $file_name = 'Expediente_' . $_GET["pqr"] . '.pdf';
                //$pdf->Output('ceibas/tmp/' . $_GET["pqr"] . "/" . $file_name, 'F', true);
                $urlFile = dirname(__FILE__) . '/ceibas/tmp/' . $_GET["pqr"] . '/Expediente_' . $_GET["pqr"] . '.pdf';
                $folder = "kagua/ceibas/predio/cartas/" . $_GET['pqr'];
                $key = uploadReport(str_replace('\\', "/", $urlFile), $file_name, $folder, $s3Client);
                $insertFile = "INSERT INTO pqr_archivos (cod_pqr, cod_munip, cod_empr, key, nombre, tipo, fecha) 
                                VALUES (" . $_GET['pqr'] . ", 1, '1', '$folder/Expediente_" . $_GET['pqr'] . ".pdf', '$file_name', 'application/pdf', now())";
                $queryInsert = $conexion->prepare($insertFile);
                $queryInsert->execute();

                $update = "UPDATE pqr SET gen_exp = 'S' WHERE cod_pqr = " . $_GET['pqr'];
                $queryUpdate = $conexion->prepare($update);
                $queryUpdate->execute();
                rmDir_rf('ceibas/tmp/' . $_GET["pqr"]);
                download_exp($urlFile);
            }
            $output->status = "200"; // success
            $output->message = "Datos encontrados";
            $output->data = $insertFile;
        }
    } else {
        $output->status = "406"; // Not Acceptable
        $output->message = "Por favor envíe una empresa válida.";
    }
} else {
    $output->status = "400"; //Bad Request
    $output->message = "Por favor envíe la empresa.";
}

echo json_encode($output);
