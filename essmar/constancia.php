<?php

function constanciaEssmar($conexion, $pqr, $tipoPQR = null) // tipo PQR = Padre - hija
{
    $cod_munip = 4;
    $cod_empr = 3;
    $sql = "SELECT pqr.cod_pqr as rad, pqr.cod_pred as matricula,(select (select c.descripcion from ciclo c
                where c.cod_cclo=p.cod_cclo and c.cod_munip=pqr.cod_munip) from predio p WHERE
                p.cod_pred=pqr.cod_pred and p.cod_munip=pqr.cod_munip ) as ciclo,
                (select p.direccion from predio p WHERE
                p.cod_pred=pqr.cod_pred and p.cod_munip=pqr.cod_munip ) as direccion,
                to_char(pqr.fec_soli, 'dd/mm/yyyy') as fecsoli,
                to_char (pqr.fecha_max_sol,'dd/mm/yyyy') as vencimiento ,pqr.dir_clte as direccion_soli,
                pqr.barrio_noti as barrio,pqr.ciudad_noti as ciudad, pqr.mail,
                pqr.tel_clte as telefono, pqr.nro_celular as celular, pqr.fax_clte as fax, predio.nombre as propietario, 
                predio.direccion,predio.telefono as protel,
                pqr.nro_docu as cedula_soli, pqr.descripcion as descripcion,
                pqr_tipo.descripcion as tipo, pqr.cod_mpqr, --pqr_motivo.descripcion as motivo,
                pqr.cod_gc_pqr,
                pqr.nom_clte as solicitante,cliente.nro_documento as cedula_pro, pqr.cod_pqr_padre,
                pqr.cod_tpqr, orden_trabajo.cod_otrb as ot, pqr.cod_usua,predio.rutareparto as ruta,
                (select pm.descripcion from pqr_medio_recepcion pm where pm.cod_mrec=pqr.cod_mrec and pm.cod_empr=pqr.cod_empr)
                as recepcion, pqr.med_noti,
                case when pqr.fec_regi>to_date('30/06/2016','dd/mm/yyyy') 
                        then 1 else 0 end as val
                from
                pqr
                        JOIN pqr_tipo   USING(cod_tpqr, cod_empr)
                        --JOIN pqr_motivo USING(cod_mpqr, cod_tpqr, cod_empr)
                        LEFT OUTER JOIN orden_trabajo 
                        USING (cod_pqr, cod_pred, cod_empr)
                        LEFT OUTER JOIN predio  USING (cod_pred, cod_empr)
                        JOIN cliente USING (cod_clte, cod_empr)
                    where
                        pqr.cod_pqr   = " . $pqr . "   and
                        pqr.cod_empr  = '" . $cod_empr . "' and
                        pqr.cod_munip = " . $cod_munip;

    $rs = $conexion->prepare($sql);
    $rs->execute();
    $result = $rs->fetch(PDO::FETCH_ASSOC);
    $radicado  = $result['rad'];
    $matricula = $result['matricula'];
    $fecsoli = $result['fecsoli'];
    $fecmaxsoli = $result['vencimiento'];
    $fecmaxsoli2 = $result['vencimiento'];
    $tipo = $result['tipo'];
    $solicitante = $result['solicitante'];
    $cedula_soli = $result['cedula_soli'];
    $propietario = $result['propietario'];
    $cedula_pro = $result['cedula_pro'];
    $direccion = $result['direccion_soli'];
    $direccion_pred = $result['direccion'];
    $telefono_pred = $result['protel'];
    $telefono = $result['telefono'];
    $descripcion = $result['descripcion'];
    $cod_mpqr = $result['cod_mpqr'];
    $cod_tipo = $result['cod_tpqr'];
    $cod_gc_pqr = $result['cod_gc_pqr'];
    $pqr_padre = $result['cod_pqr_padre'];
    $val_     = $result['val'];
    if ($val_ == 1) {
        if ($cod_tipo == '2') {
            $sqlmot = "select descripcion from pqr_motivo where cod_tpqr='" . $cod_tipo . "' and cod_mpqr='" . $cod_mpqr . "'";
        } else {
            $sqlmot = "select descripcion from pqr_causales_det pcd where pcd.cod_gc_pqr='" . $cod_gc_pqr . "' and pcd.cod_cdet_pqr='" . $cod_mpqr . "'";
        }
    } else {
        $sqlmot = "select descripcion from pqr_motivo where cod_tpqr='" . $cod_tipo . "' and cod_mpqr='" . $cod_mpqr . "'";
    }
    //$exec_mot = odbc_exec($conexion, $sqlmot);
    $exec_mot = $conexion->prepare($sqlmot);
    $exec_mot->execute();
    $result_mot = $exec_mot->fetch();

    $motivo = $result_mot['descripcion'];
    //$motivo = odbc_result($rs, 'motivo');
    $ot = $result['ot'];
    $cod_usua = $result['cod_usua'];
    $ciclo = $result['ciclo'];
    $mail = $result['mail'];
    $ciudad = $result['ciudad'];
    $barrio = $result['barrio'];
    $celular = $result['celular'];
    $fax = $result['fax'];
    $mrecep = $result['recepcion'];
    $rutar = $result['ruta'];
    $mednoti = $result['med_noti'];


    $desot = !empty($ot) ? 'CON ORDEN DE TRABAJO Nro :' . $ot : '';

    $sql = "select DISTINCT nro_factura from pqr_detalle
                    where cod_empr=$cod_empr and 
                    cod_munip=$cod_munip and cod_pqr=$_GET[pqr]";

    $rs = $conexion->prepare($sql);
    $rs->execute();
    $resultRS = $rs->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
    //$rs = odbc_exec($conexion, $sql);
    if ($resultRS) {
        $factura = $resultRS[1];
    } else {
        $factura = "N/A";
    }

    $ciclo_ = substr($ciclo, 5);

    $sql_usu = "select nombreper from usuarios where idusuario=$cod_usua ";
    $rs_usu = $conexion->prepare($sql_usu);
    $rs->execute();
    $result_usu = $rs->fetch();
    // $rs_usu = odbc_exec($conexion, $sql_usu);
    $nom_usu = $result_usu['nombreper'];
    $pdf = new FPDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('Arial', 'B', 11);

    $pdf->SetY(7);
    $pdf->SetX(10); //LOGO
    $pdf->Cell(40, 26, ' ', 1, 0, 'C', 1);
    //$pdf->Image($ruta,14,11,33,19);
    $pdf->Image('assets/library/PDF/ESSMAR_ESP_ant.jpg', 14, 11, 33, 19);

    $pdf->SetY(7);
    $pdf->SetX(51);
    $pdf->Cell(95, 26, 'CONSTANCIA DE PQRs ', 1, 0, 'C', 1);

    $pdf->SetY(7);
    $pdf->SetX(147); //RADICACION
    $pdf->Cell(58, 26, ' ', 1, 0, 'C', 1);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetY(8);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Radicación Nro ') . $radicado, 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetY(12);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Matrícula: ') . $matricula, 0, 0, 'L', 1);
    $pdf->SetY(16);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, 'Ciclo ' . $ciclo_, 0, 0, 'L', 1);
    $pdf->SetY(20);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Ubicación:') . $rutar, 0, 0, 'L', 1);
    $pdf->SetY(24);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, utf8_decode('Fecha Radicación:') . $fecsoli, 0, 0, 'L', 1);
    $pdf->SetY(28);
    $pdf->SetX(148);
    $pdf->Cell(40, 4, 'Fecha De Vencimiento:' . $fecmaxsoli, 0, 0, 'L', 1);


    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 35, 195, 15); //(10,35,195,13)
    $pdf->SetY(34);
    $pdf->SetX(12);
    $pdf->Cell(38, 3, 'DATOS DEL SOLICITANTE ', 0, 0, 'L', 1);
    $pdf->SetY(37.75);
    $pdf->SetX(12);
    $pdf->Cell(38, 2, 'Nombre y Apellidos: ' . utf8_decode($solicitante), 0, 0, 'L', 1);
    $pdf->SetY(37.75);
    $pdf->SetX(140);
    $pdf->Cell(38, 2, 'C.C Solicitante: ' . $cedula_soli, 0, 0, 'L', 1);
    /*$pdf->SetY(39);$pdf->SetX(150);
    $pdf->Cell(30,2,'C.C: ',0,0,'L',1);*/
    $pdf->SetY(40.75);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('Dirección de Notificación: ') . utf8_decode($direccion), 0, 0, 'L', 1);
    $pdf->SetY(40.75);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, utf8_decode('Teléfono Fijo: ') . $telefono, 0, 0, 'L', 1);
    //$pdf->SetY(43.75);$pdf->SetX(12);
    //$pdf->Cell(30,2,'Barrio: '.$barrio,0,0,'L',1);
    $pdf->SetY(43.75);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, 'Barrio: ' . utf8_decode($barrio), 0, 0, 'L', 1);
    $pdf->SetY(46.75);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('Correo Electrónico:  ') . utf8_decode($mail), 0, 0, 'L', 1);
    $pdf->SetY(43.75);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, utf8_decode('Teléfono Celular: ') . $celular, 0, 0, 'L', 1);
    $pdf->SetY(46.75);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, 'Fax: ' . $fax, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 8);

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 51, 195, 15); //(10,51,195,13);
    $pdf->SetY(50.5);
    $pdf->SetX(12);
    $pdf->Cell(38, 3, 'DATOS DEL PROPIETARIO ', 0, 0, 'L', 1);
    $pdf->SetY(56);
    $pdf->SetX(12);
    $pdf->Cell(80, 2, 'Propietario: ' . utf8_decode($propietario), 0, 0, 'L', 1);
    $pdf->SetY(56);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, 'C.C: ' . $cedula_pro, 0, 0, 'L', 1);
    $pdf->SetY(60);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('Dirección: ') . utf8_decode($direccion_pred), 0, 0, 'L', 1); ////FALTA
    $pdf->SetY(60);
    $pdf->SetX(140);
    $pdf->Cell(30, 2, utf8_decode('Teléfono: ') . $telefono_pred, 0, 0, 'L', 1);


    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 67, 195, 15); //(10,67,195,13)
    $pdf->SetY(66.5);
    $pdf->SetX(12);
    $pdf->Cell(20, 3, 'DATOS PQRs', 0, 0, 'L', 1);

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(71);
    $pdf->SetX(6);
    $pdf->Cell(20, 0, 'TIPO: ', 0, 0, 'C', 1);
    $pdf->SetY(74);
    $pdf->SetX(11.5); //cierre
    $pdf->Cell(30, 0, 'CONCEPTO: ', 0, 0, 'L', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(70);
    $pdf->SetX(25);
    $pdf->Cell(20, 2, $tipo, 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(74);
    $pdf->SetX(30); //cierre
    $pdf->Cell(30, 0, $motivo, 0, 0, 'L', 1);

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(70);
    $pdf->SetX(140);
    $pdf->Cell(30, 0, 'CON ORDEN DE TRABAJO Nro ' . $ot, 0, 0, 'C', 1);

    $pdf->SetFont('Arial','B',7);
    $pdf->SetY(77);
    $pqr_padre ? $pdf->SetX(137): $pdf->SetX(134);
    $pdf->Cell(30,0,'PQR Padre: '.$pqr_padre,0,0,'C',1);

    $pdf->SetY(77);
    $pdf->SetX(12);
    $pdf->Cell(30, 0, utf8_decode('MEDIO DE RECEPCIÓN: '), 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(77);
    $pdf->SetX(45);
    $pdf->Cell(30, 0, $mrecep, 0, 0, 'C', 1);

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetY(79.5);
    $pdf->SetX(13.5);
    $pdf->Cell(30, 0, utf8_decode('MEDIO DE NOTIFICACIÓN: '), 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(79.5);
    $pdf->SetX(45);
    $pdf->Cell(30, 0, utf8_decode($mednoti), 0, 0, 'C', 1);

    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(70);
    $pdf->SetX(25);
    /*if($cod_tipo==2) $pdf->Cell(20,2,'PETICION',0,0,'C',1);
    elseif ($cod_tipo==3) $pdf->Cell(20,2,'QUEJA',0,0,'C',1);
    elseif ($cod_tipo==1) $pdf->Cell(20,2,'RECLAMO',0,0,'C',1);
    elseif ($cod_tipo==4) $pdf->Cell(20,2,'RE. DE REPO',0,0,'C',1);
    elseif ($cod_tipo==5) $pdf->Cell(20,2,'RE. REPO SUB APEL',0,0,'C',1);*/
    $pdf->SetY(74);
    $pdf->SetX(30); //cierre
    /*$pdf->MultiCell(72,5,$motivo,0,'L',0);
    $pdf->SetY(71);$pdf->SetX(160);
    $pdf->Cell(15,2,$ot,0,0,'L',1);*/

    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Rect(10, 83, 195, 9); //(10,83,195,18)
    $pdf->SetY(83);
    $pdf->SetX(12);
    $pdf->Cell(23, 3, utf8_decode('DESCRIPCIÓN'), 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 5);
    $pdf->SetY(86);
    $pdf->SetX(12);
    $pdf->MultiCell(190, 2, utf8_decode($descripcion), 0, 'J', 0);
    $pdf->SetFont('Arial', '', 7);
    $pdf->SetY(92.5);
    $pdf->SetX(12);
    $trat_info = "Nota: Autorizo expresamente a Empresa de Servicios Publicos del Distrito de Santa Marta, sociedad operadora de los servicios de acueducto y alcantarillado en la ciudad de Santa Marta, el tratamiento de mi información o datos personales, para los fines establecidos en la Política de Tratamiento de Información y Política de Privacidad de la Empresa, elaboradas conforme a lo dispuesto en la Ley 1581 de 2012, su decreto reglamentario y demás normas concordantes.";
    $pdf->MultiCell(190, 2.5, utf8_decode($trat_info), 0, 'J', 0);

    $pdf->SetFont('Arial', '', 9);
    $pdf->SetY(109);
    $pdf->SetX(115); //firma
    $pdf->Cell(90, 18, ' ', 1, 0, 'C', 1);
    $pdf->SetY(113);
    $pdf->SetX(118); //datos factura
    $pdf->Cell(10, 2, 'FIRMA:___________________ ', 0, 0, 'L', 1);
    $pdf->SetY(117);
    $pdf->SetX(118);
    $pdf->Cell(10, 2, 'C.C: ', 0, 0, 'L', 1);
    $pdf->SetY(117);
    $pdf->SetX(125);
    $pdf->Cell(10, 2, $cedula_soli, 0, 0, 'L', 1);
    $pdf->SetY(121);
    $pdf->SetX(118);
    $pdf->Cell(10, 2, 'Nombre Solicitante: ', 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetY(121);
    $pdf->SetX(148);
    $pdf->Cell(10, 2, utf8_decode($solicitante), 0, 0, 'L', 1);


    $pdf->SetFont('Arial', 'B', 11);
    $pdf->SetY(105);
    $pdf->SetX(10);
    $pdf->Cell(195, 2, 'DATOS DE LA FACTURA', 0, 0, 'L', 1);


    //-----PUEDEN SER VARIOS--------------//

    $sql_peri = "select  pqr_detalle.cod_pqr, pqr_detalle.nro_factura, pqr_detalle.cod_peri, concepto.descripcion from pqr_detalle, concepto where pqr_detalle.cod_empr='" . $cod_empr . "' and pqr_detalle.cod_munip='" . $cod_munip . "' and pqr_detalle.cod_conc=concepto.cod_conc and pqr_detalle.cod_pqr='" . $pqr . "'";
    //$rs_peri = odbc_exec($conexion, $sql_peri);
    $result_peri = $conexion->prepare($sql_peri);
    $result_peri->execute();
    //$n_fact  = odbc_result($rs_peri, 'nro_factura');
    $nr = $result_peri->rowCount();


    $pdf->SetFont('Arial', '', 10);
    $pdf->SetFillColor(220, 220, 220);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetY(109);
    $pdf->SetX(15);
    $pdf->Cell(30, 5, 'SERVICIO ', 1, 0, 'C', 1);
    $pdf->SetY(109);
    $pdf->SetX(45);
    $pdf->Cell(30, 5, 'FACTURA N', 1, 0, 'C', 1);
    $pdf->SetY(109);
    $pdf->SetX(75);
    $pdf->Cell(30, 5, 'PERIODO(S)', 1, 0, 'C', 1);


    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetDrawColor(0, 0, 0);
    $j = 109;
    $z = 1;
    //while ($z<=$nr){
    while ($rw = $result_peri->fetch(PDO::FETCH_ASSOC)) {
        $n_fact  = $rw['nro_factura'];
        $descrip  = $rw['descripcion'];
        $peri = $rw['cod_peri'];
        $j = $j + 5;
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetY($j);
        $pdf->SetX(15);
        $pdf->Cell(30, 5, $descrip, 1, 0, 'L', 1);
        $pdf->SetY($j);
        $pdf->SetX(45);
        $pdf->Cell(30, 5, $n_fact, 1, 0, 'C', 1);
        $pdf->SetY($j);
        $pdf->SetX(75);
        $pdf->Cell(30, 5, $peri, 1, 0, 'C', 1);
        $pdf->SetY($j);
        $pdf->SetX(15);
        $z = $z + 1;
    }


    $periodo  = $rw['cod_peri'];
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->SetY(147);
    $pdf->SetX(10);
    $pdf->Cell(30, 2, utf8_decode('Decisión Comercial'), 0, 0, 'L', 1);
    $pdf->Line(50, 149, 145, 149);
    $pdf->Line(10, 156, 145, 156);
    $pdf->Line(10, 163, 145, 163);

    $pdf->SetY(167);
    $pdf->SetX(10);
    $pdf->Cell(30, 2, utf8_decode('Facturación'), 0, 0, 'L', 1);
    $pdf->Line(35, 170, 145, 170);
    $pdf->Line(10, 177, 145, 177);
    $pdf->Line(10, 184, 145, 184);

    if ($cod_tipo == 4 || $cod_tipo == 5) {
        $pdf->SetY(148);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, utf8_decode('CÓD   RESPUESTAS'), 0, 0, 'L', 1);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetY(153);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(4)  [  ] CONFIRMA', 0, 0, 'L', 1);
        $pdf->SetY(158);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(5)  [  ] MODIFICA', 0, 0, 'L', 1);
        $pdf->SetY(163);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(6)  [  ] REVOCA', 0, 0, 'L', 1);
        $pdf->SetY(168);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(7)  [  ] RECHAZA', 0, 0, 'L', 1);
        $pdf->SetY(173);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(8)  [  ] TRASLADA POR COMPT.', 0, 0, 'L', 1);
        $pdf->SetY(178);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(9)  [  ] PEND. DE RESPUESTA', 0, 0, 'L', 1);
        $pdf->SetY(183);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(10)  [  ] SIN RESPUESTA', 0, 0, 'L', 1);
        $pdf->SetY(188);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(11)  [  ] ARCHIVA', 0, 0, 'L', 1);
    } else {
        $pdf->SetY(151);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, utf8_decode('CÓD   RESPUESTAS'), 0, 0, 'L', 1);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetY(156);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(1)  [  ] ACCEDE', 0, 0, 'L', 1);
        $pdf->SetY(161);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(2)  [  ] ACCEDE PARCIALMENTE', 0, 0, 'L', 1);
        $pdf->SetY(166);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(3)  [  ] NO ACCEDE', 0, 0, 'L', 1);
        $pdf->SetY(171);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(8)  [  ] TRASLADA POR COMPT.', 0, 0, 'L', 1);
        $pdf->SetY(176);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(9)  [  ] PEND. DE RESPUESTA', 0, 0, 'L', 1);
        $pdf->SetY(181);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(10)  [  ] SIN RESPUESTA', 0, 0, 'L', 1);
        $pdf->SetY(186);
        $pdf->SetX(150);
        $pdf->Cell(30, 2, '(11)  [  ] ARCHIVA', 0, 0, 'L', 1);
    }

    $pdf->SetY(193);
    $pdf->SetX(10); //notificacion
    $pdf->Cell(142, 40, '', 1, 0, 'L', 1);

    $pdf->SetY(195);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('NOTIFICACIÓN PERSONAL DE RESPUESTA NÚMERO'), 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetY(202);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('El día ____ Mes ____ Año _____, se le entregá respuesta de la P.Q.R número'), 0, 0, 'L', 1);
    $pdf->SetY(207);
    $pdf->SetX(12);
    $pdf->Cell(30, 2, utf8_decode('al señor (a): ______________________________ Identificado con C.C _____________'), 0, 0, 'L', 1);


    if ($cod_tipo == '1') {
        $pdf->SetY(215);
        $pdf->SetX(12);
        $pdf->Cell(30, 2, utf8_decode('SOBRE ESTA DECISION PROCEDEN LOS RECUSOS DE REPOSICIÓN Y EN '), 0, 0, 'L', 1);
        $pdf->SetY(220);
        $pdf->SetX(12);
        $pdf->Cell(30, 2, utf8_decode('SUBSIDIO EN APELACIÓN INTERPUESTO DENTRO DE LOS TERMINOS DE LEY'), 0, 0, 'L', 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetY(227);
        $pdf->SetX(12);
        $pdf->Cell(30, 2, 'Firma del Usuario: ____________________________________ CC: ____________', 0, 0, 'L', 1);
    } else {

        $pdf->SetY(227);
        $pdf->SetX(12);
        $pdf->Cell(30, 2, 'Firma del Usuario: ____________________________________ CC: ____________', 0, 0, 'L', 1);
    }


    $pdf->SetFont('Arial', 'B', 10);

    $pdf->SetY(199);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, 'CIERRE', 0, 0, 'L', 1);
    $pdf->SetY(206);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, 'FECHA : _________________', 0, 0, 'L', 1);
    $pdf->SetY(211);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, '_________________________', 0, 0, 'L', 1);
    $pdf->SetY(216);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, '_________________________', 0, 0, 'L', 1);
    $pdf->SetY(221);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, '_________________________', 0, 0, 'L', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetY(226);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, '_______________________________', 0, 0, 'L', 1);
    $pdf->SetY(231);
    $pdf->SetX(153); //cierre
    $pdf->Cell(30, 2, utf8_decode('Elaboró: ') . $nom_usu, 0, 0, 'L', 1);


    $pdf->SetY(234);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, '__ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ ', 0, 0, 'L', 1);
    $sql_peri_ = "select (pqr_detalle.cod_peri) as peri  from pqr_detalle, concepto  
                where pqr_detalle.cod_empr='" . $cod_empr . "' and pqr_detalle.cod_munip='" . $cod_munip . "' 
                and pqr_detalle.cod_conc=concepto.cod_conc 
                and pqr_detalle.cod_pqr='" . $pqr . "' 
                group by pqr_detalle.cod_peri order 
                by pqr_detalle.cod_peri";
    // $rs_peri_ = odbc_exec($conexion, $sql_peri_);
    $rs_peri_ = $conexion->prepare($sql_peri);
    $rs_peri_->execute();
    //$nr=odbc_num_rows($rs_peri_);

    $x = 160.5;
    //while ($z<=$nr){
    while ($rw_ = $rs_peri_->fetch(PDO::FETCH_ASSOC)) {
        $peri_ = $rw['peri'];
        $pdf->SetFont('Arial', '', 8);
        $pdf->SetY(241);
        $pdf->SetX($x); //cierre
        $pdf->Cell(30, 2, $peri_, 0, 0, 'L', 1);
        $x = $x + 10.5;
    }



    $pdf->SetFont('Arial', 'B', 8);

    $pdf->Image("assets/library/PDF/ESSMAR_ESP_ant.jpg", 12, 237, 18, 12);

    $pdf->SetY(237.5);
    $pdf->SetX(30); //cierre
    $pdf->Cell(30, 2, utf8_decode('COMPROBANTE DE PQR NÚMERO: ' . $radicado), 0, 0, 'L', 1);

    $pdf->SetY(241);
    $pdf->SetX(30); //cierre
    $pdf->Cell(30, 2, 'Fecha Radicado: ' . $fecsoli, 0, 0, 'L', 1);
    $pdf->SetY(241);
    $pdf->SetX(130); //cierre traslado a otra posicion 26/07/2012
    $pdf->Cell(30, 2, 'Periodo Facturado: ', 0, 0, 'L', 1);

    $pdf->SetY(250);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, utf8_decode('Matrícula: ' . $matricula), 0, 0, 'L', 1);

    $pdf->SetY(253.5);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, utf8_decode('Nombre Del Solicitante: ' . $solicitante), 0, 0, 'L', 1);
    $pdf->SetY(257);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, 'Documento del Solicitante: ' . $cedula_soli, 0, 0, 'L', 1);

    $pdf->SetY(257);
    $pdf->SetX(130); //cierre
    $pdf->Cell(30, 2, utf8_decode('TIPO PQR: ' . $tipo), 0, 0, 'L', 1);

    /*$pdf->SetY(260);$pdf->SetX(10); //cierre
    $pdf->Cell(30,2,'Nit Usuario: '.$cedula_soli,0,0,'L',1);*/

    $pdf->SetY(260.5);
    $pdf->SetX(10); //cierre
    $pdf->Cell(30, 2, utf8_decode('CONCEPTO DE LA PQR: ' . $motivo), 0, 0, 'L', 1);
    /*$pdf->SetY(259.5);$pdf->SetX(53); //cierre
    $pdf->MultiCell(140,5,$motivo,0,'J',0);*/

    $pdf->SetFont('Arial', '', 8);

    // $dia = substr($fecmaxsoli, -2);
    // $mes = substr($fecmaxsoli, 5, 2);
    // $agno = substr($fecmaxsoli, 0, 4);
    // setlocale(LC_ALL, "es_CL");
    // $loc = setlocale(LC_TIME, NULL);
    //$fecmaxsoli = strftime("%d/%m/%Y", mktime(0, 0, 0, $mes, $dia, $agno));
    switch ($cod_tipo) {
        case "RE":
            $tipo2 = mb_convert_encoding('RECLAMO', "UTF-8");
            break;
        case "PT":
            $tipo2 = mb_convert_encoding('PETICION', "UTF-8");
            break;
        case "QJ":
            $tipo2 = mb_convert_encoding('QUEJA', "UTF-8");
            break;
        case "RR":
            $tipo2 = mb_convert_encoding('R. REPOSICION', "UTF-8");
            break;
        case "RS":
            $tipo2 = mb_convert_encoding('R REP SUB APEL', "UTF-8");
            break;
        case "RC":
            $tipo2 = mb_convert_encoding('RECURSO', "UTF-8");
            //$tipo2 = mb_convert_encoding('SILEN ADMIN POSI', "UTF-8");
            break;
        case "DP":
            $tipo2 = mb_convert_encoding('DER PETICION', "UTF-8");
            break;
    }
    $msj = 'Estimado Usuario: Su ' . $tipo2 . ' tiene un plazo para emitir respuesta de 15 días habiles (Art. 158 Ley 142/94). Por lo anterior, la fecha de vencimiento de la presente es ' . $fecmaxsoli2 . ' De igual manera, si para atender su reclamación se requiere hacer revisiones, el tiempo de respuesta se ampliará 30 días hábiles más ( Art.14 Paragrafo C.C.A.)';

    $pdf->SetY(267);
    $pdf->SetX(10); //cierre
    $pdf->MultiCell(190, 3, utf8_decode($msj), 0, 'J', 0);

    $pqr_ruta = 'essmar/tmp/' . $pqr;
    if ($tipoPQR == 'P') { // En caso la PQR sea padre, debe ser la misma carpeta de archivos de la PQR hija
        $pqr_ruta = 'essmar/tmp/' . $_GET['pqr'];
    }

    if (!file_exists($pqr_ruta)) {
        mkdir($pqr_ruta, 0777, true);
    }
    $filename = $pqr_ruta . "/Constancia" . $pqr . ".pdf";
    $pdf->Output($filename, 'F', true);
}
