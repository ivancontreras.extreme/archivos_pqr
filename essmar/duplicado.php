<?php

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // $auto_page_break = $this->getAutoPageBreak();
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        // set bacground image

        //IMAGEN DEL FRENTE-------------------
        $cod_peri = !empty($_REQUEST['cod_peri']) ? $_REQUEST['cod_peri'] : "";
        $cicloFondo = $_REQUEST['cod_cclo'];


        // FONDO DEL FRENTE PAGINA 1

        if ($cod_peri >= 201904 and $cod_peri <= 201906) {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/ingrid/ESSMAR_FACTURA-01.jpg';
        } else if ($cod_peri > 201906 and $cod_peri <= 201912) {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/jose/ESSMAR_FACTURA-01.jpg';
        } else if ($cod_peri >= 202001 and $cod_peri <= 202002) {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/carlos/ESSMAR_FACTURA-01.jpg';
        } else if ($cod_peri > 202002 and $cod_peri <= 202012) {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/jose/ESSMAR_FACTURA-01.jpg';
        } else if ($cod_peri > 202012 and $cod_peri <= 202106) {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/carlos/ESSMAR_FACTURA-01.jpg';
        } else if (($cod_peri == 202110 and $cicloFondo == '415') || $cod_peri >= 202111) {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/yahaira/ESSMAR_FACTURA-01.jpg';
        } else {
            $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/carmen/ESSMAR_FACTURA-01.png';
        }

        $this->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}

function duplicadoEssmar($conexion, $pqr, $predio, $periodo, $nro_factura = null, $tipoPQR = null)
{
    //--Propiedades PDF
    $orientacion_papel = "P";
    $medida = "pt";
    $formato = "A4";
    $unicode = true;
    $codificacion = "UTF-8";
    $clase = "TCPDF";
    $autor = "Kagua";
    $titulo = "Factura";
    $margen_izq = "0";
    $margen_der = "0";
    $margen_sup = "0";
    $margen_inf = "0";
    $margen_encabezado = "0";
    $margen_pie = "0";
    $data_font_size = "8";
    $data_font_type = "FreeSerif";
    $encbz_font_size = "6";
    $peq_font_size = "3";
    // $encbz_font_type="FreeSerif";
    $print_encbz_pg = true;
    $print_pie_pg = true;
    $y_ = 0;
    $conexion_ = 0;
    $img_jpg = 0;

    // create new PDF document
    $pdf = new MYPDF($orientacion_papel, $medida, $formato, $unicode, $codificacion, false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Kagua');
    $pdf->SetTitle('Factura ESSMAR - Kagua');
    $pdf->SetSubject('Factura ESSMAR');
    $pdf->SetKeywords('Factura ESSMAR');
    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);
    // remove default footer
    $pdf->setPrintFooter(false);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    // set some language-dependent strings (optional)
    //$pdf->setLanguageArray($l);

    // ---------------------------------------------------------
    // set font
    // $pdf->SetFont('times', '', 48);

    // Datos
    $cod_pred = $predio;
    $cod_peri = $periodo;
    $lotes = 500;
    $paginas = 0;
    $orden = "";
    $sql_ciclo = "SELECT p.cod_cclo, p.cod_munip, p.cod_empr FROM predio p WHERE p.cod_pred = $cod_pred";
    $query_ciclo = $conexion->prepare($sql_ciclo);
    $query_ciclo->execute();
    $data_ini = $query_ciclo->fetch();
    $cod_cclo = $data_ini['cod_cclo'];
    $_REQUEST['cod_cclo'] = $cod_cclo;
    $_REQUEST['cod_peri'] = $periodo;
    $empresa = $data_ini['cod_empr'];
    $municipio = $data_ini['cod_munip'];
    $cod_lote = 0;


    //--Consultas Globales
    $sql = "SELECT cod_conc_ppal,cod_conc_alc,cod_conc_aseo,cod_org,cod_ajuste_dec, codsaldfavor
            FROM empr_parametros 
            WHERE cod_empr='" . $empresa . "';";

    $query_empr = $conexion->prepare($sql);
    $query_empr->execute();
    $datos = $query_empr->fetch();

    $acueducto = $datos['cod_conc_ppal'];
    $alcantarillado = $datos['cod_conc_alc'];
    $cod_org = $datos['cod_org'];
    $ajuste_dec = $datos['cod_ajuste_dec'];
    $cod_sal_fav = $datos['codsaldfavor'];

    //--Rangos de Consumo
    $sql = "SELECT frecuencia_fact, tope_rango_1,tope_rango_2,codigo_recaudo, codigo_recaudo_, codigo_recaudo__
            FROM municipio 
            WHERE cod_munip=" . $municipio . " and cod_empr='" . $empresa . "'";
    $query_r = $conexion->prepare($sql);
    $query_r->execute();
    $datos_r = $query_r->fetch();

    $recuaudo_corriente = $datos_r["codigo_recaudo"];
    $recuaudo_deuda_total = $datos_r["codigo_recaudo_"];
    $recuaudo_deuda_anterior = $datos_r["codigo_recaudo__"];
    $frecuencia = $datos_r["frecuencia_fact"];

    $pag = ($paginas == "") ? 0 : $paginas;
    switch ($orden) {
        case "ruta":
            $orden = " fpc.rutareparto ";
            break;
        case "barrio":
            $orden = " b.descripcion, fpc.direccion";
            break;
        default:
            $orden = "fpc.rutareparto";
    }

    //--Consultar Facturacion BD
    if ($cod_pred != "") {
        $q  = " fpc.cod_pred = " . $cod_pred . " and ";
    }
    if ($cod_cclo != "") {
        $q  .= " fpc.cod_cclo = '" . $cod_cclo . "' and ";
    }
    if ($cod_lote != "") {
        $q .= " fpc.cod_lote=$cod_lote and ";
    }

    $sql = "select fpc.cod_pred,fpc.cod_peri,fpc.nro_factura,fpc.fecexp,fpc.fecvto,fpc.feclectura,fpc.fec_lectura_ant,
                fpc.fec_susp,fpc.fecabre,fpc.feccierra,fpc.total,fpc.total_sin_descto,fpc.lectura,
                fpc.lectura_anterior,fpc.consumo,fpc.cons_ajustado,fpc.consttalizador,fpc.promedio,
                fpc.cons_manual,fpc.cons_fact,fpc.metodo,fpc.nro_seq,fpc.cod_cclo,fpc.cod_lote,
                fpc.nombre,fpc.direccion,fpc.estrato,fpc.rutareparto,fpc.nropermora,fpc.cod_pred_totalizador,
                fpc.dias_facturados, Coalesce(cod_obsl_i,cod_obsl_l) as obs_lectura,
                fpc.mensaje,fpc.total_mas_cartera,fpc.deuda,fpc.tipo_sector,fpc.total,
                --fpc.cod_peri_ult_pago,
                fpc.fch_ult_pago,fpc.valor_ult_pago,fpc.nro_cuenta_cobro,
                p.fecinstalmedi,p.serialmedi,m.descripcion as marca,cb.descripcion as calibre,
                b.descripcion as barrio,c.descripcion as ciclo,u.descripcion as uso, fpc.deuda_e3
                from factura_per_cab fpc 
                join ciclo c using (cod_cclo,cod_munip,cod_empr)
                join medidor m using (cod_medi,cod_clbr,cod_empr)
                join calibre cb using (cod_clbr,cod_empr)
                join uso u using(cod_uso,cod_empr)
                join predio p using(cod_pred,cod_munip,cod_empr)
                join barrio b using(cod_barrio,cod_munip,cod_empr) 
                where " . $q . "
                fpc.cod_peri=" . $cod_peri . " and
                fpc.cod_munip=" . $municipio . " and
                fpc.cod_empr='" . $empresa . "' order by  " . $orden . " limit " . $lotes . " offset " . $pag;

    $exe = $conexion->prepare($sql);
    $exe->execute();

    if (!$exe) {
        $pdf->AddPage();
        $pdf->SetY(100);
        $pdf->SetX(200);
        $pdf->Cell(0, 0, "Error: Consultando los datos de facturacion", 0);
    } else {
        while ($result =  $exe->fetch(PDO::FETCH_ASSOC)) {

            $facturado = $result['total'];
            $deuda_ess = $result['deuda_e3'];
            $cicloFondo = $result['cod_cclo'];

            if ($result['nombre'] == "") {
                $pdf->AddPage();
                $pdf->SetY(100);
                $pdf->SetX(200);
                $pdf->Cell(0, 0, "Error: Factura sin nombre", 0);
            } else {
                // add a page
                $pdf->AddPage();
                if ($img_jpg != 0) {
                    // FONDO DEL FRENTE PAGINA 1                

                    if ($cod_peri >= 201904 and $cod_peri <= 201906) {
                        $img_file = './fondo/ingrid/ESSMAR_FACTURA-01.jpg';
                    } else if ($cod_peri > 201906 and $cod_peri <= 201912) {
                        $img_file = './fondo/jose/ESSMAR_FACTURA-01.jpg';
                    } else if ($cod_peri >= 202001 and $cod_peri <= 202002) {
                        $img_file = './fondo/carlos/ESSMAR_FACTURA-01.jpg';
                    } else if ($cod_peri > 202002 and $cod_peri <= 202012) {
                        $img_file = './fondo/jose/ESSMAR_FACTURA-01.jpg';
                    } else if ($cod_peri > 202012 and $cod_peri <= 202106) {
                        $img_file = './fondo/carlos/ESSMAR_FACTURA-01.jpg';
                    } else if (($cod_peri == 202110 and $cicloFondo == '415') || $cod_peri >= 202111) {
                        $img_file = './fondo/yahaira/ESSMAR_FACTURA-01.jpg';
                    } else {
                        $img_file = './fondo/carmen/ESSMAR_FACTURA-01.png';
                    }

                    //$pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);
                    $pdf->Image($img_file, 0, 0, 593, 0, '', '');
                }
                $img_jpg = 1;

                // EMPRESA
                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetY($y_ + 35);
                $pdf->SetX($conexion_ + 267);
                $pdf->Cell(0, 0, "ESSMAR E.S.P.", 0);
                $pdf->SetY($y_ + 45);
                $pdf->SetX($conexion_ + 267);
                $pdf->Cell(0, 0, "800181106-1", 0);
                $pdf->SetY($y_ + 56);
                $pdf->SetX($conexion_ + 267);
                $pdf->Cell(0, 0, "CALLE 22 N° 22- 111", 0);

                // INFORMACION DEL CLIENTE 
                $pdf->SetFont($data_font_type, '', 10);
                $long_factura = strlen($result['nro_factura']);
                if ($long_factura < 10) {
                    $factura_no = str_pad($result['nro_factura'], 9, '0', STR_PAD_LEFT);
                    $factura_no = "1" . $factura_no;
                } else {
                    $factura_no = $result['nro_factura'];
                }
                $pdf->SetY($y_ + 35);
                $pdf->SetX($conexion_ + 486);
                $pdf->Cell(0, 0, $factura_no, 0);

                $pdf->SetY($y_ + 85);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetY($y_ + 99);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, $result['nombre'], 0);

                $pdf->SetY($y_ + 110);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, mb_convert_encoding($result['direccion'], "UTF-8", "UTF-8"), 0);

                $pdf->SetY($y_ + 123);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, $result['barrio'], 0);

                $pdf->SetY($y_ + 135);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, $result['ciclo'], 0);

                $pdf->SetY($y_ + 147);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, $result['uso'], 0);

                if ($result['uso'] == 'RESIDENCIAL') {
                    $estrato = ($result['estrato'] == 0) ? "1" : $result['estrato'];
                } else {
                    $estrato = "";
                }
                $pdf->SetY($y_ + 159);
                $pdf->SetX($conexion_ + 460);
                $pdf->Cell(0, 0, $estrato, 0);

                $pdf->SetY($y_ + 171);
                $pdf->SetX($conexion_ + 460);
                $rutareparto = ($result['rutareparto'] == "") ? "SIN RUTA" : $result['rutareparto'];
                $pdf->Cell(0, 0, $rutareparto, 0);

                $sql_conv = "SELECT cc.cod_conv,ct.valor_capital,ct.valor_interes,ct.id_detalle,
                cc.cant_cuotas, ct.ccobro
                from convenio_cab cc, convenio_cuota ct
                WHERE cc.cod_conv=ct.cod_conv and cc.cod_empr=ct.cod_empr and cc.cod_munip=ct.cod_munip
                AND cc.cod_pred=" . $result['cod_pred'] . " and cc.cod_munip=" . $municipio . " 
                and ct.cod_peri_apli=" . $result['cod_peri'] . " ";
                $exSql_conv = $conexion->prepare($sql_conv);
                $exSql_conv->execute();
                $data_conv = $exSql_conv->fetch();
                $codconv = $data_conv['cod_conv'];

                if ($codconv > 0) {
                    $txt_web = "DUPLICADO - EN CONVENIO";
                    $tm = 40;
                    $px = 10;
                    $py = 50;

                    $sql_conv = "SELECT cc.cod_conv,ct.valor_total,ct.valor_capital,ct.valor_interes,ct.id_detalle,
                    cc.cant_cuotas, ct.ccobro
                    from convenio_cab cc, convenio_cuota ct
                    WHERE cc.cod_conv=ct.cod_conv and cc.cod_empr=ct.cod_empr and cc.cod_munip=ct.cod_munip
                    AND cc.cod_pred=" . $result['cod_pred'] . " and cc.cod_munip=" . $municipio . " 
                    and ct.cod_peri_apli=" . $cod_peri . " and cc.cod_conv=" . $codconv . " ";
                    $exSql_conv = $conexion->prepare($sql_conv);
                    $exSql_conv->execute();
                    $data_conv = $exSql_conv->fetch();
                    //$codconv = odbc_result($exSql_conv,'cod_conv');
                    $val_conv = $data_conv['valor_total'];
                    $nrocuota_conv = $data_conv['id_detalle'];
                    $cantcuotas_conv = $data_conv['cant_cuotas'];
                    //Si tiene convenio le sumo al cupo corriente la cuota del convenio
                    $facturado          = $facturado + $val_conv;
                } else {
                    $txt_web = "DUPLICADO";
                    $tm = 50;
                    $px = 180;
                    $py = 10;
                }

                /*Fondo Duplicado*/
                $pdf->SetFont('freeserif', '', $tm);
                $pdf->SetDrawColor(200);
                $pdf->SetTextColor(200);
                //MARCA DE AGUA

                $pdf->StartTransform();
                // Rotate 20 degrees counter-clockwise centered by (70,110) which is the lower left corner of the rectangle
                //$pdf->Rotate(40, 10, 125);
                $pdf->Rotate(40);
                //$pdf->Text(180, 10, 'DUPLICADO');
                //$pdf->Text(10, 50, 'DUPLICADO - EN CONVENIO');
                $pdf->Text($px, $py, $txt_web);
                // Stop Transformation
                $pdf->StopTransform();

                //Start Transformation
                /*$pdf->StartTransform();
                $pdf->Rotate(40, 650, 120);
                $pdf->Cell(500,10,'DUPLICADO',0,0,'C',0);
                $pdf->StopTransform();*/

                $pdf->SetTextColor(0, 0, 0);
                // DATOS DE MEDIDOR 
                $pdf->SetFont($data_font_type, '', 6);
                $marca = ($result['marca'] == "") ? "S/M" : $result['marca'];
                $pdf->SetY($y_ + 119);
                $pdf->SetX($conexion_ + 37);
                $pdf->Cell(0, 0, $marca, 0);

                $pdf->SetY($y_ + 119);
                $pdf->SetX($conexion_ + 125);
                $serial_medidor = ($result['serialmedi'] == "") ? "S/S" : $result['serialmedi'];
                $pdf->Cell(0, 0, $serial_medidor, 0);

                $pdf->SetY($y_ + 119);
                $pdf->SetX($conexion_ + 223);
                $pdf->Cell(0, 0, $result['calibre'], 0);

                $pdf->SetY($y_ + 119);
                $pdf->SetX($conexion_ + 310);
                $pdf->Cell(0, 0, $result['fecinstalmedi'], 0);

                // LECTURAS Y FECHAS DE LECTURA

                // $mes_periodo=Mes(substr($result['cod_peri'],-2));
                // $pdf->SetY($y_+109);$pdf->SetX($conexion_+410); 
                // $pdf->Cell(0,0,$mes_periodo,0);
                $pdf->SetY($y_ + 145);
                $pdf->SetX($conexion_ + 37);
                $pdf->Cell(0, 0, $result['fec_lectura_ant'], 0);

                $pdf->SetY($y_ + 145);
                $pdf->SetX($conexion_ + 130);
                $pdf->Cell(0, 0, $result['feclectura'], 0);

                $pdf->SetY($y_ + 145);
                $pdf->SetX($conexion_ + 223);
                $pdf->Cell(0, 0, $result['lectura_anterior'], 0);

                $pdf->SetY($y_ + 145);
                $pdf->SetX($conexion_ + 310);
                $pdf->Cell(0, 0, $result['lectura'], 0);

                $pdf->SetY($y_ + 172);
                $pdf->SetX($conexion_ + 37);
                $pdf->Cell(0, 0, number_format($result['consumo'], 0, ',', '.'), 0, 0, 'L', 0);

                $pdf->SetY($y_ + 172);
                $pdf->SetX($conexion_ + 130);
                $pdf->Cell(0, 0, number_format($result['promedio'], 0, ',', '.'), 0, 0, 'L', 0);

                $pdf->SetY($y_ + 172);
                $pdf->SetX($conexion_ + 223);
                $pdf->Cell(0, 0, $result['dias_facturados'], 0);

                $pdf->SetY($y_ + 172);
                $pdf->SetX($conexion_ + 310);
                $pdf->Cell(0, 0, $result['obs_lectura'], 0);


                // DATOS DEUDA
                $pdf->SetFont($data_font_type, '', 9);
                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 42);
                $pdf->Cell(0, 0, $result['cod_peri'], 0);

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 102);
                $pdf->Cell(0, 0, $result['fecexp'], 0);

                if ($result['nropermora'] > 0) {
                    if ($codconv > 0) {
                        $fecvto = $result['fecvto'];
                        $fecsus = $result['fec_susp'];
                    } else {
                        $fecvto = "INMEDIATO";
                        $fecsus = "INMEDIATO";
                    }
                } else {
                    $fecvto = $result['fecvto'];
                    $fecsus = $result['fec_susp'];
                }

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 167);
                $pdf->Cell(0, 0, $fecvto, 0); // FECHA PAGO OPORTUNO

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 232);
                $pdf->Cell(0, 0, $result['fch_ult_pago'], 0);

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 297);
                $pdf->Cell(0, 0, '$ ' . number_format($result['valor_ult_pago'], 0, ',', '.'), 0);

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 367);
                $pdf->Cell(0, 0, $fecsus, 0);

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 450);
                $pdf->Cell(0, 0, $result['nropermora'], 0);

                //--deuda segun tiempo - total a pagar
                // $sql = "SELECT fpc.total As facturado
                //         FROM public.factura_per_cab As fpc
                //         WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                //                 fpc.cod_peri = " . $cod_peri . " And 
                //                 fpc.cod_munip = " . $municipio . " And
                //                 fpc.cod_empr = '" . $empresa . "';";
                // $rfact = $conexion->prepare($sql);
                // $rfact->execute();
                // $facturado = $rfact->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);

                $sql = "SELECT fpc.deuda_e1, fpc.nro_cuenta_cobro
                        FROM public.factura_per_cab As fpc
                        WHERE fpc.cod_pred = " . $result['cod_pred'] . " And 
                                fpc.cod_peri = " . $cod_peri . " And 
                                fpc.cod_munip = " . $municipio . " And
                                fpc.cod_empr = '" . $empresa . "';";
                $rpago = $conexion->prepare($sql);
                $rpago->execute();
                $data_deuda = $rpago->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                $deuda = $data_deuda[0];
                $data_cobro = $rpago->fetch(PDO::FETCH_ASSOC);
                $nro_cuenta_cobro = $data_cobro['nro_cuenta_cobro'];

                // $deuda = $facturado - $pagado;
                // $total_pagar = $facturado + $deuda;
                $total_pagar = $deuda_ess + $deuda[0];

                $pdf->SetY($y_ + 223);
                $pdf->SetX($conexion_ + 502);
                $pdf->Cell(0, 0, number_format($total_pagar, 0, ',', '.'), 0, 0, 'L', 0); //deuda total a pagar


                // TOTAL POR CONCEPTO
                $sql = "SELECT * FROM factura_per_det
                        WHERE cod_pred=" . $result['cod_pred'] . " and
                            cod_peri=" . $cod_peri . " and 
                            cod_empr='" . $empresa . "' and 
                            cod_munip=" . $municipio . " order by cod_conc ";

                $exe_detalle = $conexion->prepare($sql);


                $total_concepto_acueducto       = 0;
                $total_concepto_alcantarillado  = 0;
                $total_otros_conceptos          = 0;
                // $total_veolia                   = 0;
                $total_pag                      = 0;
                $exe_detalle->execute();
                while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {
                    //--sumatoria para el resumen
                    if ($result_det['cod_conc'] == 103) { // ACUEDUCTO
                        $total_concepto_acueducto += $result_det['total_concp'];
                    }
                    if ($result_det['cod_conc'] == 104) { // ALCANTARILLADO
                        $total_concepto_alcantarillado += $result_det['total_concp'];
                    }
                    // if($result_det['cod_conc']==100)	{// VEOLIA
                    //     $total_veolia += $result_det['total_concp'];
                    // }
                    if (($result_det['cod_conc'] != 103) and ($result_det['cod_conc'] != 104) and ($result_det['cod_conc'] != 1) and ($result_det['cod_conc'] != 2)) { // and ($result_det['cod_conc']!=100)){ // OTROS
                        $total_otros_conceptos += $result_det['total_concp'];
                    }
                }
                $pdf->SetY($y_ + 355);
                $pdf->SetX($conexion_ + 54);
                $pdf->Cell(0, 0, '$' . number_format($total_concepto_acueducto, 2, ',', '.'), 0);

                $pdf->SetY($y_ + 355);
                $pdf->SetX($conexion_ + 144);
                $pdf->Cell(0, 0, '$' . number_format($total_concepto_alcantarillado, 2, ',', '.'), 0);

                $pdf->SetY($y_ + 361);
                $pdf->SetX($conexion_ + 236);
                $pdf->Cell(0, 0, '$' . number_format($total_otros_conceptos, 2, ',', '.'), 0);

                $pdf->SetY($y_ + 361);
                $pdf->SetX($conexion_ + 410);
                $pdf->Cell(0, 0, '$' . number_format($deuda, 2, ',', '.'), 0);

                //$total_pag = $total_concepto_acueducto + $total_concepto_alcantarillado + $total_otros_conceptos + $deuda;
                $total_pag = $deuda_ess + $deuda;
                $pdf->SetY($y_ + 361);
                $pdf->SetX($conexion_ + 499);
                $pdf->Cell(0, 0, '$' . number_format($total_pag, 2, ',', '.'), 0);

                // GRAFICA - HISTORICO DE CONSUMO
                GraficaHistorica($conexion, $pdf, $result['cod_pred'], $cod_peri, $result['cod_cclo'], $municipio, $empresa);

                $style = array(
                    'position' => '',
                    'align' => 'L',
                    'stretch' => false,
                    'fitwidth' => true,
                    'cellfitalign' => '',
                    'border' => false,
                    'hpadding' => 'auto',
                    'vpadding' => 'auto',
                    'fgcolor' => array(0, 0, 0),
                    'bgcolor' => false, //array(255,255,255),
                    'text' => false,
                    'font' => 'helvetica',
                    'fontsize' => 8,
                    'stretchtext' => 4
                );

                //$pdf->SetY($y_+435);$pdf->SetX($conexion_+390);
                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetFontSize('8');
                $msg = "La ESSMAR presta el servicio  de recolección, transporte y disposición final de escombros, material vegetal e inservibles. Teléfonos: 4224915 - 3005792293";
                //$pdf->writeHTMLCell(272,26,299,435,mb_convert_encoding($msg, "UTF-8","UTF-8"),0,'',0,true,'L',true);

                // TOTAL A PAGAR DEL MES
                $pdf->SetFont($data_font_type, '', 8);

                $pdf->SetY($y_ + 528);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, $factura_no, 0);

                $pdf->SetY($y_ + 539);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetY($y_ + 549);
                $pdf->SetX($conexion_ + 122);
                $pdf->Cell(0, 0, $result['fecvto'], 0); // FECHA PAGO OPORTUNO

                $pdf->SetY($y_ + 560);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, '$' . number_format($facturado[0], 2, ',', '.'), 0);

                // TOTAL A PAGAR DEL MES Codigo de barras
                // $barcode= "415".$recuaudo_corriente."8020".str_pad($factura_no,10,"0",STR_PAD_LEFT)."3902".str_pad(number_format($facturado,0,'',''),8,"0",STR_PAD_LEFT)."96".str_replace("-","",$result['fecvto']);
                // $textBarcode = "(415)".$recuaudo_corriente."(8020)".str_pad($factura_no,10,"0",STR_PAD_LEFT)."(3902)".str_pad(number_format($facturado,0,'',''),8,"0",STR_PAD_LEFT)."96".str_replace("-","",$result['fecvto']);

                $barcode = chr(241) . "415" . $recuaudo_corriente . "8020" . str_pad($factura_no, 10, "0", STR_PAD_LEFT) . chr(241) . "3900" . str_pad(number_format($facturado[0], 0, '', ''), 10, "0", STR_PAD_LEFT) . chr(241) . "96" . str_replace("-", "", $result['fecvto']);
                $textBarcode = "(415)" . $recuaudo_corriente . "(8020)" . str_pad($factura_no, 10, "0", STR_PAD_LEFT) . "(3900)" . str_pad(number_format($facturado[0], 0, '', ''), 10, "0", STR_PAD_LEFT) . "(96)" . str_replace("-", "", $result['fecvto']);

                $pdf->write1DBarcode($barcode, 'C128', $conexion_ + 207, $y_ + 525, 340, 30, 1, $style, 'N');
                $pdf->SetFontSize('6');

                $pdf->Text($conexion_ + 305, $y_ + 551, $textBarcode);

                // DEUDA TOTAL
                if ($deuda == 0) {
                    if ($deuda_ess > $facturado[0]) {
                        if ($nro_cuenta_cobro == "") {
                            $factura_no_cuenta_cobro = $factura_no;
                            $prefijo = "";
                        } else {
                            $factura_no_cuenta_cobro = $nro_cuenta_cobro;
                            $prefijo = 5;
                        }
                    } else {
                        $factura_no_cuenta_cobro = $factura_no;
                        $prefijo = "";
                    }
                } else {
                    $factura_no_cuenta_cobro = $nro_cuenta_cobro;
                    $prefijo = 5;
                }
                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetY($y_ + 611);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, $prefijo . str_pad(($factura_no_cuenta_cobro), 9, "0", STR_PAD_LEFT), 0);

                $pdf->SetY($y_ + 621);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetY($y_ + 632);
                $pdf->SetX($conexion_ + 122);
                $pdf->Cell(0, 0, $result['fecvto'], 0); // FECHA PAGO OPORTUNO

                $pdf->SetY($y_ + 643);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, '$ ' . number_format($total_pagar, 2, ',', '.'), 0, 0, 'L', 0); //total factura


                // DEUDA TOTAL Codigo de barras
                // $barcode= "415".$recuaudo_deuda_total."8020".$prefijo.str_pad(($factura_no_cuenta_cobro),9,"0",STR_PAD_LEFT)."3902".str_pad(number_format($total_pagar,0,'',''),8,"0",STR_PAD_LEFT)."96".str_replace("-","",$result['fecvto']);
                // $textBarcode = "(415)".$recuaudo_deuda_total."(8020)".$prefijo.str_pad(($factura_no_cuenta_cobro),9,"0",STR_PAD_LEFT)."(3902)".str_pad(number_format($total_pagar,0,'',''),8,"0",STR_PAD_LEFT)."96".str_replace("-","",$result['fecvto']);

                $barcode = chr(241) . "415" . $recuaudo_deuda_total . "8020" . $prefijo . str_pad(($factura_no_cuenta_cobro), 9, "0", STR_PAD_LEFT) . chr(241) . "3900" . str_pad(number_format($total_pagar, 0, '', ''), 10, "0", STR_PAD_LEFT) . chr(241) . "96" . str_replace("-", "", $result['fecvto']);
                $textBarcode = "(415)" . $recuaudo_deuda_total . "(8020)" . $prefijo . str_pad(($factura_no_cuenta_cobro), 9, "0", STR_PAD_LEFT) . "(3900)" . str_pad(number_format($total_pagar, 0, '', ''), 10, "0", STR_PAD_LEFT) . "(96)" . str_replace("-", "", $result['fecvto']);

                //$pdf->write1DBarcode($barcode, 'C128', 210, 695, '', 30, 1, $style, 'N');
                $pdf->write1DBarcode($barcode, 'C128', $conexion_ + 207, $y_ + 610, 340, 30, 1, $style, 'N');
                $pdf->SetFontSize('6');
                $pdf->Text($conexion_ + 305, $y_ + 636, $textBarcode);

                // TOTAL DEUDA ANTERIOR
                // if ($deuda != 0){
                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetY($y_ + 695);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, $prefijo . str_pad(($factura_no_cuenta_cobro), 9, "0", STR_PAD_LEFT), 0);

                $pdf->SetY($y_ + 705);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, $result['cod_pred'], 0);

                $pdf->SetY($y_ + 716);
                $pdf->SetX($conexion_ + 122);
                $pdf->Cell(0, 0, $result['fecvto'], 0); // FECHA PAGO OPORTUNO

                $pdf->SetY($y_ + 726);
                $pdf->SetX($conexion_ + 100);
                $pdf->Cell(0, 0, '$' . number_format($deuda, 2, ',', '.'), 0);

                // TOTAL DEUDA ANTERIOR Codigo de barras
                // $barcode= "415".$recuaudo_deuda_anterior."8020".$prefijo.str_pad($factura_no_cuenta_cobro,9,"0",STR_PAD_LEFT)."3902".str_pad(number_format($deuda,0,'',''),8,"0",STR_PAD_LEFT)."96".str_replace("-","",$result['fecvto']);
                // $textBarcode = "(415)".$recuaudo_deuda_anterior."(8020)".$prefijo.str_pad($factura_no_cuenta_cobro,9,"0",STR_PAD_LEFT)."(3902)".str_pad(number_format($deuda,0,'',''),8,"0",STR_PAD_LEFT)."96".str_replace("-","",$result['fecvto']);

                $barcode = chr(241) . "415" . $recuaudo_deuda_anterior . "8020" . $prefijo . str_pad($factura_no_cuenta_cobro, 9, "0", STR_PAD_LEFT) . chr(241) . "3900" . str_pad(number_format($deuda, 0, '', ''), 10, "0", STR_PAD_LEFT) . chr(241) . "96" . str_replace("-", "", $result['fecvto']);
                $textBarcode = "(415)" . $recuaudo_deuda_anterior . "(8020)" . $prefijo . str_pad($factura_no_cuenta_cobro, 9, "0", STR_PAD_LEFT) . "(3900)" . str_pad(number_format($deuda, 0, '', ''), 10, "0", STR_PAD_LEFT) . "(96)" . str_replace("-", "", $result['fecvto']);

                $pdf->write1DBarcode($barcode, 'C128', $conexion_ + 207, $y_ + 695, 340, 30, 1, $style, 'N');
                $pdf->SetFontSize('6');
                $pdf->Text($conexion_ + 305, $y_ + 721, $textBarcode);


                // INICIO DATOS PAGINA 2
                $total_concepto_acueducto       = 0;
                $total_concepto_alcantarillado  = 0;
                $total_otros_conceptos          = 0;
                $total_otros                    = 0;
                $tc1 = 0;
                $tc2 = 0;
                $tc3 = 0;
                $tca1 = 0;
                $tca2 = 0;
                $tca3 = 0;
                $secy = 0;
                $unidades = "";

                // DETALLES CONCEPTOS DE LA FACTURA
                $sql = "SELECT * FROM factura_det
                        WHERE cod_pred=" . $result['cod_pred'] . " and
                            cod_peri=" . $cod_peri . " and 
                            cod_empr='" . $empresa . "' and 
                            cod_munip=" . $municipio . " and nro_seq = " . $result['nro_seq'] . " order by cod_conc ";

                $exe_detalle = $conexion->prepare($sql);

                // add a page 2
                $pdf->setPrintHeader(false);
                $pdf->AddPage();
                // -- set new background ---
                // get the current page break margin
                $bMargin = $pdf->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $pdf->getAutoPageBreak();
                // disable auto-page-break
                $pdf->SetAutoPageBreak(false, 0);
                // set bacground image

                //IMAGEN DEL DORSO------------------
                if ($cod_peri >= 201904 and $cod_peri <= 201906) {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/ingrid/ESSMAR_FACTURA-02.jpg';
                } else if ($cod_peri > 201906 and $cod_peri <= 201912) {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/jose/ESSMAR_FACTURA-02.jpg';
                } else if ($cod_peri >= 202001 and $cod_peri <= 202002) {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/carlos/ESSMAR_FACTURA-02.jpg';
                } else if ($cod_peri > 202002 and $cod_peri <= 202012) {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/jose/ESSMAR_FACTURA-02.jpg';
                } else if ($cod_peri > 202012 and $cod_peri <= 202106) {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/carlos/ESSMAR_FACTURA-02.jpg';
                } else if (($cod_peri == 202110 and $cicloFondo == '415') || $cod_peri >= 202111) {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/yahaira/ESSMAR_FACTURA-02.jpg';
                } else {
                    $img_file = $_SERVER["DOCUMENT_ROOT"] . 'archivos_pqr/assets/library/PDF/fondo/carmen/ESSMAR_FACTURA-02.png';
                }

                $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);

                // restore auto-page-break status
                $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $pdf->setPageMark();
                $exe_detalle->execute();
                while ($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)) {

                    $suapoR1    = $result_det['subapo_r1'];
                    $suapoR2    = $result_det['subapo_r2'];
                    $suapoR3    = $result_det['subapo_r3'];

                    $suapoCF    = $result_det['subapo_cfijo'];

                    //--sumatoria para el resumen
                    if ($result_det['cod_conc'] == 103) { //acueducto
                        $subapo_t1    = $result_det['subapo_tasa_r1'];
                        $subapo_t2    = $result_det['subapo_tasa_r2'];
                        $subapo_t3    = $result_det['subapo_tasa_r3'];

                        $tasaUso    = $result_det['tasa_ur'];

                        $total_concepto_acueducto += $result_det['total_concp'];
                        //echo $total_concepto_acueducto;
                    }
                    if ($result_det['cod_conc'] == 104) { //alcantarillado
                        $subapo_t1    = $result_det['subapo_tasa_r1'];
                        $subapo_t2    = $result_det['subapo_tasa_r2'];
                        $subapo_t3    = $result_det['subapo_tasa_r3'];

                        $tasaUso    = $result_det['tasa_ur'];
                    }

                    if ($result_det['cod_conc'] == 2) { //alcantarillado
                        $total_concepto_alcantarillado += $result_det['total_concp'];
                    }
                    if (($result_det['cod_conc'] != 1) and ($result_det['cod_conc'] != 2) and ($result_det['cod_conc'] != 100)) {
                        $total_otros_conceptos += $result_det['total_concp'];
                    }

                    //--liquidacion Acueducto
                    if ($result_det['cod_conc'] == $acueducto) {
                        $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                        $tc1 = $result_det['canti_e1'] * $result_det['precioe1'];
                        $tc2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                        $tc3 = $result_det['canti_e3'] * $result_det['precioe3'];

                        $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                        $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                        $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                        $sqlacu = "SELECT fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref, fpd.tasa_ur_r1
                                from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                    fpc.cod_peri=" . $cod_peri . " and 
                                    fpc.cod_munip=" . $municipio . " and 
                                    fpc.cod_empr='" . $empresa . "' and
                                    fpd.cod_conc=" . $acueducto;

                        $sql_acu = $conexion->prepare($sqlacu) or die("Error: Detalle Acueducto \n" . $sqlacu);
                        $sql_acu->execute();
                        $data_alc = $sql_acu->fetch();

                        $cod_concep_acu = $data_alc['cod_conc']; //codigo cocepto acueducto
                        $uso_acu        = $data_alc['cod_uso']; //codigo cocepto acueducto
                        $cod_estado_acu = $data_alc['cod_estd']; //codigo estado acueducto
                        $fec_lec_acu    = $data_alc['feclectura']; //codigo estado acueducto
                        $fec_lec_ant_acu = $data_alc['fec_lectura_ant']; //codigo estado acueducto
                        $cod_tari_acu   = $data_alc['cod_tarf_ref']; //codigo tarifa acueducto
                        $cod_uso_acu    = $data_alc['cod_uso_ref']; //codigo uso acueducto
                        $codtarf        = $data_alc['cod_tarf'];
                        //--calcular tarifas
                        $sq_cal_tari_acu1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_acu', $cod_tari_acu, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                        $sql_cal_tari_acu1 = $conexion->prepare($sq_cal_tari_acu1);
                        $sql_cal_tari_acu1->execute();
                        $cod_cal_tari_acu = $sql_cal_tari_acu1->fetch();

                        $sq_cal_tari_acu2 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$uso_acu', $codtarf, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd')," . $cod_peri . ")";
                        $sql_cal_tari_acu2 = $conexion->prepare($sq_cal_tari_acu2);
                        $sql_cal_tari_acu2->execute();
                        $cod_cal_tari_acu2 = $sql_cal_tari_acu2->fetch();
                        $resto2 = str_replace("{", " ", $cod_cal_tari_acu2['fact_calcular_tarifa']);
                        $cal_tari_acu2 = explode(",", $resto2);

                        $resto = str_replace("{", " ", $cod_cal_tari_acu['fact_calcular_tarifa']);
                        $cal_tari_acu = explode(",", $resto); //si fecha esta en formato dia-mes-año 

                        $consu_basic1   = $cal_tari_acu[1];
                        $consu_comple1  = $cal_tari_acu[2];
                        $consu_sunt1    = $cal_tari_acu[3];
                        $cargo_fijo1    = $cal_tari_acu[0];

                        $porcen         = $cal_tari_acu2[5];

                        //Aqui debo tomar el dato de tasa 
                        $ta_r1 = $cal_tari_acu[15];


                        $sum_sub_tasa_acu = $subapo_t1 + $subapo_t2 + $subapo_t3;

                        $sum_sub = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF + $sum_sub_tasa_acu;

                        //--acueducto consumo
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + 71);
                        $pdf->SetX($conexion_ + 130);
                        $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                        $pdf->SetY($y_ + 82);
                        $pdf->SetX($conexion_ + 130);
                        //if ($tc1 != 0){
                        $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 94);
                        $pdf->SetX($conexion_ + 130);
                        //if ($tc2 != 0){
                        $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 106);
                        $pdf->SetX($conexion_ + 130);
                        //if ($tc3 != 0){
                        $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 117);
                        $pdf->SetX($conexion_ + 130);
                        $pdf->Cell(0, 0, ' ', 0);

                        //suma total
                        $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 125);
                        $pdf->SetX($conexion_ + 130);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--acueducto tarifa referencia
                        $pdf->SetY($y_ + 71);
                        $pdf->SetX($conexion_ + 187);
                        $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo1, 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 82);
                        $pdf->SetX($conexion_ + 187);
                        //if ($tc1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($consu_basic1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 94);
                        $pdf->SetX($conexion_ + 187);
                        //if ($tc2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($consu_comple1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 106);
                        $pdf->SetX($conexion_ + 187);
                        //if ($tc3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 117);
                        $pdf->SetX($conexion_ + 187);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--acueducto factor
                        //if ($tc1 != 0){
                        if ($porcen >= 0) {
                            $pdf->SetY($y_ + 71);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 82);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 94);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 106);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0, 0, 'L', 0);
                        } else {
                            $pdf->SetY($y_ + 71);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 82);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                        }

                        //--acueducto valor
                        $pdf->SetY($y_ + 71);
                        $pdf->SetX($conexion_ + 335);
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 82);
                        $pdf->SetX($conexion_ + 335);
                        //if ($tc1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 94);
                        $pdf->SetX($conexion_ + 335);
                        //if ($tc2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 106);
                        $pdf->SetX($conexion_ + 335);
                        //if ($tc3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 117);
                        $pdf->SetX($conexion_ + 335);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--acueducto valor Total
                        $pdf->SetFont($data_font_type, '', 8);
                        $pdf->SetY($y_ + 137);
                        $pdf->SetX($conexion_ + 335);
                        $pdf->Cell(0, 0, '$ ' . number_format($sum_sub, 2, ',', '.'), 0); //total subaporte

                        //--acueducto tarifa aplicada
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + 71);
                        $pdf->SetX($conexion_ + 435);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 82);
                        $pdf->SetX($conexion_ + 435);;
                        //if ($tc1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['precioe1'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 94);
                        $pdf->SetX($conexion_ + 435);
                        //if ($tc2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['precio_e2'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 106);
                        $pdf->SetX($conexion_ + 435);
                        //if ($tc3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['precioe3'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 117);
                        $pdf->SetX($conexion_ + 435);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--acueducto valor a pagar
                        $pdf->SetY($y_ + 71);
                        $pdf->SetX($conexion_ + 510);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 82);
                        $pdf->SetX($conexion_ + 510);
                        //if ($tc1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($tc1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 94);
                        $pdf->SetX($conexion_ + 510);
                        //if ($tc2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($tc2, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 106);
                        $pdf->SetX($conexion_ + 510);
                        //if ($tc3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($tc3, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 117);
                        $pdf->SetX($conexion_ + 510);

                        $pdf->Cell(0, 0, '$ ' . number_format(($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3']) *  $result_det['tasa_ur_r1'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 130);
                        $pdf->SetX($conexion_ + 510);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--Total concepto acueducto
                        $pdf->SetFont($data_font_type, '', 8);
                        $pdf->SetY($y_ + 137);
                        $pdf->SetX($conexion_ + 510);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                    }

                    //--Liquidacion Alcantarillado
                    if ($result_det['cod_conc'] == $alcantarillado) {
                        $unidades   = ($unidades == "") ? $result_det['nro_unidades'] : $unidades;
                        $tca1 = $result_det['canti_e1'] * $result_det['precioe1'];
                        $tca2 = $result_det['canti_e2'] * $result_det['precio_e2'];
                        $tca3 = $result_det['canti_e3'] * $result_det['precioe3'];

                        $ta_u1 = $result_det['canti_e1'] * $result_det['tasa_ur_r1'];
                        $ta_u2 = $result_det['canti_e2'] * $result_det['tasa_ur_r2'];
                        $ta_u3 = $result_det['canti_e3'] * $result_det['tasa_ur_r3'];

                        $sqlalc = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                    fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                    from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                    where fpc.cod_pred=" . $result['cod_pred'] . " and 
                                        fpc.cod_peri=" . $cod_peri . " and 
                                        fpc.cod_munip=" . $municipio . " and 
                                        fpc.cod_empr='" . $empresa . "' and
                                        fpd.cod_conc=" . $alcantarillado;
                        $sql_alc = $conexion->prepare($sqlalc) or die("Error: Detalle Alcantarillado \n" . $sqlalc);
                        $sql_alc->execute();
                        $datos_alc = $sql_alc->fetch();

                        $cod_concep_alc = $datos_alc['cod_conc'];
                        $uso_alc        = $datos_alc['cod_uso'];
                        $codtarf        = $datos_alc['cod_tarf'];
                        $cod_estado_alc = $datos_alc['cod_estd'];
                        $fec_lec_alc    = $datos_alc['feclectura'];
                        $fec_lec_ant_alc = $datos_alc['fec_lectura_ant'];
                        $cod_tari_alc   = $datos_alc['cod_tarf_ref'];
                        $cod_uso_alc    = $datos_alc['cod_uso_ref'];

                        $sq_cal_tari_alc1 = "SELECT fact_calcular_tarifa('" . $empresa . "', " . $municipio . ", '$cod_uso_alc', $cod_tari_alc, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                        $sql_cal_tari_alc1 = $conexion->prepare($sq_cal_tari_alc1);
                        $sql_cal_tari_alc1->execute();

                        $cod_cal_tari_alc = $sql_cal_tari_alc1->fetch();
                        $resto1 = str_replace("{", " ", $cod_cal_tari_alc['fact_calcular_tarifa']);
                        $cal_tari = explode(",", $resto1); //si fecha esta en formato dia-mes-año 

                        $sq_cal_tari_alca2 = "SELECT fact_calcular_tarifa('" . $empresa . "'," . $municipio . ", '$uso_alc', $codtarf, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd')," . $cod_peri . ")";
                        $sql_cal_tari_alca2 = $conexion->prepare($sq_cal_tari_alca2);
                        $sql_cal_tari_alca2->execute();

                        $cod_cal_tari_alca2 = $sql_cal_tari_alca2->fetch();
                        $resto2 = str_replace("{", " ", $cod_cal_tari_alca2['fact_calcular_tarifa']);
                        $cal_tari_alca2 = explode(",", $resto2);

                        $consu_basic    = $cal_tari[1];
                        $consu_comple   = $cal_tari[2];
                        $consu_sunt     = $cal_tari[3];
                        $cargo_fijo     = $cal_tari[0];
                        $porcen         = $cal_tari_alca2[5];

                        //Aqui debo tomar el dato de tasa 
                        $ta_r1 = $cal_tari[15];
                        $sum_sub_tasa_alc = $subapo_t1 + $subapo_t2 + $subapo_t3;
                        $sum_sub = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF + $sum_sub_tasa_alc;

                        //--alcantarillado consumo
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + 203);
                        $pdf->SetX($conexion_ + 130);
                        $pdf->Cell(0, 0, number_format($result_det['canti_fija'], 0, ',', '.'), 0);
                        $pdf->SetY($y_ + 215);
                        $pdf->SetX($conexion_ + 130);
                        //if ($tca1 != 0){
                        $pdf->Cell(0, 0, number_format($result_det['canti_e1'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 227);
                        $pdf->SetX($conexion_ + 130);
                        //if ($tca2 != 0){
                        $pdf->Cell(0, 0, number_format($result_det['canti_e2'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 239);
                        $pdf->SetX($conexion_ + 130);
                        //if ($tca3 != 0){
                        $pdf->Cell(0, 0, number_format($result_det['canti_e3'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 250);
                        $pdf->SetX($conexion_ + 130);
                        $pdf->Cell(0, 0, ' ', 0);

                        $pdf->Cell(0, 0, number_format($result_det['canti_e1'] + $result_det['canti_e2'] + $result_det['canti_e3'], 0, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 270);
                        $pdf->SetX($conexion_ + 130);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--alcantarillado tarifa referencia
                        $pdf->SetY($y_ + 203);
                        $pdf->SetX($conexion_ + 187);
                        $pdf->Cell(0, 0, '$ ' . number_format($cargo_fijo, 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 215);
                        $pdf->SetX($conexion_ + 187);
                        //if ($tca1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($consu_basic, 2, ',', '.'), 0); //} 
                        $pdf->SetY($y_ + 227);
                        $pdf->SetX($conexion_ + 187);
                        //if ($tca2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($consu_comple, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 239);
                        $pdf->SetX($conexion_ + 187);
                        //if ($tca3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($consu_sunt, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 250);
                        $pdf->SetX($conexion_ + 187);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--alcantarillado factor
                        //if ($tca1 != 0){
                        if ($porcen >= 0) {
                            $pdf->SetY($y_ + 203);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 215);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);

                            $pdf->SetY($y_ + 227);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 239);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);

                            $pdf->SetY($y_ + 249);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0, 0, 'L', 0);
                        } else {
                            $pdf->SetY($y_ + 203);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);
                            $pdf->SetY($y_ + 215);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0);

                            $pdf->SetY($y_ + 249);
                            $pdf->SetX($conexion_ + 245);
                            $pdf->Cell(0, 0, number_format($porcen, 0, ',', '.'), 0, 0, 'L', 0);
                        }
                        $sum_sub_1 = $suapoR1 + $suapoR2 + $suapoR3 + $suapoCF + $sum_sub_tasa_alc;

                        //--alcantarillado valor
                        $pdf->SetY($y_ + 203);
                        $pdf->SetX($conexion_ + 335);
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoCF, 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 215);
                        $pdf->SetX($conexion_ + 335);
                        //if ($tca1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoR1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 227);
                        $pdf->SetX($conexion_ + 335);
                        //if ($tca2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoR2, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 239);
                        $pdf->SetX($conexion_ + 335);
                        //if ($tca3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($suapoR3, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 250);
                        $pdf->SetX($conexion_ + 335);

                        $pdf->Cell(0, 0, '$ ' . number_format($sum_sub_tasa_alc, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 130);
                        $pdf->SetX($conexion_ + 335);
                        $pdf->Cell(0, 0, ' ', 0);

                        //--alcantarillado valor Total
                        $pdf->SetFont($data_font_type, '', 8);
                        $pdf->SetY($y_ + 270);
                        $pdf->SetX($conexion_ + 335);
                        $pdf->Cell(0, 0, '$ ' . number_format($sum_sub_1, 2, ',', '.'), 0);

                        //--alcantarillado tarifa aplicada
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + 203);
                        $pdf->SetX($conexion_ + 435);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 215);
                        $pdf->SetX($conexion_ + 435);
                        //if ($tca1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['precioe1'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 227);
                        $pdf->SetX($conexion_ + 435);
                        //if ($tca2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['precio_e2'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 239);
                        $pdf->SetX($conexion_ + 435);
                        //if ($tca3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['precioe3'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 250);
                        $pdf->SetX($conexion_ + 435);

                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['tasa_ur_r1'], 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 260);
                        $pdf->SetX($conexion_ + 435);
                        $pdf->Cell(0, 0, ' ', 0);;

                        //--alcantarillado valor a pagar
                        $pdf->SetY($y_ + 203);
                        $pdf->SetX($conexion_ + 510);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['cargofijo'], 2, ',', '.'), 0);
                        $pdf->SetY($y_ + 215);
                        $pdf->SetX($conexion_ + 510);
                        //if ($tca1 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($tca1, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 227);
                        $pdf->SetX($conexion_ + 510);
                        //if ($tca2 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($tca2, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 239);
                        $pdf->SetX($conexion_ + 510);
                        //if ($tca3 != 0){
                        $pdf->Cell(0, 0, '$ ' . number_format($tca3, 2, ',', '.'), 0); //}
                        $pdf->SetY($y_ + 250);
                        $pdf->SetX($conexion_ + 510);
                        $pdf->Cell(0, 0, ' ', 0);
                        //--Alcantarillado Total a pagar
                        $pdf->SetFont($data_font_type, '', 8);
                        $pdf->SetY($y_ + 270);
                        $pdf->SetX($conexion_ + 510);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                    }

                    //--Liquidacion Otros conceptos
                    if (($result_det['cod_conc'] != $acueducto) and ($result_det['cod_conc'] != $alcantarillado)) {
                        $pdf->SetFont($data_font_type, '', 6);
                        $pdf->SetY($y_ + $secy + 337);
                        $pdf->SetX($conexion_ + 33);
                        $pdf->Cell(0, 0, $result_det['descripcion'], 0);

                        $pdf->SetY($y_ + $secy + 337);
                        $pdf->SetX($conexion_ + 500);
                        $pdf->Cell(0, 0, '$ ' . number_format($result_det['total_concp'], 2, ',', '.'), 0);
                        $total_otros += $result_det['total_concp'];
                        $secy += 10;
                    }
                }

                //Anexo la cuota del convenio
                if ($codconv > 0) {
                    $pdf->SetFont($data_font_type, '', 6);
                    $pdf->SetY($y_ + $secy + 342);
                    $pdf->SetX($conexion_ + 33);
                    $pdf->Cell(0, 0, 'Numero de Cuota ' . $nrocuota_conv . ' / ' . $cantcuotas_conv, 0);
                    $pdf->SetY($y_ + $secy + 342);
                    $pdf->SetX($conexion_ + 500);  //$pdf->SetX($conexion_+145);
                    $pdf->Cell(0, 0, '$ ' . number_format($val_conv, 2, ',', '.'), 0);
                    $total_otros += $val_conv;
                    $secy += 10;
                }

                $pdf->SetFont($data_font_type, '', 8);
                $pdf->SetY($y_ + 428);
                $pdf->SetX($conexion_ + 500);
                $pdf->Cell(0, 0, '$ ' . number_format($total_otros, 2, ',', '.'), 0);

                // $msj1 = "En cumplimiento del artículo 4 del Decreto 580 de abril de 2020 la ESSMAR le da la opción a sus usuarios de aportar recursos de forma voluntaria para financiar las medidas adoptadas en el";
                $msj1 = "";

                $pdf->SetFont($data_font_type, '', 7);
                $pdf->SetY($y_ + 443);
                $pdf->SetX($conexion_ + 25);
                $pdf->Cell(0, 0, $msj1, 0);

                // $msj2 = "marco de la Emergencia Económica, Social y Ecológica, los cuales se destinarán a alimentar los fondos de solidaridad y redistribución de ingresos de estos servicios en cada municipio. Si desea";
                $msj2 = "";

                $pdf->SetY($y_ + 450);
                $pdf->SetX($conexion_ + 25);
                $pdf->Cell(0, 0, $msj2, 0);

                // $msj3 = "aportar recursos comuníquese a las líneas 3008721133 y 3046077237.";
                $msj3 = "";

                $pdf->SetY($y_ + 457);
                $pdf->SetX($conexion_ + 25);
                $pdf->Cell(0, 0, $msj3, 0);
                // FIN DATOS PAGINA 2

            }
        }
        $pqr_ruta = 'essmar/tmp/' . $pqr;
        if ($tipoPQR == 'P') { // En caso la PQR sea padre, debe ser la misma carpeta de archivos de la PQR hija
            $pqr_ruta = 'essmar/tmp/' . $_GET['pqr'];
        }

        if (!file_exists($pqr_ruta)) {
            mkdir($pqr_ruta, 0777, true);
        }
        $filename = $pqr_ruta . "/" . $nro_factura . "-" . $pqr . ".pdf";
        $pdf->Output($filename, 'F', true);
        //Close and output PDF document
    }
}

function GraficaHistorica($conexion, $pdf, $predio, $_periodo, $ciclo, $municipio, $empresa)
{
    $l          = 1;
    $largomin   = 3;
    $dra        = 0.5;
    $conta      = 0;
    $largo_1    = 0;
    $historico  = 38;
    $cont = -8;
    $ancho      = 15;
    $py         = 460;
    $py1        = 399;
    $mesnum     = date('m');
    $calmes     = $mesnum - 5;
    $query_historico = $conexion->prepare("select * from historico(" . $predio . ",'" . $empresa . "'," . $municipio . "," . $_periodo . ",'" . $ciclo . "')");
    //$num=odbc_num_rows($query_historico);

    $query_max_hist = $conexion->prepare("select max(consumo) AS maxcon from historico(" . $predio . ",'" . $empresa . "'," . $municipio . "," . $_periodo . ",'" . $ciclo . "')");
    $query_max_hist->execute();
    $data_hits = $query_max_hist->fetch();
    $max_con = $data_hits['maxcon'];

    $rs_max_peri_con = $conexion->prepare("select max(periodo) AS maxperi from historico(" . $predio . ",'" . $empresa . "'," . $municipio . "," . $_periodo . ",'" . $ciclo . "')");
    $rs_max_peri_con->execute();
    $data_peri = $rs_max_peri_con->fetch();
    $max_peri = $data_peri['maxperi'];

    $query_historico->execute();
    while ($result_h = $query_historico->fetch(PDO::FETCH_ASSOC)) {
        $conta = $conta + 1;
        $cod_peris = $result_h['periodo'];
        if ($max_con == null or $max_con == 0) {
            $largo_1 = 0;
        } else {
            if ($result_h['consumo'] == 0) {
                $consumo = 1;
            } else {
                $consumo = $result_h['consumo'];
            }
            $max_pro = $max_con;
            $por = ($consumo / $max_pro) * 100;
            $por1 = ((int)$por);
            $largo_1 = $largomin + ($por1 * $dra);
        }
        if ($cod_peris == $max_peri) {
            $pdf->SetFillColor(112, 128, 144);
            $pdf->SetDrawColor(0, 0, 0);
            if ($largo_1 == 0) {
                $ceroY = 10;
            } else {
                $ceroY = 0;
            }
        } else {
            $pdf->SetFillColor(220, 220, 220);
            $pdf->SetDrawColor(0, 0, 0);
            if ($largo_1 == 0) {
                $ceroY = 7;
            } else {
                $ceroY = 0;
            }
        }
        $pdf->SetY($py - $largo_1 - $ceroY);
        $pdf->SetX($historico + 13); //barra
        $pdf->Cell($ancho, $largo_1, ' ', 0, 0, 'L', 1); //consumo
        $pdf->SetFontSize('6');
        $pdf->SetY(400);
        $pdf->SetX($historico + 13);
        $pdf->Cell($ancho, 7, number_format($result_h['consumo'], 0, ',', '.'), 0, 1, 'L', 0); //consumo 

        // consulta para total del perido
        $sql_facturado = "SELECT DISTINCT total FROM factura_per_cab WHERE cod_pred = $predio AND cod_peri = " . $result_h['periodo'];
        $query_facturado = $conexion->prepare($sql_facturado);
        $query_facturado->execute();
        $data_fact = $query_facturado->fetch(PDO::FETCH_ASSOC);
        $total_periodo = $data_fact['total'];
        if (round($total_periodo) == 0) {
            $totalP_X = 0;
        } else {
            $totalP_X = 7;
        }

        $pdf->SetFontSize('6');
        $pdf->SetY(465);
        $pdf->SetX($historico + $totalP_X);
        $pdf->Cell(20, 7, "$" . number_format($total_periodo, 0, ',', '.'), 0, 1, 'L', 0); //total periodo

        $sql = "select cod_peri,descripcion
                from periodo_fac
                where
                cod_empr='" . $empresa . "' and 
                cod_munip='" . $municipio . "' and
                cod_peri='$cod_peris'";

        $query_peri = $conexion->prepare($sql);
        $query_peri->execute();
        while ($result_p = $query_peri->fetch(PDO::FETCH_ASSOC)) {
            $pdf->SetFontSize('6');
            $nommes = Mes(substr($result_p['cod_peri'], -2)); //echo $result_p['cod_peri']." - ";
            $pdf->SetY(475);
            $pdf->SetX($historico + 13);
            $pdf->Cell(20, 7, $nommes, 0, 1, 'L', 0); //nombre mes
            $calmes = $calmes + 1;
        }
        //$pdf->SetFontSize('8');
        $historico = $historico +  40;

        /*if ($cod_peris!=$max_peri){
            $pro = $pro + $result_h['consumo'];							
        }*/
    } //fin historico       
}

function Mes($mes_numero)
{
    switch ($mes_numero) {
        case '01':
            $mes = "ENE";
            break;
        case '02':
            $mes = "FEB";
            break;
        case '03':
            $mes = "MAR";
            break;
        case '04':
            $mes = "ABR";
            break;
        case '05':
            $mes = "MAY";
            break;
        case '06':
            $mes = "JUN";
            break;
        case '07':
            $mes = "JUL";
            break;
        case '08':
            $mes = "AGO";
            break;
        case '09':
            $mes = "SEP";
            break;
        case '10':
            $mes = "OCT";
            break;
        case '11':
            $mes = "NOV";
            break;
        case '12':
            $mes = "DIC";
            break;
    }
    return $mes;
}
