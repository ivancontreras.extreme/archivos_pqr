<?php

require_once('../assets/library/config/tcpdf_config_alt.php');
require_once('../assets/library/tcpdf/config/lang/eng.php');
require_once('../assets/library/tcpdf_nueva/tcpdf.php');
require_once('../assets/library/tcpdf_nueva/tcpdf_barcodes_1d.php');

require '../assets/database/conexion.php';
$conexion = conectar('essmar');

array_map('unlink', glob("C:/AppServ/www/kagua/archivos/facturacion/*.pdf"));



//--Propiedades PDF
$orientacion_papel="P";
$medida="pt"; 
$formato="A4";
$unicode=true;
$codificacion="UTF-8";
$clase="TCPDF";
$autor="Kagua";
$titulo="Factura";
$margen_izq="0";
$margen_der="0";
$margen_sup="0";
$margen_inf="0";
$margen_encabezado="0";
$margen_pie="0";
$data_font_size="8";
$data_font_type="FreeSerif";
$encbz_font_size="6";
$peq_font_size="3";
$print_encbz_pg=true;
$print_pie_pg=true;
$y_ = 25;
$conexion_ = 7;
$img_jpg = 0;

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
	//Page header
	public function Header() {
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // $auto_page_break = $this->getAutoPageBreak();
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
        
        // set bacground image
		
		//IMAGEN DEL FRENTE-------------------
		$img_file = 'ESSMAR_FACTURA-01.jpg';
		// $this->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);
		
		// restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();
	}
}

// create new PDF document
$pdf = new MYPDF($orientacion_papel, $medida, $formato, $unicode, $codificacion, false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kagua');
$pdf->SetTitle('Factura ESSMAR - Kagua');
$pdf->SetSubject('Factura ESSMAR');
$pdf->SetKeywords('Factura ESSMAR');
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);
// remove default footer
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
$pdf->setLanguageArray($l);

// ---------------------------------------------------------


// Datos

$cod_pred = $_GET['poliza'];
$cod_peri = $_GET['periodo'];
//$cod_lote = $_REQUEST['cod_lote'];

$lotes = 500;
$paginas = 0;
$orden = "";

$sql_ciclo = "SELECT p.cod_cclo, p.cod_munip, p.cod_empr FROM predio p WHERE p.cod_pred = $cod_pred";
$query_ciclo = $conexion->prepare($sql_ciclo);
$query_ciclo->execute();
$data_ini = $query_ciclo->fetch();

$cod_cclo = $data_ini['cod_cclo'];
$empresa = $data_ini['cod_empr'];
$municipio = $data_ini['cod_munip'];
$cod_lote = 0;

//--Consultas Globales
$sql = "SELECT cod_conc_ppal,cod_conc_alc,cod_conc_aseo,cod_org,cod_ajuste_dec, codsaldfavor
        FROM empr_parametros 
        WHERE cod_empr='".$empresa."';";

$query_empr = $conexion->prepare($sql);
$query_empr->execute();
$datos = $query_empr->fetch();

$acueducto	=  $datos["cod_conc_ppal"];
$alcantarillado	=  $datos["cod_conc_alc"];
$cod_org        =  $datos["cod_org"];
$ajuste_dec	=  $datos["cod_ajuste_dec"];
$cod_sal_fav	=  $datos["codsaldfavor"];

//--Rangos de Consumo
$sql = "SELECT frecuencia_fact, tope_rango_1,tope_rango_2,codigo_recaudo, codigo_recaudo_, codigo_recaudo__
        FROM municipio 
        WHERE cod_munip=".$municipio." and cod_empr='".$empresa."'";
$query_r = $conexion->prepare($sql);
$query_r->execute();
$datos_consumo = $query_r->fetch();

$recuaudo_corriente = $datos_consumo["codigo_recaudo"];
$recuaudo_deuda_total = $datos_consumo["codigo_recaudo_"];
$recuaudo_deuda_anterior = $datos_consumo["codigo_recaudo__"];
$frecuencia = $datos_consumo["frecuencia_fact"];

$pag =($paginas=="")?0:$paginas;

switch ($orden){
    case "ruta":
        $orden = " fpc.rutareparto ";
        break;
    case "barrio":
        $orden = " b.descripcion, fpc.direccion";
        break;
    default :
       $orden = "fpc.rutareparto";
}

//--Consultar Facturacion BD
if( $cod_pred != ""){
    $q  = " and fpc.cod_pred = ".$cod_pred." ";
    // $q  = " fpc.cod_pred in (15573,15627,15678,15681,15688,15695,15698,15700,30067,30073) and ";
    // $q  = " fpc.cod_pred in (11354) and "; //11354
    // echo"aaaa";
}
if( $cod_cclo != ""){
    $q  .= " and fpc.cod_cclo = '".$cod_cclo."' ";
    // echo"bbbb";
}
if( $cod_lote != ""){
    $q .= " and fpc.cod_lote=$cod_lote ";
    // echo"cccc";
}
  
$sql = "select fpc.cod_pred,fpc.cod_peri,fpc.nro_factura,fpc.fecexp,fpc.fecvto,fpc.feclectura,fpc.fec_lectura_ant,
			fpc.fec_susp,fpc.fecabre,fpc.feccierra,fpc.total,fpc.total_sin_descto,fpc.lectura,
			fpc.lectura_anterior,fpc.consumo,fpc.cons_ajustado,fpc.consttalizador,fpc.promedio,
			fpc.cons_manual,fpc.cons_fact,fpc.metodo,fpc.nro_seq,fpc.cod_cclo,fpc.cod_lote,
			fpc.nombre,fpc.direccion,fpc.estrato,fpc.rutareparto,fpc.nropermora,fpc.cod_pred_totalizador,
			fpc.dias_facturados, Coalesce(cod_obsl_i,cod_obsl_l) as obs_lectura,
			fpc.mensaje,fpc.total_mas_cartera,fpc.deuda,fpc.tipo_sector,fpc.total,
            fpc.cod_peri_ult_pago,
            fpc.fch_ult_pago,fpc.valor_ult_pago,fpc.nro_cuenta_cobro,
			p.fecinstalmedi,p.serialmedi,m.descripcion as marca,cb.descripcion as calibre,
			b.descripcion as barrio,c.descripcion as ciclo,u.descripcion as uso,fpc.deuda_e1,fpc.deuda_e3, fpc.nro_cuenta_cobro,
			consumo_hist1, consumo_hist2, consumo_hist3,consumo_hist4,consumo_hist5,consumo_hist6,
			coalesce((SELECT cc.cod_conv
			from convenio_cab cc, convenio_cuota ct
			WHERE cc.cod_conv=ct.cod_conv and cc.cod_empr=ct.cod_empr and cc.cod_munip=ct.cod_munip
			AND cc.cod_pred=fpc.cod_pred and cc.cod_munip=fpc.cod_munip 
			and ct.cod_peri_apli=fpc.cod_peri),0) as convenio
		from factura_per_cab fpc 
			join ciclo c using (cod_cclo,cod_munip,cod_empr)
			join medidor m using (cod_medi,cod_clbr,cod_empr)
			join calibre cb using (cod_clbr,cod_empr)
			join uso u using(cod_uso,cod_empr)
			join predio p using(cod_pred,cod_munip,cod_empr)
			join barrio b using(cod_barrio,cod_munip,cod_empr) 
		where 
            fpc.cod_peri=".$cod_peri." and
            fpc.cod_munip=".$municipio." and
            fpc.cod_empr='".$empresa."' 
			".$q."
			order by  ".$orden." limit ".$lotes." offset ". $pag;
			// fpc.cod_empr='".$empresa."' order by  ".$orden." limit 1 offset ". $pag;
  
$query_p = $conexion->prepare($sql);
$exe = $query_p->execute();
//print_r($query_p);die();
if(!$exe){
    $pdf->AddPage();
	$pdf->SetY(100);$pdf->SetX(200);
	$pdf->Cell(0,0,"Error: Consultando los datos de facturacion",0);
}else{
	while($result = $query_p->fetch(PDO::FETCH_ASSOC)){
        //echo "Alone";die();
		$sw_if_ac = 0;
		$sw_if_al = 0;
        $facturado          = $result['total']; 
        $deuda              = $result['deuda_e1'];
		$deuda_ess			= $result['deuda_e3'];
        $nro_cuenta_cobro   = $result['nro_cuenta_cobro'];


		if($result['nombre'] == ""){
            $pdf->AddPage();
			$pdf->SetY(100);$pdf->SetX(200);
			$pdf->Cell(0,0,"Error: Factura sin nombre",0);
		}else{
            // add a page
            $pdf->AddPage();
            if ($img_jpg != 0) {
				//IMAGEN DEL FRENTE DESPUES DE PRIMER PAGINA-------------
                $img_file = 'ESSMAR_FACTURA-01.jpg';
                // $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);
            } $img_jpg = 1;

            // EMPRESA
            $pdf->SetFont($data_font_type,'',8);
            $pdf->SetY($y_+35);$pdf->SetX($conexion_+267);
            $pdf->Cell(0,0,"ESSMAR E.S.P.",0);
            $pdf->SetY($y_+45);$pdf->SetX($conexion_+267);
            $pdf->Cell(0,0,"800181106-1",0);
            $pdf->SetY($y_+56);$pdf->SetX($conexion_+267);
			$pdf->Cell(0,0,"CALLE 22 N° 22- 111",0);

            // INFORMACION DEL CLIENTE 
            $pdf->SetFont($data_font_type,'',10);


			$long_factura=strlen($result['nro_factura']);
			if($long_factura < 10){
				$factura_no = str_pad($result['nro_factura'],9,'0',STR_PAD_LEFT);
				$factura_no = "1".$factura_no;
			}else{
				$factura_no=$factura_no['nro_factura'];
			}


			$pdf->SetY($y_+37);$pdf->SetX($conexion_+486);
			$pdf->Cell(0,0,$factura_no,0);

			$pdf->SetY($y_+87);$pdf->SetX($conexion_+460);
			$pdf->Cell(0,0,$result['cod_pred'],0);
			
			$pdf->SetFont($data_font_type,'',8);
            $pdf->SetY($y_+101);$pdf->SetX($conexion_+460);
			$pdf->Cell(0,0,$result['nombre'],0);
			
			$pdf->SetY($y_+112);$pdf->SetX($conexion_+460);
            $pdf->Cell(0,0,mb_convert_encoding($result['direccion'], "UTF-8","UTF-8"),0);
			
			$pdf->SetY($y_+125);$pdf->SetX($conexion_+460);
            $pdf->Cell(0,0,$result['barrio'],0);

			$pdf->SetY($y_+137);$pdf->SetX($conexion_+460);
            $pdf->Cell(0,0,$result['ciclo'],0);

			$pdf->SetY($y_+147);$pdf->SetX($conexion_+460);
            $pdf->Cell(0,0,$result['uso'],0);

            if( $result['uso'] == 'RESIDENCIAL' ){
                $estrato = ($result['estrato']==0)?"1":$result['estrato'];
            }else {
                $estrato = "";
            }
            $pdf->SetY($y_+161);$pdf->SetX($conexion_+460);
            $pdf->Cell(0,0,$estrato,0);

			$pdf->SetY($y_+173);$pdf->SetX($conexion_+460);
            $rutareparto = ($result['rutareparto'] == "")?"SIN RUTA":$result['rutareparto'];            
			$pdf->Cell(0,0,$rutareparto,0);
			
			
			//if($codconv>0){
			if($result['convenio']>0){
				
				/*Fondo Duplicado*/	
				$pdf->SetFont('freeserif','',50);
				$pdf->SetDrawColor(200);
				$pdf->SetTextColor(200);
				//MARCA DE AGUA
				
				$pdf->StartTransform();
				$pdf->Rotate(40);
				$pdf->Text(140, 40, 'EN CONVENIO');
				//$pdf->Text($px, $py, $txt_web);
				// Stop Transformation
				$pdf->StopTransform();
				
				$pdf->SetTextColor(0,0,0);
			}else{
				$txt_web="DUPLICADO";
				$tm = 50;
				$px = 180;
				$py = 10;
			}
			

			// DATOS DE MEDIDOR 
            $pdf->SetFont($data_font_type,'',6);
            $marca = ($result['marca']=="")?"S/M":$result['marca'];
            $pdf->SetY($y_+119);$pdf->SetX($conexion_+37);
            $pdf->Cell(0,0,$marca,0);

			$pdf->SetY($y_+119);$pdf->SetX($conexion_+125);
            $serial_medidor = ($result['serialmedi'] == "")?"S/S":$result['serialmedi'];
            $pdf->Cell(0,0,$serial_medidor,0);

			$pdf->SetY($y_+119);$pdf->SetX($conexion_+223);
            $pdf->Cell(0,0,$result['calibre'],0);

			$pdf->SetY($y_+119);$pdf->SetX($conexion_+310);
            $pdf->Cell(0,0,$result['fecinstalmedi'],0);

            // LECTURAS Y FECHAS DE LECTURA

			$pdf->SetY($y_+145);$pdf->SetX($conexion_+37);
			$pdf->Cell(0,0,$result['fec_lectura_ant'],0);

            $pdf->SetY($y_+145);$pdf->SetX($conexion_+130);
            $pdf->Cell(0,0,$result['feclectura'],0);

            $pdf->SetY($y_+145);$pdf->SetX($conexion_+223);
            $pdf->Cell(0,0,$result['lectura_anterior'],0);

			$pdf->SetY($y_+145);$pdf->SetX($conexion_+310);
            $pdf->Cell(0,0,$result['lectura'],0);

			$pdf->SetY($y_+172);$pdf->SetX($conexion_+37);
            $pdf->Cell(0,0,number_format($result['consumo'],0,',','.'),0,0,'L',0);

			$pdf->SetY($y_+172);$pdf->SetX($conexion_+130);
            $pdf->Cell(0,0,number_format($result['promedio'],0,',','.'),0,0,'L',0);

			$pdf->SetY($y_+172);$pdf->SetX($conexion_+223);
            $pdf->Cell(0,0,$result['dias_facturados'],0);

            $pdf->SetY($y_+172);$pdf->SetX($conexion_+310);
            $pdf->Cell(0,0,$result['obs_lectura'],0);


			// DATOS DEUDA
			$pdf->SetFont($data_font_type,'',9);
            $pdf->SetY($y_+229);$pdf->SetX($conexion_+28); //33
            $pdf->Cell(0,0,$result['cod_peri'],0);

			$pdf->SetY($y_+228);$pdf->SetX($conexion_+88); //93
            $pdf->Cell(0,0,$result['fecexp'],0);
			
			
			if($result['nropermora']>0){
			 if($result['convenio']>0){
			  $fecvto=$result['fecvto'];
			  $fecsus=$result['fec_susp'];
			 }else{
			  $fecvto="INMEDIATO";
			  $fecsus="INMEDIATO";
			 }
			}else{
			 $fecvto=$result['fecvto'];
			 $fecsus=$result['fec_susp'];
			}
			
			$pdf->SetY($y_+228);$pdf->SetX($conexion_+155); //160
			$pdf->Cell(0,0,$fecvto,0); // FECHA PAGO OPORTUNO

			$pdf->SetY($y_+228);$pdf->SetX($conexion_+225);
            $pdf->Cell(0,0,$result['fch_ult_pago'],0);

			$pdf->SetY($y_+228);$pdf->SetX($conexion_+295);
            $pdf->Cell(0,0,'$ '.number_format($result['valor_ult_pago'],0,',','.'),0);

			$pdf->SetY($y_+228);$pdf->SetX($conexion_+365);
            $pdf->Cell(0,0,$fecsus,0);

			$pdf->SetY($y_+228);$pdf->SetX($conexion_+433);
            $pdf->Cell(0,0,$result['nropermora'],0);


            //$total_pagar = $facturado + $deuda;
			$total_pagar = $deuda_ess + $deuda;
			

            $pdf->SetY($y_+228);$pdf->SetX($conexion_+503);
            $pdf->Cell(0,0,number_format($total_pagar,0,',','.'),0,0,'L',0);//deuda total a pagar


            // TOTAL POR CONCEPTO
            $sql = "SELECT cod_conc,total_concp FROM factura_per_det
                    WHERE cod_pred=".$result['cod_pred']." and
                        cod_peri=".$cod_peri." and 
						cod_munip=".$municipio." and
                        cod_empr='".$empresa."' 
                         order by cod_conc ";

            $exe_detalle = $conexion->prepare($sql);
            $exe_detalle->execute();
            $total_concepto_acueducto       = 0;
            $total_concepto_alcantarillado  = 0;
            $total_otros_conceptos          = 0;
            // $total_veolia                   = 0;
            $total_pag                      = 0;

            while($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)){
                //--sumatoria para el resumen
                if($result_det['cod_conc']==103){// ACUEDUCTO
                    $total_concepto_acueducto += $result_det['total_concp'];
                }
                if($result_det['cod_conc']==104)	{// ALCANTARILLADO
                    $total_concepto_alcantarillado += $result_det['total_concp'];
                }
                // if($result_det['cod_conc']==100)	{// VEOLIA
                //     $total_veolia += $result_det['total_concp'];
                // }
                if(($result_det['cod_conc']!=103) and ($result_det['cod_conc']!=104) ){ // OTROS  --and ($result_det['cod_conc']!=100)
                    $total_otros_conceptos +=$result_det['total_concp'];
                }
            }
            $pdf->SetY($y_+365);$pdf->SetX($conexion_+49);
            $pdf->Cell(0,0,'$'.number_format($total_concepto_acueducto,2,',','.'),0);

            $pdf->SetY($y_+365);$pdf->SetX($conexion_+144);
            $pdf->Cell(0,0,'$'.number_format($total_concepto_alcantarillado,2,',','.'),0);

            $pdf->SetY($y_+371);$pdf->SetX($conexion_+244);
            $pdf->Cell(0,0,'$'.number_format($total_otros_conceptos,2,',','.'),0);

            $pdf->SetY($y_+371);$pdf->SetX($conexion_+420);
            $pdf->Cell(0,0,'$'.number_format($deuda,2,',','.'),0);

            //$total_pag = $total_concepto_acueducto + $total_concepto_alcantarillado + $total_otros_conceptos + $deuda;
			$total_pag = $deuda_ess + $deuda;
            $pdf->SetY($y_+372);$pdf->SetX($conexion_+505);
            $pdf->Cell(0,0,'$'.number_format($total_pag,2,',','.'),0);

			// GRAFICA - HISTORICO DE CONSUMO
            GraficaHistorica($conexion,$pdf,$result['cod_pred'],$cod_peri,$result['cod_cclo'],$municipio,$empresa,$result['consumo_hist1'],$result['consumo_hist2'],$result['consumo_hist3'],$result['consumo_hist4'],$result['consumo_hist5'],$result['consumo_hist6']);

            $style = array(
                'position' => '',
                'align' => 'L',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255),
                'text' => false,
                'font' => 'helvetica',
                'fontsize' => 8,
                'stretchtext' => 4
            );
			
			//LEYENDA
			$pdf->SetFont($data_font_type,'',8);
			$pdf->SetFontSize('8');	
			$msg = "La ESSMAR presta el servicio  de recolección, transporte y disposición final de escombros, material vegetal e inservibles. Teléfonos: 4224915 - 3005792293";
			//$pdf->writeHTMLCell(272,26,302,480,mb_convert_encoding($msg, "UTF-8","UTF-8"),0,'',0,true,'L',true);
			
            // TOTAL A PAGAR DEL MES
            $pdf->SetFont($data_font_type,'',8);

            $pdf->SetY($y_+546);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,$factura_no,0);

            $pdf->SetY($y_+557);$pdf->SetX($conexion_+100);
			$pdf->Cell(0,0,$result['cod_pred'],0);

			$pdf->SetY($y_+567);$pdf->SetX($conexion_+122);
            $pdf->Cell(0,0,$result['fecvto'],0); // FECHA PAGO OPORTUNO

            $pdf->SetY($y_+578);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,'$'.number_format($facturado,2,',','.'),0);
            
            // TOTAL A PAGAR DEL MES Codigo de barras
            //$barcode= "415"."770".$recuaudo_corriente."8020".str_pad($factura_no,10,"0",STR_PAD_LEFT)."3902".number_format($facturado,0,'','')."96".str_replace("-","",$result['fecvto']);	
			$barcode= chr(241)."415".$recuaudo_corriente."8020".str_pad($factura_no,10,"0",STR_PAD_LEFT).chr(241)."3900".str_pad(number_format($facturado,0,'',''),10,"0",STR_PAD_LEFT).chr(241)."96".str_replace("-","",$result['fecvto']);
			
            $textBarcode = "(415)".$recuaudo_corriente."(8020)".str_pad($factura_no,10,"0",STR_PAD_LEFT)."(3900)".str_pad(number_format($facturado,0,'',''),10,"0",STR_PAD_LEFT)."(96)".str_replace("-","",$result['fecvto']);

            $pdf->write1DBarcode($barcode, 'C128', $conexion_+207, $y_+540,270, 35, 1, $style, 'N');
            $pdf->SetFontSize('6');
            $pdf->Text($conexion_+260,$y_+571, $textBarcode);

            // DEUDA TOTAL
            if ($deuda == 0){
			  if($deuda_ess>$facturado){
				if($nro_cuenta_cobro == ""){
				 	$factura_no_cuenta_cobro = $factura_no;
                	$prefijo = "";
				}else{
					$factura_no_cuenta_cobro = $nro_cuenta_cobro;
                	$prefijo = 5;
				}
			  }else{
				$factura_no_cuenta_cobro = $factura_no;
                $prefijo = "";
			  }
            }else {
                $factura_no_cuenta_cobro = $nro_cuenta_cobro;
                $prefijo = 5;
            }
            $pdf->SetFont($data_font_type,'',8);
			$pdf->SetY($y_+632);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,$prefijo.str_pad(($factura_no_cuenta_cobro),9,"0",STR_PAD_LEFT),0);

            $pdf->SetY($y_+642);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,$result['cod_pred'],0);
            
            $pdf->SetY($y_+652);$pdf->SetX($conexion_+122);
            $pdf->Cell(0,0,$result['fecvto'],0); // FECHA PAGO OPORTUNO

            $pdf->SetY($y_+663);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,'$ '.number_format($total_pagar,2,',','.'),0,0,'L',0);//total factura


           
			$barcode= chr(241)."415".$recuaudo_deuda_total."8020".$prefijo.str_pad(($factura_no_cuenta_cobro),9,"0",STR_PAD_LEFT).chr(241)."3900".str_pad(number_format($total_pagar,0,'',''),10,"0",STR_PAD_LEFT).chr(241)."96".str_replace("-","",$result['fecvto']);
			
			
			$textBarcode = "(415)".$recuaudo_deuda_total."(8020)".$prefijo.str_pad(($factura_no_cuenta_cobro),9,"0",STR_PAD_LEFT)."(3900)".str_pad(number_format($total_pagar,0,'',''),10,"0",STR_PAD_LEFT)."(96)".str_replace("-","",$result['fecvto']);

            //$pdf->write1DBarcode($barcode, 'C128', 210, 695, '', 30, 1, $style, 'N');
            $pdf->write1DBarcode($barcode, 'C128', $conexion_+207, $y_+625,270, 35, 1, $style, 'N');
            $pdf->SetFontSize('6');
            $pdf->Text($conexion_+260,$y_+656, $textBarcode);

            // TOTAL DEUDA ANTERIOR
            // if ($deuda != 0){
            $pdf->SetFont($data_font_type,'',8);
            $pdf->SetY($y_+719);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,$prefijo.str_pad(($factura_no_cuenta_cobro),9,"0",STR_PAD_LEFT),0);

            $pdf->SetY($y_+729);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,$result['cod_pred'],0);
            
            $pdf->SetY($y_+739);$pdf->SetX($conexion_+122);
            $pdf->Cell(0,0,$result['fecvto'],0); // FECHA PAGO OPORTUNO

            $pdf->SetY($y_+750);$pdf->SetX($conexion_+100);
            $pdf->Cell(0,0,'$'.number_format($deuda,2,',','.'),0);

            
			$barcode= chr(241)."415".$recuaudo_deuda_anterior."8020".$prefijo.str_pad($factura_no_cuenta_cobro,9,"0",STR_PAD_LEFT).chr(241)."3900".str_pad(number_format($deuda,0,'',''),10,"0",STR_PAD_LEFT).chr(241)."96".str_replace("-","",$result['fecvto']);
			
            
			$textBarcode = "(415)".$recuaudo_deuda_anterior."(8020)".$prefijo.str_pad($factura_no_cuenta_cobro,9,"0",STR_PAD_LEFT)."(3900)".str_pad(number_format($deuda,0,'',''),10,"0",STR_PAD_LEFT)."(96)".str_replace("-","",$result['fecvto']);

            //$pdf->write1DBarcode($barcode, 'C128', $conexion_+207, $y_+710,340, 30, 1, $style, 'N');
			$pdf->write1DBarcode($barcode, 'C128', $conexion_+207, $y_+710,270, 35, 1, $style, 'N');
            $pdf->SetFontSize('6');
            $pdf->Text($conexion_+260,$y_+741, $textBarcode);


            // INICIO DATOS PAGINA 2
            $total_concepto_acueducto       = 0;
            $total_concepto_alcantarillado  = 0;
            $total_otros_conceptos          = 0;
            $total_otros                    = 0;
            $tc1 =0; $tc2 =0; $tc3 =0; 
            $tca1=0; $tca2=0; $tca3=0;
            $secy = 0;
            $unidades = "";

            // DETALLES CONCEPTOS DE LA FACTURA
            $sql = "SELECT * FROM factura_per_det
                    WHERE cod_pred=".$result['cod_pred']." and
                        cod_peri=".$cod_peri." and 
						cod_munip=".$municipio." and
                        cod_empr='".$empresa."' 
                         order by cod_conc ";

            $exe_detalle= $conexion->prepare($sql);

            // add a page 2
            $pdf->setPrintHeader(false);
            $pdf->AddPage();
            // -- set new background ---
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
			
			//IMAGEN DEL DORSO------------------
            $img_file = 'ESSMAR_FACTURA-02.jpg';
            // $pdf->Image($img_file, 0, 0, 593, 0, '', '', '', false, 300, '', false, false, 0);
			
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            
            $exe_detalle->execute();
            while($result_det = $exe_detalle->fetch(PDO::FETCH_ASSOC)){
                
                $suapoR1    = $result_det['subapo_r1'];
                $suapoR2    = $result_det['subapo_r2'];
                $suapoR3    = $result_det['subapo_r3'];
                $suapoCF    = $result_det['subapo_cfijo'];
                $tasaUso    = $result_det['tasa_ur'];
               
                //--sumatoria para el resumen
                if($result_det['cod_conc']==103){//acueducto
                    $total_concepto_acueducto += $result_det['total_concp'];
                }
                if($result_det['cod_conc']==104)	{//alcantarillado
                    $total_concepto_alcantarillado += $result_det['total_concp'];
                }
                if(($result_det['cod_conc']!=103) and ($result_det['cod_conc']!=104) ){  //and ($result_det['cod_conc']!=100)
                    $total_otros_conceptos +=$result_det['total_concp'];
                }

                //--liquidacion Acueducto -- $acueducto
                if($result_det['cod_conc']==103){
                    $unidades   = ($unidades=="")?$result_det['nro_unidades']:$unidades;
                    $tc1=$result_det['canti_e1']*$result_det['precioe1'];
                    $tc2=$result_det['canti_e2']*$result_det['precio_e2'];
                    $tc3=$result_det['canti_e3']*$result_det['precioe3'];

                    $sqlacu = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                            fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                            from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                            where fpc.cod_pred=".$result['cod_pred']." and 
                                fpc.cod_peri=".$cod_peri." and 
                                fpc.cod_munip=".$municipio." and 
                                fpc.cod_empr='".$empresa."' and
                                fpd.cod_conc=".$result_det['cod_conc'];

                    $sql_acu = $conexion->prepare($sqlacu);
                    $sql_acu->execute();
                    $datos_acu = $sql_acu->fetch();

                    $cod_concep_acu = $datos_acu['cod_conc']; //codigo cocepto acueducto
                    $uso_acu        = $datos_acu['cod_uso']; //codigo cocepto acueducto
                    $cod_estado_acu = $datos_acu['cod_estd']; //codigo estado acueducto
                    $fec_lec_acu    = $datos_acu['feclectura']; //codigo estado acueducto
                    $fec_lec_ant_acu= $datos_acu['fec_lectura_ant']; //codigo estado acueducto
                    $cod_tari_acu   = $datos_acu['cod_tarf_ref']; //codigo tarifa acueducto
                    $cod_uso_acu    = $datos_acu['cod_uso_ref']; //codigo uso acueducto
                    $codtarf        = $datos_acu['cod_tarf'];

                    //--calcular tarifas
                    $sq_cal_tari_acu1 ="SELECT fact_calcular_tarifa('".$empresa."', ".$municipio.", '$cod_uso_acu', $cod_tari_acu, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd'),".$cod_peri.")";
                    $data_tari = $conexion->prepare($sq_cal_tari_acu1);
                    $data_tari->execute();
                    //print_r($data_tari->fetch());die();
                    $cod_cal_tari_acu = $data_tari->fetch();
                    //print_r($cod_cal_tari_acu);die();

                    $sq_cal_tari_acu2="SELECT fact_calcular_tarifa('".$empresa."', ".$municipio.", '$uso_acu', $codtarf, $cod_concep_acu, '$cod_estado_acu', (to_date('$fec_lec_ant_acu','yyyy-mm-dd')), to_date('$fec_lec_acu','yyyy-mm-dd'),".$cod_peri.")";
                    $sql_cal_tari_acu2 = $conexion->prepare($sq_cal_tari_acu2);
                    $sql_cal_tari_acu2->execute();
                    $cod_cal_tari_acu2= $sql_cal_tari_acu2->fetch();
                    $resto2 = str_replace("{"," ",$cod_cal_tari_acu2['fact_calcular_tarifa']);
                    $cal_tari_acu2 = explode(",", $resto2);
                    
                    $resto=str_replace("{"," ",$cod_cal_tari_acu['fact_calcular_tarifa']);
                        
                    $cal_tari_acu = explode(",", $resto);//si fecha esta en formato dia-mes-año 
                    
                    $consu_basic1   = $cal_tari_acu[1];
                    $consu_comple1  = $cal_tari_acu[2];
                    $consu_sunt1    = $cal_tari_acu[3];
                    $cargo_fijo1    = $cal_tari_acu[0];
                    $porcen         = $cal_tari_acu2[5];
                    $sum_sub=$suapoR1+$suapoR2+$suapoR3+$suapoCF;

                    //--acueducto consumo
                    $pdf->SetFont($data_font_type,'',6);
                    $pdf->SetY($y_+73);$pdf->SetX($conexion_+130);
                    $pdf->Cell(0,0,number_format($result_det['canti_fija'],0,',','.'),0,0,'L',0);
                    $pdf->SetY($y_+84);$pdf->SetX($conexion_+130); 
                    //if ($tc1 != 0){
					$pdf->Cell(0,0,number_format($result_det['canti_e1'],0,',','.'),0,0,'L',0); //}
                    $pdf->SetY($y_+95);$pdf->SetX($conexion_+130); 
                    //if ($tc2 != 0){
					$pdf->Cell(0,0,number_format($result_det['canti_e2'],0,',','.'),0,0,'L',0); //}
                    $pdf->SetY($y_+109);$pdf->SetX($conexion_+130); 
                    //if ($tc3 != 0){
					$pdf->Cell(0,0,number_format($result_det['canti_e3'],0,',','.'),0,0,'L',0); //}
                    $pdf->SetY($y_+116);$pdf->SetX($conexion_+130);
                    $pdf->Cell(0,0,' ',0,0,'L',0);
                    //--acueducto tarifa referencia
                    $pdf->SetY($y_+79);$pdf->SetX($conexion_+173);
                    $pdf->Cell(43,5,'$ '.number_format($cargo_fijo1,2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+91);$pdf->SetX($conexion_+173); 
                    //if ($tc1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($consu_basic1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+102);$pdf->SetX($conexion_+173); 
                    //if ($tc2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($consu_comple1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+113);$pdf->SetX($conexion_+173);
                    //if ($tc3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($consu_sunt1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+124);$pdf->SetX($conexion_+173);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');
                    //--acueducto factor
                    //if ($tc1 != 0){
                    if ($porcen >= 0){
                        $pdf->SetY($y_+73);$pdf->SetX($conexion_+245);
                        $pdf->Cell(0,0,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+85);$pdf->SetX($conexion_+245);
                        $pdf->Cell(0,0,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+96);$pdf->SetX($conexion_+245);
                        $pdf->Cell(0,0,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+107);$pdf->SetX($conexion_+245);
                        $pdf->Cell(0,0,number_format($porcen,0,',','.'),0,0,'L',0);
                    }else{
                        $pdf->SetY($y_+73);$pdf->SetX($conexion_+245);
                        $pdf->Cell(0,0,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+85);$pdf->SetX($conexion_+245);
                        $pdf->Cell(0,0,number_format($porcen,0,',','.'),0,0,'L',0);
                    }
                    //--acueducto valor
                    $pdf->SetY($y_+79);$pdf->SetX($conexion_+320);
                    $pdf->Cell(43,5,'$ '.number_format($suapoCF,2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+90);$pdf->SetX($conexion_+320); 
                    //if ($tc1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($suapoR1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+102);$pdf->SetX($conexion_+320); 
                    //if ($tc2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($suapoR2,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+113);$pdf->SetX($conexion_+320); 
                    //if ($tc3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($suapoR3,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+124);$pdf->SetX($conexion_+320);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');
                    //--acueducto valor Total
                    $pdf->SetFont($data_font_type,'',8);
                    $pdf->SetY($y_+142);$pdf->SetX($conexion_+333);
                    $pdf->Cell(0,0,'$ '.number_format($sum_sub,2,',','.'),0,0,'L',0);//total subaporte
                    
                    //--acueducto tarifa aplicada
                    $pdf->SetFont($data_font_type,'',6);
                    $pdf->SetY($y_+79);$pdf->SetX($conexion_+420);
                    $pdf->Cell(43,5,'$ '.number_format($result_det['cargofijo'],2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+90);$pdf->SetX($conexion_+420); ;
                    //if ($tc1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($result_det['precioe1'],2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+102);$pdf->SetX($conexion_+420); 
                    //if ($tc2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($result_det['precio_e2'],2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+113);$pdf->SetX($conexion_+420); 
                    //if ($tc3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($result_det['precioe3'],2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+124);$pdf->SetX($conexion_+420);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');
                    //--acueducto valor a pagar
                    $pdf->SetY($y_+79);$pdf->SetX($conexion_+500);
                    $pdf->Cell(43,5,'$ '.number_format($result_det['cargofijo'],2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+90);$pdf->SetX($conexion_+500); 
                    //if ($tc1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($tc1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+102);$pdf->SetX($conexion_+500); 
                    //if ($tc2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($tc2,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+113);$pdf->SetX($conexion_+500); 
                    //if ($tc3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($tc3,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+124);$pdf->SetX($conexion_+500);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');
                    //--Total concepto acueducto
                    $pdf->SetFont($data_font_type,'',8);
                    $pdf->SetY($y_+142);$pdf->SetX($conexion_+505);
                    $pdf->Cell(0,0,'$ '.number_format($result_det['total_concp'],2,',','.'),0,0,'L',0);
                }

                //--Liquidacion Alcantarillado  -- $alcantarillado
                if($result_det['cod_conc']==104){
                    $unidades   = ($unidades=="")?$result_det['nro_unidades']:$unidades;
                    $tca1=$result_det['canti_e1']*$result_det['precioe1'];
                    $tca2=$result_det['canti_e2']*$result_det['precio_e2'];
                    $tca3=$result_det['canti_e3']*$result_det['precioe3'];

                    $sqlalc = "select fpd.cod_conc, fpd.descripcion, fpd.cod_uso, fpc.cod_estd, fpc.feclectura, fpc.fec_lectura_ant,
                                fpd.cod_tarf, fpd.cod_tarf_ref, fpd.cod_uso_ref
                                from factura_per_cab fpc join factura_per_det fpd using(cod_pred,cod_peri,cod_munip,cod_empr)
                                where fpc.cod_pred=".$result['cod_pred']." and 
                                    fpc.cod_peri=".$cod_peri." and 
                                    fpc.cod_munip=".$municipio." and 
                                    fpc.cod_empr='".$empresa."' and
                                    fpd.cod_conc=".$result_det['cod_conc'];
                    $sql_alc = $conexion->prepare($sqlalc) or die ("Error: Detalle Alcantarillado \n".$sqlalc);
                    $sql_alc->execute();
                    $datos_alc = $sql_alc->fetch();

                    $cod_concep_alc = $datos_alc['cod_conc'];
                    $uso_alc        = $datos_alc['cod_uso'];
                    $codtarf        = $datos_alc['cod_tarf'];
                    $cod_estado_alc = $datos_alc['cod_estd'];
                    $fec_lec_alc    = $datos_alc['feclectura'];
                    $fec_lec_ant_alc= $datos_alc['fec_lectura_ant'];
                    $cod_tari_alc   = $datos_alc['cod_tarf_ref'];
                    $cod_uso_alc    = $datos_alc['cod_uso_ref'];

                    $sq_cal_tari_alc1="SELECT fact_calcular_tarifa('".$empresa."', ".$municipio.", '$cod_uso_alc', $cod_tari_alc, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd'),".$cod_peri.")";
                    $sql_cal_tari_alc1= $conexion->prepare($sq_cal_tari_alc1);
                    $sql_cal_tari_alc1->execute();

                    $cod_cal_tari_alc = $sql_cal_tari_alc1->fetch();
                    $resto1=str_replace("{"," ",$cod_cal_tari_alc['fact_calcular_tarifa']);
                    $cal_tari = explode(",", $resto1);//si fecha esta en formato dia-mes-año 
                    
                    $sq_cal_tari_alca2="SELECT fact_calcular_tarifa('".$empresa."',".$municipio.", '$uso_alc', $codtarf, $cod_concep_alc, '$cod_estado_alc', (to_date('$fec_lec_ant_alc','yyyy-mm-dd')), to_date('$fec_lec_alc','yyyy-mm-dd'),".$cod_peri.")";
                    $sql_cal_tari_alca2= $conexion->prepare($sq_cal_tari_alca2);
                    $sql_cal_tari_alca2->execute();

                    $cod_cal_tari_alca2= $sql_cal_tari_alca2->fetch();
                    $resto2=str_replace("{"," ",$cod_cal_tari_alca2['fact_calcular_tarifa']);
                    $cal_tari_alca2 = explode(",", $resto2);
                    
                    $consu_basic    = $cal_tari[1]; 
                    $consu_comple   = $cal_tari[2]; 
                    $consu_sunt     = $cal_tari[3]; 
                    $cargo_fijo     = $cal_tari[0];
                    $porcen         = $cal_tari_alca2[5];
			
                    //--alcantarillado consumo
                    $pdf->SetFont($data_font_type,'',6);
                    $pdf->SetY($y_+209);$pdf->SetX($conexion_+130);
                    $pdf->Cell(0,0,number_format($result_det['canti_fija'],0,',','.'),0,0,'L',0);
                    $pdf->SetY($y_+219);$pdf->SetX($conexion_+130);
                    //if ($tca1 != 0){
					$pdf->Cell(0,0,number_format($result_det['canti_e1'],0,',','.'),0,0,'L',0); //}
                    $pdf->SetY($y_+232);$pdf->SetX($conexion_+130);
                    //if ($tca2 != 0){
					$pdf->Cell(0,0,number_format($result_det['canti_e2'],0,',','.'),0,0,'L',0); //}
                    $pdf->SetY($y_+244);$pdf->SetX($conexion_+130);
                    //if ($tca3 != 0){
					$pdf->Cell(0,0,number_format($result_det['canti_e3'],0,',','.'),0,0,'L',0); //}
                    $pdf->SetY($y_+256);$pdf->SetX($conexion_+130);
                    $pdf->Cell(0,0,' ',0,0,'L',0);

                    //--alcantarillado tarifa referencia
                    $pdf->SetY($y_+216);$pdf->SetX($conexion_+173);
                    $pdf->Cell(43,5,'$ '.number_format($cargo_fijo,2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+227);$pdf->SetX($conexion_+173); 
                    //if ($tca1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($consu_basic,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //} 
                    $pdf->SetY($y_+238);$pdf->SetX($conexion_+173); 
                    //if ($tca2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($consu_comple,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+250);$pdf->SetX($conexion_+173);
                    //if ($tca3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($consu_sunt,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+262);$pdf->SetX($conexion_+173);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');

                    //--alcantarillado factor
                    //if ($tca1 != 0){
                    if ($porcen >= 0){
                        $pdf->SetY($y_+209);$pdf->SetX($conexion_+245);
                        $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+220);$pdf->SetX($conexion_+245);
                        $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);

                        $pdf->SetY($y_+232);$pdf->SetX($conexion_+245);
                        $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+244);$pdf->SetX($conexion_+245);
                        $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);
                        // $pdf->SetY($y_+255);$pdf->SetX($conexion_+245);
                        // $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);
                    }else{
                        $pdf->SetY($y_+209);$pdf->SetX($conexion_+245);
                        $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);
                        $pdf->SetY($y_+220);$pdf->SetX($conexion_+245);
                        $pdf->Cell(43,5,number_format($porcen,0,',','.'),0,0,'L',0);
                    }
                    $sum_sub_1=$suapoR1+$suapoR2+$suapoR3+$suapoCF;

                    //--alcantarillado valor
                    $pdf->SetY($y_+216);$pdf->SetX($conexion_+320);
                    $pdf->Cell(43,5,'$ '.number_format($suapoCF,2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+227);$pdf->SetX($conexion_+320); 
                    //if ($tca1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($suapoR1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+238);$pdf->SetX($conexion_+320); 
                    //if ($tca2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($suapoR2,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+250);$pdf->SetX($conexion_+320);
                    //if ($tca3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($suapoR3,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    //--alcantarillado valor Total
                    $pdf->SetFont($data_font_type,'',8);
                    $pdf->SetY($y_+278);$pdf->SetX($conexion_+333);
                    $pdf->Cell(0,0,'$ '.number_format($sum_sub_1,2,',','.'),0,0,'L',0);

                    //--alcantarillado tarifa aplicada
                    $pdf->SetFont($data_font_type,'',6);
                    $pdf->SetY($y_+216);$pdf->SetX($conexion_+420);
                    $pdf->Cell(43,5,'$ '.number_format($result_det['cargofijo'],2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+227);$pdf->SetX($conexion_+420);
                    //if ($tca1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($result_det['precioe1'],2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+239);$pdf->SetX($conexion_+420); 
                    //if ($tca2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($result_det['precio_e2'],2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+250);$pdf->SetX($conexion_+420);
                    //if ($tca3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($result_det['precioe3'],2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+262);$pdf->SetX($conexion_+420);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');
                    //--alcantarillado valor a pagar
                    $pdf->SetY($y_+216);$pdf->SetX($conexion_+500);
                    $pdf->Cell(43,5,'$ '.number_format($result_det['cargofijo'],2,',','.'),0,0,'R',0,0,0,true,'L','C');
                    $pdf->SetY($y_+227);$pdf->SetX($conexion_+500); 
                    //if ($tca1 != 0){
					$pdf->Cell(43,5,'$ '.number_format($tca1,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+239);$pdf->SetX($conexion_+500); 
                    //if ($tca2 != 0){
					$pdf->Cell(43,5,'$ '.number_format($tca2,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+250);$pdf->SetX($conexion_+500);
                    //if ($tca3 != 0){
					$pdf->Cell(43,5,'$ '.number_format($tca3,2,',','.'),0,0,'R',0,0,0,true,'L','C'); //}
                    $pdf->SetY($y_+262);$pdf->SetX($conexion_+500);
                    $pdf->Cell(43,5,' ',0,0,'R',0,0,0,true,'L','C');
                    //--Alcantarillado Total a pagar
                    $pdf->SetFont($data_font_type,'',8);
                    $pdf->SetY($y_+278);$pdf->SetX($conexion_+505);
                    $pdf->Cell(0,0,'$ '.number_format($result_det['total_concp'],2,',','.'),0,0,'L',0);
                }

                //--Liquidacion Otros conceptos  -- $acueducto - $alcantarillado
                if(($result_det['cod_conc']!=103) and ($result_det['cod_conc']!=104)){
				  if((($result_det['cod_conc']==4) or ($result_det['cod_conc']==105))and($sw_if_ac==0)){
					  		$sql_if="select sum(coalesce(fd.total_concp,0)) as total from factura_per_det fd
							where fd.cod_pred=".$result['cod_pred']." and fd.cod_peri=$cod_peri and fd.cod_munip=$municipio 
							and fd.cod_empr='$empresa' and fd.cod_conc in (4,105)";
							$query_if= $conexion->prepare($sql_if);
                            $query_if->execute();
                            //print_r($query_if->setFetchMode(PDO::FETCH_CLASS,"total"));die();
							$totfin_ac = $query_if->fetch();
							$sw_if_ac = $sw_if_ac + 1;
							
							$pdf->SetFont($data_font_type,'',6);			
							$pdf->SetY($y_+$secy+342);$pdf->SetX($conexion_+33);
							$pdf->Cell(0,0,$result_det['descripcion'],0);
							$pdf->SetY($y_+$secy+342);$pdf->SetX($conexion_+500);  //$pdf->SetX($conexion_+145);
							$pdf->Cell(0,0,'$ '.number_format($totfin_ac["total"],2,',','.'),0);
							$total_otros += $totfin_ac["total"];
							$secy += 10;
				  }
				  if((($result_det['cod_conc']==5) or ($result_det['cod_conc']==106))and($sw_if_al==0)){
					  		$sql_if="select sum(coalesce(fd.total_concp,0)) as total from factura_per_det fd
							where fd.cod_pred=".$result['cod_pred']." and fd.cod_peri=$cod_peri and fd.cod_munip=$municipio 
							and fd.cod_empr='$empresa' and fd.cod_conc in (5,106)";
							$query_if = $conexion->prepare($sql_if);
                            $query_if->execute();

							$totfin_al = $query_if->fetch();
							$sw_if_al = $sw_if_al + 1;
							
							$pdf->SetFont($data_font_type,'',6);			
							$pdf->SetY($y_+$secy+342);$pdf->SetX($conexion_+33);
							$pdf->Cell(0,0,$result_det['descripcion'],0);
							$pdf->SetY($y_+$secy+342);$pdf->SetX($conexion_+500);  //$pdf->SetX($conexion_+145);
							$pdf->Cell(0,0,'$ '.number_format($totfin_al["total"],2,',','.'),0);
							$total_otros += $totfin_al["total"];
							$secy += 10;  
				  }
				  if(($result_det['cod_conc']!=4) and ($result_det['cod_conc']!=5) and ($result_det['cod_conc']!=105) and ($result_det['cod_conc']!=106)){
                    $pdf->SetFont($data_font_type,'',6);			
                    $pdf->SetY($y_+$secy+342);$pdf->SetX($conexion_+33);
                    $pdf->Cell(0,0,$result_det['descripcion'],0);
                    $pdf->SetY($y_+$secy+342);$pdf->SetX($conexion_+500);  //$pdf->SetX($conexion_+145);
                    $pdf->Cell(0,0,'$ '.number_format($result_det['total_concp'],2,',','.'),0);
                    $total_otros += $result_det['total_concp'];
                    $secy += 10;
				  }
                } 
            }
            $pdf->SetFont($data_font_type,'',8);
            $pdf->SetY($y_+441);$pdf->SetX($conexion_+500);
            $pdf->Cell(0,0,'$ '.number_format($total_otros,2,',','.'),0);

            $msj1 = "En cumplimiento del artículo 4 del Decreto 580 de abril de 2020 la ESSMAR le da la opción a sus usuarios de aportar recursos de forma voluntaria para financiar las medidas adoptadas en el";

            $pdf->SetFont($data_font_type,'',7);
            $pdf->SetY($y_+455);$pdf->SetX($conexion_+25);
            $pdf->Cell(0,0,$msj1,0);

            $msj2 = "marco de la Emergencia Económica, Social y Ecológica, los cuales se destinarán a alimentar los fondos de solidaridad y redistribución de ingresos de estos servicios en cada municipio. Si desea";

            $pdf->SetY($y_+462);$pdf->SetX($conexion_+25);
            $pdf->Cell(0,0,$msj2,0);

            $msj3 = "aportar recursos comuníquese a las líneas 3008721133 y 3046077237.";

            $pdf->SetY($y_+469);$pdf->SetX($conexion_+25);
            $pdf->Cell(0,0,$msj3,0);
            // FIN DATOS PAGINA 2
           

		}
		if($cod_pred == ""){
		$prb->setLabelValue('txt1','Procesando '.$cont.'/'.$lotes);
		$prb->moveStep(($cont * 100)/$lotes);
    	$cont++;
		}
	}
	
}
//Close and output PDF document
//$pdf->Output('Factura.pdf', 'I');
if($cod_pred == ""){
$nom_file = "C".$cod_cclo."-P".$cod_peri."-P".$paginas."_".$lotes.".pdf"; 
$pdf->Output($nom_file,'F');
print "<a href=\"".$nom_file."\">--Descargar Aqui--</a>";
}else{
	$pdf->Output('Factura.pdf', 'I');
}
/*        $fp=fopen($nom_file, "wb");
        fwrite($fp,$code);
        fclose($fp);*/



function GraficaHistorica($conexion,$pdf,$predio,$_periodo,$ciclo,$municipio,$empresa,$c1,$c2,$c3,$c4,$c5,$c6){
    $l          = 1;
    $largomin   = 3;
    $dra        = 0.5;
    $conta      = 0;
    $largo_1    = 0;
    $historico  = 38; $cont=-8;
    $ancho      = 15;
    $py         = 500;
    $py1        = 399;
    $mesnum     = date('m');	
    $calmes     = $mesnum-5;
	
    
    
    $i=1;
    $max_con = max($c1,$c2,$c3,$c4,$c5,$c6);
    while($i<=6){
		
		if($i==1)$consumo=$c1;
		if($i==2)$consumo=$c2;
		if($i==3)$consumo=$c3;
		if($i==4)$consumo=$c4;
		if($i==5)$consumo=$c5;
		if($i==6)$consumo=$c6;
		
		$sqlper = $conexion->prepare("select limite_in('".$ciclo."',".$_periodo.",".$municipio.",'".$empresa."',".$i.");");
		$sqlper->execute();
        $data_sqlper = $sqlper->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
		$periodos = $data_sqlper[0];

        if ($max_con == null or $max_con == 0){
                $largo_1_pro = 0;
        } 
        else{
            $max_pro = $max_con;
            $por=($consumo/ $max_pro)*100;
            $por1=((int)$por);
            $largo_1=$largomin+($por1*$dra);
        }
        //if ($periodos==$max_peri){
		if ($periodos==$_periodo){	
            $pdf->SetFillColor(112,128,144);
            $pdf->SetDrawColor(0,0,0);	
            if($largo_1 == 0){
                $ceroY = 10;
            }else{
                $ceroY = 0;
            } 					
        }else{
            $pdf->SetFillColor(220,220,220);
            $pdf->SetDrawColor(0,0,0);	
            if($largo_1 == 0){
                $ceroY = 7;
            }else{
                $ceroY = 0;
            }
        }
        $pdf->SetY($py-$largo_1-$ceroY);$pdf->SetX($historico+13); //barra
        $pdf->Cell($ancho,$largo_1,' ',0,0,'L',1); //consumo
        $pdf->SetFontSize('6');
        $pdf->SetY(440);$pdf->SetX($historico+13);
        $pdf->Cell($ancho,7,number_format($consumo,0,',','.'),0,1,'L',0); //consumo 

        
         // consulta para total del perido
         $sql_facturado = "SELECT DISTINCT total FROM factura_per_cab WHERE cod_pred = $predio AND cod_peri = ".$periodos;
         $query_facturado = $conexion->prepare($sql_facturado);
         $query_facturado->execute();
         $data_fact = $query_facturado->fetch(PDO::FETCH_ASSOC);
         $total_periodo = $data_fact['total'];
         if(round($total_periodo) == 0){
            $totalP_X = 0;
        }else{
            $totalP_X = 7;
        }
         
         $pdf->SetFontSize('6');
         $pdf->SetY(505);$pdf->SetX($historico+$totalP_X);
         $pdf->Cell(20,7,"$".number_format($total_periodo,0,',','.'),0,1,'L',0); //total periodo
        
        $pdf->SetFontSize('6');	        

        $nommes= Mes(substr($periodos,-2));
        $pdf->SetY(515);$pdf->SetX($historico+13);
        $pdf->Cell(20,7,$nommes,0,1,'L',0); //nombre mes
        $calmes=$calmes+1;                    
        
        
        $historico = $historico +  40;
        $pro = 0;
        if ($periodos!=$_periodo){
            $pro = $pro + $consumo;							
        }
		$i=$i+1;
    } //fin historico       
}

function Mes($mes_numero){
    switch ($mes_numero){
        case '01':
            $mes = "ENE";
            break;
        case '02':
            $mes = "FEB";
            break;
        case '03':
            $mes = "MAR";
            break;
        case '04':
            $mes = "ABR";
            break;
        case '05':
            $mes = "MAY";
            break;
        case '06':
            $mes = "JUN";
            break;
        case '07':
            $mes = "JUL";
            break;
        case '08':
            $mes = "AGO";
            break;
        case '09':
            $mes = "SEP";
            break;
        case '10':
            $mes = "OCT";
            break;
        case '11':
            $mes = "NOV";
            break;
        case '12':
            $mes = "DIC";
            break;
    }
    return $mes;
}
?>
