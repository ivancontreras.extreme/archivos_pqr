<?php
function generar_archivos($conexion, $pqr_padre, $pqr, $array_files, $empresa)
{
    $sql_key = "SELECT pc.key_file, pc.file_name FROM predio_carta pc WHERE pc.cod_pqr = " . $pqr_padre;
    $query_key = $conexion->prepare($sql_key);
    $query_key->execute();
    while ($row = $query_key->fetch(PDO::FETCH_ASSOC)) {
        $array_files[] = $row['file_name'];
        $file = downloadFile($row['key_file']);
        $empresa == 'E' ? file_put_contents("essmar/tmp/" . $pqr . "/" . $row['file_name'], fopen($file, 'r')) : file_put_contents("ceibas/tmp/" . $pqr . "/" . $row['file_name'], fopen($file, 'r'));
    }
    // Facturas PQR Padre
    $sql_detalle = "SELECT pd.cod_peri , fc.cod_pred, pd.nro_factura FROM pqr_detalle pd LEFT JOIN factura_cab fc ON pd.nro_factura = fc.nro_factura 
                    WHERE pd.cod_pqr = $pqr_padre GROUP BY 1,2,3 ORDER BY pd.cod_peri ASC";
    $query_detalle = $conexion->prepare($sql_detalle);
    $query_detalle->execute();
    $count = 0;
    while ($row2 = $query_detalle->fetch(PDO::FETCH_ASSOC)) {
        if ($empresa == 'E') {
            duplicadoEssmar($conexion, $pqr_padre, $row2['cod_pred'], $row2['cod_peri'], $row2['nro_factura'], 'P');
            $array_files[] =  $row2['nro_factura'] . "-" . $pqr_padre . ".pdf";
            //6 periodos antes de la factura asignada a la pqr
            if($count == 0){
                $periodo_actual = $row2['cod_peri'];
                for ($i=6; $i; $i--) { 
                    $periodo_actual = $periodo_actual - 1;
                    if(strpos($periodo_actual,'00')){
                        $periodo_actual = ($periodo_actual - 1) - 99 + 12;
                    }
                    $sql_vali = "SELECT nro_factura  FROM factura_cab fc WHERE fc.cod_pred = $pqr_padre AND fc.cod_peri = $periodo_actual ";
                    $query_vali = $conexion->prepare($sql_vali);
                    $query_vali->execute();
                    $datos_vali = $query_vali->fetch(PDO::FETCH_ASSOC);
                    if($datos_vali['nro_factura']){
                        duplicadoEssmar($conexion, $pqr_padre, $row2['cod_pred'], $periodo_actual, $datos_vali['nro_factura'], 'P');   
                        $array_files[] =  $datos_vali['nro_factura'] . "-" . $pqr_padre . ".pdf";
                    }    
                }
            }
            $count++;
        } else {
            $duplicado = "http://ceibas.extreme.com.co/archivos/facturacion/factura_copy.php?cod_pred=" . $row2['cod_pred'] . "&municipio=1&cod_peri=" . $row2['cod_peri'] . "&externo=S&totalf=1&lotes=500&paginas=&duplicado=1&fac=2&crear=Crear&formato=archivos%2Ffacturacion%2Ffactura_copy.php";
            file_put_contents("ceibas/tmp/" . $pqr . "/" .  $row2['nro_factura'] . "-" . $pqr_padre . ".pdf", fopen($duplicado, 'r'));
            $array_files[] =  $row2['nro_factura'] . "-" . $pqr_padre . ".pdf";
        }
    }

    $array_files[] = 'Constancia' . $pqr . '.pdf';
    // Carta resolucion hija
    $sql_key = "SELECT pc.key_file, pc.file_name FROM predio_carta pc WHERE pc.cod_pqr =" . $pqr . " AND pc.file_name LIKE '%carta_resolucion_" . $pqr . "%'";
    $query_key = $conexion->prepare($sql_key);
    $query_key->execute();
    while ($row3 = $query_key->fetch(PDO::FETCH_ASSOC)) {
        $array_files[] = $row3['file_name'];
        $file = downloadFile($row3['key_file']);
        $empresa == 'E' ? file_put_contents("essmar/tmp/" . $pqr . "/" . $row3['file_name'], fopen($file, 'r')) : file_put_contents("ceibas/tmp/" . $pqr . "/" . $row3['file_name'], fopen($file, 'r'));
    }
    // Facturas PQR hija
    $sql_detalleh = "SELECT pd.cod_peri , fc.cod_pred, pd.nro_factura FROM pqr_detalle pd LEFT JOIN factura_cab fc ON pd.nro_factura = fc.nro_factura 
                    WHERE pd.cod_pqr = $pqr GROUP BY 1,2,3 ORDER BY pd.cod_peri ASC";
    $query_detalle = $conexion->prepare($sql_detalleh);
    $query_detalle->execute();
    while ($row4 = $query_detalle->fetch(PDO::FETCH_ASSOC)) {
        if ($empresa == 'E') {
            //duplicadoEssmar($conexion, $pqr, $row4['cod_pred'], $row4['cod_peri'], $row4['nro_factura']); // periodo actual
        } else {
            $duplicado = "http://ceibas.extreme.com.co/archivos/facturacion/factura_copy.php?cod_pred=" . $row4['cod_pred'] . "&municipio=1&cod_peri=" . $row4['cod_peri'] . "&externo=S&totalf=1&lotes=500&paginas=&duplicado=1&fac=2&crear=Crear&formato=archivos%2Ffacturacion%2Ffactura_copy.php";
            file_put_contents("ceibas/tmp/" . $pqr . "/" .  $row4['nro_factura'] . "-" . $pqr . ".pdf", fopen($duplicado, 'r'));
            $array_files[] =  $row4['nro_factura'] . "-" . $pqr . ".pdf";
        }
    }

    return $array_files;
}

function download_exp($urlFile)
{
    header('Content-Disposition: attachment; filename="' . basename($urlFile) . '"');
    header('Content-Type: application/octet-stream'); // Downloading on Android might fail without this
    ob_clean();

    readfile($urlFile);
}
