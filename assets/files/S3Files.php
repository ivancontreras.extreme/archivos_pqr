<?php

$url = file_get_contents("Properties.json", true);
$property = json_decode($url, true);
$link = $property['url'];
$bucket = $property['bucket'];

function uploadReport($file, $fileName, $folder, $s3Client)
{
    global $bucket;
    $file_Path = $file;
    $key = basename($file_Path);
    try {
        $result = $s3Client->putObject([
            'Bucket' => $bucket['permanent'],
            'Key'    => $folder . "/" . $key,
            'Body'   => fopen($file_Path, 'r'),
            //'ACL'    => 'public-read', // make file 'public'
        ]);
        return $result->get('ObjectURL');
        //echo "Image uploaded successfully. Image path is: " . $result->get('ObjectURL');
    } catch (Aws\S3\Exception\S3Exception $e) {
        echo "There was an error uploading the file.\n";
        echo $e->getMessage();
    }
}

function downloadFile($key, $temp = false)
{
    global $bucket, $link;
    $thisBucket = $temp ? $bucket['temporary'] : $bucket['permanent'];
    $thisKey = urlencode($key);
    $res = json_decode(file_get_contents("$link?key=$thisKey&bucket=$thisBucket"), true);
    return str_replace('https', 'http', $res['data']['fileUrl']);
}

function rmDir_rf($carpeta)
{
    foreach (glob($carpeta . "/*") as $archivos_carpeta) {
        if (is_dir($archivos_carpeta)) {
            rmDir_rf($archivos_carpeta);
        } else {
            $val = strpos($archivos_carpeta, "Expediente");
            if ($val === false) {
                unlink($archivos_carpeta);
            }
        }
    }
    //rmdir($carpeta);
}
