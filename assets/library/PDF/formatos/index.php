<?php 

include '../../../archivos/comun.inc';

require_once('../../../../tcpdf/config/lang/eng.php');
require_once('../../../../tcpdf_nueva/tcpdf.php');
require_once('../../../../tcpdf_nueva/tcpdf_barcodes_1d.php');


// Datos del PDF
$orientacion_papel="P";
$medida="pt"; 
$formato="letter";
$unicode=true;
$codificacion="UTF-8";
$clase="TCPDF";
$autor="Kagua";
$titulo="";
$margen_izq="0";
$margen_der="0";
$margen_sup="0";
$margen_inf="0";
$margen_encabezado="0";
$margen_pie="0";
$data_font_size="6";
$data_font_type="FreeSerif";
$encbz_font_size="6";
$peq_font_size="3";
$print_encbz_pg=true;
$print_pie_pg=true;

//----------------creacion PDF-----------------------//
$pdf = new TCPDF($orientacion_papel,$medida,$formato,$unicode);
$pdf->SetCreator($clase);
$pdf->SetAuthor($autor);
$pdf->SetTitle($titulo);
$pdf->SetSubject($doc_subject);
$pdf->SetKeywords($doc_keywords);
$pdf->SetMargins($margen_izq,$margen_sup,$margen_de);
$pdf->SetAutoPageBreak(true, $margen_inf);
$pdf->SetHeaderMargin($margen_encabezado);
$pdf->SetFooterMargin($margen_pie);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setHeaderFont(Array($encbz_font_type, '', $encbz_font_size));
$pdf->setFooterFont(Array($data_font_type, '', $font_size));
$pdf->setPrintHeader($print_encbz_pg);
$pdf->setPrintFooter($print_pie_pg);
$pdf->setLanguageArray($l); 
$pdf->setHeaderFont(Array('helvetica', '', 6));


//--
$pdf->AddPage();
$pdf->Image('../../../../FormatosEjecucionOT/img/ActaVisitaAcueductoRedPrincipal.jpg', -48, 0, 760, 650);
$pdf->Output();