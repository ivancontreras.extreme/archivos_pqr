<?php
include '../../../archivos/comun.inc';
require('../../../assets/database/index.php');
require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf_nueva/tcpdf.php');
require_once('../../../tcpdf_nueva/tcpdf_barcodes_1d.php');

if (isset($_GET['crear'])) die("Hola");

// Variables
$orden = !empty($_GET['orden']) ? "
    AND o.cod_otrb = {$_GET['orden']}
" : '';

$fechas = !empty($_GET['inicio']) && !empty($_GET['fin']) ? " 
    AND    
    to_date(o.fecha_grab::text, 'yyyy-mm-dd') 
    BETWEEN
        to_date('{$_GET['inicio']}', 'dd/mm/yyyy') AND 
        to_date('{$_GET['fin']}', 'dd/mm/yyyy')
" : '';

$queja = $_GET['queja'] != 'todas' ? "
    AND o.cod_tque = '{$_GET['queja']}'
" : '';

// Consults SQL
$sql = "
    SELECT
        o.cod_tque AS queja,
        o.cod_otrb AS orden,
        o.fecha_grab AS fechaemision,
        s.dir_clte AS direccion,
        b.descripcion AS barrio,
        s.nom_clte AS solicitante,
        s.tel_clte AS telefono,
        s.descripcion AS descripcion,
        p.serialmedi AS medidor,
        p.edad_cartera AS edad,
        p.cod_pred AS cuenta,
        c.descripcion AS ciclo,
        u.descripcion AS uso,
        p.nro_documento as nro_documento,
        p.nombre as suscriptor_nombrepredio,
        m.descripcion as marca_medidor,
        p.cod_pred as codigo_predio,
        tq.descripcion as tipo_servicio,
        tt.descripcion as servicio, 
        tq.cod_ttrb as tipo_trabajo,
        (SELECT coalesce(lectura,0) from factura_cab
            where cod_pred = p.cod_pred and nro_factura is not null
            and nro_seq = 1
            order by cod_peri desc, nro_seq desc limit 1
        ) as lectura,
        (
            select  correo from fact_vta_cab where  cod_pred = p.cod_pred order by fec_reg desc limit 1
        ) as correo_electronico,
        p.ruta,
        p.ruta_consecutivo
    FROM
        public.orden_trabajo o
        LEFT JOIN solicitud s ON (o.cod_solicitud=s.cod_soli)
        left JOIN barrio b ON (b.cod_barrio=s.cod_barrio)
        FULL OUTER JOIN predio p ON ( o.cod_pred = p.cod_pred )
        LEFT JOIN  uso u ON ( u.cod_uso = p.cod_uso )
        LEFT JOIN  ciclo c ON ( c.cod_cclo = p.cod_cclo )
        left join medidor m on m.cod_medi = p.cod_medi
        inner join tipo_queja tq on tq.cod_tque = o.cod_tque
        inner join tipo_trabajo tt on tt.cod_ttrb  = o.cod_ttrb
    WHERE
        o.cod_otrb IS NOT NULL
        $orden
        $fechas
        $queja
    ORDER BY fecha_grab DESC;
";

// Ejecucion consultas
$resOt = odbc_exec($conexion, $sql) or die("Fallo la ejecucion de la consulta: <pre> $sql </pre>");

// die( "<pre> $sql </pre>" );

// Datos del PDF
$orientacion_papel = "P";
$medida = "pt";
$formato = "letter";
$unicode = true;
$codificacion = "UTF-8";
$clase = "TCPDF";
$autor = "Kagua";
$titulo = "Acta de visita Acuerdo Red Principal";
$margen_izq = "0";
$margen_der = "0";
$margen_sup = "0";
$margen_inf = "0";
$margen_encabezado = "0";
$margen_pie = "0";
$data_font_size = "6";
$data_font_type = "FreeSerif";
$encbz_font_size = "6";
$peq_font_size = "3";
$print_encbz_pg = true;
$print_pie_pg = true;



//----------------creacion PDF-----------------------//
$pdf = new TCPDF($orientacion_papel, $medida, $formato, $unicode);
$pdf->SetCreator($clase);
$pdf->SetAuthor($autor);
$pdf->SetTitle($titulo);
$pdf->SetSubject($doc_subject);
$pdf->SetKeywords($doc_keywords);
$pdf->SetMargins($margen_izq, $margen_sup, $margen_de);
$pdf->SetAutoPageBreak(true, $margen_inf);
$pdf->SetHeaderMargin($margen_encabezado);
$pdf->SetFooterMargin($margen_pie);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setHeaderFont(array($encbz_font_type, '', $encbz_font_size));
$pdf->setFooterFont(array($data_font_type, '', $font_size));
$pdf->setPrintHeader($print_encbz_pg);
$pdf->setPrintFooter($print_pie_pg);
$pdf->setLanguageArray($l);
$pdf->setHeaderFont(array('helvetica', '', 6));
$pdf->setPrintHeader(false);

// Creacion de hojas
while ($ot = odbc_fetch_array($resOt)) {

    if ($ot['tipo_trabajo'] == 'GEO') {

        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/Acta_de_visita_Geofonia_parte_1.jpg', 0, 0, '', '');

        $proceso = 'GESTIÓN DE ACUEDUCTO';
        $vigencia = '01/06/2017';

        $variable1 = $ot['orden'];
        $variable2 = $ot['fechaemision'];
        $variable3 = $ot['solicitante'];
        $variable4 = $ot['codigo_predio']; //PREDIO
        $variable5 = $ot['direccion'];
        $variable6 = $ot['uso'];
        $variable7 = $ot['ciclo'];
        $variable8 = $ot['suscriptor_nombrepredio']; //nombre del predio
        $variable9 = $ot['medidor'];
        $variable10 = $ot['descripcion'];


        $variable15 = $ot['lectura'];
        $variable16 = $ot['marca_medidor']; // marca del medidor tabla medidor


        $pdf->SetFontSize('8.5');
        $pdf->SetY(146);
        $pdf->SetX(95);
        $pdf->MultiCell(160, 5, $proceso, 1, "", 0, 1, "", "", true); // Proceso

        $pdf->SetFontSize('8.5');
        $pdf->SetY(145);
        $pdf->SetX(299);
        $pdf->Cell(0, 0, $vigencia, 0); // vigencia

        //----

        //$pdf->SetFont('','B',null);
        $pdf->SetFontSize('8.5');
        $pdf->SetY(175);
        $pdf->SetX(143);
        $pdf->Cell(0, 0, $variable1, 0); // Orden de vista

        $pdf->SetFontSize('8.5');
        $pdf->SetY(175);
        $pdf->SetX(310);
        $fechaEmision = explode(' ', $variable2);
        $pdf->Cell(0, 0, $fechaEmision[0], 0); // Fecha de emision

        $pdf->SetFontSize('8.5');
        $pdf->SetY(175);
        $pdf->SetX(455);
        $pdf->Cell(0, 0, $variable3, 0); // Solicitado por

        $pdf->SetFontSize('8.5');
        $pdf->SetY(187);
        $pdf->SetX(92);
        $pdf->Cell(0, 0, $variable4, 0); // Codigo

        $pdf->SetFontSize('8.5');
        $pdf->SetY(187);
        $pdf->SetX(190);
        $pdf->Cell(0, 0, $variable5, 0); // Direccion

        $pdf->SetFontSize('8.5');
        $pdf->SetY(187);
        $pdf->SetX(420);
        $pdf->Cell(0, 0, $variable6, 0); // Uso

        $pdf->SetFontSize('8.5');
        $pdf->SetY(187);
        $pdf->SetX(540);
        $pdf->Cell(0, 0, $variable7, 0); // Ciclo

        $pdf->SetFontSize('8.5');
        $pdf->SetY(198);
        $pdf->SetX(105);
        $pdf->Cell(0, 0, $variable8, 0); // Suscriptor

        $pdf->SetFontSize('8.5');
        $pdf->SetY(209);
        $pdf->SetX(130);
        $pdf->Cell(0, 0, $variable9, 0); // Numero de medidor

        $pdf->SetFontSize('8.5');
        $pdf->SetY(221);
        $pdf->SetX(145);
        $pdf->Cell(0, 0, $variable10, 0); // Objeto de visita

        //-----


        $pdf->SetFontSize('8.5');
        $pdf->SetY(198);
        $pdf->SetX(455);
        $pdf->Cell(0, 0, $variable15, 0); // Lectura actual

        $pdf->SetFontSize('8.5');
        $pdf->SetY(210);
        $pdf->SetX(315);
        $pdf->Cell(0, 0, $variable16, 0); // Marca del medidor


        /* $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/Acta_de_visita_Geofonia_parte_2.jpg', 0, 0, '', '');

        $pdf->SetFontSize('8.5');
        $pdf->SetY(144);
        $pdf->SetX(95);
        $pdf->MultiCell(160, 5, $proceso, 1, "", 0, 1, "", "", true);//proceso

        $pdf->SetFontSize('8.5');
        $pdf->SetY(145);
        $pdf->SetX(299);
        $pdf->Cell(0, 0, $vigencia, 0); // vigencia */
    } else if ($ot['tipo_trabajo'] == 'OS' || $ot['tipo_trabajo'] == 'CER') {
        $pdf->AddPage('P');
        $pdf->Image('../../../FormatosEjecucionOT/img/Acta_de_visita_inspeccion.jpg', 0, 0, '', '');

        $proceso = 'GESTION COMERCIAL Y ATENCION AL USUARIO';
        $variable1 = $ot['orden'];
        $variable2 = $ot['fechaemision'];
        $variable3 = $ot['solicitante'];
        $variable4 = $ot['direccion'];
        $variable5 = $ot['nro_documento'];
        $variable6 = $ot['servicio'];
        $variable7 = $ot['tipo_servicio'];
        $variable8 = $ot['codigo_predio'];
        $variable9 = $ot['barrio'];
        $variable10 = $ot['telefono'];
        $variable11 = $ot['correo_electronico'];
        $observacion = '                                ' . $ot['descripcion'];


        $pdf->SetFontSize('7.5');
        $pdf->SetY(125);
        $pdf->SetX(105);
        $pdf->MultiCell(190, 5, $proceso, 0, "", 0, 1, "", "", true); // Proceso


        //----------

        $pdf->SetFontSize('7.5');
        $pdf->SetY(159);
        $pdf->SetX(128);
        $pdf->Cell(0, 0, $variable1, 0); // Consecutivo

        $pdf->SetFontSize('7.5');
        $pdf->SetY(159);
        $pdf->SetX(368);
        $pdf->Cell(0, 0, $variable2, 0); // Fecha

        $pdf->SetFontSize('7.5');
        $pdf->SetY(176);
        $pdf->SetX(108);
        $pdf->Cell(0, 0, $variable3, 0); // Contacto

        $pdf->SetFontSize('7.5');
        $pdf->SetY(192);
        $pdf->SetX(100);
        $pdf->MultiCell(120, 5, $variable4, 0, "", 0, 1, "", "", true); // Direccion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(217);
        $pdf->SetX(160);
        $pdf->Cell(0, 0, $variable5, 0); // Nro de Identificacion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(233);
        $pdf->SetX(92);
        $pdf->Cell(0, 0, $variable6, 0); // Servicio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(248);
        $pdf->SetX(113);
        $pdf->Cell(0, 0, $variable7, 0); // Tipo Servicio

        $pdf->SetFontSize('8.0');
        $pdf->SetY(293);
        $pdf->SetX(48);
        $pdf->MultiCell(500, 5, substr(strtoupper($observacion), 0, 680), 0, "", 0, 1, "", "", true); // Observacion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(176);
        $pdf->SetX(368);
        $pdf->Cell(0, 0, $variable8, 0); // Nro Cuenta


        $pdf->SetFontSize('7.5');
        $pdf->SetY(192);
        $pdf->SetX(258);
        $pdf->MultiCell(120, 5, strtoupper($variable9)  , 0, "", 0, 1, "", "", true); //barrio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(192);
        $pdf->SetX(435);
        $pdf->MultiCell(110, 5, $variable10 , 0, "", 0, 1, "", "", true);  // telefono



        $pdf->SetFontSize('7.5');
        $pdf->SetY(217.5);
        $pdf->SetX(318);
        $pdf->Cell(0, 0, $variable11, 0); // email


        /* ADICION DE VACTOR A OS  ------------------------------------------------------------------------------------------------------------------------*/

        $pdf->AddPage('P');
        $pdf->Image('../../../FormatosEjecucionOT/img/Acta_ot_formato_solicitud_de_servicio_encargado_parte_1.jpg', 0, 0, '', '');
        //------

        $proceso = 'GESTION COMERCIAL Y ATENCION AL USUARIO';
        $vigencia = '2017/09/04';
        $variable1 = $ot['orden'];
        $variable2 = $ot['fechaemision'];
        $variable3 = $ot['solicitante'];
        $variable4 = $ot['codigo_predio'];
        $variable5 = $ot['direccion'];
        $variable6 = $ot['telefono'];
        $variable7 = $ot['nro_documento'];
        $variable8 = $ot['servicio'];
        $variable9 = $ot['tipo_servicio'];
        $observacion = '                               ' . $ot['descripcion'];

        //---
        $pdf->SetFontSize('7.5');
        $pdf->SetY(108);
        $pdf->SetX(90);
        $pdf->MultiCell(190, 5, $proceso, 0, "", 0, 1, "", "", true); // Proceso

        $pdf->SetFontSize('7.5');
        $pdf->SetY(113);
        $pdf->SetX(375);
        $pdf->Cell(0, 0, $vigencia, 0); // Vigencia
        //----------

        $pdf->SetFontSize('8.5');
        $pdf->SetY(148);
        $pdf->SetX(143);
        $pdf->Cell(0, 0, $variable1, 0); // Orden de TRABAJO

        $pdf->SetFontSize('8.5');
        $pdf->SetY(148);
        $pdf->SetX(330);
        $pdf->Cell(0, 0, $variable2, 0); // Fecha de emision

        $pdf->SetFontSize('8.5');
        $pdf->SetY(164);
        $pdf->SetX(92);
        $pdf->Cell(0, 0, $variable3, 0); // Contacto

        $pdf->SetFontSize('8.5');
        $pdf->SetY(164);
        $pdf->SetX(350);
        $pdf->Cell(0, 0, $variable4, 0); // nro cuenta

        $pdf->SetFontSize('8');
        $pdf->SetY(180);
        $pdf->SetX(82);
        $pdf->Cell(0, 0, $variable5, 0); // Direccion

        $pdf->SetFontSize('8.5');
        $pdf->SetY(180);
        $pdf->SetX(350);
        $pdf->Cell(0, 0, $variable6, 0); // telefono

        $pdf->SetFontSize('9.5');
        $pdf->SetY(194);
        $pdf->SetX(125);
        $pdf->Cell(0, 0, $variable7, 0); // nro identificacion

        $pdf->SetFontSize('8.5');
        $pdf->SetY(210);
        $pdf->SetX(80);
        $pdf->Cell(0, 0, $variable8, 0); // servicio

        $pdf->SetFontSize('8.5');
        $pdf->SetY(226);
        $pdf->SetX(105);
        $pdf->Cell(0, 0, $variable9, 0); // tipo servicio

        $pdf->SetFontSize('8.5');
        $pdf->SetY(194);
        $pdf->SetX(318);
        $pdf->Cell(0, 0, $variable11, 0); // email


        $pdf->SetFontSize('8.5');
        $pdf->SetY(270);
        $pdf->SetX(35);
        $pdf->MultiCell(520, 5, substr(strtoupper($observacion), 0, 810), 1, "", 0, 1, "", "", true); // Observacion



    } else if ($ot['tipo_trabajo'] == 'VAC' || $ot['tipo_trabajo'] == 'SUM') {
        $pdf->AddPage('P');
        $pdf->Image('../../../FormatosEjecucionOT/img/Acta_ot_formato_solicitud_de_servicio_encargado_parte_1.jpg', 0, 0, '', '');
        //------

        $proceso = 'GESTION COMERCIAL Y ATENCION AL USUARIO';
        $vigencia = '2017/09/04';

        $variable1 = $ot['orden'];
        $variable2 = $ot['fechaemision'];
        $variable3 = $ot['solicitante'];
        $variable4 = $ot['codigo_predio'];
        $variable5 = $ot['direccion'];
        $variable6 = $ot['telefono'];
        $variable7 = $ot['nro_documento'];
        $variable8 = $ot['servicio'];
        $variable9 = $ot['tipo_servicio'];
        $variable11 = $ot['correo_electronico'];
        $observacion = '                              ' . $ot['descripcion'];

        //---
        $pdf->SetFontSize('7.5');
        $pdf->SetY(108);
        $pdf->SetX(90);
        $pdf->MultiCell(190, 5, $proceso, 0, "", 0, 1, "", "", true); // Proceso

        $pdf->SetFontSize('7.5');
        $pdf->SetY(113);
        $pdf->SetX(375);
        $pdf->Cell(0, 0, $vigencia, 0); // Vigencia
        //----------

        $pdf->SetFontSize('8.5');
        $pdf->SetY(148);
        $pdf->SetX(143);
        $pdf->Cell(0, 0, $variable1, 0); // Orden de TRABAJO

        $pdf->SetFontSize('8.5');
        $pdf->SetY(148);
        $pdf->SetX(330);
        $pdf->Cell(0, 0, $variable2, 0); // Fecha de emision

        $pdf->SetFontSize('8.5');
        $pdf->SetY(164);
        $pdf->SetX(92);
        $pdf->Cell(0, 0, $variable3, 0); // Contacto

        $pdf->SetFontSize('8.5');
        $pdf->SetY(164);
        $pdf->SetX(350);
        $pdf->Cell(0, 0, $variable4, 0); // nro cuenta

        $pdf->SetFontSize('8');
        $pdf->SetY(180);
        $pdf->SetX(82);
        $pdf->Cell(0, 0, $variable5, 0); // Direccion

        $pdf->SetFontSize('8.5');
        $pdf->SetY(180);
        $pdf->SetX(350);
        $pdf->Cell(0, 0, $variable6, 0); // telefono

        $pdf->SetFontSize('9.5');
        $pdf->SetY(194);
        $pdf->SetX(125);
        $pdf->Cell(0, 0, $variable7, 0); // nro identificacion

        $pdf->SetFontSize('8.5');
        $pdf->SetY(210);
        $pdf->SetX(80);
        $pdf->Cell(0, 0, $variable8, 0); // servicio

        $pdf->SetFontSize('8.5');
        $pdf->SetY(226);
        $pdf->SetX(105);
        $pdf->Cell(0, 0, $variable9, 0); // tipo servicio

        $pdf->SetFontSize('8.5');
        $pdf->SetY(270);
        $pdf->SetX(35);
        $pdf->MultiCell(520, 5, substr(strtoupper($observacion), 0, 790), 1, "", 0, 1, "", "", true); // Observacion

        $pdf->SetFontSize('8.5');
        $pdf->SetY(194);
        $pdf->SetX(318);
        $pdf->Cell(0, 0, $variable11, 0); // email


        //----------PAGINA 2

        if ($ot['tipo_trabajo'] == 'SUM'){
            $pdf->AddPage('L');
            $pdf->Image('../../../FormatosEjecucionOT/img/Acta_formato_control_entrega.jpg', 0, 0, '790', '');
    
            //---
            $pdf->SetFontSize('6.0');
            $pdf->SetY(67);
            $pdf->SetX(65);
            $pdf->MultiCell(190, 5, $proceso, 0, "", 0, 1, "", "", true); // Proceso
    
            $pdf->SetFontSize('6.5');
            $pdf->SetY(67);
            $pdf->SetX(305);
            $pdf->Cell(0, 0, $vigencia, 0); // Vigencia
        }

        //-----------PAGINA 3-------------------------------------------------------------------------------------------------------------------


        $pdf->AddPage('P');
        $pdf->Image('../../../FormatosEjecucionOT/img/Acta_de_visita_inspeccion.jpg', 0, 0, '', '');


        $proceso = 'GESTION COMERCIAL Y ATENCION AL USUARIO';


        $variable1 = $ot['orden'];
        $variable2 = $ot['fechaemision'];
        $variable3 = $ot['solicitante'];
        $variable4 = $ot['direccion'];
        $variable5 = $ot['nro_documento'];
        $variable6 = $ot['servicio'];
        $variable7 = $ot['tipo_servicio'];
        $variable8 = $ot['codigo_predio'];
        $variable9 = $ot['barrio'];
        $variable10 = $ot['telefono'];
        $variable11 = $ot['correo_electronico'];

        $observacion = '                                ' . $ot['descripcion'];


        $pdf->SetFontSize('7.5');
        $pdf->SetY(125);
        $pdf->SetX(105);
        $pdf->MultiCell(190, 5, $proceso, 0, "", 0, 1, "", "", true); // Proceso

        //----------

        $pdf->SetFontSize('7.5');
        $pdf->SetY(159);
        $pdf->SetX(128);
        $pdf->Cell(0, 0, $variable1, 0); // Consecutivo

        $pdf->SetFontSize('7.5');
        $pdf->SetY(159);
        $pdf->SetX(368);
        $pdf->Cell(0, 0, $variable2, 0); // Fecha

        $pdf->SetFontSize('7.5');
        $pdf->SetY(176);
        $pdf->SetX(108);
        $pdf->Cell(0, 0, $variable3, 0); // Contacto

        $pdf->SetFontSize('7.5');
        $pdf->SetY(192);
        $pdf->SetX(100);
        $pdf->MultiCell(120, 5, $variable4, 0, "", 0, 1, "", "", true); // Direccion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(217);
        $pdf->SetX(160);
        $pdf->Cell(0, 0, $variable5, 0); // Nro de Identificacion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(233);
        $pdf->SetX(92);
        $pdf->Cell(0, 0, $variable6, 0); // Servicio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(248);
        $pdf->SetX(113);
        $pdf->Cell(0, 0, $variable7, 0); // Tipo Servicio

        $pdf->SetFontSize('8.0');
        $pdf->SetY(293);
        $pdf->SetX(48);
        $pdf->MultiCell(500, 5, substr(strtoupper($observacion), 0, 690), 0, "", 0, 1, "", "", true); // Observacion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(176);
        $pdf->SetX(368);
        $pdf->Cell(0, 0, $variable8, 0); // Nro Cuenta


        $pdf->SetFontSize('7.5');
        $pdf->SetY(192);
        $pdf->SetX(258);
        $pdf->MultiCell(120, 5, strtoupper($variable9)  , 0, "", 0, 1, "", "", true); // barrio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(192);
        $pdf->SetX(435);
        $pdf->MultiCell(110, 5, $variable10 , 0, "", 0, 1, "", "", true); // telefono

        $pdf->SetFontSize('7.5');
        $pdf->SetY(217.5);
        $pdf->SetX(318);
        $pdf->Cell(0, 0, $variable11, 0); // email




    } else if (
        $ot['queja'] == '102' || $ot['queja'] == '109' ||  $ot['queja'] == '110' || $ot['queja'] == '111' ||  $ot['queja'] == '205' ||
        $ot['queja'] == '126' || $ot['queja'] == '131' || $ot['queja'] == '135' || $ot['queja'] == '137' || $ot['queja'] == '195') {
        /* AC-FR-08 Acueducto Red Principal*/
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaAcueductoRedPrincipal.jpg', -48, 0, 760, 650);

        $pdf->SetFontSize('6.3');
        $pdf->SetY(124);
        $pdf->SetX(122);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('6.3');
        $pdf->SetY(124);
        $pdf->SetX(340);
        $pdf->Cell(0, 0, date("d/m/yy", strtotime($ot['fechaemision'])), 0); //Fecha de Emision

        $pdf->SetFontSize('5.1');
        $pdf->SetY(125);
        $pdf->SetX(508);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Solicitante

        $pdf->SetFontSize('5.2');
        $pdf->SetY(136);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('6.7');
        $pdf->SetY(135);
        $pdf->SetX(441);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('5');
        $pdf->SetY(147);
        $pdf->SetX(123);
        $pdf->Cell(0, 0, '', 0); //Reportado

        $pdf->SetFontSize('6');
        $pdf->SetY(147);
        $pdf->SetX(461);
        $pdf->Cell(0, 0, $ot['telefono'], 0); //telefono

        $pdf->SetFontSize('6.3');
        $pdf->SetY(157);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['descripcion'], 0); //Descripcion

    } else if ($ot['queja'] == '125' ||  $ot['queja'] == 'RES') {
        /* AC-FR-11 Campo Reparcheo*/
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaCampoReparcheo.jpg', -48, 0, 760, 650);

        $pdf->SetFontSize('6.3');
        $pdf->SetY(126);
        $pdf->SetX(123);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('6.3');
        $pdf->SetY(126);
        $pdf->SetX(344);
        $pdf->Cell(0, 0, $ot['fechaemision'], 0); //Fecha de Emision

        $pdf->SetFontSize('5.2');
        $pdf->SetY(127);
        $pdf->SetX(499);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Solicitante

        $pdf->SetFontSize('5.2');
        $pdf->SetY(137.8);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('6.7');
        $pdf->SetY(137);
        $pdf->SetX(447);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('5');
        $pdf->SetY(149);
        $pdf->SetX(124);
        $pdf->Cell(0, 0, '', 0); //Reportado

        $pdf->SetFontSize('6.3');
        $pdf->SetY(148);
        $pdf->SetX(468);
        $pdf->Cell(0, 0, $ot['telefono'], 0); //telefono

        $pdf->SetFontSize('6.3');
        $pdf->SetY(158);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['descripcion'], 0); //Descripcion

    } else if ($ot['queja'] == '103' || $ot['queja'] == '106') {
        /* CA-FR-N05 */
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaEjecucionOT.jpg', -20, 0, 640, 650);

        $pdf->SetFontSize('6.5');
        $pdf->SetY(100);
        $pdf->SetX(115);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('6.5');
        $pdf->SetY(100);
        $pdf->SetX(280);
        $pdf->Cell(0, 0, $ot['fechaemision'], 0); //Fecha de Emision

        $pdf->SetFontSize('6.1');
        $pdf->SetY(100);
        $pdf->SetX(457);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Solicitante

        $pdf->SetFontSize('6.1');
        $pdf->SetY(121.3);
        $pdf->SetX(70);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('6.5');
        $pdf->SetY(121.3);
        $pdf->SetX(323);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('6.5');
        $pdf->SetY(121.3);
        $pdf->SetX(488);
        $pdf->Cell(0, 0, $ot['cuenta'], 0); //Numero de cuenta

        $pdf->SetFontSize('6.5');
        $pdf->SetY(142.3);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['descripcion'], 0); //Descripcion

    } else if ($ot['queja'] == '265' || $ot['queja'] == '266' || $ot['queja'] == '315') {
        /* CA-FR-N06 */
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaInstalacionRetiroMedidor.jpg', 0, 0, '790', '1024');

        $pdf->SetFontSize('7.5');
        $pdf->SetY(108);
        $pdf->SetX(65);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('7.5');
        $pdf->SetY(108);
        $pdf->SetX(252);
        $pdf->Cell(0, 0, date("yy/m/d", strtotime($ot['fechaemision'])), 0); //Fecha de Emision

        $pdf->SetFontSize('7.5');
        $pdf->SetY(108);
        $pdf->SetX(437);
        $pdf->Cell(0, 0, $ot['cuenta'], 0); //Cuenta

        $pdf->SetFontSize('7');
        $pdf->SetY(122);
        $pdf->SetX(93);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Solicitante

        $pdf->SetFontSize('7.5');
        $pdf->SetY(122);
        $pdf->SetX(308);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(122);
        $pdf->SetX(508);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(134.5);
        $pdf->SetX(90);
        $pdf->Cell(0, 0, $ot['medidor'], 0); //Medidor

        $pdf->SetFontSize('7.5');
        $pdf->SetY(134.5);
        $pdf->SetX(395);
        $pdf->Cell(0, 0, $ot['ciclo'], 0); //Ciclo

        $pdf->SetFontSize('7.5');
        $pdf->SetY(134.5);
        $pdf->SetX(478);
        $pdf->Cell(0, 0, $ot['uso'], 0); //Uso

        $pdf->SetFontSize('7');
        $pdf->SetY(151.5);
        $pdf->SetX(25);
        $pdf->setCellHeightRatio(1.55);
        $pdf->MultiCell(560, 0, '                                                   ' . substr($ot['descripcion'], 0, 430), 0, 'L', 0, 0);
    } else if (
        $ot['queja'] == '209' || $ot['queja'] == '104' || $ot['queja'] == '105' || $ot['queja'] == '114' || $ot['queja'] == '115' || $ot['queja'] == '118' || $ot['queja'] == '119' ||
        $ot['queja'] == '129' || $ot['queja'] == '133' || $ot['queja'] == '136') {
        /* AL-FR-02 */
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaAlcantarillado.jpg', -30, 0, 670, 650);

        $pdf->SetFontSize('6.3');
        $pdf->SetY(126.5);
        $pdf->SetX(126);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('6.3');
        $pdf->SetY(126.5);
        $pdf->SetX(316);
        $pdf->Cell(0, 0, $ot['fechaemision'], 0); //Fecha de Emision

        $pdf->SetFontSize('5.1');
        $pdf->SetY(127);
        $pdf->SetX(481.5);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Solicitante

        $pdf->SetFontSize('5.2');
        $pdf->SetY(138);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('6.7');
        $pdf->SetY(138);
        $pdf->SetX(435);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('5.1');
        $pdf->SetY(150);
        $pdf->SetX(126.5);
        $pdf->Cell(0, 0, '', 0); //Reportado

        $pdf->SetFontSize('6');
        $pdf->SetY(150.5);
        $pdf->SetX(481);
        $pdf->Cell(0, 0, $ot['telefono'], 0); //telefono

        $pdf->SetFontSize('6.7');
        $pdf->SetY(160.8);
        $pdf->SetX(103);
        $pdf->Cell(0, 0, $ot['descripcion'], 0); //Descripcion

    } else if ($ot['queja'] == '117' || $ot['queja'] == '107' || $ot['queja'] == '207') {
        /* AL-FR-03 */
        $pdf->AddPage();

        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaAlcantarilladoVactor.jpg', -35, 0, 770, 650);

        $pdf->SetFontSize('6.3');
        $pdf->SetY(131.5);
        $pdf->SetX(127);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('6.3');
        $pdf->SetY(131.5);
        $pdf->SetX(311);
        $pdf->Cell(0, 0, $ot['fechaemision'], 0); //Fecha de Emision

        $pdf->SetFontSize('5.1');
        $pdf->SetY(133);
        $pdf->SetX(473);
        $pdf->Cell(0, 0, '', 0); //Solicitante

        $pdf->SetFontSize('5.2');
        $pdf->SetY(146);
        $pdf->SetX(105);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('7');
        $pdf->SetY(145);
        $pdf->SetX(427);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('5.1');
        $pdf->SetY(158);
        $pdf->SetX(128);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Reportado

        $pdf->SetFontSize('6');
        $pdf->SetY(158);
        $pdf->SetX(450);
        $pdf->Cell(0, 0, $ot['telefono'], 0); //telefono

        $pdf->SetFontSize('7');
        $pdf->SetY(168.5);
        $pdf->SetX(105);
        $pdf->Cell(0, 0, $ot['descripcion'], 0); //Descripcion
    } else if ($ot['queja'] == '318') {
        /* CA-FR-N03 ACTA DE VISITA RESOLUCION 0371 DE 2016 */
        $pdf->AddPage();

        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaResolucion0371_2016_parte1.jpg', 15, 35, 600, 750);

        $pdf->SetFontSize('7');
        $pdf->SetY(123);
        $pdf->SetX(82);
        $pdf->Cell(0, 0, $ot['fechaemision'], 0); //Fecha de Emision

        $pdf->SetFontSize('7');
        $pdf->SetY(138);
        $pdf->SetX(518);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('7');
        $pdf->SetY(138);
        $pdf->SetX(68);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('7');
        $pdf->SetY(138);
        $pdf->SetX(290);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('7');
        $pdf->SetY(159);
        $pdf->SetX(76);
        $pdf->Cell(0, 0, $ot['cuenta'], 0); //Numero de cuetna

        $pdf->SetFontSize('7');
        $pdf->SetY(159);
        $pdf->SetX(296);
        $pdf->Cell(0, 0, $ot['medidor'], 0); //Medidor

        $pdf->SetFontSize('7');
        $pdf->SetY(159);
        $pdf->SetX(505);
        $pdf->Cell(0, 0, $ot['edad'], 0); //Edad

        $pdf->SetFontSize('7');
        $pdf->SetY(201);
        $pdf->SetX(27);
        $pdf->setCellHeightRatio(1.55);
        $pdf->MultiCell(560, 0, '                           ' . substr($ot['descripcion'], 0, 400), 0, 'L', 0, 0);


        //Second Page
        //-------------------------------
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaResolucion0371_2016_parte2.jpg', -5, 0, 650, 750);

        $pdf->SetFontSize('7');
        $pdf->SetY(93);
        $pdf->SetX(65);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo 2


    } else if ($ot['queja'] == '603') {
        /* CA-FR-N03 ACTA DE VISITA ANC */
        $pdf->AddPage();

        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaANC.jpg', -20, 16, 650, 650);

        $pdf->SetFontSize('7.5');
        $pdf->SetY(110);
        $pdf->SetX(64);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Orden de Trabajo

        $pdf->SetFontSize('7.5');
        $pdf->SetY(110);
        $pdf->SetX(250);
        $pdf->Cell(0, 0, date("d/m/yy", strtotime($ot['fechaemision'])), 0); //Fecha de Emision

        $pdf->SetFontSize('7.5');
        $pdf->SetY(110);
        $pdf->SetX(440);
        $pdf->Cell(0, 0, $ot['cuenta'], 0); //Cuenta

        $pdf->SetFontSize('6.1');
        $pdf->SetY(124.5);
        $pdf->SetX(90);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); //Suscriptor

        $pdf->SetFontSize('7.5');
        $pdf->SetY(123);
        $pdf->SetX(303);
        $pdf->Cell(0, 0, $ot['direccion'], 0); //Direccion

        $pdf->SetFontSize('7');
        $pdf->SetY(123);
        $pdf->SetX(505);
        $pdf->Cell(0, 0, $ot['barrio'], 0); //Barrio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(137.5);
        $pdf->SetX(93);
        $pdf->Cell(0, 0, $ot['medidor'], 0); //Medidor

        $pdf->SetFontSize('7.5');
        $pdf->SetY(137.5);
        $pdf->SetX(394);
        $pdf->Cell(0, 0, $ot['ciclo'], 0); //Ciclo

        $pdf->SetFontSize('7.5');
        $pdf->SetY(137.5);
        $pdf->SetX(480);
        $pdf->Cell(0, 0, $ot['uso'], 0); //Uso

        $pdf->SetFontSize('7.5');
        $pdf->SetY(152.5);
        $pdf->SetX(22);
        $pdf->setCellHeightRatio(1.65);
        $pdf->MultiCell(560, 0,'                                                 '.substr( $ot['descripcion'], 0, 410 ), 0, 'L',0,0);
    
    } else if ($ot['queja'] == 'VSM') {
        
        /* Visita solicitud de matricula*/
       
        
        $pdf->AddPage();
        $pdf->Image('../../../FormatosEjecucionOT/img/Solicitud Matrícula_page-0001.jpg', 25, 0, '', '');

        $cod_otrb =  $ot['orden'];

        $sql = "select cod_aco, cod_otrb, fec_sol, document_propiet, nombre_propiet, telefono_solict,direccion_newpre,
            (select descripcion from barrio b where b.cod_barrio = na.cod_barrio) as barrio, estrato, numero_medidor,observacion 
            from nuevacometida na where cod_otrb = $cod_otrb";
        $exe=odbc_exec($conexion, $sql);

        $cod_aco =  odbc_result($exe, 'cod_aco');
        $fecha =  odbc_result($exe, 'fec_sol');
        $fecha = date("d/m/Y", strtotime($fecha));
        $documentpropie =odbc_result($exe, 'document_propiet');
        $nompropietario = ucwords(odbc_result($exe, 'nombre_propiet'));
        $telCli = odbc_result($exe, 'telefono_solict');
        $direccion = odbc_result($exe, 'direccion_newpre');
        $barrio = odbc_result($exe, 'barrio');
        $estrato = odbc_result($exe, 'estrato');
        $numeroMedidor = odbc_result($exe, 'numero_medidor');
        if($numeroMedidor == 'NULL'){
            $numeroMedidor = '';
        }
        $observacion = odbc_result($exe, 'observacion');
        
        $pdf->SetFontSize('7.5');
        $pdf->SetY(115);
        $pdf->SetX(130);
        $pdf->MultiCell(190, 5, $cod_aco, 0, "", 0, 1, "", "", true);

        $pdf->SetFontSize('7.5');
        $pdf->SetY(115);
        $pdf->SetX(210);
        $pdf->MultiCell(190, 5, $cod_otrb, 0, "", 0, 1, "", "", true);

        $pdf->SetFontSize('7.5');
        $pdf->SetY(115);
        $pdf->SetX(330);
        $pdf->Cell(0, 0, $fecha, 0); // fecha

        $pdf->SetFontSize('7.5');
        $pdf->SetY(115);
        $pdf->SetX(485);
        $pdf->Cell(0, 0, $telCli, 0); // telCli

        $pdf->SetFontSize('7.5');
        $pdf->SetY(135);
        $pdf->SetX(195);
        $pdf->Cell(0, 0, ucwords($nompropietario), 0); // nompropietario

        $pdf->SetFontSize('7.5');
        $pdf->SetY(135);
        $pdf->SetX(520);
        $pdf->Cell(0, 0, $documentpropie, 0); // documentpropie

        $pdf->SetFontSize('7.5');
        $pdf->SetY(150);
        $pdf->SetX(120);
        $pdf->Cell(0, 0, $observacion, 0); // observacion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(135);
        $pdf->SetX(400);
        $pdf->Cell(0, 0, $numeroMedidor, 0); // numero Medidor

        $pdf->SetFontSize('7.5');
        $pdf->SetY(185);
        $pdf->SetX(140);
        $pdf->Cell(0, 0, $direccion, 0); // direccion

        $pdf->SetFontSize('7.5');
        $pdf->SetY(202);
        $pdf->SetX(95);
        $pdf->Cell(0, 0, $barrio, 0); // barrio

        $pdf->SetFontSize('7.5');
        $pdf->SetY(220);
        $pdf->SetX(95);
        $pdf->Cell(0, 0, $estrato, 0); // estrato


    } else if ($ot['queja'] == 'SUP') {
        $cuenta = $ot['cuenta'];
        $cod_otrb = $ot['orden'];
        // Se trae el ultimo periodo
        $sql = "SELECT max(cod_peri) AS periodo
            FROM factura_cab
            WHERE nro_factura IS NOT NULL AND cod_pred = $cuenta
        ";

        $exec = exe($sql) or die('Error al traer el último periodo de el predio ' . $cuenta. ' ' . $sql );
        $periodo = result($exec, 'periodo');

        if ($periodo) {
            // Acta de suspensión
            $pdf->AddPage();
            $pdf->Image('../../../FormatosEjecucionOT/img/ActaSuspension.jpg', 0, 0, '', '');

            $sql = "SELECT ot.fecha_grab::DATE AS fecha,
                fc.cod_cclo AS ciclo,
                p.nombre AS solicitante,
                ot.direccion_clte,
                p.estrato,
                (SELECT SUM(coalesce(deuda_e1, 0) + coalesce(deuda_e2, 0) + coalesce(deuda_e3,0))
                    AS saldo
                FROM factura_cab
                WHERE cod_pred = $cuenta AND cod_peri = $periodo),
                fc.nropermora AS edad,
                fc.lectura,
                fc.nro_factura,
                UPPER(ot.descripcion) AS observacion_ot
            FROM orden_trabajo ot
                LEFT JOIN factura_cab fc on fc.cod_pred = ot.cod_pred
                LEFT JOIN predio p ON p.cod_pred = ot.cod_pred
            WHERE ot.cod_pred = $cuenta AND ot.cod_ttrb = '".$ot['tipo_trabajo']."' AND fc.cod_peri = $periodo";

            $exec = exe($sql) or die('Error al consultar la orden de trabajo ' . $cod_otrb . ' ' . $sql);

            while ($res = fetch_object($exec)) {
                $pdf->SetFontSize('8');
                $pdf->SetY(159.5);
                $pdf->SetX(460);
                $pdf->Cell(0,0,$cod_otrb,0);// Orden de Trabajo

                $pdf->SetFontSize('8');
                $pdf->SetY(193);
                $pdf->SetX(70);
                $pdf->Cell(0,0,$cuenta,0); //Predio
        
                $fecha = date("Y/m/d", strtotime($ot['fechaemision']));
                $pdf->SetFontSize('8');
                $pdf->SetY(193);
                $pdf->SetX(190);
                $pdf->Cell(0,0,$fecha, 0); //Fecha de Emision

                $pdf->SetFontSize('8');
                $pdf->SetY(209);
                $pdf->SetX(72);
                $pdf->Cell(0,0,$res->solicitante, 0); // Solicitante

                $pdf->SetFontSize('8');
                $pdf->SetY(225.5);
                $pdf->SetX(82);
                $pdf->Cell(0,0,$res->direccion_clte, 0); // Dirección del solicitante

                $pdf->SetFontSize('8');
                $pdf->SetY(242.5);
                $pdf->SetX(77);
                $pdf->Cell(0,0,$res->nro_factura, 0); // Número de la última factura

                $pdf->SetFontSize('8');
                $pdf->SetY(242);
                $pdf->SetX(188);
                $pdf->Cell(0,0,number_format($res->saldo, 2, ',' , '.'), 0); // Saldo

                $pdf->SetFontSize('8');
                $pdf->SetY(258);
                $pdf->SetX(75);
                $pdf->Cell(0,0,$res->estrato, 0); // Estrato

                $pdf->SetFontSize('8');
                $pdf->SetY(258);
                $pdf->SetX(210);
                $pdf->Cell(0,0,$ot['uso'], 0); // Uso

                $pdf->SetFontSize('8');
                $pdf->SetY(274);
                $pdf->SetX(58);
                $pdf->Cell(0,0,$ot['ciclo'], 0); // Ciclo

                $pdf->SetFontSize('8');
                $pdf->SetY(274);
                $pdf->SetX(183);
                $pdf->Cell(0,0,$ot['ruta'], 0); // Ruta

                $pdf->SetFontSize('8');
                $pdf->SetY(290);
                $pdf->SetX(73);
                $pdf->Cell(0,0,$ot['medidor'], 0); // Número del medidor

                $pdf->SetFontSize('8');
                $pdf->SetY(290);
                $pdf->SetX(183);
                $pdf->Cell(0,0,$res->edad, 0); // Periodos mora
        
                $observacion = strlen($res->observacion_ot) > 92 ? substr($res->observacion_ot, 0,92) . '...' : $res->observacion_ot;
                $observacion = "                                       ". $observacion;
                $pdf->SetFontSize('8');
                $pdf->SetY(353);
                $pdf->SetX(25);
                $pdf->setCellHeightRatio(1.9);
                $pdf->MultiCell(275, 5, strtoupper($observacion), 0, "", 0, 1, "", "", true); // Observación
            }
        }
    } else if ($ot['queja'] == 'RCX') {
        $cuenta = $ot['cuenta'];
        $cod_otrb = $ot['orden'];
        // Se trae el ultimo periodo
        $sql = "SELECT max(cod_peri) AS periodo
            FROM factura_cab
            WHERE nro_factura IS NOT NULL AND cod_pred = $cuenta
        ";

        $exec = exe($sql) or die('Error al traer el último periodo de el predio ' . $cuenta. ' ' . $sql );
        $periodo = result($exec, 'periodo');

        if ($periodo) {
            // Acta de reconexión
            $pdf->AddPage();
            $pdf->Image('../../../FormatosEjecucionOT/img/ActaReconexion.jpg', 0, 0, '', '');

            $sql = "SELECT
                p.nombre AS solicitante,
                ot.direccion_clte,
                p.estrato,
                (SELECT SUM(coalesce(deuda_e1, 0) + coalesce(deuda_e2, 0) + coalesce(deuda_e3,0))
                    AS saldo
                FROM factura_cab
                WHERE cod_pred = $cuenta AND cod_peri = $periodo),
                fc.nropermora AS edad,
                fc.nro_factura,
                UPPER(ot.descripcion) AS observacion_ot
            FROM orden_trabajo ot
                LEFT JOIN factura_cab fc on fc.cod_pred = ot.cod_pred
                LEFT JOIN predio p ON p.cod_pred = ot.cod_pred
            WHERE ot.cod_pred = $cuenta AND ot.cod_ttrb = '".$ot['tipo_trabajo']."' AND fc.cod_peri = $periodo";

            $exec = exe($sql) or die('Error al consultar la orden de trabajo ' . $cod_otrb . ' ' . $sql);

            while ($res = fetch_object($exec)) {
                $fecha = date("Y/m/d", strtotime($ot['fechaemision']));

                $pdf->SetFontSize('8');
                $pdf->SetY(160);
                $pdf->SetX(470);
                $pdf->Cell(0,0,$cod_otrb,0);// Orden de Trabajo

                $pdf->SetFontSize('8');
                $pdf->SetY(178);
                $pdf->SetX(70);
                $pdf->Cell(0,0,$fecha, 0); //Fecha de Emision

                $pdf->SetFontSize('8');
                $pdf->SetY(178);
                $pdf->SetX(260);
                $pdf->Cell(0,0,$ot['ciclo'], 0); // Ciclo

                $pdf->SetFontSize('8');
                $pdf->SetY(178);
                $pdf->SetX(430);
                $pdf->Cell(0,0,$ot['ruta'], 0); // Ruta

                $pdf->SetFontSize('8');
                $pdf->SetY(193);
                $pdf->SetX(115);
                $pdf->Cell(0,0,$cuenta,0); //Predio

                $pdf->SetFontSize('8');
                $pdf->SetY(193);
                $pdf->SetX(273);
                $pdf->Cell(0,0,$res->solicitante, 0); // Solicitante

                $pdf->SetFontSize('8');
                $pdf->SetY(209);
                $pdf->SetX(89);
                $pdf->Cell(0,0,$res->direccion_clte, 0); // Dirección del solicitante

                $pdf->SetFontSize('8');
                $pdf->SetY(223.5);
                $pdf->SetX(92);
                $pdf->Cell(0,0,$ot['medidor'], 0); // Número del medidor

                $pdf->SetFontSize('8');
                $pdf->SetY(239);
                $pdf->SetX(250);
                $pdf->Cell(0,0,$ot['telefono'], 0); // Teléfono del solicitante

                $pdf->SetFontSize('8');
                $pdf->SetY(239);
                $pdf->SetX(445);
                $pdf->Cell(0,0,$res->nro_factura, 0); // Número de la última factura

                $pdf->SetFontSize('8');
                $pdf->SetY(254);
                $pdf->SetX(68);
                $pdf->Cell(0,0,number_format($res->saldo, 2, ',' , '.'), 0); // Saldo

                $pdf->SetFontSize('8');
                $pdf->SetY(254);
                $pdf->SetX(258);
                $pdf->Cell(0,0,$res->edad, 0); // Periodos mora

                $pdf->SetFontSize('8');
                $pdf->SetY(254);
                $pdf->SetX(450);
                $pdf->Cell(0,0,$ot['lectura'], 0); // Lectura del medidor

                $observacion = strlen($res->observacion_ot) > 218 ? substr($res->observacion_ot, 0, 218) . '...' : $res->observacion_ot;
                $observacion = "                            ". $observacion;
                $pdf->SetFontSize('8');
                $pdf->SetY(435);
                $pdf->SetX(30);
                $pdf->setCellHeightRatio(2.9);
                $pdf->MultiCell(560, 5, strtoupper($observacion), 0, "", 0, 1, "", "", true); // Observación
            }
        }
    } else if ($ot['queja'] == '591') {    
        // PDF DE INSTALACIÓN MACROMEDIDORES
        $fechaEmision = date("Y/m/d", strtotime($ot['fechaemision']));

        $pdf->AddPage('P');
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaInstRetMacromedidor1.jpg', 0, 0, '', '');

        $pdf->SetFontSize('8');
        $pdf->SetY(102);
        $pdf->SetX(67);
        $pdf->Cell(0, 0, $ot['orden'], 0); // Número de Orden de trabajo

        $pdf->SetFontSize('8');
        $pdf->SetY(102);
        $pdf->SetX(255);
        $pdf->Cell(0, 0, $fechaEmision, 0); // Fecha de emisión

        $pdf->SetFontSize('8');
        $pdf->SetY(102);
        $pdf->SetX(437);
        $pdf->Cell(0, 0, $ot['solicitante'], 0); // Nombre del solicitante

        $pdf->SetFontSize('8');
        $pdf->SetY(114);
        $pdf->SetX(72);
        $pdf->Cell(0, 0, $ot['cuenta'], 0); // Predio

        $pdf->SetFontSize('8');
        $pdf->SetY(114);
        $pdf->SetX(254);
        $pdf->Cell(0, 0, $ot['direccion'], 0); // Dirección

        $pdf->SetFontSize('8');
        $pdf->SetY(114);
        $pdf->SetX(448);
        $pdf->Cell(0, 0, strtoupper($ot['barrio']), 0); // Barrio

        $pdf->SetFontSize('8');
        $pdf->SetY(125);
        $pdf->SetX(93);
        $pdf->Cell(0, 0, $ot['medidor'], 0); // Número o serial del medidor

        $pdf->SetFontSize('8');
        $pdf->SetY(125);
        $pdf->SetX(450);
        $pdf->Cell(0, 0, $ot['lectura'], 0); // Última lectura registrada en el sistema

        $pdf->SetFontSize('8');
        $pdf->SetY(135);
        $pdf->SetX(65);
        $pdf->Cell(0, 0, $ot['ciclo'], 0); // Ciclo

        $pdf->SetFontSize('8');
        $pdf->SetY(135);
        $pdf->SetX(258);
        $pdf->Cell(0, 0, $ot['ruta'], 0); // Ruta

        $pdf->SetFontSize('8');
        $pdf->SetY(135);
        $pdf->SetX(371);
        $pdf->Cell(0, 0, $ot['ruta_consecutivo'], 0); // Ruta consecutivo

        $pdf->SetFontSize('8');
        $pdf->SetY(135);
        $pdf->SetX(450);
        $pdf->Cell(0, 0, $ot['uso'], 0); // Uso

        $descripcion = strlen($ot['descripcion']) > 319 ? substr($ot['descripcion'], 0, 317) . '...' : $ot['descripcion'];
        $descripcion = "                                 ". $descripcion;
        $pdf->SetFontSize('8');
        $pdf->SetY(149);
        $pdf->SetX(30);
        $pdf->setCellHeightRatio(1.55);
        $pdf->MultiCell(550, 5, strtoupper($descripcion), 0, "", 0, 1, "", "", true); // Observación

        $pdf->AddPage('P');
        $pdf->Image('../../../FormatosEjecucionOT/img/ActaVisitaInstRetMacromedidor2.jpg', 0, 0, '', '');
    }
}
$pdf->Output();
