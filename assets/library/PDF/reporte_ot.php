<?PHP
include '../../../archivos/comun.inc';	
require('fpdf.php');

if(!empty($_GET['ejecutar'])){
	if(!empty($_GET['cont'])&&!empty($_GET['empl']))
	$q = " AND tp.contasignado = ".$_GET['cont']." AND tp.emplasignado = ".$_GET['empl'];
	elseif(!empty($_GET['cont'])){
	$q = " AND tp.contasignado = ".$_GET['cont'];
	}
	$q.= (!empty($_GET['cod_otrb']) && !empty($_GET['municipio']))?" AND tp.cod_otrb =".$_GET['cod_otrb']:"";
	$tab = empty($_GET['cierre'])?"trabajos_pendientes":"trabajos_cerrados";
	
	$sql = "SELECT
			tp.cod_otrb, tp.matricula, tp.municipio,
			tq.descripcion AS conceptot, tp.barrio,
			tp.solicitante, tp.observacion, tp.fecha_grab, tp.prioridad, tp.usureporta,
			tp.direcciontrabajo, tp.telefono,
			ct.nombre      AS ncontrati,
			em.nombre      AS nempleado,
			pr.cod_cclo, pr.serialmedi, coalesce(tp.cod_pqr, 0) as cod_pqr,
			mu.descripcion AS nmunicipio, pri.descripcion as nprio, pri.t_respuesta
			FROM
				".$tab."    tp
				JOIN municipio mu ON (tp.municipio = mu.cod_munip AND
                                      tp.empresa =mu.cod_empr)
				JOIN tipo_queja        tq ON (tp.cod_concep   = tq.cod_tque AND
											  tp.empresa      = tq.cod_empr)
		        JOIN prioridad_ot pri ON (tp.prioridad = pri.cod_prio AND
                                          tp.empresa   = pri.cod_empr)
				JOIN contratista  ct ON (tp.contasignado = ct.cod_cont AND
											  tp.empresa      = ct.cod_empr)
				JOIN empleado     em ON (tp.contasignado = em.cod_cont AND
											  tp.emplasignado = em.cod_empl AND
											  tp.empresa      = ct.cod_empr)
				LEFT OUTER JOIN predio pr
                            ON (pr.cod_pred = tp.matricula AND pr.cod_munip = tp.municipio AND
                                pr.cod_empr = tp.empresa)
			WHERE
			to_date(tp.fecha_grab, 'yyyy-mm-dd') BETWEEN
			to_date('".$_GET['inicio']."', 'dd/mm/yyyy')    AND
			to_date('".$_GET['final']."', 'dd/mm/yyyy')    AND
			tp.municipio = ".$_GET['municipio']."     AND
			tp.empresa = '".$_SESSION['cod_empr']."'  AND  
			tq.restriccion = 0 AND
			fechainicio IS NULL ".$q."ORDER BY cod_otrb";

	$rs = odbc_exec($conexion, $sql);
	$n  = odbc_num_rows($rs); 
	
}

class PDF extends FPDF
{
//Cabecera de p�gina
function Header()
{
    //Logo
    $this->Image('logo.jpg',10,8,33);
    //Arial bold 15
    $this->SetFont('Arial','B',12);
    $this->Cell(53); //Movernos a la derecha
    //T�tulo
	$this->Cell(86); //Movernos a la derecha
	//$this->Cell(3,30,'Orden de Trabajo N�',0,0,'C');
    $this->Ln(5);
    $this->SetFont('Arial','B',10);
	$this->Cell(126); //Movernos a la derecha
	//$this->Cell(3,30,'Matricula',0,0,'C');

    //Salto de l�nea
   // $this->Ln(20);
}

//Pie de p�gina
}
$pdf=new PDF();
$pdf->AliasNbPages();
//echo $n;
if(!empty($n)){
	while( $rw = odbc_fetch_array($rs) ){
		if ($rw['matricula'] > 0){
			
			$sql ="SELECT
					fc.cod_pred, fc.nombre, fc.direccion, fc.rutareparto,
					fc.lectura, pr.nombre
					FROM
						factura_cab fc
						JOIN predio pr USING (cod_pred, cod_munip, cod_empr)
					WHERE cod_pred = ".$rw['matricula']."
					AND cod_peri = (
					Select max(cod_peri)
					from fact_ciclo_peri
					Where cod_cclo  = '".$rw['cod_cclo']."' AND
						  cod_empr  = '".$_SESSION['cod_empr']."' AND
						  cod_munip = ".$_GET['municipio']." AND
						  feccierra is NOT NULL)
					AND cod_munip = ".$_GET['municipio']." AND 
					cod_empr = '".$_SESSION['cod_empr']."' AND estado = 'A'";
			$rsp = odbc_exec($conexion, $sql);
			$predio = odbc_result($rsp, 1);
			$nombre = odbc_result($rsp, 2);
			$direccion = odbc_result($rsp, 3);
			$rutareparto = odbc_result($rsp, 4);
			$lectura = odbc_result($rsp, 5);
			$nombre_dueno = odbc_result($rsp, 6);
			
		}else{
			$predio = "-NA-";
			$nombre = $rw['solicitante'];
			$direccion = $rw['direcciontrabajo'];
			$telefono=$rw['telefono'];
			$rutareparto = "-NA- ";
			$lectura = " ";
			$nombre_dueno = "-NA-";
		}
		if($rw['cod_pqr']>0){
			$sql = "SELECT pm.descripcion 
					FROM 
						pqr pq 
						JOIN pqr_motivo pm USING (cod_tpqr, cod_mpqr, cod_empr)
					WHERE cod_pqr = ".$rw['cod_pqr']." AND 
						  cod_munip = ".$_GET['municipio']." AND 
						  cod_empr = '".$_SESSION['cod_empr']."'";
			$rspq  = odbc_exec($conexion, $sql);
			$concepqr = odbc_result($rspq, 1);
		}else{  $concepqr="-NA-"; }

		$pdf->AddPage();
		$user = odbc_result($rs, 10);
		$query_muni=odbc_exec($conexion,"SELECT razonsocial, direccion FROM municipio WHERE cod_munip=".$_GET['municipio']."");
		$nom_muni=odbc_result($query_muni,'razonsocial');
		$direc_muni=odbc_result($query_muni,'direccion');
		$query_user=odbc_exec($conexion,"SELECT nombreper FROM usuarios WHERE idusuario='$user' and cod_empr='".$_SESSION['cod_empr']."'");
		$nom_user=odbc_result($query_user,'nombreper');

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','B',11);

$pdf->SetY(29);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,''.$nom_muni,0,0,'L',1);
$pdf->SetY(24);$pdf->SetX(125);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,'ORDEN DE TRABAJO N�',0,0,'L',1);
$pdf->SetY(24);$pdf->SetX(175);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,''.$rw['cod_otrb'],0,0,'L',1);
$pdf->SetY(30);$pdf->SetX(125);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,'MATRICULA',0,0,'L',1);
$pdf->SetY(30);$pdf->SetX(150);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,''.$predio,0,0,'L',1);

$pdf->SetFont('Times','',10);


$pdf->SetY(35);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Direccion: '.$direccion,0,'L',1);
$pdf->SetY(35);$pdf->SetX(122);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Barrio: '.$rw['barrio'],0,'L',1);


$pdf->SetY(40);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Solicitante: '.$rw['solicitante'],0,'L',1);
$pdf->SetY(40);$pdf->SetX(122);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Tel.: '.$rw['telefono'],0,'L',1);//telefono OT

$pdf->SetY(45);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande


//$pdf->Cell(20,4,'Propietario: '.$predio,0,'L',1);

//Codigo nuevo ////
/*$sql_dueno = "SELECT nombre FROM predio 
					WHERE  cod_munip = ".$_GET['cod_munip']." 
					  AND  cod_empr ='".$_SESSION['cod_empr']."'
					  AND  cod_pred = ".$predio;
	$res_dueno = odbc_exec($conexion, $sql_dueno);
	$row_dueno = odbc_fetch_array($res_dueno);
	$nombre_dueno = $row_dueno["nombre"];	*/
	
$pdf->Cell(20,4,'Propietario: '.$nombre_dueno,0,'L',1);
///Fin del codigo nuevo///


$pdf->SetY(45);$pdf->SetX(122);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Ruta: '.$rutareparto,0,'L',1);


$pdf->SetY(50);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'N� Medidor: '.$rw['serialmedi'],0,'L',1);

$pdf->SetY(50);$pdf->SetX(122);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Ult Lectura: '.$lectura,0,'L',1);

$pdf->SetY(50);$pdf->SetX(162);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'prioridad: '.$rw['nprio'].'  '.$rw['t_respuesta'],0,'L',1);

$pdf->SetY(51);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,5,'__________________________________________________________________________________________________________',0,'L',1);

$pdf->SetY(57);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Contratista: '.$rw['ncontrati'],0,'L',1);

$pdf->SetY(57);$pdf->SetX(112);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Empleado: '.$rw['nempleado'],0,'L',1);

$pdf->SetY(62);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,5,'Concepto PQR: '.$concepqr,0,'L',1);

$pdf->SetY(62);$pdf->SetX(112);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,5,'N PQR: '.$rw['cod_pqr'],0,'L',1);

$pdf->SetY(68);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Concepto O.T:',0,'L',1);
$pdf->SetY(68);$pdf->SetX(33);//$pdf->SetY(70);//recuadro grande
$pdf->MultiCell(75, 4,''.$rw['conceptot'],0,'L',1);

$pdf->SetY(68);$pdf->SetX(112);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Fecha O.T: '.$rw['fecha_grab'],0,'L',1);


$pdf->SetY(78);$pdf->SetX(150);
$pdf->Cell(12,4,''.$nom_user,0,0,'C',1);
$pdf->SetY(78);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'Descripcion de la Orden de Trabajo ',0,'L',1);
$pdf->Rect(10, 82, 190, 15, f); 
$pdf->SetY(82);//recuadro grande
$pdf->SetX(10);
$pdf->MultiCell(190, 4,' '.$rw['observacion'] ,0 ,'T' , 0) ;

$pdf->SetY(100);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(80,4,'Actividades Ejecutadas',1,0,'C',1);
$pdf->SetFont('Times','',12);
$pdf->SetY(104);//recuadro grande
$pdf->SetX(10);
$pdf->Cell(80,88,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(10, 112, 90, 112); 
$pdf->Line(10, 117, 90, 117); 
$pdf->Line(10, 122, 90, 122);
$pdf->Line(10, 127, 90, 127);
$pdf->Line(10, 132, 90, 132);
$pdf->Line(10, 137, 90, 137);
$pdf->Line(10, 142, 90, 142);
$pdf->Line(10, 147, 90, 147);
$pdf->Line(10, 152, 90, 152);
$pdf->Line(10, 157, 90, 157);
$pdf->Line(10, 162, 90, 162);
$pdf->Line(10, 167, 90, 167);
$pdf->Line(10, 172, 90, 172);
$pdf->Line(10, 177, 90, 177);
$pdf->Line(10, 182, 90, 182);
$pdf->Line(10, 187, 90, 187);

$pdf->SetFont('Times','B',10);
$pdf->SetY(107);$pdf->SetX(11);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Lectura:',0,0,'C',1);

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetFont('Times','B',10);
$pdf->SetY(100);$pdf->SetX(95);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(105,4,'Funcionarios',1,0,'C',1);

$pdf->SetY(104);$pdf->SetX(95);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(45,4,'Nombre',1,0,'C',1);
$pdf->SetY(108);$pdf->SetX(95);
$pdf->Cell(45,32,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(95, 113, 160, 113);
$pdf->Line(95, 118, 160, 118);
$pdf->Line(95, 123, 160, 123);
$pdf->Line(95, 128, 160, 128);
$pdf->Line(95, 133, 160, 133);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetY(104);$pdf->SetX(140);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Fecha',1,0,'C',1);
$pdf->SetY(108);$pdf->SetX(140);
$pdf->Cell(20,32,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(140, 113, 160, 113);
$pdf->Line(140, 118, 160, 118);
$pdf->Line(140, 123, 160, 123);
$pdf->Line(140, 128, 160, 128);
$pdf->Line(140, 133, 160, 133);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetY(104);$pdf->SetX(160);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Hora Inicio',1,0,'C',1);
$pdf->SetY(108);$pdf->SetX(160);
$pdf->Cell(20,32,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(160, 113, 180, 113);
$pdf->Line(160, 118, 180, 118);
$pdf->Line(160, 123, 180, 123);
$pdf->Line(160, 128, 180, 128);
$pdf->Line(160, 133, 180, 133);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetY(104);$pdf->SetX(180);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(20,4,'Hora Final',1,0,'C',1);
$pdf->SetY(108);$pdf->SetX(180);
$pdf->Cell(20,32,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(180, 113, 200, 113);
$pdf->Line(180, 118, 200, 118);
$pdf->Line(180, 123, 200, 123);
$pdf->Line(180, 128, 200, 128);
$pdf->Line(180, 133, 200, 133);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetFont('Times','B',10);
$pdf->SetY(144);$pdf->SetX(95);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(105,4,'Materiales Utilizados',1,0,'C',1);

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);
$pdf->SetY(148);$pdf->SetX(95);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(12,4,'Cod',1,0,'C',1);
$pdf->SetY(152);$pdf->SetX(95);
$pdf->Cell(12,40,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(95, 157, 115, 157);
$pdf->Line(95, 162, 115, 162);
$pdf->Line(95, 167, 115, 167);
$pdf->Line(95, 172, 115, 172);
$pdf->Line(95, 177, 115, 177);
$pdf->Line(95, 182, 115, 182);
$pdf->Line(95, 187, 115, 187);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);



$pdf->SetY(148);$pdf->SetX(107);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(57,4,'Descripcion',1,0,'C',1);
$pdf->SetY(152);$pdf->SetX(107);
$pdf->Cell(57,40,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(107, 157, 165, 157);
$pdf->Line(107, 162, 165, 162);
$pdf->Line(107, 167, 165, 167);
$pdf->Line(107, 172, 165, 172);
$pdf->Line(107, 177, 165, 177);
$pdf->Line(107, 182, 165, 182);
$pdf->Line(107, 187, 165, 187);

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetY(148);$pdf->SetX(164);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(13,4,'DIAM',1,0,'C',1);
$pdf->SetY(152);$pdf->SetX(164);
$pdf->Cell(13,40,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(164, 157, 177, 157);
$pdf->Line(164, 162, 177, 162);
$pdf->Line(164, 167, 177, 167);
$pdf->Line(164, 172, 177, 172);
$pdf->Line(164, 177, 177, 177);
$pdf->Line(164, 182, 177, 182);
$pdf->Line(164, 187, 177, 187);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetY(148);$pdf->SetX(177);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(12,4,'CANT',1,0,'C',1);
$pdf->SetY(152);$pdf->SetX(177);
$pdf->Cell(12,40,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(177, 157, 188, 157);
$pdf->Line(177, 162, 188, 162);
$pdf->Line(177, 167, 188, 167);
$pdf->Line(177, 172, 188, 172);
$pdf->Line(177, 177, 188, 177);
$pdf->Line(177, 182, 188, 182);
$pdf->Line(177, 187, 188, 187);

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetY(148);$pdf->SetX(188);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(12,4,'Und',1,0,'C',1);
$pdf->SetY(152);$pdf->SetX(188);
$pdf->Cell(12,40,' ',1,0,'C',1);
$pdf->SetDrawColor(192,192,200);
$pdf->Line(188, 157, 200, 157);
$pdf->Line(188, 162, 200, 162);
$pdf->Line(188, 167, 200, 167);
$pdf->Line(188, 172, 200, 172);
$pdf->Line(188, 177, 200, 177);
$pdf->Line(188, 182, 200, 182);
$pdf->Line(188, 187, 200, 187);

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);

$pdf->SetFont('Times','B',10);

$pdf->SetY(193);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'Fecha Inicio:',0,0,'C',1);
$pdf->SetY(193);$pdf->SetX(50);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'Hora Inicio:',0,0,'C',1);

$pdf->SetY(193);$pdf->SetX(95);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'Fecha Final:',0,0,'C',1);
$pdf->SetY(193);$pdf->SetX(155);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'Hora Final:',0,0,'C',1);

$pdf->SetY(198);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'Facturar:   SI',0,0,'C',1);
$pdf->Line(38, 198, 38, 202);
$pdf->Line(43, 198, 43, 202);
$pdf->Line(38, 198, 43, 198);
$pdf->Line(38, 202, 43, 202);
$pdf->SetY(198);$pdf->SetX(44);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,4,'NO ',0,0,'C',1);
$pdf->Line(63, 198, 63, 202);
$pdf->Line(68, 198, 68, 202);
$pdf->Line(63, 198, 68, 198);
$pdf->Line(63, 202, 68, 202);


$pdf->SetY(207);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(90,2,'___________________________',0,0,'C',1);
$pdf->SetY(212);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(90,2,'EMPLEADO',0,0,'C',1);

$pdf->SetY(207);$pdf->SetX(110);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(90,2,'___________________________',0,0,'C',1);
$pdf->SetY(212);$pdf->SetX(110);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(90,4,'USUARIO',0,0,'C',1);


$pdf->SetFont('Times','B',14);
$pdf->SetY(218);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(190,4,'- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ',0,0,'C',1);

$pdf->SetFont('Times','',10);

$pdf->SetY(225);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,''.$nom_muni,0,0,'L',1);

$pdf->SetY(231);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,'N PQR:'.$rw['cod_pqr'],0,0,'L',1);

$pdf->SetY(231);$pdf->SetX(120);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,'Orden de Trabajo N�: '.$rw['cod_otrb'],0,0,'L',1);

$pdf->SetY(239);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,'Se�or Suscriptor y/o Usuario: '.$rw['solicitante'],0,0,'L',1);

$pdf->SetY(235);$pdf->SetX(120);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,2,'Matricula: '.$predio,0,0,'L',1);

$pdf->SetY(244);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(180,2,'EL DIA _____ MES _____ A�O _____ A LAS ______ SE REALIZO VISITA TECNICA A SU PREDIO CON EL FIN',0,0,'L',1);
$pdf->SetY(248);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(180,2,'DE _______________________________________________________________',0,0,'L',1);


$pdf->SetY(253);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(180,4,'OBSERVACIONES __________________________________________________________________________________ ',0,0,'L',1);

$pdf->SetFont('Times','',8);

$pdf->SetY(257);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
/*$pdf->Cell(180,4,'Por favor presentarse en los siguientes cinco d�as, a las oficinas de Atenci�n al Cliente o llamar a los telefonos que aparecen al respaldo de la factura',0,0,'L',1);*/
$pdf->SetY(261);$pdf->SetX(15);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(180,2,'para averiguar por su solicitud ',0,0,'L',1);

$pdf->SetFont('Times','B',7);
$pdf->SetY(265);$pdf->SetX(10);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(190,10,' ',1,0,'C',1);
$pdf->SetY(266);$pdf->SetX(20);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(170,2,'VIGILADO POR LA SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS',0,0,'C',1);
$pdf->SetY(269);$pdf->SetX(20);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(170,2,''.$direc_muni,0,0,'C',1);
$pdf->SetY(272);$pdf->SetX(20);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(170,2,'Linea Gratuita de Atenci�n al Cliente las 24h Marque 116',0,0,'C',1);
	}
}
$pdf->Output();
?> 