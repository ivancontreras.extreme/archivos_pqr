<?PHP
include '../../../archivos/comun.inc';	
require('fpdf.php');

if(!empty($_GET['ejecutar'])){
	if(!empty($_GET['cont'])&&!empty($_GET['empl']))
	$q = " AND tp.contasignado = ".$_GET['cont']." AND tp.emplasignado = ".$_GET['empl'];
	elseif(!empty($_GET['cont'])){
	$q = " AND tp.contasignado = ".$_GET['cont'];
	}
	$q.= (!empty($_GET['cod_otrb']) && !empty($_GET['municipio']))?" AND tp.cod_otrb =".$_GET['cod_otrb']:"";
	$tab = empty($_GET['cierre'])?"trabajos_pendientes":"trabajos_cerrados";
	$ejecu = empty($_GET['cierre'])?" AND fechainicio IS NULL ":" ";
	$cod_munip=$_GET['municipio'];
	/*$sql = "SELECT
			tp.cod_otrb, tp.matricula, tp.municipio,
			tq.descripcion AS conceptot, tp.barrio,
			tp.solicitante, tp.observacion, tp.fecha_grab, tp.prioridad, tp.usureporta,
			tp.direcciontrabajo, tp.telefono,
			ct.nombre      AS ncontrati,
			em.nombre      AS nempleado,
			pr.cod_cclo, pr.serialmedi, coalesce(tp.cod_pqr, 0) as cod_pqr,
			mu.descripcion AS nmunicipio, pri.descripcion as nprio, pri.t_respuesta
			FROM
				".$tab."    tp
				JOIN municipio mu ON (tp.municipio = mu.cod_munip AND
                                      tp.empresa =mu.cod_empr)
				JOIN tipo_queja        tq ON (tp.cod_concep   = tq.cod_tque AND
											  tp.empresa      = tq.cod_empr)
		        JOIN prioridad_ot pri ON (tp.prioridad = pri.cod_prio AND
                                          tp.empresa   = pri.cod_empr)
				JOIN contratista  ct ON (tp.contasignado = ct.cod_cont AND
											  tp.empresa      = ct.cod_empr)
				JOIN empleado     em ON (tp.contasignado = em.cod_cont AND
											  tp.emplasignado = em.cod_empl AND
											  tp.empresa      = ct.cod_empr)
				LEFT OUTER JOIN predio pr
                            ON (pr.cod_pred = tp.matricula AND pr.cod_munip = tp.municipio AND
                                pr.cod_empr = tp.empresa)
			WHERE
			to_date(tp.fecha_grab, 'yyyy-mm-dd') BETWEEN
			to_date('".$_GET['inicio']."', 'dd/mm/yyyy')    AND
			to_date('".$_GET['final']."', 'dd/mm/yyyy')    AND
			tp.municipio = ".$_GET['municipio']."     AND
			tp.empresa = '".$_SESSION['cod_empr']."'  AND  
			tq.restriccion = 0 AND
			fechainicio IS NULL ".$q."ORDER BY cod_otrb";*/
	
		$sql = "SELECT
			tp.cod_otrb, tp.matricula, tp.municipio,
			tq.descripcion AS conceptot, tp.barrio,
			tp.solicitante, tp.observacion, tp.fecha_grab, tp.prioridad, tp.usureporta,
			tp.direcciontrabajo, tp.telefono, 
			coalesce(tp.cod_pqr, 0) as cod_pqr,
			mu.descripcion AS nmunicipio, pri.descripcion as nprio, pri.t_respuesta, tp.feclim
			FROM
				".$tab."    tp, tipo_queja tq, prioridad_ot pri, municipio mu
			WHERE
			tp.cod_concep=tq.cod_tque and tp.empresa=tq.cod_empr and
			tp.prioridad=pri.cod_prio and tp.empresa=pri.cod_empr and
			tp.municipio=mu.cod_munip and tp.empresa=mu.cod_empr and
			date(tp.fecha_grab) BETWEEN
			to_date('".$_GET['inicio']."', 'dd/mm/yyyy')    AND
			to_date('".$_GET['final']."', 'dd/mm/yyyy')    AND
			tp.municipio = ".$_GET['municipio']."     AND
			tp.empresa = '".$_SESSION['cod_empr']."' ".$q."  AND  
			tq.restriccion = 0 ".$ejecu." 
			ORDER BY cod_otrb";

	$rs = odbc_exec($conexion, $sql);
	$n  = odbc_num_rows($rs); 
	
}

class PDF extends FPDF
{
//Cabecera de p�gina
function Header()
{
    /*//Logo quitado 25/07/2012 para poner logo de AB
    
    $this->Image('logo.jpg',10,8,33);*/
    //Arial bold 15
    $this->SetFont('Arial','B',12);
    $this->Cell(53); //Movernos a la derecha
    //T�tulo
	$this->Cell(86); //Movernos a la derecha
	//$this->Cell(3,30,'Orden de Trabajo N�',0,0,'C');
    $this->Ln(5);
    $this->SetFont('Arial','B',10);
	$this->Cell(126); //Movernos a la derecha
	//$this->Cell(3,30,'Matricula',0,0,'C');

    //Salto de l�nea
   // $this->Ln(20);
}

//Pie de p�gina
}
$pdf=new PDF();
$pdf->AliasNbPages();
//echo $n;
if(!empty($n)){
	while( $rw = odbc_fetch_array($rs) ){
		if ($rw['matricula'] > 0){
			
			$sql ="SELECT
					pr.cod_pred, pr.direccion, pr.rutareparto,
					0 as lectura, substr(pr.nombre, 0, 36) as nombre, pr.serialmedi,
					pr.cod_cclo
					FROM
						predio pr 
					WHERE cod_pred = ".$rw['matricula']."
					AND cod_munip = ".$_GET['municipio']." AND 
					cod_empr = '".$_SESSION['cod_empr']."' AND estado = 'A'
					order by cod_peri desc  limit 1 offset 0";
			$rsp = odbc_exec($conexion, $sql);
			$predio = odbc_result($rsp, 'cod_pred');
			$nombre = odbc_result($rsp, 'nombre');
			$direccion = odbc_result($rsp, 'direccion');
			$telefono = $rw['telefono'];
			$rutareparto = odbc_result($rsp, 'rutareparto');
			$lectura = odbc_result($rsp, 'lectura');
			$nombre_dueno = odbc_result($rsp, 'nombre');
			$vserialmedi = odbc_result($rsp, 'serialmedi');
			$cclo = odbc_result($rsp, 'cod_cclo');
			
		}else{
			$predio = "-NA-";
			$nombre = $rw['solicitante'];
			$direccion = $rw['direcciontrabajo'];
			$telefono=$rw['telefono'];
			$rutareparto = "-NA- ";
			$lectura = " ";
			$nombre_dueno = "-NA-";
			$vserialmedi = " ";
			$cclo = "-NA-";
		}
		if($rw['cod_pqr']>0){
			$sql = "SELECT pm.descripcion 
					FROM 
						pqr pq 
						JOIN pqr_motivo pm USING (cod_tpqr, cod_mpqr, cod_empr)
					WHERE cod_pqr = ".$rw['cod_pqr']." AND 
						  cod_munip = ".$_GET['municipio']." AND 
						  cod_empr = '".$_SESSION['cod_empr']."'";
			$rspq  = odbc_exec($conexion, $sql);
			$concepqr = odbc_result($rspq, 1);
		}else{  $concepqr="-NA-"; }

		$pdf->AddPage();
		$user = odbc_result($rs, 10);
		$query_muni=odbc_exec($conexion,"SELECT razonsocial, direccion FROM municipio WHERE cod_munip=".$_GET['municipio']."");
		$nom_muni=odbc_result($query_muni,'razonsocial');
		$direc_muni=odbc_result($query_muni,'direccion');
		$query_user=odbc_exec($conexion,"SELECT nombreper FROM usuarios WHERE idusuario='$user' and cod_empr='".$_SESSION['cod_empr']."'");
		$nom_user=odbc_result($query_user,'nombreper');

$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','B',11);
$style3 = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => '5, 5', 'color' => array(0, 0, 0));

//Logo 27/007/2012 logo AB o Caudales
    if($cod_munip==6){
	$pdf->SetY(15);$pdf->SetX(12);
	$pdf->Image('AB_LOGO.JPG',10,16,65);
	}
	//$pdf->Image('logo.jpg',10,8,33);
	if($cod_munip==1){
	//$pdf->Image('LogoCaudalesMELGAR.jpg',10,8,33);
	$pdf->Image('LAS_CEIBAS.jpg',10,8,33);
	}
	if($cod_munip==2){
	//$pdf->Image('LogoCaudalesMOSQUERA.jpg',10,8,33);
	$pdf->Image('Hmosquera-Logo.jpg',10,8,33);
	}
	if($cod_munip==3){
	$pdf->Image('LogoCaudalesCHIA.jpeg',10,8,33);
	}
	if($cod_munip==4){
	$pdf->Image('ESSMAR_ESP.jpg',10,8,33);
	}
    
/*$pdf->SetY(15);$pdf->SetX(110);//$pdf->SetY(70);//recuadro grande
$pdf->Cell(30,0,''.$nom_muni,0,0,'L',1);25/07/2012 r. social*/

//$pdf->SetDrawColor(192,192,200);
$pdf->Line(10, 30, 200, 30);
	$pdf->SetFont('Times','',10);
	$pdf->SetY(32);$pdf->SetX(12);
	$pdf->Cell(30,0,'ORDEN DE TRABAJO',0,0,'L',1);
	$pdf->SetY(36);$pdf->SetX(35);/////Dato
	$pdf->Cell(30,0,$rw['cod_otrb'],0,0,'L',1); ////Datos	
			$pdf->Line(79, 30, 79, 39);
	
	$pdf->SetY(32);$pdf->SetX(80);
	$pdf->Cell(30,0,'PQR No.',0,0,'L',1);			
	$pdf->SetY(36);$pdf->SetX(80);/////Dato
	$pdf->Cell(30,0,$rw['cod_pqr'],0,0,'L',1); ////Datos	
	//$pdf->Line(139, 30, 139, 39);
	$pdf->Line(100, 30, 100, 39);
	
	$pdf->SetY(32);$pdf->SetX(104);
	$pdf->Cell(30,0,'CICLO',0,0,'L',1);			
	$pdf->SetY(36);$pdf->SetX(104);/////Dato
	$pdf->Cell(30,0,$cclo,0,0,'L',1); ////Datos	
	$pdf->Line(139, 30, 139, 39);
	
	$pdf->SetY(32);$pdf->SetX(140);
	$pdf->Cell(30,0,'MATRICULA No.',0,0,'L',1);
	$pdf->SetY(36);$pdf->SetX(160);//dattos
	$pdf->Cell(30,0,''.$predio,0,0,'L',1); ///datos
	
	
$pdf->Line(10, 39, 200, 39);
	$pdf->SetY(41);$pdf->SetX(12);
	$pdf->Cell(30,0,'PROPIETARIO',0,0,'L',1);
	$pdf->SetY(45);$pdf->SetX(12);
	$pdf->Cell(30,0,$nombre_dueno,0,0,'L',1);
	
			$pdf->Line(89, 39, 89, 48);			
			
	$pdf->SetY(41);$pdf->SetX(90);
	$pdf->Cell(30,0,'SOLICITANTE',0,0,'L',1);
			$pdf->Line(149, 39, 149, 48);
	$pdf->SetY(45);$pdf->SetX(90);
	$pdf->Cell(30,0,$rw['solicitante'],0,0,'L',1);		
		
	$pdf->SetFont('Times','',8);
	$pdf->SetY(41);$pdf->SetX(150);
	$pdf->Cell(30,0,'FECHA OT.',0,0,'L',1);
	//$pdf->SetY(45);$pdf->SetX(160);
	$pdf->SetY(41);$pdf->SetX(170);
	$pdf->Cell(30,0,$rw['fecha_grab'],0,0,'L',1);
	
	$pdf->SetY(45);$pdf->SetX(150);
	$pdf->Cell(30,0,'FEC. LIMITE',0,0,'L',1);
	$pdf->SetY(45);$pdf->SetX(170);
	$pdf->Cell(30,0,$rw['feclim'],0,0,'L',1);
	
$pdf->Line(10, 48, 200, 48);
	$pdf->SetFont('Times','',5);
	$pdf->SetY(50);$pdf->SetX(12);
	$pdf->Cell(30,0,'Concepto	',0,0,'L',1);
	$pdf->SetFont('Times','',8);
	$pdf->SetY(51);$pdf->SetX(12);
	$pdf->MultiCell(75, 2.3,''.$rw['conceptot'] ,0 ,'T' , 0) ;
	
			$pdf->Line(87, 48, 87, 63);
	
	$pdf->SetFont('Times','',5);
	$pdf->SetY(50);$pdf->SetX(87);
	$pdf->Cell(30,0,'Descripcion',0,0,'L',1);
	
	$pdf->SetFont('Times','',8);
	$pdf->SetY(51);$pdf->SetX(88);
	$pdf->MultiCell(110, 2.3,''.$rw['observacion'] ,0 ,'T' , 0) ;
	$pdf->SetFont('Times','',10);
	
$pdf->Line(10, 63, 200, 63);
$pdf->Line(10, 30, 10, 63);
$pdf->Line(200, 30, 200, 63);

//----
$pdf->SetFont('Times','',9);
$pdf->Line(10, 64, 200, 64);
		$pdf->SetY(66);$pdf->SetX(15);
		$pdf->Cell(30,0,'INFORMACI�N DEL PREDIO',0,0,'L',1);
				$pdf->Line(65, 64, 65, 92);		
		$pdf->SetY(66);$pdf->SetX(75);
		$pdf->Cell(30,0,'INFORMACI�N  ACOMETIDA',0,0,'L',1);
				$pdf->Line(128, 64, 128, 92);
		$pdf->SetY(66);$pdf->SetX(135);
		$pdf->Cell(30,0,'INFORMACI�N DEL MEDIDOR',0,0,'L',1);	
$pdf->Line(10, 68, 200, 68);
		$pdf->SetY(70);$pdf->SetX(11);
		$pdf->Cell(30,0,'Direccion Predio : ',0,0,'L',1);
		
		$pdf->SetFont('Times','',6);
		$pdf->SetY(73);$pdf->SetX(11);
		//$pdf->Cell(30,0,$direccion,0,0,'L',1);
		$pdf->Cell(30,0,$rw['direcciontrabajo'],0,0,'L',1);
		
		
		$pdf->SetFont('Times','',8);		
		$pdf->SetY(70);$pdf->SetX(65);
		$pdf->Cell(30,0,'En buen estado y funcionamiento : ',0,0,'L',1);
			 	$pdf->Line(122, 68, 122, 92);
		
		$pdf->SetY(70);$pdf->SetX(128);
		$pdf->Cell(30,0,'Lectura : ',0,0,'L',1);
				$pdf->Line(155, 68, 155, 92);

		$pdf->SetY(70);$pdf->SetX(155);
		$pdf->Cell(30,0,'Funcionando Normalmente : ',0,0,'L',1);
					$pdf->Line(194, 68, 194, 92);
$pdf->Line(65, 72, 200, 72);
		$pdf->SetY(74);$pdf->SetX(65);
		$pdf->Cell(30,0,'Fallando/ Mal estado : ',0,0,'L',1);
		
		$pdf->SetY(74);$pdf->SetX(128);
		$pdf->Cell(30,0,'Diametro : ',0,0,'L',1);
		
		$pdf->SetY(74);$pdf->SetX(155);
		$pdf->Cell(30,0,'Fallando/ Mal estado : ',0,0,'L',1);
		
$pdf->Line(10, 76, 200, 76);
		$pdf->SetY(78);$pdf->SetX(11);
		$pdf->Cell(30,0,'Barrio : ',0,0,'L',1);
		
		$pdf->SetFont('Times','',7);
		$pdf->SetY(81);$pdf->SetX(11);
		$pdf->Cell(30,0,$rw['barrio'].' - Tel:'.$telefono,0,0,'L',1);
		$pdf->SetFont('Times','',8);
		
		
		
		$pdf->SetY(78);$pdf->SetX(65);
		$pdf->Cell(30,0,'Suspendida : ',0,0,'L',1);
		
		$pdf->SetY(78);$pdf->SetX(128);
		$pdf->Cell(30,0,'Marca : ',0,0,'L',1);
		
		$pdf->SetY(78);$pdf->SetX(155);
		$pdf->Cell(30,0,'No se puede Leer : ',0,0,'L',1);
		
$pdf->Line(65, 80, 128, 80);	
$pdf->Line(155, 80, 200, 80);	
	
		$pdf->SetY(82);$pdf->SetX(65);
		$pdf->Cell(30,0,'No Instalada : ',0,0,'L',1);		
				
		$pdf->SetY(82);$pdf->SetX(155);
		$pdf->Cell(30,0,'No instalado : ',0,0,'L',1);
		
$pdf->Line(10, 84, 200, 84);
		$pdf->SetY(86);$pdf->SetX(11);
		$pdf->Cell(30,0,'No Medidor : ',0,0,'L',1);
		$pdf->SetY(90);$pdf->SetX(11);
		//$pdf->Cell(30,0,$rw['serialmedi'],0,0,'L',1);
		$pdf->Cell(30,0,$vserialmedi,0,0,'L',1);
		
		
				$pdf->Line(35, 84, 35, 92);
		$pdf->SetY(86);$pdf->SetX(35);
		$pdf->Cell(30,0,'Ult Lectura : ',0,0,'L',1);
		$pdf->SetY(90);$pdf->SetX(46);
		$pdf->Cell(30,0,$lectura,0,0,'L',1);
		
		
		
		$pdf->SetY(86);$pdf->SetX(65);
		$pdf->Cell(30,0,'Directa: ',0,0,'L',1);
		
		$pdf->SetY(86);$pdf->SetX(128);
		$pdf->Cell(30,0,'No Serie : ',0,0,'L',1);
		
		$pdf->SetY(86);$pdf->SetX(155);
		$pdf->Cell(30,0,'No Coincide : ',0,0,'L',1);
		
$pdf->Line(65, 88, 128, 88);
$pdf->Line(155, 88, 200, 88);			
		$pdf->SetY(90);$pdf->SetX(65);
		$pdf->Cell(30,0,'Posible Irregularidad: ',0,0,'L',1);		
	
		$pdf->SetY(90);$pdf->SetX(155);
		$pdf->Cell(30,0,'Con posible Irregularidad : ',0,0,'L',1);

$pdf->Line(10, 92, 200, 92);
$pdf->Line(10, 64, 10, 92);
$pdf->Line(200, 64, 200, 92);	
	
////--

$pdf->Line(10, 93, 200, 93);
	$pdf->SetY(95);$pdf->SetX(10);
	$pdf->Cell(30,0,'ESTADO DEL PREDIO',0,0,'L',1);
			$pdf->Line(44, 93, 44, 113);
	$pdf->SetY(95);$pdf->SetX(45);
	$pdf->Cell(30,0,'USO DEL PREDIO',0,0,'L',1);
			$pdf->Line(73, 93, 73, 113);
	$pdf->SetY(95);$pdf->SetX(85);
	$pdf->Cell(30,0,'DATOS DEL PREDIO',0,0,'L',1);
			$pdf->Line(129, 93, 129, 113);
	$pdf->SetY(95);$pdf->SetX(130);
	$pdf->Cell(30,0,'SERVICIOS PREDIO',0,0,'L',1);
			$pdf->Line(160, 93, 160, 113);
	$pdf->SetY(95);$pdf->SetX(160);
	$pdf->Cell(30,0,'RESULTADO INSPECCION',0,0,'L',1);	
$pdf->Line(10, 97, 200, 97);

$pdf->Line(10, 101, 160, 101);
$pdf->Line(190, 101, 200, 101);
	$pdf->SetY(99);$pdf->SetX(10);
	$pdf->Cell(30,0,'Habitado y en uso',0,0,'L',1);
		$pdf->Line(39, 97, 39, 113);
		
	$pdf->SetY(99);$pdf->SetX(45);
	$pdf->Cell(30,0,'Residencial',0,0,'L',1);
		$pdf->Line(68, 97, 68, 105);
		
	$pdf->SetY(99);$pdf->SetX(73);
	$pdf->Cell(30,0,'No Familia',0,0,'L',1);
		$pdf->Line(93, 97, 93, 109);
		$pdf->Line(98, 97, 98, 113);
	$pdf->SetY(99);$pdf->SetX(98);
	$pdf->Cell(30,0,'Direc. Correcta',0,0,'L',1);
		$pdf->Line(123, 97, 123, 105);
	$pdf->SetY(99);$pdf->SetX(130);	
	$pdf->Cell(30,0,'Acueducto',0,0,'L',1);
		$pdf->Line(155, 97, 155, 109);
	$pdf->Cell(30,0,'Se encontro fugas en',0,0,'L',1);
	
$pdf->Line(10, 105, 200, 105);
	$pdf->SetY(103);$pdf->SetX(10);
	$pdf->Cell(30,0,'No habitado',0,0,'L',1);	
	$pdf->SetY(103);$pdf->SetX(45);
	$pdf->Cell(30,0,'No residencial',0,0,'L',1);	
	$pdf->SetY(103);$pdf->SetX(73);
	$pdf->Cell(30,0,'No Habitantes',0,0,'L',1);	
	$pdf->SetY(103);$pdf->SetX(98);
	$pdf->Cell(30,0,'Direc. Incorrecta',0,0,'L',1);	
	$pdf->SetY(103);$pdf->SetX(130);	
	$pdf->Cell(30,0,'Alcantarillado',0,0,'L',1);	
	$pdf->SetY(103);$pdf->SetX(160);	
	$pdf->Cell(30,0,'Las Instalaciones?',0,0,'L',1);
		$pdf->Line(190, 101, 190, 113);
	$pdf->SetY(103);$pdf->SetX(190);	
	$pdf->Cell(30,0,'Si',0,0,'L',1);
		$pdf->Line(195, 101, 195, 113);
	$pdf->SetY(103);$pdf->SetX(195);	
	$pdf->Cell(30,0,'No',0,0,'L',1);

$pdf->Line(10, 109, 44, 109);	
$pdf->Line(73, 109, 98, 109);
$pdf->Line(129, 109, 200, 109);
	$pdf->SetY(107);$pdf->SetX(10);
	$pdf->Cell(30,0,'En construccion',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(45);
	$pdf->Cell(30,0,'Cual ?',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(73);
	$pdf->Cell(30,0,'No Pisos',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(98);
	$pdf->Cell(30,0,'Cual ?',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(130);	
	$pdf->Cell(30,0,'Alcan. Aforado',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(160);	
	$pdf->Cell(30,0,'Perceptible?',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(190);	
	$pdf->Cell(30,0,'Si',0,0,'L',1);	
	$pdf->SetY(107);$pdf->SetX(195);	
	$pdf->Cell(30,0,'No',0,0,'L',1);
	
$pdf->Line(10, 113, 200, 113);
	$pdf->SetY(111);$pdf->SetX(10);
	$pdf->Cell(30,0,'Lote Bald�o',0,0,'L',1);	
	$pdf->SetY(111);$pdf->SetX(160);	
	$pdf->Cell(30,0,'Imperceptible?',0,0,'L',1);	
	
	if($_GET['municipio']==1)
	{
		$pdf->SetY(106);$pdf->SetX(130);	
		$pdf->Cell(30,0,'Aseo',0,0,'L',1);
		$pdf->Line(155, 92, 155, 108);
	}
	
	$pdf->SetY(111);$pdf->SetX(190);	
	$pdf->Cell(30,0,'Si',0,0,'L',1);	
	$pdf->SetY(111);$pdf->SetX(195);	
	$pdf->Cell(30,0,'No',0,0,'L',1);

$pdf->Line(10, 93, 10, 113);
$pdf->Line(200, 93, 200, 113);
///-

$pdf->Line(10, 114.5, 200, 114.5);
	$pdf->SetY(116.5);$pdf->SetX(82);
	$pdf->Cell(30,0,'LOCALIZACI�N DE FUGAS',0,0,'L',1);	

$pdf->Line(10, 118.5, 200, 118.5);
	$pdf->SetY(120.5);$pdf->SetX(11);
	$pdf->Cell(30,0,'Localizacion de fuga perceptible',0,0,'L',1);	
		$pdf->Line(109, 118.5, 109, 128.5);
	$pdf->SetY(120.5);$pdf->SetX(110);
	$pdf->Cell(30,0,'Localizacion de fuga Imperceptible',0,0,'L',1);	
		
$pdf->Line(10, 128.5, 200, 128.5);	
$pdf->Line(10, 114.5, 10, 128.5);
$pdf->Line(200, 114.5, 200, 128.5);

///-

$pdf->Line(10, 130, 200, 130);
	$pdf->SetY(132);$pdf->SetX(77);
	$pdf->Cell(30,0,'TRABAJOS EJECUTADOS: OBSERVACIONES',0,0,'L',1);	

$pdf->Line(10, 134, 200, 134);

	$pdf->SetY(181);$pdf->SetX(10);
	$pdf->Cell(30,0,'Metros sondeados',0,0,'L',1);
		$pdf->Line(35, 179, 45, 179);
		$pdf->Line(35, 179, 35, 183);
		$pdf->Line(45, 179, 45, 183);	
	
	$pdf->SetY(181);$pdf->SetX(50);
	$pdf->Cell(30,0,'Cant. Lonas',0,0,'L',1);
		$pdf->Line(68, 179, 78, 179);
		$pdf->Line(68, 179, 68, 183);
		$pdf->Line(78, 179, 78, 183);
	
	$pdf->SetY(181);$pdf->SetX(87);
	$pdf->Cell(30,0,'Sumideros limpiados',0,0,'L',1);
		$pdf->Line(116, 179, 126, 179);
		$pdf->Line(116, 179, 116, 183);
		$pdf->Line(126, 179, 126, 183);
	
	$pdf->SetY(181);$pdf->SetX(135);
	$pdf->Cell(30,0,'Presion Tomada',0,0,'L',1);
		$pdf->Line(158, 179, 168, 179);
		$pdf->Line(158, 179, 158, 183);
		$pdf->Line(168, 179, 168, 183);
	
	$pdf->SetY(181);$pdf->SetX(172);
	$pdf->Cell(30,0,'Cant Pozos',0,0,'L',1);
		$pdf->Line(188, 179, 198, 179);
		$pdf->Line(188, 179, 188, 183);
		$pdf->Line(198, 179, 198, 183);
		
$pdf->Line(10, 183, 200, 183);	
$pdf->Line(10, 130, 10, 183);
$pdf->Line(200, 130, 200, 183);


$pdf->Line(10, 185, 200, 185);	
	$pdf->SetY(187);$pdf->SetX(45);
	$pdf->Cell(30,0,'SALIDA DE MATERIALES N� ',0,0,'L',1);
		//$pdf->Line(110, 150, 110, 178);//
		$pdf->Line(110, 185, 110, 237);//
	$pdf->SetY(187);$pdf->SetX(134);
	$pdf->Cell(30,0,'TRABAJO REALIZADO POR',0,0,'L',1);
	
$pdf->Line(10, 189, 200, 189);	
	$pdf->SetY(191);$pdf->SetX(11);
	$pdf->Cell(30,0,'Se usaron materiales ? ',0,0,'L',1);
			$pdf->Line(43, 189, 43, 193);
	$pdf->SetY(191);$pdf->SetX(44);
	$pdf->Cell(30,0,'Si',0,0,'L',1);
			$pdf->Line(50, 189, 50, 193);
	$pdf->SetY(191);$pdf->SetX(51);
	$pdf->Cell(30,0,'No',0,0,'L',1);
			$pdf->Line(57, 189, 57, 193);			
	$pdf->SetY(191);$pdf->SetX(70);
	$pdf->Cell(30,0,'Cobro al usuario ?',0,0,'L',1);
			$pdf->Line(96, 189, 96, 193);			
	$pdf->SetY(191);$pdf->SetX(97);
	$pdf->Cell(30,0,'Si',0,0,'L',1);
			$pdf->Line(103, 189, 103, 193);			
	$pdf->SetY(191);$pdf->SetX(103);
	$pdf->Cell(30,0,'No',0,0,'L',1);	
	//Nuevo Mensaje
	$pdf->SetY(191);$pdf->SetX(111);
	$pdf->Cell(30,0,'Cobrar mano de obra al usuario ?',0,0,'L',1);
			$pdf->Line(157, 189, 157, 193);			
	$pdf->SetY(191);$pdf->SetX(158);
	$pdf->Cell(30,0,'Si',0,0,'L',1);
			$pdf->Line(164, 189, 164, 193);			
	$pdf->SetY(191);$pdf->SetX(164);
	$pdf->Cell(30,0,'No',0,0,'L',1);	
			$pdf->Line(171, 189, 171, 193);			
	//Fin Nuevo Mensaje
	
	//$pdf->SetY(156);$pdf->SetX(111);
	$pdf->SetY(195);$pdf->SetX(111);
	$pdf->Cell(30,0,'Codigo',0,0,'L',1);
			//$pdf->Line(123, 154, 123, 178);	
			$pdf->Line(123, 193, 123, 237);	
	//$pdf->SetY(156);$pdf->SetX(134);
	$pdf->SetY(195);$pdf->SetX(134);
	$pdf->Cell(30,0,'Fecha',0,0,'L',1);
			//$pdf->Line(155, 154, 155, 178);			
			$pdf->Line(155, 193, 155, 237);			
	//$pdf->SetY(156);$pdf->SetX(158);
	$pdf->SetY(195);$pdf->SetX(158);
	$pdf->Cell(30,0,'Hora Inicio',0,0,'L',1);
			//$pdf->Line(179, 154, 179, 178);	
			$pdf->Line(179, 193, 179, 237);	
	//$pdf->SetY(156);$pdf->SetX(181);
	$pdf->SetY(195);$pdf->SetX(181);
	$pdf->Cell(30,0,'Hora Final',0,0,'L',1);

$pdf->Line(10, 193, 200, 193);	
	$pdf->SetY(195);$pdf->SetX(11);
	$pdf->Cell(30,0,'Codigo',0,0,'L',1);
		//$pdf->Line(26, 158, 26, 178);	
		$pdf->Line(26, 193, 26, 237);	
		
	$pdf->SetY(195);$pdf->SetX(42);
	$pdf->Cell(30,0,'Descripcion',0,0,'L',1);	
		//$pdf->Line(75, 158, 75, 178);
		$pdf->Line(75, 193, 75, 237);
		
	$pdf->SetY(195);$pdf->SetX(75);
	$pdf->Cell(30,0,'DIAM',0,0,'L',1);
		//$pdf->Line(87, 158, 87, 178);
		$pdf->Line(87, 193, 87, 237);
		
	$pdf->SetY(195);$pdf->SetX(88);
	$pdf->Cell(30,0,'UND',0,0,'L',1);
		//$pdf->Line(98, 158, 98, 178);
		$pdf->Line(98, 193, 98, 237);
		
	$pdf->SetY(195);$pdf->SetX(99);
	$pdf->Cell(30,0,'CANT',0,0,'L',1);
	
$pdf->Line(10, 197, 200, 197);	
$pdf->Line(10, 201, 200, 201);	
$pdf->Line(10, 205, 200, 205);	
$pdf->Line(10, 209, 200, 209);	
$pdf->Line(10, 213, 200, 213);
//agregadas
$pdf->Line(10, 217, 200, 217);	
$pdf->Line(10, 221, 200, 221);	
$pdf->Line(10, 225, 200, 225);	
$pdf->Line(10, 229, 200, 229);	
$pdf->Line(10, 233, 200, 233);	
$pdf->Line(10, 237, 200, 237);

// Lineas de los lados del cuadro de 9 registros
$pdf->Line(10, 185, 10, 237); 
$pdf->Line(200, 185, 200, 237);

//Aqui empieza Cuadro Resultado de Orden de Trabajo
/*
$pdf->Line(10, 201, 200, 201);
	$pdf->SetY(203);$pdf->SetX(80);
	$pdf->Cell(30,0,'RESULTADO DE ORDEN DE TRABAJO',0,0,'L',1);
//$pdf->Line(10, 185, 200, 185);
$pdf->Line(10, 205, 200, 205);

	//$pdf->SetY(187);$pdf->SetX(10);
	$pdf->SetY(207);$pdf->SetX(10);
	$pdf->Cell(30,0,'1',0,0,'L',1);
		//$pdf->Line(14, 185, 14, 217);		
		$pdf->Line(14, 205, 14, 237);		
	//$pdf->SetY(187);$pdf->SetX(15);
	$pdf->SetY(207);$pdf->SetX(15);
	$pdf->Cell(30,0,'Cumplido',0,0,'L',1);
		//$pdf->Line(70, 185, 70, 217);			
		$pdf->Line(70, 205, 70, 237);			
	//$pdf->SetY(187);$pdf->SetX(71);
	$pdf->SetY(207);$pdf->SetX(71);
	$pdf->Cell(30,0,'9',0,0,'L',1);
		//$pdf->Line(77, 185, 77, 217);
		$pdf->Line(77, 205, 77, 237);
	//$pdf->SetY(187);$pdf->SetX(78);
	$pdf->SetY(207);$pdf->SetX(78);
	$pdf->Cell(30,0,'No se repar� fuga',0,0,'L',1);
		//$pdf->Line(137, 185, 137, 217);		
		$pdf->Line(137, 205, 137, 237);		
	//$pdf->SetY(187);$pdf->SetX(138);
	$pdf->SetY(207);$pdf->SetX(138);
	$pdf->Cell(30,0,'17',0,0,'L',1);
		//$pdf->Line(143, 185, 143, 217);		
		$pdf->Line(143, 205, 143, 237);		
	//$pdf->SetY(187);$pdf->SetX(144);
	$pdf->SetY(207);$pdf->SetX(144);
	$pdf->Cell(30,0,'Servicio suspendido',0,0,'L',1);

//$pdf->Line(10, 189, 200, 189);
$pdf->Line(10, 209, 200, 209);
	//$pdf->SetY(191);$pdf->SetX(10);
	$pdf->SetY(211);$pdf->SetX(10);
	$pdf->Cell(30,0,'2',0,0,'L',1);				
	//$pdf->SetY(191);$pdf->SetX(15);
	$pdf->SetY(211);$pdf->SetX(15);
	$pdf->Cell(30,0,'Fuga visible (Perceptible) ',0,0,'L',1);					
	//$pdf->SetY(191);$pdf->SetX(71);
	$pdf->SetY(211);$pdf->SetX(71);
	$pdf->Cell(30,0,'10',0,0,'L',1);	
	//$pdf->SetY(191);$pdf->SetX(78);
	$pdf->SetY(211);$pdf->SetX(78);
	$pdf->Cell(30,0,'Lectura bien tomada',0,0,'L',1);				
	//$pdf->SetY(191);$pdf->SetX(138);
	$pdf->SetY(211);$pdf->SetX(138);
	$pdf->Cell(30,0,'18',0,0,'L',1);			
	//$pdf->SetY(191);$pdf->SetX(144);
	$pdf->SetY(211);$pdf->SetX(144);
	$pdf->Cell(30,0,'Se sondeo acometida de alcantarillado ',0,0,'L',1);
	
//$pdf->Line(10, 193, 200, 193);
$pdf->Line(10, 213, 200, 213);
	//$pdf->SetY(195);$pdf->SetX(10);
	$pdf->SetY(215);$pdf->SetX(10);
	$pdf->Cell(30,0,'3',0,0,'L',1);				
	//$pdf->SetY(195);$pdf->SetX(15);
	$pdf->SetY(215);$pdf->SetX(15);
	$pdf->Cell(30,0,'Fuga no visible (imperseptible) ',0,0,'L',1);					
	//$pdf->SetY(195);$pdf->SetX(71);
	$pdf->SetY(215);$pdf->SetX(71);
	$pdf->Cell(30,0,'11',0,0,'L',1);	
	//$pdf->SetY(195);$pdf->SetX(78);
	$pdf->SetY(215);$pdf->SetX(78);
	$pdf->Cell(30,0,'Lectura mal tomada',0,0,'L',1);				
	//$pdf->SetY(195);$pdf->SetX(138);
	$pdf->SetY(215);$pdf->SetX(138);
	$pdf->Cell(30,0,'19',0,0,'L',1);			
	//$pdf->SetY(195);$pdf->SetX(144);
	$pdf->SetY(215);$pdf->SetX(144);
	$pdf->Cell(30,0,'Se sondeo red de alcantarillado ',0,0,'L',1);
	
//$pdf->Line(10, 197, 200, 197);
$pdf->Line(10, 217, 200, 217);
	//$pdf->SetY(199);$pdf->SetX(10);
	$pdf->SetY(219);$pdf->SetX(10);
	$pdf->Cell(30,0,'4',0,0,'L',1);				
	//$pdf->SetY(199);$pdf->SetX(15);
	$pdf->SetY(219);$pdf->SetX(15);
	$pdf->Cell(30,0,'No se detectaron fugas ',0,0,'L',1);					
	//$pdf->SetY(199);$pdf->SetX(71);
	$pdf->SetY(219);$pdf->SetX(71);
	$pdf->Cell(30,0,'12',0,0,'L',1);	
	//$pdf->SetY(199);$pdf->SetX(78);
	$pdf->SetY(219);$pdf->SetX(78);
	$pdf->Cell(30,0,'No se encontr� el predio',0,0,'L',1);				
	//$pdf->SetY(199);$pdf->SetX(138);
	$pdf->SetY(219);$pdf->SetX(138);
	$pdf->Cell(30,0,'20',0,0,'L',1);			
	//$pdf->SetY(199);$pdf->SetX(144);
	$pdf->SetY(219);$pdf->SetX(144);
	$pdf->Cell(30,0,'Medidor para banco de prueba',0,0,'L',1);
		
//$pdf->Line(10, 201, 200, 201);
$pdf->Line(10, 221, 200, 221);
	//$pdf->SetY(203);$pdf->SetX(10);
	$pdf->SetY(223);$pdf->SetX(10);
	$pdf->Cell(30,0,'5',0,0,'L',1);				
	//$pdf->SetY(203);$pdf->SetX(15);
	$pdf->SetY(223);$pdf->SetX(15);
	$pdf->Cell(30,0,'Se repar� fuga en medidor',0,0,'L',1);					
	//$pdf->SetY(203);$pdf->SetX(71);
	$pdf->SetY(223);$pdf->SetX(71);
	$pdf->Cell(30,0,'13',0,0,'L',1);	
	//$pdf->SetY(203);$pdf->SetX(78);
	$pdf->SetY(223);$pdf->SetX(78);
	$pdf->Cell(30,0,'Se instal� acometida de acueducto',0,0,'L',1);				
	//$pdf->SetY(203);$pdf->SetX(138);
	$pdf->SetY(223);$pdf->SetX(138);
	$pdf->Cell(30,0,'21',0,0,'L',1);			
	//$pdf->SetY(203);$pdf->SetX(144);
	$pdf->SetY(223);$pdf->SetX(144);
	$pdf->Cell(30,0,'Servicios no instalados',0,0,'L',1);
	
//$pdf->Line(10, 205, 200, 205);
$pdf->Line(10, 225, 200, 225);
	//$pdf->SetY(207);$pdf->SetX(10);
	$pdf->SetY(227);$pdf->SetX(10);
	$pdf->Cell(30,0,'6',0,0,'L',1);				
	//$pdf->SetY(207);$pdf->SetX(15);
	$pdf->SetY(227);$pdf->SetX(15);
	$pdf->Cell(30,0,'Se repar� fuga en acometida',0,0,'L',1);					
	//$pdf->SetY(207);$pdf->SetX(71);
	$pdf->SetY(227);$pdf->SetX(71);
	$pdf->Cell(30,0,'14',0,0,'L',1);	
	//$pdf->SetY(207);$pdf->SetX(78);
	$pdf->SetY(227);$pdf->SetX(78);
	$pdf->Cell(30,0,'Se instal� acometida de alcantarillado',0,0,'L',1);				
	//$pdf->SetY(207);$pdf->SetX(138);
	$pdf->SetY(227);$pdf->SetX(138);
	$pdf->Cell(30,0,'22',0,0,'L',1);			
	//$pdf->SetY(207);$pdf->SetX(144);
	$pdf->SetY(227);$pdf->SetX(144);
	$pdf->Cell(30,0,'Se limpio o repar� sumidero',0,0,'L',1);
	
//$pdf->Line(10, 209, 200, 209);
$pdf->Line(10, 229, 200, 229);
	//$pdf->SetY(211);$pdf->SetX(10);
	$pdf->SetY(231);$pdf->SetX(10);
	$pdf->Cell(30,0,'7',0,0,'L',1);				
	//$pdf->SetY(211);$pdf->SetX(15);
	$pdf->SetY(231);$pdf->SetX(15);
	$pdf->Cell(30,0,'Se repar� fuga en red de acueducto',0,0,'L',1);					
	//$pdf->SetY(211);$pdf->SetX(71);
	$pdf->SetY(231);$pdf->SetX(71);
	$pdf->Cell(30,0,'15',0,0,'L',1);	
	//$pdf->SetY(211);$pdf->SetX(78);
	$pdf->SetY(231);$pdf->SetX(78);
	$pdf->Cell(30,0,'Se instal� medidor',0,0,'L',1);				
	//$pdf->SetY(211);$pdf->SetX(138);
	$pdf->SetY(231);$pdf->SetX(138);
	$pdf->Cell(30,0,'23',0,0,'L',1);			
	//$pdf->SetY(211);$pdf->SetX(144);
	$pdf->SetY(231);$pdf->SetX(144);
	$pdf->Cell(30,0,'Se realizo reconstrucci�n o mntto de pozo',0,0,'L',1);
	
//$pdf->Line(10, 213, 200, 213);
$pdf->Line(10, 233, 200, 233);
	//$pdf->SetY(215);$pdf->SetX(10);
	$pdf->SetY(235);$pdf->SetX(10);
	$pdf->Cell(30,0,'8',0,0,'L',1);				
	//$pdf->SetY(215);$pdf->SetX(15);
	$pdf->SetY(235);$pdf->SetX(15);
	$pdf->Cell(30,0,'Se repar� fuga  red de alcantarillado',0,0,'L',1);					
	//$pdf->SetY(215);$pdf->SetX(71);
	$pdf->SetY(235);$pdf->SetX(71);
	$pdf->Cell(30,0,'16',0,0,'L',1);	
	//$pdf->SetY(215);$pdf->SetX(78);
	$pdf->SetY(235);$pdf->SetX(78);
	$pdf->Cell(30,0,'No se localiza acometida',0,0,'L',1);				
	//$pdf->SetY(215);$pdf->SetX(138);
	$pdf->SetY(235);$pdf->SetX(138);
	$pdf->Cell(30,0,'24',0,0,'L',1);			
	//$pdf->SetY(215);$pdf->SetX(144);
	$pdf->SetY(235);$pdf->SetX(144);
	$pdf->Cell(30,0,'Se encontr� Irregularidad',0,0,'L',1);
	
//$pdf->Line(10, 217, 200, 217);
//$pdf->Line(10, 181, 10, 217);
//$pdf->Line(200, 181, 200, 217);
$pdf->Line(10, 237, 200, 237);
$pdf->Line(10, 201, 10, 237);
$pdf->Line(200, 201, 200, 237);
*/

///--

//Aqui empieza Cuadro Orden de Trabajo Pendiente
//$pdf->Line(10, 220, 200, 220);  
$pdf->Line(10, 239, 200, 239);
	//$pdf->SetY(222);$pdf->SetX(83);
	$pdf->SetY(241);$pdf->SetX(83);
	$pdf->Cell(30,0,'ORDEN DE TRABAJO PENDIENTE',0,0,'L',1);
	
//$pdf->Line(10, 224, 200, 224);
$pdf->Line(10, 243, 200, 243);
	//$pdf->SetY(226);$pdf->SetX(11);
	$pdf->SetY(245);$pdf->SetX(11);
	$pdf->Cell(30,0,'Es necesario realizar otra orden ?',0,0,'L',1);
		//$pdf->Line(60, 224, 60, 236);
		$pdf->Line(60, 243, 60, 255);
		
	//$pdf->SetY(226);$pdf->SetX(63);
	$pdf->SetY(245);$pdf->SetX(63);
	$pdf->Cell(30,0,'Concepto ',0,0,'L',1);		
	
	//$pdf->SetY(233);$pdf->SetX(25);
	$pdf->SetY(252);$pdf->SetX(25);
	$pdf->Cell(30,0,'Si ',0,0,'L',1);	
//		$pdf->Line(25, 230, 30, 230);
//		$pdf->Line(25, 235, 30, 235);
//		$pdf->Line(25, 230, 25, 235);
//		$pdf->Line(30, 230, 30, 235);
		$pdf->Line(25, 249, 30, 249);
		$pdf->Line(25, 254, 30, 254);
		$pdf->Line(25, 249, 25, 254);
		$pdf->Line(30, 249, 30, 254);

		
	//$pdf->SetY(233);$pdf->SetX(35);
	$pdf->SetY(252);$pdf->SetX(35);
	$pdf->Cell(30,0,'No ',0,0,'L',1);	
//		$pdf->Line(35, 230, 40, 230);
//		$pdf->Line(35, 235, 40, 235);
//		$pdf->Line(35, 230, 35, 235);
//		$pdf->Line(40, 230, 40, 235);
		$pdf->Line(35, 249, 40, 249);
		$pdf->Line(35, 254, 40, 254);
		$pdf->Line(35, 249, 35, 254);
		$pdf->Line(40, 249, 40, 254);		

//$pdf->Line(10, 236, 200, 236);
//$pdf->Line(10, 220, 10, 236);
//$pdf->Line(200, 220, 200, 236);
$pdf->Line(10, 255, 200, 255);
$pdf->Line(10, 239, 10, 255);
$pdf->Line(200, 239, 200, 255);

////-

//$pdf->Line(10, 239, 200, 239);
$pdf->Line(10, 257, 200, 257);
	//$pdf->SetY(241);$pdf->SetX(11);
	$pdf->SetY(259);$pdf->SetX(11);
	$pdf->Cell(30,0,'NOMBRE DEL USUARIO ',0,0,'L',1);
		//$pdf->Line(105, 239, 105, 260);
		$pdf->Line(105, 257, 105, 274);
		
	//$pdf->SetY(241);$pdf->SetX(110);
	$pdf->SetY(259);$pdf->SetX(110);
	$pdf->Cell(30,0,'NOMBRE DEL QUE EJECUT� LA ORDEN  ',0,0,'L',1);
	
		//$pdf->Line(15, 250, 100, 250);	
		$pdf->Line(15, 266, 100, 266);	

	//$pdf->SetY(258);$pdf->SetX(11);
	$pdf->SetY(272);$pdf->SetX(11);
	$pdf->Cell(30,0,'No. C.C.   ',0,0,'L',1);	
	//$pdf->SetY(258);$pdf->SetX(58);
	$pdf->SetY(272);$pdf->SetX(58);
	$pdf->Cell(30,0,'Firma.',0,0,'L',1);
	
		//$pdf->Line(110, 250, 195, 250);
		$pdf->Line(110, 266, 195, 266);
	
	//$pdf->SetY(258);$pdf->SetX(107);
	$pdf->SetY(272);$pdf->SetX(107);
	$pdf->Cell(30,0,'No. C.C.   ',0,0,'L',1);	
	//$pdf->SetY(258);$pdf->SetX(157);
	$pdf->SetY(272);$pdf->SetX(157);
	$pdf->Cell(30,0,'Firma.',0,0,'L',1);

//$pdf->Line(10, 260, 200, 260);
//$pdf->Line(10, 239, 10, 260);
//$pdf->Line(200, 239, 200, 260);
$pdf->Line(10, 274, 200, 274);
$pdf->Line(10, 257, 10, 274);
$pdf->Line(200, 257, 200, 274);
	
/*Ultima lineas punteadas*/	
$pdf->Line(10, 138, 200, 138, $style3);	
$pdf->Line(10, 142, 200, 142, $style3);	
$pdf->Line(10, 146, 200, 146, $style3);	
$pdf->Line(10, 150, 200, 150, $style3);	
$pdf->Line(10, 154, 200, 154, $style3);	
$pdf->Line(10, 158, 200, 158, $style3);	
$pdf->Line(10, 162, 200, 162, $style3);	
$pdf->Line(10, 166, 200, 166, $style3);	
$pdf->Line(10, 170, 200, 170, $style3);	
$pdf->Line(10, 174, 200, 174, $style3);	
$pdf->Line(10, 178, 200, 178, $style3);	

$pdf->SetY(276);$pdf->SetX(10);
//$info="<b>Funcionario: No olvide usas sus elementos de protecci�n personal (EPP)</b>";
//$pdf->WriteHTML($info);
$pdf->Cell(50,0,'Funcionario: No olvide usar sus elementos de protecci�n personal (EPP)',0,0,'L',1);	
		
////-




	}
}
$pdf->Output();
?> 