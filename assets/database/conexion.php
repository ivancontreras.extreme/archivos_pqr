<?php

function conectar($empresa)
{
    $file = json_decode(file_get_contents("database.json", true))->$empresa;
    $user = $file->user;
    $host = $file->host;
    $database = $file->database;
    $password = $file->password;
    $port = $file->port;
    try {
        $conexion = new PDO("pgsql:host=$host;port=$port;dbname=$database", "$user", "$password");
    } catch (Exception $e) {
        die(json_encode(array("status" => "error", "message" => $e->getMessage())));
    }
    return $conexion;
}
